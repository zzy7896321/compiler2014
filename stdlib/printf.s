LBFprintf:
	or $at, $0, $a0
	addiu $t8, $sp, 4
	
	lb $a0, 0($at)
	beq $a0, $0, printf_end

printf_exec:
	addiu $at, $at, 1

	xori $t9, $a0, '%'
	bne $t9, $0, printf_plain
	
	lb $a0, 0($at)
	addiu $at, $at, 1
	beq $a0, $0, printf_end
	
	xori $t9, $a0, '%'
	beq $t9, $0, printf_plain
	
	xori $t9, $a0, 'd'
	ori $v0, $0, 1
	beq $t9, $0, printf_print_obj
	
	xori $t9, $a0, 'c'
	ori $v0, $0, 11
	beq $t9, $0, printf_print_obj
	
	xori $t9, $a0, 's'
	ori $v0, $0, 4
	beq $t9, $0, printf_print_obj
	
	xori $t9, $a0, '0'
	beq $t9, $0, printf_print_int_with_width
	
	j printf_plain
	
printf_print_int_with_width:
	
	lb $a0, 0($at)
	addiu $at, $at, 1
	beq $a0, $0, printf_end
	
	addiu $a0, $a0, -48
	bltz $a0, printf_end
	slti $t9, $a0, 10
	beq $t9, $0, printf_end		#error
	
	lb $t9, 0($at)
	addiu $at, $at, 1
	beq $t9, $0, printf_end
	
	xori $t9, $t9, 'd'
	bne $t9, $0, printf_end
	
	lw $t9, 0($t8)
	
	lui $v0, 0xF000
	xor $v0, $t9, $v0
	beq $v0, $0, printf_print_int_with_width_U2147483648
	
	slt $v0, $t9, $0
	beq $v0, $0, printf_print_int_with_width_non_neg
	
	subu $t9, $0, $t9;
	addiu $a0, $a0, -1
	sw $t9, 0($t8);		
	
	or $t9, $a0, $0
	addiu $v0, $0, 11
	addiu $a0, $0, 45
	syscall
	
	or $a0, $t9, $0
	
printf_print_int_with_width_non_neg:
	
	lw $t9, 0($t8)
	
	addiu $a0, $a0, -1
	addiu $v0, $0, 9
	slt $v0, $v0, $t9
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	addiu $v0, $0, 99
	slt $v0, $v0, $t9
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	addiu $v0, $0, 999
	slt $v0, $v0, $t9
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	addiu $v0, $0, 9999
	slt $v0, $v0, $t9
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	lui $v0, 0x1
	or $v0, $0, 0x869F 		#99999
	slt $v0, $v0, $t9
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	lui $v0, 0xF
	or $v0, $0, 0x423F 		#999999
	slt $v0, $v0, $t9
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	lui $v0, 0x98
	or $v0, $0, 0x967F 		#9999999
	slt $v0, $v0, $t9
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	lui $v0, 0x5F5
	or $v0, $0, 0xE0FF 		#99999999
	slt $v0, $v0, $t9
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	lui $v0, 0x3B9A
	or $v0, $0, 0xC9FF 		#999999999
	slt $v0, $v0, $t9
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	
printf_print_int_with_width_print_padding_zeros:
	
	or $t9, $a0, $0
	ori $a0, $0, 48
	or $v0, $0, 11
	
	blez $t9, printf_print_int_with_width_print_padding_zeros_loop_end
printf_print_int_with_width_print_padding_zeros_loop:
	syscall
	addiu $t9, $t9, -1
	bgtz $t9, printf_print_int_with_width_print_padding_zeros_loop
	
printf_print_int_with_width_print_padding_zeros_loop_end:
	
	lw $a0, 0($t8)
	addiu $t8, $t8, 4
	or $v0, $0, 1
	syscall
	
	j printf_loop
	
printf_print_int_with_width_U2147483648:

	addiu $t8, $t8, 4
	or $a0, $0, $t9
	ori $v0, $0, 1
	syscall

	j printf_loop

printf_print_obj:	
	lw $a0, ($t8)
	addiu $t8, $t8, 4
	syscall
	j printf_loop
	
printf_plain:
	ori $v0, $0, 11
	syscall
	
printf_loop:
	lb $a0, 0($at)
	bne $a0, $0, printf_exec
	
printf_end:
	jr $ra