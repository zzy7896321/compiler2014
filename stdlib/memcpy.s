LBFmemcpy:

LBFaligned_memcpy:
LBaligned_memcpy0:	
	addiu $a2, $a2, -4
	lw $t8, 0($a1)
	addiu $a1, $a1, 4
	sw $t8, 0($a0)
	addiu $a0, $a0, 4
	bne $a2, $0, LBaligned_memcpy0
	
	jr $ra