LBFmalloc:
	lw $v0, -32768($gp)
	addu $a0, $v0, $a0
	addiu $a0, $a0, 3		#align on 4 byte boundary
	srl $a0, $a0, 2
	sll $a0, $a0, 2
	sw $a0, -32768($gp)
	
	jr $ra
	