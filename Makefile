
JAVAC = javac
JAVA = java

BIN = ./bin
SRC = ./src
LIB = ./lib
CLASSPATH = ${BIN}:${LIB}/java-cup-11b-runtime.jar

JFLAGS = 

CUP = ${LIB}/java-cup-11b-mod.jar
CUPFLAGS = -destdir ${SRC}/parser -package parser -parser Parser -symbols CUP_sym -interface -nopositions -expect 1

JFLEX = ${LIB}/jflex-1.5.0.jar
JFLEXFLAGS = --noinputstreamctor -d ${SRC}/parser



.PHONY:all
all: jflex cup all_java
	mkdir -p bin/stdlib && cp stdlib/* bin/stdlib/

.PHONY:all_java
all_java: 
	mkdir -p ${BIN}; ${JAVAC} -cp ${CLASSPATH} -d ${BIN} ${SRC}/*/*.java

.PHONY:jflex
jflex:
	${JAVA} -jar ${JFLEX} ${JFLEXFLAGS} ${SRC}/parser/Lexer.flex

.PHONY:cup
cup:
	${JAVA}	-jar ${CUP} ${CUPFLAGS} ${SRC}/parser/Parser.cup

.PHONY:jflex_cup
jflex_cup: jflex cup


.PHONY:cup_debug 
cup_debug:
	${JAVA}	-jar ${CUP} ${CUPFLAGS} -dump ${SRC}/parser/Parser.cup 2> cup_out.txt

.PHONY:jflex_debug
jflex_debug:
	${JAVA} -jar ${JFLEX} ${JFLEXFLAGS} --dump ${SRC}/parser/Lexer.flex > jflex_out.txt

.PHONY: clean
clean:
	-rm ${SRC}/parser/Parser.java ${SRC}/parser/CUP_sym.java ${SRC}/parser/Lexer.java
	rm -vrf ${BIN}/*

