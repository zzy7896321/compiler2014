\chapter{Optimization}
\label{ch:optimization}

	Optimization transforms the original program so that the time or the space it takes to execute the program is reduced without changing the semantic. In order to implement optimization, the compiler may make several passes on the program.
	
	Optimization can be done at various phases: IR generation, individual optimization passes, code emission, and machine-dependent optimization. The optimization of this implementation mainly takes place at IR generation, several optimization passes and code emission. Optimization of IR generation has been discussed in chapter \ref{ch:semantic} and optimization of code emission will be discussed in chapter \ref{ch:code-emission}. This chapter discusses the individual optimization passes, including converting IR to static single assignment form, sparse conditional constant propagation, constant-folding, copy-propagation, and dead-code elimination. Register allocation and converting out of SSA will be discussed in chapter \ref{ch:register}.
	
\section{Converting to Static Single Assignment Form}
	
	The static single assignment form is an intermediate representation where each variable has only one definition (assignment). The SSA can enable or simplify various kinds of optimization. The optimization performed in this implementation are all based on SSA.
	
	Unfortunately, most programming language are not naturally in SSA form. The traditional algorithm to construct SSA form was introduced Cytron et al. \cite{cytron1991efficiently}, which is adopted in this implementation. The algorithm transforms a non-SSA form IR organized in a control flow graph (CFG) to a SSA form IR by calculating the dominator frontier, placing $\phi$-functions at appropriate places and renaming variables. The dominance frontier is computed from a dominator tree, which can be efficiently constructed using the algorithm introduced by Lengauer and Tarjan \cite{lengauer1979fast}. There is also SSA construction algorithm that allows direct translation from an abstract syntax tree \cite{braun2013simple}.
	
	The following subsections briefly review the SSA construction algorithm and discusses the implementation details.
	
\subsection{SSA Construction Algorithm}
	
	SSA form require each variable to be assigned only once. Renaming each definition of the same variable would suffice if there is no branch in the code. Branches complicate the problem. Suppose a variable is defined in two branch, it would be generally impossible to decide which value the variable contains when the branches join. A notion of $\phi$-function is introduced to cope with the problem. $\phi$-function takes the renamed variables from all the predecessors originated from the same variable as input, and defines a new variable that has the value of the variable in the taken branch.

\begin{center}
\begin{tabular}{|l|l|l|}
	\hline
		& Non-SSA form	& SSA form \\\hline
	
	1&	i = 0;		&			$i_1$ = 0;			\\
	2&	j = 1;		&			$j_1$ = 1;			\\
	3&	if (j < 100)	&		if ($j_1$ < 100)	\\
	4&	$\quad$ i = i + 1;	&	$\quad$ $i_2$ = $i_1$ + 1;	\\
	5&	else		&			else				\\
	6&	$\quad$	i = 2;	&		$\quad$ $i_3$ = 2;	\\
	7&	print(i);		&		$i_4$ = $\phi$($i_2$, $i_3$)	\\
	8&					&		print($i_4$)		\\
	\hline
\end{tabular}
\end{center}

	In the example above, the i used by the last statement is from either the true branch or the false branch of the if-statement. $\phi$-function has the value of $i_2$ if the true branch is taken, or have the value of $i_3$ if the false branch is taken. The i used by print is replaced with the new variable $i_4$, which contains the right value of i.
	
	Intuitively, $\phi$-functions are needed whenever two control flow join. However, placing $\phi$-functions at all blocks is not necessary and may affect the performance. To place a minimal amount of $\phi$-functions, the notion of dominance frontier is introduced. This notion is derived from the notion of dominance. Definitions of these notions are shown below.
	
\begin{defn}[Dominance]
	Suppose the entry block in a control flow graph is s. A block n dominates a block m if every path of directed edges from s to m must go through n. If n dominates m, then n is a dominator of m. n strictly dominates m if n dominates m and n $\neq$ m.
\end{defn}

\begin{defn}[Dominance frontier]
	The dominance frontier of a block n is the set of all blocks m such that x dominates a predecessor of m in the CFG, but does not strictly dominate m.
\end{defn}
	
	If a variable a is defined in block n, $\phi$-functions are inserted for a in all the blocks in the dominance frontier of n. Inserting $\phi$-functions can be implemented with a work list of variables. The algorithm runs in time proportional to the variables in the graph, plus the number of quadruples, plus the total size of all the dominance frontiers, plus the number of $\phi$-functions inserted. Except the last term, the terms are linear in the size of the program. The number of $\phi$-functions inserted could be quadratic in the size of the program in the worst case, but is usually proportional to the size of the program in practice. Therefore, inserting $\phi$-functions runs in linear time in practice.
	
	Once $\phi$-functions are inserted, variables can renamed by numbering them. This can be done by traversing the CFG from the entry block and numbering each variable with a numbering stack. It only takes approximately linear time.
	
	The only problem is to find the dominance frontiers. As mentioned before, Lengauer and Tarjan\cite{lengauer1979fast} introduced an algorithm that efficiently computes the immediate dominator, which can be implemented in $O(nlogn)$ time with path compression or $O(n\alpha(n))$ with balanced path compression, where $\alpha$ is the inverse Ackermann function. From the immediate dominator, dominance frontiers can be computed in time proportional to the size of the original graph plus the size of dominance frontiers, which is practically linear time although pathological cases do exist.
	
	\cite[ch.19]{aho2009compilers} and \cite{cytron1991efficiently} give a detailed introduction to the algorithms mentioned above.
	
\subsection{Implementation Details}

	The algorithms to convert non-SSA form to SSA form are implemented in optimization.SSAConstructor. SSAConstructor.convertToSSA takes in  as the input a control flow graph built on-the-fly during IR generation and converts it to the SSA form.
	
	The init method is firstly invoked to initialize data structures used by the algorithms. Then convertToSSA method invokes Dominators.dominators to compute the immediate dominators using the Lengauer and Tarjan algorithm implemented with path compression. Using the immediate dominators, dominance frontiers are computed. In the next pass the program scans the CFG to find all the definitions and initialize numbering stack for each of them. Once all the definitions are collected, phi functions are inserted at appropriate places. The last pass traverse the CFG to rename all the variables.
	
	Lacking of alias analysis, this implementation only renames local variables that are not taken address of, arguments that are not taken address of and temporary variables. Global variables, aggregate type variables (struct/union/array type) and variables that are taken address of are forced to stay in the memory. 
	
	The temporaries are not renamed because they are naturally in SSA form with the only exception that results of short-circuit operators and conditional expression are not. The result of the exception cases are declared as local variable due to this reason. This trick can greatly reduce the amount of moves brought by $\phi$-functions after converting out of SSA form.
	
\section{Propagations}
	
	The SSA form IR first goes through a propagation pass, which performs the following optimizations:

\begin{itemize}
	\item Sparse Conditional Constant Propagation
	
	\item Constant folding
	
	\item Copy propagation
\end{itemize}
	
	These optimization could be done on non-SSA IR but requires a iterated data-flow analysis, which is significantly slower than the one-pass SSA version which runs in linear time.
	
	To avoid having to mark executable edges, the graph needs to be edge split. The CFG is first converted to an edge-split version. Then the propagations are performed. Finally redundant edge splitting blocks are removed.
	
	Detailed introduction to these optimizations are available in \cite{wegman1991constant}, \cite[19.3]{appel2003modern}, \cite{cytron1991efficiently}.
	
\subsection{Sparse Conditional Constant Propagation}
	
	The idea of constant propagation comes from the intuition that if a variable is assigned a constant value, each use of the definition of the variable can be replaced by the constant value. This is usually less time-consuming on most architectures. Moreover, the definition could be eliminated later by dead code elimination if each use of it is eliminated. If a branch has both operands with constant value, then the direction of the branch can be determined at compile time, enabling elimination of the branch and more dead code on the not-taken branch eliminated. The SSA form guarantees that each variable has only one definition, propagation can be directly performed without data-flow analysis. This is called Conditional Constant Propagation (CC).
	
\lstinputlisting[caption=Example: Conditional Constant Propagation demo]
{../demo/optimization/CC.c}

\lstinputlisting[language={[mips]Assembler}, caption=CC example assembly code]
{../demo/optimization/CC.s}

	The example above combined with dead code elimination, all code except line 7 will be removed, resulting in directly print out 1.
	
	However, CC cannot find the constant value in the following example since it assumes all the blocks are reachable until evidence is found that it is actually not. SCC can find the constant value since SCC does not assume any block except exit block or block with side effect as executable until it find evidence to prove that.
	
\lstinputlisting[caption=Example: Sparse Conditional Constant Propagation demo, label=code:SCC]
{../demo/optimization/SCC.c}

\lstinputlisting[language={[mips]Assembler}, caption=SCC example assembly code, label=asm:SCC]
{../demo/optimization/SCC.s}

	SCC works in two steps. The first step is to compute a maximal fixed point over a lattice which marks the possible value of each variable. The lattice has three kinds of values. $\bot$ means no evidence that any assignment to the variable is ever executed. Any constant is greater the $\bot$ and has no relation with each other, meaning evidence has been found that the variable is only possibly assigned with the constant. Address, integer value, labels are all regarded as constant. $\top$ means that evidence has been found that the variable will have, at various times, at least two different values or some value unknown at compile time. In addition, each block is given a boolean value indicating whether evidence has shown that the block is executable. Initially all the variables are set to $\bot$ and all the block except the entry block are set to non-executable. The program find a maximal fixed point by examining all the definitions of variables and blocks that are marked executable. Each variable can only be raised to higher in the lattice, and blocks can only stay executable once marked as executable. Therefore, the program runs in linear time.
	
	The maximal fixed point gives the information about whether the variables are constant or not, and whether the blocks are executable. The second step uses the information does the propagation and eliminate non-executable blocks.
	
\subsection{Constant Folding}
	
	Constant folding calculates operations that have operands all evaluates to constant at compile time and replace the operation with a assignment. Move operations is usually quicker than other operations on most architectures. The constant result can be further used as a constant value to be propagated, enabling more constant value to be found. Constant folding is done in this implementation along with the first and second step of SCC.
	
\subsection{Copy propagation}

	A single assignment from a variable a to another variable b can be eliminated if all use of b is replaced with a. This will reduce instructions and enable more constant and copy propagation. Copy propagation is only done along with the second step of SCC in this implementation.
	
\section{Simple Dead Code Elimination}
	
	The result code after sparse conditional constant propagation usually contains a lot of dead code, i.e. unused definition. They can be safely removed if they have no side effects. After dead code elimination, the result of SCC contains less redundant instructions, like the example assembly code \ref{asm:SCC}. 
	
	The simple dead code elimination only checks all the definition of variables. If the definition has no use (easy to maintain for SSA form), the definition is removed and the use of the operands are removed from respective use list. This procedure is done with a work list that maintains all the variables that have not been considered or has update to its use list. Since all the variable and each use of them are spent a constant time on, the algorithm runs in linear time to the number of variables and the size of their use lists.
	
	More aggressive dead code elimination can be done with the control dependence graph, which is based on the dominator tree of the inverse graph of the CFG. It can remove unused loops like in the code \ref{code:SCC}. One drawback of the aggressive dead code elimination is that it may remove unused infinite loop in the code, which change the behaviour of the code. This is the reason why aggressive dead code elimination is not adopted in this implementation.
	
	Simple dead code elimination is implemented in optimization. SimpleDeadCodeElimination class.