
\chapter{Lexical Analysis}
\label{ch:lexical}

\section{Overview}

	Lexical analysis is a process of breaking a stream of characters into tokens, i.e. meaningful sequences of characters that can be treated as single units in the grammar of the programming language. The lexical analyser, or the lexer, is a program that performs lexical analysis. The output of the lexer are input to the next phase, syntax analysis. This chapter discusses the general method to implement a lexer and the special considerations in this implementation.
	
\section{Regular Expressions}

	Regular expressions are a class of expressions representing sets of strings over a finite alphabet and closed under concatenation, union and kleene star. They are powerful tools to specify lexical tokens in lexical analysis. The following is an example of how regular expressions are used to specify some common tokens in the C programming language. They specify octal, decimal and hexadecimal integer literals:
	
\begin{itemize}
	\item	Octal:	0[0-7]*

	\item 	Decimal:	[1-9][0-9]*
	
	\item	Hexadecimal:	0[xX][0-9a-fA-F]+
\end{itemize}
	
	Octal integer literals are specified as strings that has a leading 0 followed by 0 or more digits ranging from 0 to 7. Decimal integer literals are specified as strings that have a leading digit ranging from 1 to 9 followed by 0 or more decimal digits. Hexadecimal integer literals are specified as strings that have a leading string "0x" or "0X" followed by at least 1 hexadecimal digit.
	
\section{Finite Automata}
	
	The finite automaton is a computing model with a finite set of states, a finite alphabetic and a final control, which takes a finite input string and decides whether to accept the string. The finite automata are equivalent to regular expression in terms of the ability to accept the same class of language. Finite automata is  easier to be implemented as a computer program.
	
	Moreover, algorithms exist to convert expressions to equivalent finite automata, which, despite exponential time complexity in the worst case, runs efficiently in practice. These algorithms take a regular expression as input and convert it to a deterministic finite automaton (DFA). The finite automaton has a finite states and makes a transition move on each input symbol. This process can be implemented using a loop that takes in a symbol on each iteration, looks up in the transition table, makes a transition move and reports whether the string is accepted. The running time is linear in the input size, making the lexical analysis quite efficient.
	
	For more information on the regular expressions and finite automata, refer to \cite{hopcroft2008automata}, \cite{aho2009compilers}, \cite{appel2003modern}.
	
\section{Jflex}

	In this implementation, Jflex is chosen as the lexical analyser generator from a variety of choices. Jflex\cite{klein2014jflex} is a lexer generator for Java written in Java. 
	
	Jflex converts regular expression specifications to a equivalent DFA in Java with full unicode support. The specification syntax of Jflex is similar to that of the classical C lexer generator Lex, which is introduced in \cite[Ch.4, pp.187-188]{aho2009compilers}. Jflex can easily interface with CUP, a parser generator that will be discussed in the next chapter.
	
	The version of Jflex used in this implementation is 1.5.0.	
	
	The basic structure of a Jflex lexical specification is shown as follows:
	
	User Code
	
	\%\%
	
	Options and declarations
	
	\%\%
	
	Lexical rules

\section{Implementation details}
	
\subsection{User code}

	This part contains code that is copied verbatim into the beginning of the generated lexer. In this implementation, only package specification, imports of necessary classes and a javadoc comment are included. The classes imported are:
\begin{itemize}
	\item java\_cup.runtime.*:	the CUP runtime classes. java\_cup.runtime.Symbol serves as the class of the returned tokens.
	
	\item node.*:	the AST (Abstract Syntax Tree) node classes. Constants are using classes defined in the node package.
	
	\item main.ErrorHandling: the error handling class of the compiler. Static method ErrorHandling.ReportError is invoked whenever an error occurs during lexical analysis.
	
\end{itemize}
	
\subsection{Options and declarations}

	The lexer class is renamed to Lexer instead of the default name Yylex. In order to let the main function in another package be able to create a Lexer object, the generated class is made public. Unicode support is enabled by \%unicode option. In order to provide detailed error information in lexical analysis and semantic checking, \%line and \%column options are enabled.
	
	Four CUP related options are used, \%cupsym CUP\_sym, \%cup, \%implements CUP\_sym, \%cupdebug, to interface with  the CUP generated parser, designate CUP symbol number interface, and provide debugging utility to the CUP compatible Lexer. The last option cupdebug creates a main function in the generated Lexer that takes the name of an input file and runs the Lexer on the file. Line number, column number and tokens are printed to the standard output.
	
	By default, Jflex compresses the DFA transition table and stores it in several string literals. Upon initialization, the table is unpacked from the string literals. The running time is propotional to the size of the transition table. Although the unpacking process will take a small amount of time, the process is usually not significant compared to the scanning process that includes a lot of much slower I/O operations. The benefit brought by the compressing technique is that the generated java class is relatively smaller and does not crash at run time due to the size limitation of 64KB of Java methods.\cite[pp.18-19]{klein2014jflex}
	
	This part also includes some code that is copied verbatim into the Lexer class. These code defines essential methods and members to implement the Lexer. 
	
	The newSymbol method is invoked to create a new symbol object passed to the parser whenever a token is recognized. 
	
	ReportError and ReportWarning methods provide interface to call the ErrorHandling.ReportError when errors and warnings are to be prompted. 
	
	The StringBuilder member \_string\_buffer is used as a buffer when a string literal is being recognized. The content of it comprises the entire string literal at the end of the recognition of a string literal. \_string\_buffer is initialized at initialization of the lexer.
	
	Public methods get\_yyline and get\_yycolumn provides interface for AST nodes to retrieve the accurate line and column number when the node is initialized. The reason of using these two methods to retrieve position information instead of the information passed by java\_cup.runtime.Symbol is that some constructs of the C programming language are represented by AST nodes that need to gather information after its creation. The position of the last token used by the AST node could be far away from its real position. The position information of the last token is quite confusing if used in the error message. Using these two methods, however, does not completely solve the problem of confusing position information, for some AST nodes are created very early during the parsing while some are very late. As a result, the position information at creation could be that of either the beginning of the construct or the end of the construct. Such inconsistency still leads to confusion sometimes. Nevertheless, the line/column numbers from these two methods are still adopted instead of those in Symbol objects because they are more appropriate in terms of correctness. The error messages emitted by semantic checking are mostly informative with this strategy.
	
	The rest of this part defines macros as abbreviation of common regular expressions in the lexical rule part. The macros define the characters allowed in identifiers, the characters treated as whitespace, comment style and the lexical rules of integer literals.
	
	Three exclusive states, YYSTRING, YYCHAR and YYIGNORENEXTLINE, are defined to indicate what kind of token the lexer is reading. YYSTRING indicates that the lexer is reading a string and all characters are directly copied to the string, except double quote that marks the end of a string and escaping characters that begins with a back slash that are interpreted as the characters they represent.
	
\subsection{Lexical rules}
	
	This subsection specifies the lexical rules of this implementation.
	
	All preprocessing directives are ignored except that the back slash at the end of a line is skipped along with the end-of-line character following it. The latter exception allows string literals written in multiple lines.

\lstinputlisting[caption=Example: multi-line help string]
{../demo/lex/help_string.c}
%\begin{tabular}{|l|}
%	\hline
%		
%		"Usage: [Options] $\langle$filename$\rangle$ $\backslash$n$\backslash$	\\
%		Options:$\backslash$n$\backslash$	\\
%		$\quad$ \texttt{-}h, \texttt{-{}-}help			prints the help prompt and exit$\backslash$n$\backslash$	\\
%		$\quad$ \texttt{-{}-}debug				enables debug print$\backslash$n"\\
%	\hline
%\end{tabular}

	The content in the box is recognized as a single string literal and can be printed out with a single printf function call.
	
	The string literal in multiple lines supported by this syntax is different from the raw string literal introduced in C++11. The latter does not interpret any escaping character and is not terminated by a eol marker. In C++11, the above string can be written as below. The raw string literal extension is not supported by this implementation.
	
\begin{tabular}{|l|}
	\hline
		
		R"(Usage: [Options] $\langle$filename$\rangle$	\\
		Options:	\\
		$\quad$ \texttt{-}h, \texttt{-{}-}help			prints the help prompt and exit	\\
		$\quad$ \texttt{-{}-}debug				enables debug print	\\
		)"\\
	\hline
\end{tabular}

	All keywords defined in \cite[6.4.1]{C99} are recognized by the lexer but only those required in \cite{projectwiki} are passed to the parser. The other keywords are simply ignored. Ignoring the unsupported keywords is correct for programs that conform to the standard with no unsupported keywords, but could incorrectly render as correct ill-formed programs that have unsupported keywords at inappropriate places.
	
\begin{tabular}{|ll|}
	\hline	
		int number = 5;		&	correct	\\
		const int number = 5; & correct, const is ignored	\\
		int number = const 5;	& wrong, but accepted since const is ignored	\\
	\hline	
\end{tabular}

	Integer literal suffixes are recognized but ignored. For instance, 1U, 1UL, 1ULL are all legal integer literal but are treated as a signed integer literal 1.
	
	When a double quote is found at YYINITIAL state, the lexer goes to YYSTRING state starting to recognize a string literal. The YYSTRING state does not end until an unescaped double quote is found. All escaping characters defined in \cite[6.4.4-5]{C99} are supported. In addition, an unescaped backslash with a character following that does not comprise an escaping sequence is treated as if the following character alone, but a warning is prompted. EOL marker cannot appear in a string literal unless following an unescaped backslash.
	
	The char literal is recognized in the similar fashion like the string literal. Upon the occurence of a single quote at YYINITIAL state, the lexer goes to YYCHAR state, in which the lexer tries to recognized a single char literal and then goes back to YYINITIAL. The escaping rules are identical to that of string literal.
	
	When an unexpected character that does not match any of the specification above, an error message will be prompted.