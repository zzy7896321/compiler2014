\chapter{Code Emission}
\label{ch:code-emission}

\section{Overview}
	
	Code emission is the last phase of a compiler if no optimization is performed on the assembly code. The quality of translating the intermediate representation to assembly code is crucial to the performance of the code emitted. Multiple optimizations can be applied during this phase. The way of code emission also highly depends on the target platform. In this case, the target platform is MIPS32, a RISC architecture, which has a highly reduced instruction set. Code has to be carefully chosen to improve the performance of the program. The program will run on SPIM, a MIPS32 simulator. Several features of SPIM affects the code emission as well. This implementation completely conforms to the MIPS call convention.
	
	Code emission contains three parts in this implementation: data segment translation, text segment translation and standard library. The following sections shows the translation details.
	
\section{Data Segment Translation}

	The data segment contains data of global variables, static data, and data of the runtime system. In this implementation, static data only includes string literals since storage class specifier syntax is not supported.
	
	On the MIPS32 architecture, memory access performance is related to the memory layout since the address that can be efficiently accessed with a register only ranges from -32768 byte to 32767 byte. The global data are addressed by the global pointer register \$gp, which is set to 0x10008000 by SPIM at initialization. The memory space efficiently addressed by \$gp is the first 64KB of the data segment. Those data outside this region have to use two instructions for each access. Therefore, all the global and static data are sorted in the increasing order of size in order to maximize the memory access efficiency.
	
	The only runtime system datum is a pointer that marks the end of data segment used by malloc. The 4-byte pointer is placed at the initial 4 byte of the data segment, i.e. at 0x10000000. The pointer is initially set to the end of the data segment using a label placed at the end.
	
	Another issue with the data segment is that the data segment is initially one quarter MB despite a maximum size up to 1MB. To expand the data segment, the regular way is to use the sbrk syscall. This method has an obvious drawback: it costs at least three instructions to perform the syscall each time. It can tremendously impair the performance of programs with a lot of dynamically allocated memory. However, the drawback can be overcomed with a non-standard trick: declares a space of 1MB and reset the data segment to 0x10000000. SPIM will automatically expand the data segment to the maximum size.
	
\lstinputlisting[caption=Example: data segment translation]
{../demo/code_emission/data_segment_translation.c}

\lstinputlisting[language={[mips]Assembler}, caption=Data segment assembly code]
{../demo/code_emission/data_segment_translation.s}

\section{Text Segment Translation}

\subsection{General Consideration}
	Text segment contains the code to be executed. The choice of appropriate instructions can greatly affect the performance. Except for the label related operations, this implementation does not use pseudo-instructions to avoid overhead brought by SPIM's poor implementation of pseudo-instruction.
	
	The arithmetic, multiply and divide, logical and bit-field operations has instructions requiring two register operands or one register and one 16-bit immediate operands. To generalize the load and store (not limited to memory access) operations, two methods loadOperand and storeResult are created. The loadOperand method loads the required operand to a register or represent it as an immediate according to the requirement. If a indirection is to be performed, the spill register are used as the target. After the instruction of operation is emitted, storeResult will be invoked if the destination is a memory location to write back the result.
	
	Not using pseudo-instructions spares the assembler reserved register \$at free to use. This implementation uses \$at for immediate loading(lui), addressing and other IR translation. To use \$at, .set noat has to be declared to disable the assembler warning.
	
\subsection{Reconsider the Register Mapping}
	The register allocation only maps the virtual registers to callee-saved registers as mentioned before. This could lead to massive amount of load/store when calling another function. 
	
	However, it would save a lot of load/store by remapping the registers needed to be saved across calls to the callee-saved register if the called function does not call another function, in which case the register is only loaded and stored once rather than the times of function calls. The test case nqueencon-5100379110-daibo.c shows the problem. With the technique, this implementation finally passed the performance test of nqueencon.
	
\subsection{Stack management}
	

\begin{center}
	\includegraphics[scale=0.5]{stack_frame.png}
\end{center}	
	

	At the entry point and exit point of a function, stack management code has to be executed. According to the call convention, \$sp is the stack pointer which should points to the stack top, and \$fp is the frame pointer which points the current stack frame. Frame pointer register \$fp and Return address register are callee-saved registers. If there are function calls in the function, \$ra should be saved at entry. If the size of the stack frame of the function is not zero, \$fp should be saved at entry. In addition, use of caller saved registers \$s0-\$s7 causes them to be saved at entry and loaded at exit.
	
\lstinputlisting[caption=Example: stack management]
{../demo/code_emission/stack_management.c}

\lstinputlisting[language={[mips]Assembler}, caption=assembly code of max]
{../demo/code_emission/max_func_stack.s}

\lstinputlisting[language={[mips]Assembler}, caption=assembly code of max\_elem\_of]
{../demo/code_emission/max_elem_of_func_stack.s}

\subsection{Conditional Test Translation}

	The conditional test instructions of MIPS32 are slt, slti, sltu, sltiu, i.e. signed and unsigned less-than test operations with two registers or a register and an 16-bit immediate. Other 5 kinds of test instructions can be expressed using a combination of a less-than test and a logical instruction or a less-than test alone. They are listed below: (operands are rs and rt, destination is rd, only the two-register are listed)

\begin{center}
\begin{tabular}{|l|l|}
	\hline
	Operation					&	Instruction		\\\hline
	rs < rt						&	slt rd, rs, rt	\\\hline
	rs > rt						&	slt rd, rt, rs	\\\hline
	\multirow{2}{*}{rs <= rt}	&	slt rd, rt, rs	\\
								&	xori rd, rd, 1	\\\hline 
	\multirow{2}{*}{rs >= rt}	&	slt rd, rs, rt	\\
								&	xori rd, rd, 1	\\\hline
	\multirow{2}{*}{rs == rt}	&	xor rd, rs, rt	\\
								&	sltiu rd, rd, 1	\\\hline
	\multirow{2}{*}{rs != rt}	&	xor rd, rs, rt	\\
								&	sltu rd, \$0, rd	\\\hline
\end{tabular}
\end{center}

\subsection{Merging Conditional Test and Branch}

	If the result of a conditional test is only used by a branch, and is compared against a constant. Then the value of the conditional test is not important. The branch and the conditional test can be merged, reducing the number of instructions. An example is shown below:
	
\begin{center}
\begin{tabular}{|l|l|l|}
	\hline
	Original							&		Merged		\\\hline 
	xor rd, rs, rt						&	beq rs, rt, L1	\\
	sltiu rd, rd, 1						&					\\
	bne rd, 0, L1	\# branch on rs==rt	&					\\\hline
	
	slt rd, rs, rt						&	slt rd, rs, rt	\\
	xori rd, rd, 1 						&	beq rd, 0, L1	\\
	bne rd, 0, L1 \# branch on rs >= rt	&					\\\hline
\end{tabular}
\end{center}

\subsection{Strength Reduction}
	Strength reduction is applied to mod or multiply n, where n is a power of two. The former is transformed to AND n-1, and the latter is transformed to left shift by log(n).
	
	Strength reduction is not applied to divide because left shifting by log(n) is wrong is the dividend is negative. It is also not applied to other operations because it does not reduce the number of instructions.
	
\section{Standard Library}
	Standard library functions are written in MIPS32 assembly code. They are copied verbatim to the end of the asm file. Only printf, malloc and memcpy are implemented. They can be found in stdlib directory. stdlib directory has to be copied to the bin directory for the Java to find it.
	
	The printf function only supports format of \%d, \%c, \%s and \%0nd where $0 \leq n \leq 9$. The return value of printf is NOT properly set.
	
	The malloc function does not check the memory limit. It only updates the pointer at 0x10000000, keeps alignment to 4 and returns the pointer to the allocated space.
	
	The memcpy function only supports aligned copy of memory space whose size is a multiple of 4. If memcpy is only applied to variables that has size being a multiple of 4, it will work correctly. Otherwise, undefined behaviour could occur.
