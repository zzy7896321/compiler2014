\chapter{Semantic Checking and Intermediate Representation Generation}
\label{ch:semantic}

\section{Overview}
	
	A syntactically well-formed program has to be checked if its meaning makes sense. This process is semantic checking. A program that makes sense can be transformed to an intermediate representation that is neither so machine-dependent like the assembly code, nor that high-level like the original programming language. The intermediate representation consists of several tens of basic operations and types of operands, which makes it easy to optimize and translate to assembly code on different target platform.
	
	This implementation incorporates intermediate representation generation pass into the semantic checking pass, instead of two separate passes. One of the reason of doing that is to reduce the time overhead of traversing the AST and avoid extra space and over-expanded AST node classes in order to support annotated AST. The one pass solution can detect semantic errors and generate IR code on the fly. It is easy to implement and quite flexible to enable optimization of IR generation.
	
\section{Intermediate Representation}

	The intermediate representation designed for this project is comprised of three parts: Quadruple (i.e. Three-address code), Operands and static variable entries. All classes that implement these IR components are in irpack package.
	
	The quadruples support a subset of MIPS32 instruction set with extended semantic, together with a few pseudo-instructions for loading address and function calls. Most arithmetic, load/store, branch, jump instructions can be represented as the counterparts of MIPS32 instructions in the IR. Function calls, argument passing and return value retrieval are represented as specially designed quadruples. The operations related to function calls has to use special quadruples because they do not fit the general format of three-address code. Some of them has to use or define fixed registers according to the MIPS calling convention, and arguments has to be pushed to locations offset from \$sp that cannot be represented as normal operands.
	
 	The operands of quadruples can be a global or local variable called memory location (MemLoc) in this implementation, a int constant, an address constant, a label, or an indirection (load/store) to the operands listed before. The representation of operands is very flexible such that IR generation is greatly simplified. In addition, optimization passes are simplified by the abstraction of operands, for it reduces the temporary variables used in the IR. Operands all implement the Operand interface, and most of them implement either ConstantOperand or Addressable interface. The Label class is derived from Quadruple and implements ConstantOperand, as labels are both quadruple in the code and operands of branch and jump instructions.
 	
 	Static variable entries contain information of external (global variables) and static (string literal) objects. They are not scanned in the optimization pass and are directly written to the data segment in the assembly code.
 	
\section{The environment package}
	The semantic checking and IR generation highly depends on a symbol table which records the objects defined and visible in the context of the program and a properly implemented type system that conforms to the standard. These components are implemented in environment package. 	
 	
 	The symbol table implemented in Environment class is comprised with a hash map for normal name space, a hash map for tag name space, and a pointer to the Environment instance of the upper scope. The hash map maps string names to objects, functions, and types. The entries are instances of subclasses of SymbolInfo class. Common interfaces are provided by SymbolInfo, including the name, the type and the linkage while additional interfaces to access additional information are provided by each subclass.
 	
 	Type classes are derived from environment.Type. Common interfaces include access to size, align requirement, etc. Struct/union classes are derived from StructOrUnion Type. Separate classes are created for struct and union to implement different memory layout.
 	
\section{Implementation Details}

	The semantic checking and IR generation of declarations, statements are implemented as static methods in semantics.SemanticCheck class, while those of expressions are implemented as methods inherited from ExpressionBase class. The declarations and statements are relatively irrelevant to each other and share no common interface of semantic checking and IR generation, so they are implemented together in a single class. Expressions are recursively defined and, as a result, the best way to check them is doing it by recursion.
	
	Detailed error and warning are prompted when error or undefined-behavioural constructs are found in the program. Error will cause the compiler exit as soon as this semantic checking and IR generation pass ends, while warnings are omitted leaving the compiler to continue. The compiler does not exit upon an occurrence of error. It instead make some assumptions on the program and continue checking in order to spot more errors in the program. This will help users to diagnose their compilation error without having to run the compiler multiple times.
	
\subsection{Pre-checking and Post-checking Actions}

	When checkAndGenerate method is invoked, it initiates a top level environment, sets the current level environment to the top level. Then it invokes setRunningInstance to provide expression checking methods with access to itself. Sizeof expression may need to access the running instance to update the environment when it's applied to a new struct/union type. The next step is invoking include\_library method to include the declarations of built-in functions. They are regarded as defined although no actual definition are included at this point. So trying to define supported built-in functions will result in a re-definition error.
	
\lstinputlisting[caption=Error: re-definition of built-in function printf]
{../demo/semantics/redef_builtin.c}

	After the completion of pre-checking actions, checkAndGenerate starts traversing the AST to check semantic and generate IR code.
	
	The first step of post-checking actions is to check the completeness of all the external symbols. Error will be reported on incomplete type objects and undefined functions. But incomplete types of which no objects are defined are not errors.
	
\lstinputlisting[caption=Example: incomplete type is not an error]
{../demo/semantics/incomplete_type_not_an_error.c}

	After all the above steps are done, cleaning up are performed to enable Java GC recycles the memory space taken by the environment and symbol table, which will not be used in the following passes.
	
\subsection{Declarations}

	External Declarations are checked by checkExternalDeclaration method.
	
	The external declarations are all on the top level and no code for executing are generated for them. Static variable entries are created for top level objects and recorded in the IR instance. Declaration specifiers are firstly checked by invoking checkDeclarationSpecifier. The type declared by the type specifier is returned and new struct/union types are registered in the top level environment. Then the declarators are iterated through and specific derived types are calculated for each of them. The derived type combined with the identifier is checked within the top level environment. A conflict error is prompted if there is conflicting entries and a SymbolInfo entry is inserted into the top level environment if not. An initializer associated to a declarator is passed to checkInitializerForExternalDeclaration to calculate the initializing value.
	
	Tentative definition is supported by this implementation. Tentative definition refers to declarations in the top level environment without an initializer or a storage-class specifier. Tentative definition can appear for any times, even after a declaration with initializer appears. If no declaration with initializer of an top-level object ever appears, the object is initialized with 0.
	
\lstinputlisting[caption=Example: tentative definitions]
{../demo/semantics/tentative_definition.c}

	However, having multiple initializers for the same object is an error, even if the initializer evaluates to the same value.
	
\lstinputlisting[caption=Error: redefinition of variable]
{../demo/semantics/redef_var.c}

	External Declarations can be declaration of an incomplete type. The type can be completed later.
\lstinputlisting[caption=Example: external object declaration with type completed later]
{../demo/semantics/external_incomplete.c}

	Function declaration is supported by this implementation as well. A incomplete function designator symbol is created for a function declaration. Declarations of a function can appear any times, as long as the declarations are compatible.

\lstinputlisting[caption=Example: multiple function declarations]
{../demo/semantics/func_decl.c}

	Block level declarations are checked and generated code for by checkBlockLevelDeclaration method. The steps of block level declaration checking is similar to that of external ones. The only difference is that the initialization of block level variables is done by assignment.

\subsection{Initializer}
	
	There are two types of initializer: expression initializer and list initializer.
	
	Expression initializers are evaluated for a single value. The initialization with expression initializer has the same semantic with assignment expression.
	
	List initializers consists of a list of initializer of both types, preceding each of which could be a designator indicating which element of the aggregate type is to be initialized.
	
\lstinputlisting[caption=Example:initializer]
{../demo/semantics/initializer.c}

	The semantic of initializer is related the concept of current object. Initially the current object is the initialized object. Initialization are performed in the order of the fields declared or from low index to high index of an array. Refer to \cite[6.7.8]{C99} for the exact semantic of initialization.
	
	 Initializer can also complete array types that lack dimension designated. The size of the array is set to the number of elements initialized by the initializer.
	 
\lstinputlisting[caption=Example: size of array determined by initializer]
{../demo/semantics/initializer_array.c}

\subsection{Bit-fields}

	Bit-fields of struct or union are regarded as signed integer with designated bit length. Since this implementation only supports 4-byte int, the maximum of bit length is 32. Each bit-field is assigned a mask that does not overlap with other portion of the struct. The mask assigned to a.a in the following example is 0x1f while the mask of a.b is 0xf00. 0xe0 is assigned to an unnamed bit-field as a padding space.
	
\lstinputlisting[caption=Example: bit-field]
{../demo/semantics/bit-field.c}

	When bit-fields are used, the entire integer is loaded to a temporary variable. It is shifted to the left by the number of the leading zeros of its mask. Then it is shifted to the right with sign extension by the number of the trailing zeros plus the number of the leading zeros of its mask. The result in the temporary variable is the sign-extended integer of the bit-field.
	
	When bit-fields are stored, the source operands is shifted to the left by the number of leading zeros of the mask plus the number of trailing zeros of the mask, and then shifted to the right without sign extension by the number of leading zeros of the mask. Denote the result as t1: a integer with the bit-field properly set and other bits set to zero. The entire integer is loaded to another register and apply AND bit-wise not of the mask to the integer. Denote the result as t2. Apply OR to t1 and t2 and store the result to the struct.
	
\subsection{Function definition}

	Function definitions are checked by checkFunctionDefinition method. 
	
	The method first checks the type specifier for return type. Functions with return type of void have no return value. Then the declarator is checked to get the parameter type list. Since identifier list is not supported by this implementation, an empty list is regarded as empty parameter type list, which specifies no parameter shall be provided to the function, rather than an empty identifier list, which specifies nothing about the parameters. This practice does not conform to \cite[6.7.5.3]{C99} and results in different behaviour from GCC. However, the (void) style parameter list declaring a no-parameter function is supported.

\lstinputlisting[caption=Error: empty is regarded as parameter type list]
{../demo/semantics/not_conforming_func_id_list.c}

	Notice that a new scope begins upon the occurrence of the parameter type list and each declaration does not share a common scope. Therefore, the following program has a conflict type of function because of the incompatible parameter type, although the two struct A look the same.
	
\lstinputlisting[caption=Error: scope begins at the parameter type list]
{../demo/semantics/conflicting_para_type.c}
	
	After the checking of function prototype completes, a new environment with all the parameters in it is created, linked to the top-level and set as the current environment. In addition, an environment.RuntimeEnvironment object is created, containing the return type of the function, a break target stack, a continue target stack, a FunctionEntry object to be written IR code to and be registered to the IR, and a short-circuit branch target for short-circuit operation IR generation. These information are essential for the IR generation of the function body.
	
	The last step is to check and generate IR for the function body by invoking checkCompoundStatement method.
	
	Another issue with function definition is that the return type and parameter type could be struct/union, which exceeds the size of a register. Extra spaces are allocated for them and the address of the space is passed across calls. To support the semantic in the IR, an additional return value address MemLoc object is created for a struct/union return type and a struct/union parameter will be set true isPassByAddress member, which leads to different semantic than the normal MemLoc operand when they appear in the quadruples.
	
\subsection{Statements}

	Statements are checked by several methods in SemanticCheck. The checking and IR generation are performed simultaneously. Whenever part of the statement is checked, IR code is generated for it. Errors are reported but do not terminate the checking. Labels are created at the entry of each checking methods if necessary.
	
	The checking of compound statement involves three steps: creating a new scope linked to the current scope, invoking checkCompoundStatement to check declarations and statements in it, and restore the scope.
	
	The return statements may or may not have return value specified. In functions with return type of void, no return value shall be specified. In functions with return type other than void, it is optional to specify a return value. The return value has the same compatible requirement as assignment, but has a different semantic. The return value is copied to \$v0 register when the return type is not struct/union, or is copied to the space pointed by the pointer previously provided by the caller and store the pointer to \$v0 if the return type is struct/union. After the return value is properly copied, a jump instruction is generated to jump to the exit block of the function where the clean up code are located.
	
	Break and continue statements generates IR that directly jumps to the top of break/continue target stack set by for/while statements. If the stack is empty, an error is reported.
	
	The if-statement consists of a control expression, a true branch statement and an optional false branch statement. More details on the control expression IR generation are talked about in subsection \ref{subsec:short-circuit}.
	
\subsection{Loop statements}
	
	For and while statements constructs loop structures in a C program. 
	
	A for statement has four parts. The initialization expression is evaluated before the loop. The control expression is evaluated each iteration of the loop to determine whether to continue. The step expression evaluates to void at the end of each iteration right before the evaluation of the control expression in order to update loop variables. The last part is the loop body. The break target is the following instruction of the entire loop and the continue target is the step expression. 
	
	Loop inversion is applied to the for-statement. Loop inversion moves the condition expression to the end of the loop body. It can eliminate a jump instruction to the condition expression each iteration. Usually loop inversion replaces a while loop by a if statement containing a do-while loop. This implementation, however, directly jump to the condition expression rather than a conditional branch at the header of the loop. The reason for that is copying the condition expression might affect the efficiency of register allocation and lead to expansion of code, cancelling out the benefit brought by reducing one redundant jump. The test result shows that the loop inversion without a conditional branch at the header could work better or worse depending on different cases.
	
	With loop inversion optimization, a for statement is structured as below:
	
\begin{center}
\begin{tabular}{|l|}
	\hline
	initialization expression	\\
	j L3						\\
	L1:							\\
	loop body					\\
	L2: (continue target)		\\
	step expression				\\
	L3:							\\
	control expression			\\
	if control is true goto L1	\\
	L4: (break target)			\\
	\hline
\end{tabular}
\end{center}

	The IR of while statement is similar to that of the for-statement.
	
\begin{center}
\begin{tabular}{|l|}
	\hline
	j L2						\\
	L1:							\\
	loop body					\\
	L2: (continue target)		\\
	control expression			\\
	if control is true goto L1	\\
	L3: (break target)			\\
	\hline
\end{tabular}
\end{center}

\subsection{Expressions}

	Expressions checking and IR generation methods are distributed in the AST node classes. Each subclass derived from ExpressionNode implement the checkEvaluate\_NoGenerating and checkEvaluate method. The former method only performs semantic checking and try to evaluate the expression at compile time, while the latter performs both semantic checking and IR generation.
	
	When a constant expression is required, the checkEvaluate\_NoGenerating method is invoked with the current scope. If the expression has no error, invoking to getType method returns the type the expression evaluates to. Otherwise, null is returned. If the result is constant, getValue method returns the constant value. Otherwise, null is returned. When a non-constant expression is required, the checkEvaluate method is invoked with the current scope and the RuntimeEnvironment object. Invoking to getType method returns the type if no error is found or null if not. The getValue method returns an operand that containing the result. In both cases, a checked boolean member is set to prevent the expression from being re-evaluated, which could happen in initializer checking. If re-evaluation is the desired behaviour, clearEvaluatedState should be invoked.
	
	The sizeof expression may create new struct/union type. To commit the change to the environment, it accesses the SemanticCheck object by invoking getRunningInstance.
	
\lstinputlisting[caption=Example: sizeof expression creates new a type]
{../demo/semantics/sizeof_create_new_type.c}

\subsection{Conditional Branch and Short-Circuit Expression}
\label{subsec:short-circuit}

	The IR generation adopts the scheme as described in \cite[6.6]{aho2009compilers}.
	
	Control expressions are evaluated for conditional branch. Non-short-circuit expressions are evaluated for value as a control expression. The branch is taken if the value is non-zero. If the control expression is a short-circuit expression, i.e. logical OR expression and logical AND expression, the RuntimeEnvironment object's sc\_target\_set is set to true. The true branch target and false branch target is correspondingly set according to the semantic of the statement. If one of the target is the instruction immediately following the branch, the target is set to null to indicate fall.
	
	The checkEvaluate does not perform normal checking and IR generation, if it is invoked on a short-circuit expression. Instead, it checks whether the sc\_target\_set is true. If sc\_target\_set is true, the result of the expression is only used as branch condition meaning the result value is not essential. The checkEvaluate will directly invoke checkEvaluate\_short\_circuit and branch directly on the result of its operands. The checkEvaluate\_short\_circuit method preserves and clears the sc\_target information, and evaluates its operands. If the operand is another short-circuit expression, the sc\_target is properly set according to the short-circuit semantic.
	
	When sc\_target\_set is false, the short-circuit expression should evaluate for value. Then checkEvaluate of the expression will create a true branch that sets the result to 1 and a false branch that sets the result to 0 before invoking checkEvaluate\_short\_circuit.
	
\lstinputlisting[caption=Example: short-circuit expression]
{../demo/semantics/short_circuit.c}
