\chapter{Register Allocation}
\label{ch:register}

\section{Overview}
	
	Arbitrary number of variables are used in the intermediate representation, but there are only a few registers on the real machines. Register allocation is a procedure that maps the massive number of variables to a limited number of registers and store some of the variable on the memory (spill) if necessary.
	
	Traditional register allocation is based on graph colouring\cite[Ch.10,11]{aho2009compilers}. Liveliness analysis is performed on the given CFG using the iteration method. A interference graph can be built from the liveliness information. The nodes in the interference graph are variables. There is an edge between two variable if their have overlapping live ranges. Then a heuristic graph colouring algorithm is run on the interference graph. Variables that cannot be coloured are spilled to the memory. If any actual spill happens, the program has insert load/store instructions and repeat the whole allocation from liveliness analysis until no spill happens.
	
	The graph colouring algorithm can be very slow due to repeating the iteration for multiple times. To cope with the problem, the linear scan algorithm was reported in \cite{poletto1999linear}. The live ranges are approximated with intervals numbered with instructions. The algorithm linearly scan the intervals and heuristically allocate register to the variables and spill those that cannot reside in register. The algorithm runs much faster than the traditional graph colouring algorithm with similar code quality.
	
	The original linear scan does not take the SSA form into account. A SSA based linear scan algorithm was introduced in \cite{mossenbock2002linear}. The liveliness analysis can be performed more efficiently. It worst case time complexity is $O(n^2)$, but runs linearly in practice. The live intervals are refined by taking into consideration holes. To cope with $\phi$-functions, additional variables are inserted at each predecessor and are coalesced with the $\phi$-function. Linear scan is then performed on the intervals. The algorithm does not split variables because SSA variables usually have short live intervals.
	
	The SSA based linear scan algorithm is implemented in optimization. SSABasedLinearScan. The following sections shows the implementation details.
	
\section{Inserting Moves for $\phi$-functions and Other Transformations}
	
	$\phi$-functions do not have counterpart on real architectures. Moves have to be inserted at predecessor block for them. An example is shown below. The $i_3$, $i_3'$ and $i_3''$ will be coalesced. To reduce these moves, it is best for $i_3'$ and $i_3''$ are allocated with the same register respectively as $i_1$ and $i_2$, but this is not always possible for live intervals could overlap even for variables originated from the same variable.
	
\begin{center}
\begin{tabular}{|l|l|}
	\hline
		Before	&	After			\\\hline
		if ...	&	if ...			\\
		$\quad$ $i_1$ = 1 & $\quad$ $i_1$ = 1		\\
		else	&			$\quad$ $i_3'$ = $i_1$	\\
		$\quad$ $i_2$ = 2 & else		\\
		$i_3$ = $\phi$($i_1$, $i_2$) & $\quad$ $i_2$ = 2		\\	
		print($i_3$)	&	$\quad$ $i_3''$ = $i_2$	\\
				&	$i_3$ = $\phi$($i_3'$, $i_3''$)	\\
							&		print($i_3$)	\\
	\hline	
\end{tabular}
\end{center}

	To provide live information for function calls, JAL quadruples are replaced with Call quadruple. The latter contains a field to record the liveliness of registers at the function call. Loads are inserted before pushing argument if it is an indirection to avoid troublesome to code emission.
	
\section{Numbering Instructions}
	
	The numbering of instructions can greatly affect the quality of register allocation. Since loop inversion is implemented with a jump at loop header to the control expression, the sequence of the quadruples is not that of their appearance. A "topological" sort is performed on the CFG. However, CFG could have cycles and cannot be applied real topological sort to. The strategy to break tie is to number the block with the smaller index first. $\phi$-functions are not numbered since moves has been inserted for them.
	
\section{Liveliness Analysis}
	
	The liveliness analysis can be performed in reverse topological sorted order on the SSA form instead of calculating a maximal fixed pointer using iteration. This fact makes the liveliness much more efficient.
	
	Denote the current block as b. The live set of b is initialized to the union of all the successor's liveIn minus all the define of successor's $\phi$-functions union all the use of successor's $\phi$-functions. Each variable in live is extended with live range from the first instruction to the last instruction in b. Then the program iterates from the end to the beginning of b, remove definition from the live and add use to the live, with live intervals properly updated. Finally, output of $\phi$-functions are removed from the live set.
	
	The only problem with the algorithm is that it does not properly handle the loops. Another liveliness analysis algorithm for SSA described in \cite{wimmer2010linear} gives a solution: if b is a loop header, then add range for each variable in live set from the loop header to the loop end. To calculate the loop end, each block has a loopEnd array initialized to -1. Whenever a block has a successor not visited yet, the block is a loop end candidate of the successor's. The loop end of the successor is the block that has the maximum number of the last instruction.
	
\section{Linear Scan}

	The linear scan algorithm scans the intervals in the increasing order of their start points. Four sets are maintained:
\begin{center}
\begin{tabular}{ll}
	unhandled:	&	intervals that starts after the current point	\\
	active:	&	intervals where one of their ranges overlap the current point	\\
	inactive:	& intervals with a hole of which the current point falls into	\\
	handled:	& intervals that ends before the current point	\\
\end{tabular}
\end{center}

	In each iteration, an unhandled interval with the smallest start point is picked. The current point moves to the start point of the picked interval. Active, inactive and handled sets and free register set are updated. Since no precolouring is done in this implementation, only registers that used by inactive variables overlapping the picked variable need to be further removed from the free set. If the free set is empty, then the picked variable or variables sharing one of the registers with the lowest spill cost will be spilled. Spill cost is estimated by the size of use lists. Otherwise, the variable is assigned with a register from the free set and moved to active set. Some variable may have preferred register. The first four arguments prefer their argument register and a variable in a move prefer the same register with that of the other operand. The preferred register is assigned to the variable if not occupied. Otherwise, a random picked variable from the free set is assigned to the variable. The randomness causes the quality of register allocation may differ within a certain range. In terms of instruction numbers, the performance differs in a range from 0\% to 5\%.

\section{Post Linear Scan Process}
	
	After linear scan, 	several post process takes place. First, a live set is calculated for each function call using the live intervals. Then register allocation results are copied from the representative of coalesced variables. Next, memory is allocated for each non-SSA variables.
	
	Spills could happen during linear scan, so two spill registers (\$t8 and \$t9) are reserved. In addition, there are callee-saved registers that does not participate register allocation and system reserved registers. The register allocation result is actually assignment of virtual registers. The virtual registers have to be mapped to the real registers on the real machine. 14 registers are used in register allocation, including \$a0-\$a3, \$v0-\$v1 and \$t0-\$t7. To avoid redundant moves for arguments, the arguments' register has priority to be mapped to \$a0 - \$a3.
	
\section{Calculating Registers Used by A Function}

	To reduce load/store of caller-saved registers across function calls, register usage of each function has to be calculated. This can be done by a simple maximal fixed point calculation. Initially, the registers used by a function are those allocated to its variables. Whenever a function f is called by a function g, registers used by g includes all the registers used by f. Therefore, the register usage is propagated along the inverse calling relation. When a maximal fixed point is reached, the register usages are correctly computed.
	
	