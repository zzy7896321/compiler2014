#include <stdio.h>

int main(int argc, char* argv[]){
	//primary expression
	main;
	111;
	"a";
	'a';
	(main);

	//postfix expression
	argv[5];
	argv['a'];
	argv[argc-1];
	max(1, 2);
	init();
	x.b;
	x.$salary;
	argc--;
	argc++;
	(&x)->b;
	(&x)->salary;

	//unary expression
	++argc;
	--argc;
	++argc--;
	*p++;
	&x;
	*(&x);
	*0x12345678;
	+123;
	-2147483648;
	~0xffffffff;
	!0;
	sizeof(int****);
	sizeof(int);
	sizeof 123;
	//sizeof(int[5]);	//not supported


	//cast expression
	(int) argc;
	(int) (1 + 2 - 3);
	(char) (0x0b);
	(int*) 0x12345678;

	//multiplicative expression
	1 * 2 / 3;
	1 * (2 / 3) % 4;

	//additive expression
	1 + 2 / 3 - 4 % 5;
	1-x%yy/(123+_$123);

	//shift expression
	100000 >> 4-1<<1;

	//relational expression
	1+2>>1 > 1 <= 5;
	1 < 2 > 3 <= 4 >=5;

	//equality expression
	1 <2 == 3 < 4;
	1<2 != 3 > 4;

	//&, %, |, &&, || expression
	1||2&&3|4^5&6 == 7;

	//conditional expression
	x > y ? "greater" : x < y ? "less" : "equal";

	//assignment expression
	a = b += c++ -= d *= a /= e %= b-- <<= f.b >>= e->c &= ++x |= --y ^= e |= 1>2?0x10000:0x20000;

	//comma expression
	a = b, b = c,eee.cc[1] =  ddd ^123; 
}

