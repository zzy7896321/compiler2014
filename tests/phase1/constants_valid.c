#include <stdio.h>



int main(int argc, char** argv){
	//identifiers
	int _abc,
	_123,
	test_env,
	$global;
	
	//char literals
	'a';
	'A';
	'0';
	'\'';
	'\"';
	'\?';
	'\\';
	'\a';
	'\b';
	'\f';
	'\n';
	'\r';
	'\t';
	'\v';
	'\0';
	'\141';		// 'a'
	'\12';		// '\n'
	'\7';		// '\a'
	'\07';		// '\a'
	'\007';		// '\a'
	'\x07';		// '\a'
	'\xa';		// '\n'
	'\x30';		// '0'
	
	//string literals
	"Hello, world!";
	"";
	"\'\"\?\\\a\b\f\n\r\t\v\0\141\12\7\07\007\x07\xa\x030";
	"Usage: <input file>	\n\
Note: only one file will be processed! \n\
\n\
Lexer help prompt message.\n";
	"\0\0\0\0";

	//integer literals
	100;
	0x64;
	0X64;
	0X064;
	0144;
	00144;
	0xff;
	0XfF;
	0x7fffffff;


}

