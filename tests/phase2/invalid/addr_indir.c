int main(){
	typedef int INT;
	INT a[10];
	struct A{
		INT a;
		INT b:5;
		INT c;
	}	cc;

	//&INT		//syntactically illegal
	
	&1;
	&*1;
	*1;
	
	&main[0];
	&0[main];

	&(1 + 2);	
	&cc.b;

}
