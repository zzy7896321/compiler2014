#include <stdio.h>

void f(int x, int* y){
}

void g(void){

}

void h(char * args[]){

}
struct A{
	int x;
} s;
int main(){
	int x;
	char y;
	void* z;
	char a[10][10];

	f(&x, z);	//warning
	h(main);	//warning

	h(s);		//error
	g(x);		//error
	h();		//error

	z();		//error

	printf("%d %d\n", x, y);
}
