#include <stdio.h>

int main(){
	int* a;
	int b[10];

	struct A{
		int xx;
		int yy[10];
	} aaa;

	&a;
	&*a;
	&*&*&*a;
	&b[2];
	&b;
	&*b;

	&aaa;
	&aaa.xx;
	&aaa.yy[2];

	*a;
	*&a;
	*&aaa.xx;
	*(aaa.yy + 2);
}
