int main(){
	int a, b;
	char c, d;
	void* e, *f;

	a/b;
	a/c;
	c/a;
	c/d;
	a/1;
	1/a;
	'a'/a;
	a/'a';
	'a'/c;
	c/'a';
	1/2;
	1/'a';

	a*b;
	a*c;
	c*a;
	d*c;
	1*a;
	a*1;
	'a'*a;
	a*'a';
	'a'*c;
	c*'a';
	1*2;
	1*'a';
	'a'*(char)1;

	a%b;
	a%c;
	c%a;
	d%c;
	1%a;
	a%1;
	'a'%c;
	c%'a';
	1%2;
	1%'a';
	(char)1 % 'a';

}
