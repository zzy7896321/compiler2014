#include <stdio.h>

int a;
char b;
void* c;
struct A{int y;} f(int x, struct B{int x;} b){}
struct A d;
union C { int x; int y;} e;
int* g[10][12][13];


int main(){
	// integer constant
	0;
	123;
	0x123;
	0123;
	0x7FFFFFFF;
	0x80000000;		//warning
	2147483648;		//warning
	020000000000;	//warning	

	// char constant
	'a';
	'A';
	'\n';
	'\x0b';
	'\007';
	'\x100';	//warning
	'\0';

	// string literal
	"Hello, world!";
	"\n\r\t\0";
	"\0\0";
	"123123\
doremi";

	// identifier
	a;
	b;
	c;
	d;
	e;
	f;
	g;
}
