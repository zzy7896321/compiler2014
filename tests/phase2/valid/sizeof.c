int main(){
	int a;
	char b;
	void* c;
	struct A{
		int x, y;
	} d[10], e;
	union B{
		int x, y;
	} f[10], g;

	typedef struct A A;
	typedef union B B;

	sizeof(a);
	sizeof(b);
	sizeof(c);
	sizeof(d);
	sizeof(e);
	sizeof(f);
	sizeof(g);
	
	sizeof(e.x + e.y);
	sizeof(&a - &e.x);

	sizeof(1);
	sizeof('a');
	sizeof("Hello, world!");

	sizeof(int);
	sizeof(char);
	sizeof(int*);
	sizeof(char****);
	sizeof(int (*)[10]);
	sizeof(char [10]);
	sizeof(struct A);
	sizeof(union B);
	sizeof(A);
	sizeof(B);
}
