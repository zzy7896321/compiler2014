package optimization;

import irpack.Addressable;
import irpack.Label;
import irpack.MemLoc;
import irpack.Operand;
import irpack.Phi;
import irpack.Quadruple;
import irpack.Temp;
import irpack.VarWrapper;

import java.io.IOException;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;

import util.IntVector;
import util.MyLinkedList;
import graph.*;

/**
 * The SSAConstructor class converts intermediate representation to
 * Static Single Assignment form.
 * <p>
 * I, currently, makes the following assumptions:<br>
 * 1. Only local and temporary variables undergo optimizations. Global and explicitly address-taken objects 
 * will be on the memory and require explicit load/store.
 *
 *
 * @author Zhao Zhuoyue
 * @version last update: May 12, 2014
 */
@SuppressWarnings("unused")
public class SSAConstructor {

	/**
	 * Converts a normal three-address code to static single assignment form.
	 * 
	 * @param controlFlowGraph		the control flow graph
	 * @param entry					the entry block
	 * @param exit					the exit block
	 */
	public static void convertToSSA(Graph<Block> controlFlowGraph, Block entry, Block exit){
		
		/* initialize */
		init(controlFlowGraph, entry, exit);
		
		/* calculate immediate dominators and build the dominator tree */
		idom = Dominators.dominators(graph, SSAConstructor.entry.getIndex());
		buildDominatorTree();
		
		/* calculate the dominance frontiers */
		computeDF(entry.getIndex());
		
		/* collect define/use information */
		collectInformation();
		
		/* place phi functions */
		placePhi();
		
		/* rename variables */
		renameVariables();
		
	}
	
	/**
	 * Initialize data structures.
	 * 
	 * @param controlFlowGraph			the control flow graph
	 * @param entry						the entry block
	 * @param exit						the exit block
	 */
	private static void init(Graph<Block> controlFlowGraph, Block entry, Block exit){
		graph = controlFlowGraph;
		N = graph.size();
		SSAConstructor.entry = entry;
		SSAConstructor.exit = exit;
		
		DF = new IntVector[N];
		children = new IntVector[N];
		for (int i = 0; i < N; ++i){
			DF[i] = new IntVector(8);
			children[i] = new IntVector(8);
		}
	}
	
	/**
	 * Builds the dominator tree based on the information of the immediate dominators.
	 * 
	 */
	private static void buildDominatorTree(){
		for (int i = 0; i < N; ++i){
			if (idom[i] != i)
				children[idom[i]].add(i);
		}
	}
	
	/**
	 * Computes dominator frontiers based on information of dominator tree.
	 *
	 * 
	 * @param n
	 */
	private static void computeDF(int n){
		//TODO implement non-recursive version
		
		/* computes DF local[n]: the successors of n that are not strictly dominated by n */
		for (int y : graph.successor(n)){
			if (idom[y] != n)
				DF[n].add(y);
		}
		
		/* computes DF up[n]: nodes in the dominance frontier of n that are not strictly dominated by n's idom */
		IntVector children = SSAConstructor.children[n];
		for (int i = 0, szi = children.size(); i < szi; ++i){
			int c = children.get(i);
			computeDF(c);
			
			IntVector DFc = DF[c];
			for (int j = 0, szj = DFc.size(); j < szj; ++j){
				int w = DFc.get(j);
				if (idom[w] != n) DF[n].add(w);
			}
		}
	}
	
	/**
	 * Collects information of definitions and uses for placing phi functions and variable renaming.
	 * 
	 */
	@SuppressWarnings("unchecked")
	private static void collectInformation(){
		originalDefSites = new HashMap<MemLoc, WorkList<Block>>();
		origin = (HashSet<MemLoc>[]) new HashSet<?>[N];
		hasPhi = (HashSet<MemLoc>[]) new HashSet<?>[N];
		varRenamingStack = new HashMap<MemLoc, RenamingStack>();
		
//		for (MemLoc memloc : originalDefSites.keySet()){
//			varRenamingStack.put(memloc, new RenamingStack());
//		}
				
		for (int i = 0; i < N; ++i){
			Block block = graph.getIthNode(i);
			origin[i] = new HashSet<MemLoc>();
			hasPhi[i] = new HashSet<MemLoc>();
			
			for (MyLinkedList<Quadruple>.Node node = block.getList().iterator(); node.hasNext(); ){
				node = node.next();
				Quadruple quad = node.value();
				
				{
					Addressable[] defList = quad.getDefine();
					for (Addressable defined : defList){
						
						/* add to origin i */
						origin[i].add((MemLoc) defined);
						
						/* add to work list of the variable */
						WorkList<Block> worklist = originalDefSites.get(defined);
						if (worklist == null){
							worklist = new WorkList<Block>(N);
							originalDefSites.put((MemLoc) defined, worklist);
						}
						worklist.insert(block);
						
						/* add to variable renaming stack */
						if (!varRenamingStack.containsKey(defined))
							varRenamingStack.put((MemLoc) defined, new RenamingStack());
					}
				}
				
				for (Operand operand : quad.getUse()){
					if (operand == null) break;
					
					/* add to variable renaming stack */
					if (!varRenamingStack.containsKey(operand))
						varRenamingStack.put((MemLoc) operand, new RenamingStack());
				}
			}
		}
	}
	
	/**
	 * Places phi functions.
	 * 
	 */
	private static void placePhi(){
		
		/* insert phi function for each variable */
		for (Map.Entry<MemLoc, WorkList<Block>> mapEntry : originalDefSites.entrySet()){
			MemLoc memloc = mapEntry.getKey();
			if (memloc instanceof Temp) continue;		// don't place phi for temporaries
			
			WorkList<Block> W = mapEntry.getValue();
			
			while (!W.isEmpty()){
				Block block = W.remove();
				IntVector DFn = DF[block.getIndex()];
				for (int i = 0, szi = DFn.size(); i < szi; ++i){
					int frontier = DFn.get(i);
					if (! hasPhi[frontier].contains(memloc)){
						
						/* insert phi function with as many arguments as the predecessors of the dominance frontier */
						Block frontierBlock = graph.getIthNode(frontier);
						frontierBlock.addFirst(new Phi(memloc, graph.predecessorCount(frontier)));
						
						/* set hasPhi */
						hasPhi[frontier].add(memloc);
						
						/* add additional node to work list */
						if (!origin[frontier].contains(memloc)){
							W.insert(frontierBlock);
						}
					}
				}
			}
		}
		
		/* clears up */
		originalDefSites = null;
		origin = null;
		hasPhi = null;
	}
	
	/**
	 * Rename all variables.
	 * 
	 */
	private static void renameVariables(){
		
		varWrapperSet = new HashMap<VarWrapper, VarWrapper>();
		
		rename(entry);
	}
	
	/**
	 * Returns the unique VarWrapper originated from variable with the given number.
	 * 
	 * @param variable
	 * @param number
	 * @return
	 */
	private static VarWrapper getVarWrapper(MemLoc variable, int number){
		VarWrapper var = new VarWrapper(variable, number);
		VarWrapper ret = varWrapperSet.get(var);
		
		if (ret == null){
			ret = var;
			varWrapperSet.put(ret, ret);
		}
		
		return ret;
	}
	
	/**
	 * Rename the variables in the block and collect use/definition statistic.
	 * 
	 * @param block			the block in which the variables is to be renamed
	 */
	private static void rename(Block block){
		
		/* rename variables of the normal quadruples in the block*/
		for (MyLinkedList<Quadruple>.Node node= block.getList().iterator(); node.hasNext(); ){
			node = node.next();
			Quadruple quad = node.value();
			
			/* only definition of phi function needs to be renamed here */
			if (quad instanceof Phi) {
				Phi phi = (Phi) quad;
				MemLoc orig = phi.getOriginalVariable();
				RenamingStack rstack = varRenamingStack.get(orig);
				
				rstack.stack.add(++rstack.count);
				
				VarWrapper var = getVarWrapper(orig, rstack.stack.getLast());
				var.setDefSite(node);
				phi.setDest(var);
			}
			
			/* rename both the defined and used variables for normal quadruples */
			else {
				
				for (Operand operand : quad.getUse()){
					if (operand == null) break;
					
					MemLoc orig = (MemLoc) operand;
					RenamingStack rstack = varRenamingStack.get(orig);
					
					VarWrapper var = getVarWrapper(orig, rstack.stack.getLast());
					quad.replaceUse(operand, var);
					var.addUse(node);
				}
				
				/* rename used ones before defined ones*/
				{
					Addressable[] defList = quad.getDefine();
					for (Addressable defined : defList){
						MemLoc orig = (MemLoc) defined;
						RenamingStack rstack = varRenamingStack.get(orig);
						
						rstack.stack.add(++rstack.count);
						
						VarWrapper var = getVarWrapper(orig, rstack.stack.getLast());
						var.setDefSite(node);
						quad.replaceDefine(var);	
					}
				}			
			}
		}
		
		/* rename successors' phi function sources */
		for (int succ_num : graph.successor(block.getIndex())){			
			Block succ = graph.getIthNode(succ_num);
			MyLinkedList<Quadruple>.Node node = succ.getList().iterator();
			
			/* skip the first label */
			if (node.hasNext() && (node.next().value() instanceof Label)){
				node = node.next();
			}
			
			/* rename variables in phi functions */
			while (node.hasNext()){
				node = node.next();
				if (! (node.value() instanceof Phi)) break;	// phi functions only appear at the beginning of each block
				
				Phi phi = (Phi) node.value();
				MemLoc orig = phi.getOriginalVariable();
				RenamingStack rstack = varRenamingStack.get(orig);
				
				VarWrapper var = getVarWrapper(orig, rstack.stack.getLast());
				phi.setSource(var, block.getIndex());
				var.addUse(node);
			}
		}
		
		/* rename each child of the block */
		IntVector children = SSAConstructor.children[block.getIndex()];
		for (int i = 0, szi = children.size(); i < szi; ++i){
			
			rename(graph.getIthNode(children.get(i)));
		}
		
		/* pop the stack for each definition in the stack */
		for (MyLinkedList<Quadruple>.Node node= block.getList().iterator(); node.hasNext(); ){
			node = node.next();
			Quadruple quad = node.value();
			
			/* pop defined variables*/
			{
				Addressable[] defList = quad.getDefine();
				for (Addressable def : defList){
					VarWrapper defined = (VarWrapper) def;
					RenamingStack rstack = varRenamingStack.get((MemLoc) defined.getOrigin());

					rstack.stack.removeLast();	
				}
			}
		}
	}
	
	/** 
	 * Clears the data structures.
	 * 
	 */
	public static void clear(){
		graph = null;
		entry = null;
		exit = null;
		idom = null;
		children = null;
		DF = null;
		varWrapperSet = null;
	}
	
	/**
	 *	Accesses the variable wrapper set.
	 *
	 * @return the varWrapperSet
	 */
	public static HashMap<VarWrapper, VarWrapper> getVarWrapperSet() {
		return varWrapperSet;
	}
	
	/**
	 * Accesses the variable renaming stack.
	 * 
	 */
	public static HashMap<MemLoc, RenamingStack> getVarRenamingStack(){
		return varRenamingStack;
	}
	
	/** the size of the graph */
	private static int N;
	
	/** the control graph */
	private static Graph<Block> graph;
	
	/** entry block of the graph */
	private static Block entry;
	
	/** exit block of the graph */
	private static Block exit;
	
	/** the immediate dominators */
	private static int[] idom;
	
	/** the children in the dominator tree */
	private static IntVector[] children;
	
	/** the dominance frontiers */
	private static IntVector[] DF; 
	
	/** the defining sites of each variable, initialized by collectInformation and cleared by placePhi */
	private static HashMap<MemLoc, WorkList<Block>> originalDefSites;
	
	/** the counter and stack for variable renaming, initialized by collectionInformation and cleared by renameVariabes */
	private static HashMap<MemLoc, RenamingStack> varRenamingStack;
	
	/** the variables defined in node n, initialized by collectInformation and cleared by placePhi */
	private static HashSet<MemLoc>[] origin;
	
	/** whether node n has been inserted phi function to; initialized by collectionInformation and cleared by placePhi */
	private static HashSet<MemLoc>[] hasPhi;
	
	/** the variable wrapper set which guarantees the uniqueness of each variable wrapper; initialized by ranameVariables */
	private static HashMap<VarWrapper, VarWrapper> varWrapperSet;

	/**
	 * 
	 * The RenamingStack class is the internal class functioning as a counter and a stack for variable renaming.
	 *
	 * @author Zhao Zhuoyue
	 * @version last update: May 13, 2014
	 */
	public static class RenamingStack{
		public RenamingStack(){
			count = 0;
			stack = new IntVector();
			stack.add(0);
		}
		
		public int count;
		public IntVector stack;
	}
	
//	/**
//	 * Test SSA construction.
//	 * 
//	 * @param args
//	 * @throws IOException
//	 */
//	public static void main(String[] args) throws IOException{
//		Scanner scanner = new Scanner(System.in);
//		
//		int n = scanner.nextInt();
//		
//		Graph<Block> graph = new Graph<Block>();
//		for (int i = 0; i < n; ++i)
//			graph.addNode(new Block());
//		
//		for (int x= scanner.nextInt(); x != -1; x = scanner.nextInt()){
//			int y = scanner.nextInt();
//			graph.addEdge(x, y);
//		}
//		
//		
//		/* edge splitting */
//		graph.splitEdge();
//		
//		init(graph, graph.getFirstNode(), graph.getLastNode());
//		
//		/* calculate immediate dominators and build the dominator tree */
//		idom = Dominators.dominators(graph, SSAConstructor.entry.getNum());
//		buildDominatorTree();
//		
//		/* calculate the dominance frontiers */
//		computeDF(entry.getNum());
//		
//		for (int i = 0; i < N; ++i){
//			System.out.print("DF[");
//			System.out.print(i);
//			System.out.print("]:");
//			for (int j = 0; j < DF[i].size(); ++j){
//				System.out.print(" ");
//				System.out.print(DF[i].get(j));
//			}
//			System.out.print("\n");
//		}
//		
//		clear();
//		
//		scanner.close();
//		
//	}
}
