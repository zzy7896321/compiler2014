package optimization;

import java.util.HashMap;
import java.util.LinkedList;

import util.MyLinkedList;
import main.ErrorHandling;
import graph.Block;
import graph.Graph;
import irpack.*;

/**
 * The ControlFlowGraphBuilder class helps build control flow graph and perform some local optimizations.<br>
 * 1. Removes redundant consecutive labels.<br>
 *
 * @author Zhao Zhuoyue
 * @version last update: May 6, 2014
 */
@SuppressWarnings("unused")
public class ControlFlowGraphBuilder {

	/**
	 * Constructs a new control flow graph.
	 * 
	 */
	public ControlFlowGraphBuilder(){
		this.cfg = new Graph<Block>();
		this.currentBlock = null;
		this.labelPatchList = new HashMap<Label, LinkedList<Quadruple>>();
		this.labelMergeList = new HashMap<Label, Label>();
	}
	
	/**
	 * Append a quadruple to the end of the code list without building the graph and optimizations.
	 * 
	 * @param quad
	 */
	public void appendCodeRaw(Quadruple quad){
		if (currentBlock == null){
			currentBlock = new Block();
			cfg.addNode(currentBlock);
			currentBlock.addLast(quad);
			return;
		}
		
		if (quad instanceof Label){
			currentBlock = new Block();
			cfg.addNode(currentBlock);
			currentBlock.addLast(quad);
		}
		
		else if (quad.getOpcode() == Quadruple.J || quad instanceof Branch){
			currentBlock.addLast(quad);
			currentBlock = null;
		}
		
		else{
			currentBlock.addLast(quad);
		}
	}
	
	/**
	 * Append a quadruple to the end of the code list.
	 * 
	 * @param quad
	 */
	public void appendCode(Quadruple quad){		
		if (quad instanceof Label){
			Label label = (Label)quad;
			
			/* try merging redundant label */
			if (mergeLabel(label)){
				return;
			}
			
			/* starts a new block */
			createNewBlock();
			currentBlock.addLast(label);
			
			/* build edges in the patch list */
			patchLabel(label);
		}
		
		else if (quad instanceof Branch){
			if (currentBlock == null) createNewBlock();
			
			Branch branch = (Branch)quad;
			branch.replaceTarget(resolveMergedTarget(branch.getTarget()));		// find the label that the target is merged to
			currentBlock.addLast(quad);
			
			/* build edge from the current block to the block target resides in or add branch to the label's patch list */
			buildEdge(branch, branch.getTarget());
			
			/* require a new block */
			currentBlock = null;
		}
		
		else if (quad.getOpcode() == Quadruple.J){
			if (currentBlock == null) createNewBlock();
			
			Jump jump = (Jump)quad;
			jump.replaceTarget(resolveMergedTarget((Label)jump.getTarget()));	// find the label that the target is merged to
			currentBlock.addLast(quad);
			
			/* build edge from the current block to the block target resides in or add jump to the label's patch list */
			buildEdge(jump, (Label)jump.getTarget());	// jump target is always label in this implementation
			
			/* require a new block */
			currentBlock = null;
		}
		
		else{
			if (currentBlock == null) createNewBlock();
			
			/* jal and call target is register or function header label, which could not be merged to other labels */
			
			currentBlock.addLast(quad);
		}
	}
	
	/**
	 * Finish the construction of the control flow graph.
	 * 
	 */
	public void finish(){
		currentBlock = null;
	}
	
	/**
	 *	Accesses the control flow graph.
	 *
	 * @return the control flow graph
	 */
	public Graph<Block> getCfg() {
		return cfg;
	}
	
	/**
	 * Creates a new block and add an edge from previous one and the current one if there is control flow from the previous one to the latter.
	 * 
	 */
	private void createNewBlock(){
		currentBlock = new Block();
		cfg.addNode(currentBlock);
		
		if (cfg.size() < 2) return;
		Quadruple last = cfg.getIthNode(cfg.size() - 2).getList().getLast();
		if (last == null || last.getOpcode() != Quadruple.J){
			cfg.addEdge(cfg.size() - 2, cfg.size() - 1);
			
			if (last instanceof Branch) cfg.getIthNode(cfg.size()-2).setFalseBranchSuccessor(cfg.size() - 1);
		}
	}
	
	/**
	 * Builds a label from the current block to the block target resides in. The quadruple is added to the label's
	 * patch list if the label has not appeared yet.
	 * 
	 * @param target
	 */
	private void buildEdge(Quadruple quad, Label target){
		
		/* target has not appeared yet; add currentBlock to patch list */
		if (target.getAssociatedBlock() == null){
			LinkedList<Quadruple> patchList = labelPatchList.get(target);
			if (patchList == null) {
				patchList = new LinkedList<Quadruple>();
				labelPatchList.put(target, patchList);
			}
			
			patchList.add(quad);
		}
		
		/* target has appeared; build an edge */
		else {
			cfg.addEdge(quad.getAssociatedBlock().getIndex(), target.getAssociatedBlock().getIndex());
			quad.getAssociatedBlock().setTrueBranchSuccessor(target.getAssociatedBlock().getIndex());
		}
	}
	
	/**
	 * Builds edges in the patch list of label and clears the patch list.
	 * 
	 * @param label
	 */
	private void patchLabel(Label label){
		LinkedList<Quadruple> patchList = labelPatchList.get(label);
		if (patchList == null) return;
		
		int y = label.getAssociatedBlock().getIndex();
		for (Quadruple quad : patchList){
			int x = quad.getAssociatedBlock().getIndex();
			cfg.addEdge(x, y);
			quad.getAssociatedBlock().setTrueBranchSuccessor(y);
		}
	}
	
	/**
	 * Tries to merge the label with the previous one if they're consecutive in the code.
	 * 
	 * @param label
	 * @return		if the label is merged
	 */
	private boolean mergeLabel(Label label){
		Block previousBlock = currentBlock;
		
		/* Preceding quadruple is not label */
		if (previousBlock == null || 
			previousBlock.getList().size() != 1 || 
			!(previousBlock.getList().getFirst() instanceof Label)) return false;
		
		/* merge the label */
		Label mergedTo = (Label)previousBlock.getList().getFirst();
		labelMergeList.put(label, mergedTo);
		
		
		/* update labels and build edges in the patch list */
		LinkedList<Quadruple> patchList = labelPatchList.get(label);
		if (patchList != null){
			
			for (Quadruple quad: patchList){
				if (quad instanceof Branch){
					Branch branch = (Branch) quad;
					branch.replaceTarget(mergedTo);
				}
				
				else if (quad instanceof Jump){
					Jump jump = (Jump) quad;
					jump.replaceTarget(mergedTo);
				}
				
				else {
					ErrorHandling.ReportError("impossible branch in ControlFlowGraphBuilder.mergeLabel", ErrorHandling.ERROR);
					continue;
				}
			
				cfg.addEdge(quad.getAssociatedBlock().getIndex(), mergedTo.getAssociatedBlock().getIndex());
				quad.getAssociatedBlock().setTrueBranchSuccessor(mergedTo.getAssociatedBlock().getIndex());
			}
			
		}
		
		return true;
	}
	
	/**
	 * Returns merged label if the parameter has been merged to a previous one.
	 * 
	 * @param label
	 * @return
	 */
	private Label resolveMergedTarget(Label label){
		Label mergedTo = labelMergeList.get(label);
		if (mergedTo != null) return mergedTo;
		return label;
	}
	
	/** the control flow graph */
	private Graph<Block> cfg;

	/** the last block that is not finished */
	private Block currentBlock;
	
	/** the label patch list which records references of label*/
	private HashMap<Label, LinkedList<Quadruple>> labelPatchList;
	
	/** the label merge list which records redundant labels to merge */
	private HashMap<Label, Label> labelMergeList;
	
}
