package optimization;

import java.util.HashMap;
import java.util.Map;

import util.MyLinkedList;
import graph.*;
import irpack.*;

/**
 * The ConstantOptimization class implements the sparse conditional constant propagation optimization.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 12, 2014
 */
public class ConstantOptimization {

	/**
	 * Performs constant propagation.
	 * 
	 * @param graph
	 * @param entry
	 * @param varSet
	 */
	public static void optimize(Graph<Block> graph, Block entry, HashMap<VarWrapper, VarWrapper> varSet){
		ConstantOptimization.graph = graph;
		ConstantOptimization.entry = entry;
		ConstantOptimization.varSet = varSet;
		
		/* splits the edges */
		splitEdge(graph);
		
		/* initialize data structures */
		initialize();
		
		/* calculate information */
		calculateInformation();
		
		/* Do constant propagation, copy propagation, constant folding and non-executable code removal. */
		DoPropagation();
		
		/* remove the redundant blocks */
		removeEdgeSplittingBlocks(graph);
	}

	/**
	 * Transform edge <u, v> with multiple successors to u and multiple predecessors to v to satisfy unique
	 * successor or predecessor property by adding an empty node. 
	 * 
	 */
	private static void splitEdge(Graph<Block> graph){
		int size = graph.size();
		originalN = size;
		
		for (int x = 0; x < size; ++x){
			if (graph.successorCount(x) <= 1) continue;
			
			for (int y : graph.successor(x).toArray(new Integer[0])){
				if (graph.predecessorCount(y) <= 1) continue;
				
				Block xblock = graph.getIthNode(x);
				graph.addNode(xblock.newEmptyNode());
				int z = graph.size() - 1;
				
				graph.removeEdge(x, y);
				graph.addEdge(x, z);
				graph.addEdge(z, y);
				
				if (xblock.getTrueBranchSuccessor() == y){
					xblock.setTrueBranchSuccessor(z);
				}
				if (xblock.getFalseBranchSuccessor() == y){
					xblock.setFalseBranchSuccessor(z);
				}
			}
		}
		
	}
	
	/**
	 * Removes the redundant blocks.
	 * 
	 * @param graph
	 */
	private static void removeEdgeSplittingBlocks(Graph<Block> graph){
		for (int z = N - 1; z >= originalN; --z){
			
			for (int x : graph.predecessor(z).toArray(new Integer[0]))
			for (int y : graph.successor(z).toArray(new Integer[0])){
				
				Block xblock = graph.getIthNode(x);
				
				
				graph.removeEdge(x, z);
				graph.removeEdge(z, y);
				graph.addEdge(x, y);
				
				graph.removeNode(z);
				
				if (xblock.getTrueBranchSuccessor() == z)
					xblock.setTrueBranchSuccessor(y);
				if (xblock.getFalseBranchSuccessor() == z)
					xblock.setFalseBranchSuccessor(y);
			}
			
		}
	}
	
	/**
	 * Initialize data structures.<br>
	 * 1. Entry block is marked as executable.
	 * 2. All variables without definitions are marked as TOP.
	 * 
	 * @param varSet
	 */
	private static void initialize(){
		N = graph.size();
		
		We = new WorkList<Block>(N);
		executable = new boolean[N];	// defaults to false
		markBlockAsExecutable(entry.getIndex());
		
		Wv = new WorkList<VarWrapper>(varSet.size());
		value = new byte[varSet.size()];	// defaults to BOTTOM
		constant = new ConstantOperand[varSet.size()];
		
		int now = 0;
		for (VarWrapper var : varSet.keySet()){
			var.setIndex(now);
			
			MyLinkedList<Quadruple>.Node defsite = var.getDefSite();
			
			/* uninitialized variables or formal parameters */
			if (defsite == null){
				value[now] = TOP;
				Wv.insert(var);
			}
			
			++now;
		}
	}
	
	/**
	 * Calculates information about whether the blocks are executable and what the values of variables are.
	 * 
	 */
	private static void calculateInformation(){
		
		/* iterate until no block is newly marked as executable and no variable is raised */
		while (We.size() > 0 || Wv.size() > 0){
			
			/* processes blocks */
			while (We.size() > 0){
				Block block = We.remove();
				int block_index = block.getIndex();
				
				/* Any executable block with only one successor C, set E[C] = true */
				if (graph.successorCount(block_index) == 1){
					int succ_index = graph.successor(block_index).iterator().next();
					markBlockAsExecutable(succ_index);
				}
				
				for (MyLinkedList<Quadruple>.Node node = block.getList().iterator(); node.hasNext(); ){
					node = node.next();
					
					examineQuadruple(node.value());
				}
			}
			
			/* processes variable */
			while (Wv.size() > 0){
				VarWrapper var = Wv.remove();
				for (MyLinkedList<Quadruple>.Node entry : var.getUseList()){
					examineQuadruple(entry.value());
				}
			}
		}
		
		
	/*	for (int i = 0; i < N; ++i){
			System.out.printf("executable[%d] = %b\n", i, executable[i]);
		}
		
		for (VarWrapper var : varSet.keySet()){
			System.out.printf("%s value[%d] = %d, constant[%d] = %s\n", var, var.getIndex(), value[var.getIndex()], var.getIndex(), constant[var.getIndex()]);
		}
		*/
		
	}
	
	/**
	 * Examine the quadruple for constant information.
	 * 
	 * @param quad
	 */
	private static void examineQuadruple(Quadruple quad){
		if (!executable[quad.getAssociatedBlock().getIndex()]) return;
		
		/* ALU operations */
		if (quad instanceof ALUOp){
			ALUOp aluop = (ALUOp) quad;
			
			Operand rd = aluop.getRd();
			Operand rs = aluop.getRs();
			Operand rt = aluop.getRt();
			
			/* write to memory */
			if (!(rd instanceof VarWrapper)) return;
			VarWrapper vard = (VarWrapper) rd;
			
			VarWrapper vars = null;
			byte value_s;
			ConstantOperand constant_s;
			if (!(rs instanceof VarWrapper)){
				
				/* constant */
				if (rs instanceof ConstantOperand){
					value_s = CONSTANT;
					constant_s = (ConstantOperand) rs;
				}
				
				/* case 7, rs is in memory */
				else {
					
					updateVariable(vard, TOP);
					return; 
				}
			}
			else {
				vars = (VarWrapper) rs;
				
				value_s = value[vars.getIndex()];
				constant_s = constant[vars.getIndex()];
			}
			
			VarWrapper vart = null;
			int value_t;
			ConstantOperand constant_t;
			if (!(rt instanceof VarWrapper)){
				
				/* constant */
				if (rt instanceof ConstantOperand){
					value_t = CONSTANT;
					constant_t = (ConstantOperand) rt;
				}
				
				/* case 7, rt is in memory */
				else {
					
					updateVariable(vard, TOP);
					return; 
				}
			}
			else {				
				vart = (VarWrapper) rt;
				
				value_t = value[vart.getIndex()];
				constant_t = constant[vart.getIndex()];
			}
			
			/* case 4, at least one of the rhs is TOP */
			if (value_s == TOP || value_t == TOP){
				updateVariable(vard, TOP);
				return;
			}
			
			/* case 3, both of the rhs are CONSTANT */
			if (value_s == CONSTANT && value_t == CONSTANT){
				
				ConstantOperand result = null;
				
				switch (aluop.getOpcode()){
				case Quadruple.ADD:
					
					result = constant_s.add(constant_t);
					break;
				
				case Quadruple.SUB:
					
					result = constant_s.sub(constant_t);
					break;
					
				case Quadruple.MUL:
					
					result = constant_s.mul(constant_t);					
					break;
					
				case Quadruple.DIV:
					
					result = constant_s.div(constant_t);
					break;
					
				case Quadruple.MOD:
					
					result = constant_s.mod(constant_t);
					break;
				
				case Quadruple.SLL:
					
					result = constant_s.sll(constant_t);
					break;
				
				case Quadruple.SRA:
					
					result = constant_s.sra(constant_t);					
					break;
				
				case Quadruple.SRL:
					
					result = constant_s.srl(constant_t);					
					break;
				
				case Quadruple.AND:
					
					result = constant_s.and(constant_t);					
					break;
					
				case Quadruple.OR:
					
					result = constant_s.or(constant_t);
					break;
				
				case Quadruple.XOR:
					
					result = constant_s.xor(constant_t);					
					break;
				
				case Quadruple.NOR:
					
					result = constant_s.nor(constant_t);
					break;
					
				case Quadruple.SLT:
					
					result = constant_s.slt(constant_t);
					break;
				
				case Quadruple.SLE:

					result = constant_s.sle(constant_t);
					break;
					
				case Quadruple.SGT:

					result = constant_s.sgt(constant_t);
					break;
					
				case Quadruple.SGE:

					result = constant_s.sge(constant_t);
					break;
					
				case Quadruple.SEQ:

					result = constant_s.seq(constant_t);
					break;
					
				case Quadruple.SNE:

					result = constant_s.sne(constant_t);
					break;
					
				case Quadruple.SLTU:
					
					result = constant_s.sltu(constant_t);
					break;
				
				default:	
					/* debug assert */
					assert(false) : "invalid ALU opcode";
				}
				
				if (result == null){
					updateVariable(vard, TOP);
				}
				
				else{
					updateVariable(vard, CONSTANT, result);						
				}
				
			}
			
			return;
		}

		else if (quad instanceof Branch){
			Branch branch = (Branch) quad;
			
			Operand rs = branch.getRs();
			Operand rt = branch.getRt();
			//Label target = branch.getTarget();
			
			VarWrapper vars = null;
			byte value_s;
			ConstantOperand constant_s = null;
			if (!(rs instanceof VarWrapper)){
				
				/* constant */
				if (rs instanceof ConstantOperand){
					value_s = CONSTANT;
					constant_s = (ConstantOperand) rs;
				}
				
				/* case 7, rs is in memory */
				else {
					
					value_s = TOP;
				}
			}
			else {
				vars = (VarWrapper) rs;
				
				value_s = value[vars.getIndex()];
				constant_s = constant[vars.getIndex()];
			}
			
			VarWrapper vart = null;
			int value_t;
			ConstantOperand constant_t = null;
			if (!(rt instanceof VarWrapper)){
				
				/* constant */
				if (rt instanceof ConstantOperand){
					value_t = CONSTANT;
					constant_t = (ConstantOperand) rt;
				}
				
				/* case 7, rt is in memory */
				else {
					
					value_t = TOP;
				}
			}
			else {				
				vart = (VarWrapper) rt;
				
				value_t = value[vart.getIndex()];
				constant_t = constant[vart.getIndex()];
			}
			
			/* case 10, at least one of the rhs is TOP */
			if (value_s == TOP || value_t == TOP){
				
				for (int succ_index : graph.successor(branch.getAssociatedBlock().getIndex())){
					markBlockAsExecutable(succ_index);
				}
				
				return;
			}
			
			/* case 11 */
			if (value_s == CONSTANT && value_t == CONSTANT){
				
				ConstantOperand result = constant_s.seq(constant_t);
			
				if (result != null){
					int value = ((IntConstant) result).getValue();
					
					if ( (branch.getOpcode() == Quadruple.BEQ && value == 1) ||
						 (branch.getOpcode() == Quadruple.BNE && value == 0)){
						
						markBlockAsExecutable(branch.getAssociatedBlock().getTrueBranchSuccessor());
					}
					
					else {
						
						markBlockAsExecutable(branch.getAssociatedBlock().getFalseBranchSuccessor());
					}
				}
			}
			
			return;	
		}
		
		else if (quad instanceof Jump){
			Jump jump = (Jump) quad;
			
			if (jump.getOpcode() == Quadruple.J){
				
				Label target = (Label) jump.getTarget();
				
				markBlockAsExecutable(target.getAssociatedBlock().getIndex());
			}
		}
		
		else if (quad instanceof FunctionInfo){
			// nothing to do
		}
		
		else if (quad instanceof LoadStore){
			LoadStore loadstore = (LoadStore) quad;
			
			Operand rd = loadstore.getRd();
			Operand rs = loadstore.getRs();
			
			if (!(rd instanceof VarWrapper)) return;
			
			VarWrapper vard = (VarWrapper) rd;
			
			switch (loadstore.getOpcode()){
			case Quadruple.LA:
				
			{
				if (rs instanceof ConstantOperand){
					
					updateVariable(vard, CONSTANT, (ConstantOperand) rs);
					
				}
				
				else if (rs instanceof Indirection){
					
					Operand source = ((Indirection) rs).getSource();
					
					if (source instanceof ConstantOperand){
						updateVariable(vard, CONSTANT, (ConstantOperand) rs);
					}
					
				}
				
				else if (rs instanceof MemLoc){
					
					updateVariable(vard, CONSTANT, new Address(rs, 0));					
				}
				
				else if (rs instanceof VarWrapper){
					/* debug assert */
					assert(false) : "SSA Variables are not taken address of." ;
				}
				
			}
				
				break;
				
			case Quadruple.LOAD:
			case Quadruple.STORE:
				/* debug assert */
				assert(false):" no explicit load/store in my ir";
				
				break;
				
			case Quadruple.MOV:
			{
				VarWrapper vars = null;
				byte value_s;
				ConstantOperand constant_s;
				if (!(rs instanceof VarWrapper)){
					
					/* constant */
					if (rs instanceof ConstantOperand){
						value_s = CONSTANT;
						constant_s = (ConstantOperand) rs;
					}
					
					/* case 7, rs is in memory */
					else {
						
						updateVariable(vard, TOP);
						return; 
					}
				}
				else {
					vars = (VarWrapper) rs;
					
					value_s = value[vars.getIndex()];
					constant_s = constant[vars.getIndex()];
				}
				
				updateVariable(vard, value_s, constant_s);
			}	
				break;
			
			}
			
			return;
		}
		
		else if (quad instanceof MoveRV){
			MoveRV moverv = (MoveRV) quad;
			
			Operand dest = moverv.getDest();
			
			if (!(dest instanceof VarWrapper)) return;
			
			VarWrapper vard = (VarWrapper) dest;
			
			updateVariable(vard, TOP);
		
			return;
		}
		
		else if (quad instanceof Phi){
			Phi phi = (Phi) quad;
						
			VarWrapper vard = (VarWrapper) phi.getDest();
			
			byte val = -1;
			ConstantOperand c = null;
			
			for (Operand src : phi.getSourceList().values()){
				VarWrapper var = (VarWrapper) src;
				MyLinkedList<Quadruple>.Node defSite = var.getDefSite();
				
				if (defSite != null && !executable[defSite.value().getAssociatedBlock().getIndex()]) continue;
				
				if (value[var.getIndex()] == TOP){
					
					updateVariable(vard, TOP);
					return ;
				}
				
				else if (value[var.getIndex()] == CONSTANT){
					
					if (val == CONSTANT){
						
						if (! c.theSameValueWith(constant[var.getIndex()])){
							updateVariable(vard, TOP);
							
							return;
						}
						
					}
					
					else {
						
						val = CONSTANT;
						c = constant[var.getIndex()];
					}
					
				}
				
			}
			
			if (val == CONSTANT){
				updateVariable(vard, CONSTANT, c);
			}
			
			return;
		}
		
		else if (quad instanceof Push){
			
			// nothing to do
		}
		
		else if (quad instanceof StoreRV){
			
			// nothing to do
		}
		
	}
	
	/**
	 * Updates the value and constant for the variable.
	 * 
	 * @param var
	 * @param value
	 * @param constant
	 */
	private static void updateVariable(VarWrapper var, byte value, ConstantOperand constant){
		int index = var.getIndex();
		
		if (value > ConstantOptimization.value[index]){
			ConstantOptimization.value[index] = value;
			ConstantOptimization.constant[index] = constant;
			
			Wv.insert(var);
		}
	}
	
	/**
	 * Updates the value and constant for the variable.
	 * 
	 * @param var
	 * @param value
	 * @param constant
	 */
	private static void updateVariable(VarWrapper var, byte value){
		int index = var.getIndex();
		
		if (value > ConstantOptimization.value[index]){
			ConstantOptimization.value[index] = value;
			
			Wv.insert(var);
		}
	}
	
	/**
	 * Mark the block as executable.
	 * 
	 * @param block
	 */
	private static void markBlockAsExecutable(int index){
		if (!executable[index]){
			executable[index] = true;
			We.insert(graph.getIthNode(index));
		}
	}
	
	/**
	 * Does the propagation.
	 * 
	 */
	private static void DoPropagation(){
		
		/* remove non-executable block */
		for (int i = 0; i < N; ++i)
		if (!executable[i])
		{
			Block block = graph.getIthNode(i);
			for (MyLinkedList<Quadruple>.Node node = block.getList().iterator(); node.hasNext(); ){
				node = node.next();
				Quadruple quad = node.value();
				
				{
					
					Addressable[] defList = quad.getDefine();
					
					for (Addressable def : defList){
						VarWrapper var = (VarWrapper) def;
						varSet.remove(var);
					}
				}
				
				for (Addressable operand : quad.getUse()){
					if (operand == null) break;
					
					VarWrapper var = (VarWrapper) operand;
					var.removeUse(quad);
				}
			}
			
			block.clear();
			graph.disconnectNode(block.getIndex());
		
		}
		
		/* constant propagation/folding and copy propagation */
		
		for (VarWrapper var : varSet.keySet()){
			Wv.insert(var);
		}
		
		for (VarWrapper var = Wv.remove(); var != null; var = Wv.remove()){
			MyLinkedList<Quadruple>.Node defSite = var.getDefSite();
			if (defSite == null) continue;
			Quadruple quad = defSite.value();
			
			
			Operand toSubstitute = null;
			if (value[var.getIndex()] == CONSTANT){
				toSubstitute = constant[var.getIndex()];
			}
			
			else{
				/* those can be copy propagated */
				
				if (quad instanceof LoadStore){
					LoadStore loadstore = (LoadStore) quad;
					
					if (loadstore.getOpcode() == Quadruple.MOV){
						Operand rs = loadstore.getRs();
						if (rs instanceof VarWrapper){
							toSubstitute = rs;
						}
					}
				}
				
				else if (quad instanceof ALUOp){
					ALUOp aluop = (ALUOp) quad;
					
					Operand rs = aluop.getRs();
					Operand rt = aluop.getRt();
					
					switch (aluop.getOpcode()){
					case Quadruple.ADD:
						
						if (rs instanceof IntConstant){
							if (((IntConstant) rs).getValue() == 0){
								if (rt instanceof VarWrapper){
									toSubstitute = rt;
								}
							}
						}
						
						else if (rt instanceof IntConstant){
							if (((IntConstant) rt).getValue() == 0){
								
								if (rs instanceof VarWrapper){
									toSubstitute = rs;
								}
							}
						}
						
						break;
						
					case Quadruple.SUB:
						
						if (rt instanceof IntConstant){
							if (((IntConstant) rt).getValue() == 0){
								
								if (rs instanceof VarWrapper){
									toSubstitute = rs;
								}
							}
						}
					
						break;
					// and other cases
					}
					
				}
				
				else if (quad instanceof Phi){
					
					Phi phi = (Phi) quad;
					
					HashMap<Integer, Operand> source = phi.getSourceList();
					Integer[] from = source.keySet().toArray(new Integer[0]);
					
					boolean filled = false;
					Operand common_op = null;
					for (int i = 0; i < from.length; ++i){
						/* remove non-executable */
						if (!executable[from[i]]){
							
							Operand removed = source.remove(from[i]);
							
							if (removed instanceof VarWrapper){
								((VarWrapper) removed).removeUse(phi);
							}
							
							continue;
						}
						
												
						if (!filled){
							filled = true;
							common_op = source.get(from[i]);
						}
						
						else {
							Operand op = source.get(from[i]);
							if (common_op != null && !common_op.equals(op)) 
								common_op = null;
						}
					}
					
					/* empty phi */
					if (source.isEmpty()){
						defSite.remove();
						/* debug assert */
						assert(var.getUseList().isEmpty()) : "non-empty use list with empty phi function";
						
						continue;
					}
				
					if (filled && common_op != null){
						toSubstitute = common_op;
					}
				
				
				}
				
			}
			
			if (toSubstitute != null){
				
				for (MyLinkedList<Quadruple>.Node useNode : var.getUseList()){
					Quadruple use = useNode.value();
					
					if (use instanceof Branch){
						Branch branch = (Branch) use;
						
						Label target = branch.getTarget();
						
						if (!executable[branch.getAssociatedBlock().getTrueBranchSuccessor()]){
							
							MyLinkedList<Quadruple>.Node node = var.getUse(branch);
							
							node.remove();
						}
						
						else if (!executable[branch.getAssociatedBlock().getFalseBranchSuccessor()]){
							MyLinkedList<Quadruple>.Node node = var.getUse(branch);
							
							Quadruple toReplace = new Jump(Quadruple.J, target);
							toReplace.setAssociatedBlock(branch.getAssociatedBlock());
							
							node.setValue(toReplace);
						}
						
						/* both executable */
						else {
							branch.replaceUse(var, toSubstitute);
							
							if (toSubstitute instanceof VarWrapper){
								((VarWrapper) toSubstitute).addUse(useNode);
							}
						}
					}
					
					else {
						if (use.replaceUse(var, toSubstitute)){
							
							Addressable[] defList = use.getDefine();
							
							for (Addressable defined : defList){
								/* re-consider the definition */
								if (defined instanceof VarWrapper){
									Wv.insert((VarWrapper) defined);
								}
							}
							
							if (toSubstitute instanceof VarWrapper){
								((VarWrapper) toSubstitute).addUse(useNode);
							}
						}
					}
				}
				
				/* cannot remove here */
				//defSite.remove();
				var.getUseList().clear();
			}
			
		}
		
	}

	/** the size of the control flow graph */
	private static int N;
	
	/** the original size of the control flow graph */
	private static int originalN;
	
	/** the control flow graph */
	private static Graph<Block> graph;
	
	/** the entry block */
	private static Block entry;

	/** whether there is evidence that the block can be executed */
	private static boolean executable[];
	
	/** the run-time value of each variable */
	private static byte value[];
	
	/** the value of those variables marked as constant*/
	private static ConstantOperand constant[];

	/** the work list of blocks recently marked as executable */
	private static WorkList<Block> We;
	
	/** the work list of variables recently raised */
	private static WorkList<VarWrapper> Wv;
	
	/** the variable set */
	private static HashMap<VarWrapper, VarWrapper> varSet;
	
	/* lattice values */
	
	/** no evidence that any assignment to the variable is ever executed */
	@SuppressWarnings("unused")
	private final static byte BOTTOM = 0;
	
	/** has evidence that the variable is only possibly assigned with constant */
	private final static byte CONSTANT = 1;
	
	/** has evidence that the variable will have, at various times, at least two different value or 
	 * some value that is not known at compile time */
	private final static byte TOP = 2;
}


