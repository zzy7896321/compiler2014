package optimization;

import graph.WorkList;
import irpack.*;

import java.util.HashMap;
import java.util.LinkedList;

import util.MyLinkedList;

/**
 * The SimpleDeadCodeElimination class implements a simple dead code elimination that eliminates defines without use.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 11, 2014
 */
public class SimpleDeadCodeElimination {

	public static void eliminateDeadCode(HashMap<VarWrapper, VarWrapper> varSet){
		WorkList<VarWrapper> Wv = new WorkList<VarWrapper>(varSet.size());
		
		for (VarWrapper var : varSet.keySet()){
			var.setIndex(Wv.size());
			Wv.insert(var);
		}
		
		for (VarWrapper var = Wv.remove(); var != null; var = Wv.remove()){
			LinkedList<MyLinkedList<Quadruple>.Node> useList = var.getUseList();
			
			if (useList.isEmpty()){
				MyLinkedList<Quadruple>.Node defSite = var.getDefSite();
				varSet.remove(var);
				
				if (defSite == null) continue;
				defSite.remove();
				Quadruple defQuad = defSite.value();
				
				for (Addressable use : defQuad.getUse()){
					if (use == null) break;
					
					if (use instanceof VarWrapper){
						((VarWrapper) use).removeUse(defQuad);
						
						if (varSet.containsKey(use))
							Wv.insert((VarWrapper) use);
					}
				}
			}
		}
	}

}
