package optimization;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Random;

import main.ErrorHandling;
import util.MyLinkedList;
import util.MyLinkedList.Node;
import irpack.*;
import graph.*;

/**
 * The SSABasedLinearScan class implements an SSA based linear scan register allocation algorithm.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 14, 2014
 */
public class SSABasedLinearScan implements mips.Registers{

	/**
	 * Allocates registers for the function.
	 * 
	 * @param funcEntry
	 */
	public static void allocateRegister(FunctionEntry funcEntry, HashMap<MemLoc, SSAConstructor.RenamingStack> varRenamingStack){
		SSABasedLinearScan.funcEntry = funcEntry;
		SSABasedLinearScan.varRenamingStack = varRenamingStack;
		
		/* initializes the data structures */
		initialize();
	
		/* expands the intermediate representation into assembly code */
		expand();
		
		/* numbers the instructions */
		numberInstructions();
		
		/* calculates the live intervals */
		calculateLiveIntervals();
		
		/* coalesces phi-functions and their respective operands */
		coalesce();
		
		/* allocates registers for intervals */
		linearScan();
		
		/* allocates memory space for non-SSA variables */
		allocateMemoryForNonSSAVariables();
	
		/* rearranges register mapping to eliminate argument moves at entry of the function.
		 * and adds argument and return value registers to the used register list.
		 * Maps the virtual registers to the physical registers */
		mapVirtualRegToPhysicalReg();
	}
	

	/**
	 * Initializes the data structure.
	 * 
	 */
	private static void initialize(){
		graph = funcEntry.getControlFlowGraph();
		N = graph.size();		
		entry = funcEntry.getEntryBlock();
		exit = funcEntry.getExitBlock();
		
		blockOrder = new int[N];
		first = new int[N];
		phi = new int[N];
		last = new int[N];
		instructionCount = 0;
		blockCount = 0;
		instructions = new ArrayList<MyLinkedList<Quadruple>.Node>();
		
		varSet = new HashSet<VarWrapper>();
		varCount = 0;
		
		callList = new LinkedList<Integer>();
		random = new Random(System.currentTimeMillis());
	}
	
	/**
	 * Expands the intermediate representation into the assembly code. Following transformations are performed:<br>
	 * 1. Moves are generated for phi-operands.<br>
	 * 2. Jump(CALL) is transformed to the Call quadruple.<br>
	 * 3. CALLEND Quadruple is inserted after Call without MoveRV followed.
	 * 4. Push indirection of variable is expanded to an indirection and a register move.
	 * 
	 * The following transformation were planned but not supported:
	 * 1. Push is transformed to moving-to-a?/moving-to-v0/store operation.<br>
	 * 2. StoreRV is transformed to moving-to-v0 operation.<br>
	 * 3. MoveRV is transformed to moving-from-v0 operation.<br>
	 */
	private static void expand(){
		boolean in_call_sequence = false;
		MyLinkedList<Quadruple>.Node first_push_site = null;
		
		for (int block_num = 0; block_num < N; ++block_num){
			Block block = graph.getIthNode(block_num);
			
			/* iterates through the block */
			for (MyLinkedList<Quadruple>.Node node = block.getList().iterator(); node.hasNext(); ){
				node = node.next();
				Quadruple quad = node.value();
				
				if (quad instanceof Phi){
					Phi phi = (Phi) quad;
					VarWrapper dest = phi.getDest();
					MemLoc orig = dest.getOrigin();
					SSAConstructor.RenamingStack renamingStack = varRenamingStack.get(orig);
					
					HashMap<Integer, Operand> sourceList = phi.getSourceList();
					
					for (int pred_block_num : sourceList.keySet().toArray(new Integer[0])){
						Operand src = sourceList.get(pred_block_num);
						Block pred_block = graph.getIthNode(pred_block_num);
						
						if (src instanceof VarWrapper){
							VarWrapper src_var = (VarWrapper) src;
							if (src_var.getNumber() == 0 && !(src_var.getOrigin() instanceof Argument))
								continue;		// no need to generate move for uninitialized value
						}
						
						VarWrapper var = new VarWrapper(orig, ++renamingStack.count);
						
						/* insert move at the end of the predecessor block */
						LoadStore move = new LoadStore(Quadruple.MOV, var, src);
						move.setAssociatedBlock(pred_block);
						MyLinkedList<Quadruple>.Node move_node = pred_block.addPhiMove(move);
						var.setDefSite(move_node);
						
						/* replace the source from the predecessor with the new variable */
						if (src instanceof VarWrapper){
							((VarWrapper) src).removeUse(phi);
							((VarWrapper) src).addUse(move_node);
						}
						sourceList.put(pred_block_num, var);
						var.addUse(node);
						
						/* coalesce the destination and the new variable later */
					}
				}
				
				else if (quad instanceof Jump){
					Jump jump = (Jump) quad;
					
					if (jump.getOpcode() == Quadruple.CALL){
						VarWrapper var = new VarWrapper(new Temp(), 0);	
						
						Call call = new Call(var, jump.getTarget());
						call.setAssociatedBlock(block);
						node.setValue(call);
						
						/* v0 does not preserve through function call */
						funcEntry.addToUsedRegisterSet(v0);
						
						
						for (Addressable use : jump.getUse()){
							if (use == null) break;
							
							((VarWrapper) use).removeUse(jump);
							((VarWrapper) use).addUse(node);
						}
						
						if (!(node.next().value() instanceof MoveRV)){
							Quadruple callend = new Quadruple(Quadruple.CALLEND);
							callend.setAssociatedBlock(block);
							node.insert(callend);
						}
						
						in_call_sequence = false;
						first_push_site = null;
					}
				}
				
				else if (quad instanceof Push){
					
					if (!in_call_sequence){
						in_call_sequence = true;
						first_push_site = node;
					}
					
					Operand src = ((Push) quad).getSrc();
					
					if (src instanceof Indirection){
						Operand ind_src = ((Indirection) src).getSource();
						
						if (ind_src instanceof VarWrapper){
							
							VarWrapper tmp = new VarWrapper(new Temp(), 1);
							((VarWrapper) ind_src).removeUse(quad);
							
							LoadStore ls = new LoadStore(Quadruple.MOV, tmp, src);
							ls.setAssociatedBlock(first_push_site.value().getAssociatedBlock());
							first_push_site.insertBefore(ls);
							((VarWrapper) ind_src).addUse(first_push_site.previous());
							
							((Push) quad).setSrc(tmp);
							tmp.setDefSite(first_push_site.previous());
							tmp.addUse(node);
						}
					}
					
				}
			} 
			
		}
		
	}
	
	/**
	 * Numbers the instructions in topological sorted order and numbers the variables.
	 * 
	 */
	private static void numberInstructions(){		
		in_degree = new int[N];
		Arrays.fill(in_degree, -1);		// -1 denotes the block has not been visited
		
		/* starts from the entry block */
		in_degree[entry.getIndex()] = 0;
		numberInstruction(entry.getIndex());		
		
		/* initialize variables */
		variables = new VarWrapper[varCount];
		for (VarWrapper var : varSet){
			variables[var.getIndex()] = var;
		}
		
		varSet = null;
		
		/* initialize coalesced */
		coalescedRep = new int[varCount];
		for (int i = 0; i < varCount; ++i) coalescedRep[i] = i;
		
		coalescedRank = new int[varCount];
	}
	
	private static void numberInstruction(int block_num){
		blockOrder[blockCount++] = block_num;
		Block block = graph.getIthNode(block_num);		
		
		/* numbers the instructions in the block */
		first[block_num] = -1;
		phi[block_num] = -1;
		for (MyLinkedList<Quadruple>.Node node = block.getList().iterator(); node.hasNext(); ){
			node = node.next();
			Quadruple quad = node.value();
			
			/* labels does not need to be numbered */
			if (quad instanceof Label) continue;
			
			/* index variables */
			for (Addressable use : quad.getUse()){
				if (use == null) break;
				
				VarWrapper var_use = (VarWrapper) use;
									
				if (varSet.add(var_use)){
					var_use.setIndex(varCount++);
					
					/* if the registers allocated for the first four arguments and RVA are in wrong register, moves should be emitted for them  */
					MemLoc orig = var_use.getOrigin();
					if (var_use.getNumber() == 0 && orig instanceof Argument){
						
						Argument arg = (Argument) orig;
						/* the return value address */
						if (arg.getIndex() == -1){
							
							/* prefers $v0 */
							var_use.setReg(-2 - v0);
							funcEntry.addToUsedRegisterSet(v0);
							funcEntry.setWrappedReturnValueAddress(var_use);
						}
						
						else if (arg.getIndex() < 4){
							
							/* prefers $ai */
							var_use.setReg(-2 - a[arg.getIndex()]);
							funcEntry.addToUsedRegisterSet(a[arg.getIndex()]);
							funcEntry.setWrapperArgument(var_use);
						}
						
						else {
							
							funcEntry.setWrapperArgument(var_use);
						}  
						
					}
					
				}
			}
			
			{
				Addressable[] definedList = quad.getDefine();
				for (Addressable def : definedList){
					VarWrapper defined = (VarWrapper) def;
					if (varSet.add(defined)){
						defined.setIndex(varCount++); 
					}
				}
			}
			
			instructions.add(node);
			quad.setIndex(instructionCount++);
			
			if (quad instanceof Phi) {
				if (phi[block_num] == -1) phi[block_num] = instructionCount - 1;
			}
			else if (first[block_num] == -1) first[block_num] = instructionCount - 1;
			
			if (quad instanceof Call){
				callList.addLast(instructionCount - 1);
				
				Call call = (Call) quad;
				Operand target = call.getTarget();
				
				if (target instanceof Label){
					funcEntry.addToCalledSet((Label) target);
				}
				
				else {
					funcEntry.disableCalledSet();
				}
			}
			
			last[block_num] = instructionCount - 1;
		}
		
		if (first[block_num] == -1) first[block_num] = last[block_num] + 1;
		if (phi[block_num] == -1) phi[block_num] = first[block_num];
		
		/* visit the successors */
		int candidate = N;
		for (int succ_num : graph.successor(block_num)){
			if (in_degree[succ_num] == 0) continue;		// already numbered
			if (in_degree[succ_num] == -1) in_degree[succ_num] = graph.predecessorCount(succ_num);
			
			--in_degree[succ_num];
			
			
			/* all predecessor of the block has been traversed */
			if (in_degree[succ_num] == 0){
				numberInstruction(succ_num);
				candidate = -1;
			}
			
			/* break tie by the smaller index of the block*/
			else {
				
				if (succ_num < candidate){
					candidate = succ_num;
				}
			}
		}
		
		if (candidate != -1 && candidate != N){
			in_degree[candidate] = 0;
			numberInstruction(candidate);
		}
	}
	
	/**
	 * Calculates the live intervals.
	 * 
	 */
	@SuppressWarnings("unchecked")
	private static void calculateLiveIntervals(){
		/* initialize live interval data structures */
		liveBegin = (LinkedList<Integer>[]) new LinkedList<?>[varCount];
		liveEnd = (LinkedList<Integer>[]) new LinkedList<?>[varCount];
		
		for (int i = 0; i < varCount; ++i){
			liveBegin[i] = new LinkedList<Integer>();
			liveEnd[i] = new LinkedList<Integer>();
		}
		
		BitSet[] liveIn = new BitSet[N];
		int[] loopEnd = new int[N];
		Arrays.fill(loopEnd, -1);		// -1 denotes block i is not a loop header
		
		/* traverse the graph in reverse topological sorted order */
		for (int i = blockCount - 1; i >=0; --i){
			int block_num = blockOrder[i];
			BitSet live = new BitSet(varCount);
			
			/* union successor.liveIn for each successor of the block and 
			 * add successors' phi function inputs to the live set*/
			for (int succ_num : graph.successor(block_num)){
				
				if (loopEnd[block_num] == -1){
					loopEnd[block_num] = block_num;
				}
				
				/* loop edge */
				if (liveIn[succ_num] == null){
					if (loopEnd[succ_num] == -1 || last[loopEnd[block_num]] > last[loopEnd[succ_num]])
						loopEnd[succ_num] = block_num;
				}
				else {
					live.or(liveIn[succ_num]);
				}
				
				Block succ = graph.getIthNode(succ_num);
				
				for (MyLinkedList<Quadruple>.Node node = succ.getList().iterator(); node.hasNext(); ){
					node = node.next();
					Quadruple quad = node.value();
					
					if (quad instanceof Label) continue;
					
					if (!(quad instanceof Phi)) break;
					
					Phi phi = (Phi) quad;
					VarWrapper output = phi.getDest();
					live.clear(output.getIndex());
					
					VarWrapper input = (VarWrapper) phi.getSourceList().get(block_num);
					live.set(input.getIndex());
				}
			}
			
			/* propagated live range */
			for (int opd = live.nextSetBit(0); opd != -1; opd = live.nextSetBit(opd+1)){
				addRange(opd, first[block_num], last[block_num] + 1);
			}
			
			/* calculate liveliness for each operation in the block in reverse order */
			for (int op = last[block_num]; op >= first[block_num]; --op){
				Quadruple instr = instructions.get(op).value();
				
				for (Addressable use : instr.getUse()){
					if (use == null) break;
					
					VarWrapper var_use = (VarWrapper) use;
					
					addRange(var_use.getIndex(), first[block_num], instr.getIndex());
					live.set(var_use.getIndex());
				}
				
				Addressable[] defList = instr.getDefine();
				for (Addressable def : defList){
					VarWrapper defined = (VarWrapper) def;
					
					setBegin(((VarWrapper) defined).getIndex(), instr.getIndex());
					live.clear(((VarWrapper) defined).getIndex());
				}
			}
			
			/* remove the output of phi functions from the live set */
			for (int op = phi[block_num]; op < first[block_num]; ++op){
				Phi phi = (Phi) instructions.get(op).value();
				
				for (Addressable use : phi.getUse()){
					VarWrapper var_use = (VarWrapper) use;
					
					live.clear(var_use.getIndex());
				}
			}
			
			/* deal with loop header */
			if (loopEnd[block_num] != -1){
				int loop_end = loopEnd[block_num];
				
				for (int opd = live.nextSetBit(0); opd >= 0; opd = live.nextSetBit(opd+1)){
					addRange(opd, first[block_num], last[loop_end] + 1);
				}
			}
			
			liveIn[block_num] = live;
		}
	}
	
	/**
	 * Coalesce phi functions and their inputs.
	 * 
	 */
	private static void coalesce(){
		
		for (int i = 0; i < blockCount; ++i){
			int block_num = blockOrder[i];
			
			for (int op = phi[block_num]; op < first[block_num]; ++op){
				Phi phi = (Phi) instructions.get(op).value();
				
				for (Operand opd : phi.getSourceList().values()){
					VarWrapper var = (VarWrapper) opd;
					
					joinVariables(var.getIndex(), phi.getDest().getIndex());
				}
			}
		}
		
	}
	
	/**
	 * Allocates registers for the intervals.
	 * 
	 */
	private static void linearScan(){
		/* initializes sets */
		unhandled = new ArrayList<Integer>();
		active = new LinkedList<Integer>();
		inactive = new LinkedList<Integer>();
		handled = new BitSet(varCount);
		busy = new BitSet(regCount);
		weight = new int[regCount];
		precolored = new LinkedList<Integer>();
		
		for (int i = 0; i < varCount; ++i){
			int rep = fatherOfVariable(i);
			if (rep == i && !liveBegin[rep].isEmpty()){
				unhandled.add(rep);
			}
		}
		
		Collections.sort(unhandled, new Comparator<Integer>(){

			@Override
			public int compare(Integer arg0, Integer arg1) {
				
				return liveBegin[arg0.intValue()].getFirst() - liveBegin[arg1.intValue()].getFirst();
			}
			
		});
		
		for (int i : unhandled){
			if (variables[i].getReg() >= 0){
				precolored.addLast(i);
			}
		}
		
		/* does linear scan */ 
		for (int cur_var_index : unhandled){
			if (variables[cur_var_index].getReg() >= 0) precolored.removeFirst();
			
			VarWrapper cur_var = variables[cur_var_index];
			int cur_begin = liveBegin[cur_var_index].getFirst().intValue();			
			
			/* check for active intervals that expired */
			for (ListIterator<Integer> active_iter = active.listIterator(); active_iter.hasNext(); ){
				int i = active_iter.next();

				/* the interval has expired */
				if (liveEnd[i].getLast().intValue() <= cur_begin){
					active_iter.remove();
					handled.set(i);
					busy.clear(variables[i].getReg());
				}
				
				/* the interval does not overlap the current begin */
				else if (!overlaps(i, cur_begin)){
					active_iter.remove();
					inactive.add(i);
					busy.clear(variables[i].getReg());
				}
			}
			
			/* check for inactive intervals that become expired or reactivated */
			for (ListIterator<Integer> inactive_iter = inactive.listIterator(); inactive_iter.hasNext(); ){
				int i = inactive_iter.next();
							
				/* the interval has expired */
				if (liveEnd[i].getLast().intValue() <= cur_begin){
					inactive_iter.remove();
					handled.set(i);
				}
				
				/* the interval overlaps the current begin*/
				else if (overlaps(i, cur_begin)){
					inactive_iter.remove();
					active.add(i);
					busy.set(variables[i].getReg());
				}
			}
		
			/* collect available registers */
			BitSet b = (BitSet) busy.clone();
			for (int i : inactive){
				if (!intersectionIsEmpty(i, cur_var_index)){
					b.set(variables[i].getReg());
				}
			}
			
			for (int i : precolored){
				if (!intersectionIsEmpty(i, cur_var_index)){
					b.set(variables[i].getReg());
				}
			}
			
			/* try allocating the same register if the variable is defined by a move */
			if (cur_var.getReg() == -1) {
				MyLinkedList<Quadruple>.Node defSite = cur_var.getDefSite();
				if (defSite != null){
					Quadruple defQuad = defSite.value();
					
					if (defQuad.getOpcode() == Quadruple.MOV){
						Operand src = ((LoadStore) defQuad).getRs();
						
						if (src instanceof VarWrapper && !((VarWrapper) src).isAllocated() && ((VarWrapper) src).getReg() >= 0){
							cur_var.setReg(-2 - ((VarWrapper) src).getReg());
						}
					}
				}
			}
			
			/* try allocating the same register if the variable is only used by a move */
			if (cur_var.getReg() == -1){
				LinkedList<MyLinkedList<Quadruple>.Node> use_list = cur_var.getUseList();
				
				/* not likely, should be eliminated */
				if (use_list.size() == 0){
					ErrorHandling.ReportError("not used variable?", ErrorHandling.WARNING);
				}
				
				else if (use_list.size() == 1){
					Quadruple use = use_list.getFirst().value();
					
					if (use.getOpcode() == Quadruple.MOV){
						Operand dest = ((LoadStore) use).getRd();
						
						if (dest instanceof VarWrapper && !((VarWrapper) dest).isAllocated() && ((VarWrapper) dest).getReg() >= 0){
							cur_var.setReg(-2 - ((VarWrapper) dest).getReg());
						}
					}
				}
			}
			
			/* allocate register or spill */
			if (b.nextClearBit(0) >= regCount){
				spill(cur_var_index);
			}
			
			else {
				if (cur_var.getReg() < 0){
					
					if (cur_var.getReg() < -1){
					
						int preferred = -cur_var.getReg() - 2;
						if (!b.get(preferred)){
							cur_var.setReg(preferred);
							funcEntry.addToUsedRegisterSet(preferred);
							busy.set(preferred);
						}
					}
					
					if (cur_var.getReg() < 0){
						
						int reg = randomlyPickARegister(b);
						cur_var.setReg(reg);
						funcEntry.addToUsedRegisterSet(reg);
						busy.set(reg);
					}
				}
				
				active.add(cur_var_index);
			}
		}
		
		/* record active register set at the function call */
		for (int call_index : callList){
			Call call = (Call) instructions.get(call_index).value();
			
			BitSet used = new BitSet(regCount);
			
			for (int var_index : unhandled){
				VarWrapper var = variables[var_index];
				
				if (!var.isAllocated() && !used.get(var.getReg())){
					if (overlaps(var_index, call_index)){
						used.set(var.getReg());
					}
				}
			}
			
			//System.out.printf("%d: %s: %s\n", call_index, call.getTarget(), used);
			
			call.setBusy(used);
		}
		
		
		/*for (int i = 0; i < instructions.size(); ++i){
			System.out.printf("%d: %s\n", i, instructions.get(i).value());
		}*/
		
		/* sets register allocation result properly for coalesced variables */
		for (int i = 0; i < varCount; ++i){
			int rep = fatherOfVariable(i);
			if (i != rep){
				VarWrapper var = variables[i];
				VarWrapper rep_var = variables[rep];
				
				var.setAllocated(rep_var.isAllocated());
				var.setOffset(rep_var.getOffset());
				var.setReg(rep_var.getReg());
			}
			
			/*System.out.printf("%s.reg = %s, rep = %s, allocated = %b, offset = %d\n", 
					variables[i], reg[regMapping[variables[i].getReg()]], variables[rep], variables[rep].isAllocated()
					, variables[rep].getOffset());
			System.out.printf("liveBegin = %s\n", liveBegin[rep]);
			System.out.printf("liveEnd = %s\n", liveEnd[rep]);*/
		}
		
	}
	
	private static int randomlyPickARegister(BitSet b) {
		int n = regCount - b.cardinality();
		
		assert(n > 0) : "no available register";
		
		n = random.nextInt(n);
		
		int ret = 0;
		for (ret = b.nextClearBit(ret); n > 0; --n, ret = b.nextClearBit(ret + 1));
		
		return ret;
	}


	/**
	 * Spill some variable.
	 * 
	 * @param cur
	 */
	private static void spill(int cur){
		
		Arrays.fill(weight, 0);
		
		/* calculates spill cost for each register */
		for (int i : active){
			if (!intersectionIsEmpty(i, cur)){
				VarWrapper var = variables[i];
				weight[var.getReg()] += var.getSpillCost();
			}
		}
		
		for (int i : inactive){
			if (!intersectionIsEmpty(i, cur)){
				VarWrapper var = variables[i];
				weight[var.getReg()] += var.getSpillCost();
			}
		}
		
		for (int i : precolored){
			if (!intersectionIsEmpty(i, cur)){
				VarWrapper var = variables[i];
				weight[var.getReg()] += var.getSpillCost();
			}
		}
		
		/* register with minimum spill cost */
		int reg = 0;
		for (int r = 1; r < regCount; ++r){
			if (weight[r] < weight[reg]) reg = r;
		}
		
		VarWrapper cur_var = variables[cur];
		if (cur_var.getSpillCost() < weight[reg]){
			

			allocateForIthVariable(cur);
			handled.set(cur);
		}
		
		else {
			
			for (ListIterator<Integer> active_iter = active.listIterator(); active_iter.hasNext(); ){
				int i = active_iter.next();
				if (!intersectionIsEmpty(i, cur)){
					allocateForIthVariable(i);
					handled.set(i);
					active_iter.remove();
				}
			}
			
			for (ListIterator<Integer> inactive_iter = inactive.listIterator(); inactive_iter.hasNext(); ){
				int i = inactive_iter.next();
				if (!intersectionIsEmpty(i, cur)){
					allocateForIthVariable(i);
					handled.set(i);
					inactive_iter.remove();
				}
			}
			
			cur_var.setReg(reg);
			funcEntry.addToUsedRegisterSet(reg);
			
			active.add(cur);
		}
	}
	
	/**
	 * Allocates memory for the Ith variable.
	 * 
	 * @param i
	 */
	private static void allocateForIthVariable(int i){
		VarWrapper var = variables[i];
		MemLoc orig = var.getOrigin();
		
		if (orig instanceof Argument){
			if (var.getNumber() == 0)
				var.setOffset(orig.getOffset());
			else
				var.setOffset(funcEntry.allocateLocalSpace(/*((Argument) orig).isPassedByAddress() ? 4 : */orig.getSize()));
				// struct/union must have been taken address once to be live
		}
		else {
			var.setOffset(funcEntry.allocateLocalSpace(orig.getSize()));
		}
		var.setAllocated(true);
	}
	
	/**
	 * Allocates memory space for non-SSA variables.
	 * 
	 */
	private static void allocateMemoryForNonSSAVariables(){
		
		HashSet<MemLoc> addressedMemLocSet = new HashSet<MemLoc>();
		
		for (int i = 0; i < blockCount; ++i){
			int block_num = blockOrder[i];
			Block block = graph.getIthNode(block_num);
			
			for (MyLinkedList<Quadruple>.Node node = block.getList().iterator(); node.hasNext(); ){
				node = node.next();
				Quadruple quad = node.value();
				
				Operand[] operands = quad.getOperands();
				for (Operand opd : operands){
					
					if (opd instanceof Indirection){
						opd = ((Indirection) opd).getSource();
					}
					
					if (opd instanceof Address){
						opd = ((Address) opd).getBase();
					}
					
					if (opd instanceof MemLoc){
						MemLoc memloc = (MemLoc) opd;
						
						if (memloc.atLocal() && memloc.isTakenAddress() && !memloc.isAllocated()){
							addressedMemLocSet.add((MemLoc) opd);
						}
						
					}
					
				}
			}
		}
		
		MemLoc[] addressedMemLoc = addressedMemLocSet.toArray(new MemLoc[0]);
		
		/* sorts the memory locations in increasing size order */
		Arrays.sort(addressedMemLoc, new Comparator<MemLoc>(){

			@Override
			public int compare(MemLoc arg0, MemLoc arg1) {
				
				return arg0.getSize() - arg1.getSize();
			}
			
		});
		
		for (MemLoc memloc : addressedMemLoc){
			
			memloc.setAllocated(true);
			if (!(memloc instanceof Argument)){
				memloc.setOffset(funcEntry.allocateLocalSpace(memloc.getSize()));
			}
		}
		
	}
	
	/**
	 * Rearranges the register mapping to eliminate redundant moves from argument registers.
	 * And map all virtual registers to the physical registers.
	 * 
	 */
	private static void mapVirtualRegToPhysicalReg(){
		
		int[] mapping = new int[regCount];
		Arrays.fill(mapping, -1);
		
		boolean[] mapped = new boolean[regCount];	// whether the physical register has been mapped to
		
		/* map return value address's register to $v0 */{
			
			VarWrapper rva_var = funcEntry.getWrappedReturnValueAddress();
			
			if (rva_var != null && !rva_var.isAllocated()){
				
				int vreg = rva_var.getReg();
				mapping[vreg] = $v0;
				mapped[v0] = true;
			}
		}
		
		/* map argument 0 ~ 3 to $a0 ~ $a3 */{
			
			ArrayList<VarWrapper> arg_var_list = funcEntry.getWrappedArgumentTable();
			
			int tar = arg_var_list.size();
			if (tar > 4) tar = 4;
			
			for (int i = 0; i < tar; ++i){
				VarWrapper arg_var = arg_var_list.get(i);
				
				if (arg_var != null && !arg_var.isAllocated()){
					int vreg = arg_var.getReg();
					
					assert(mapping[vreg] == -1) : 
						"It is impossible for arg_i_0 to be allocated to the same register, for their live intervals always overlap.";
				
					mapping[vreg] = $a0 + i;
					mapped[a[i]] = true;
				}
			}
		}
		
		/* map the rest of the registers */
		for (int i = 0, j = 0; i < regCount; ++i){
			if (mapping[i] == -1){
				
				for (; mapped[j]; ++j);
				mapping[i] = regMapping[j];
				mapped[j] = true;
			}
		}
		
		/* map registers of variables */
		for (int i = 0; i < varCount; ++i){
			VarWrapper var = variables[i];
			
			if (!var.isAllocated())
				var.setReg(mapping[var.getReg()]);
		}
		
		/* map registers in the busy sets of function calls */
		for (int call_index : callList){
			Call call = (Call) instructions.get(call_index).value();
		
			BitSet busy = call.getBusy();
			BitSet mapped_busy = new BitSet(mips.Registers.reg.length);
			
			for (int i = busy.nextSetBit(0); i != -1; i = busy.nextSetBit(i+1)){
				
				mapped_busy.set(mapping[i]);
			}
			
			call.setBusy(mapped_busy);
		}
		
		/* map registers in the used register set of the function */{
			
			BitSet used_reg_set = funcEntry.getUsedRegisterSet();
			BitSet mapped_used_reg_set = new BitSet(mips.Registers.reg.length);
			
			for (int i = used_reg_set.nextSetBit(0); i != -1; i = used_reg_set.nextSetBit(i+1)){
				
				mapped_used_reg_set.set(mapping[i]);
			}
			
			/* adds argument registers and $v0 to the function's live set */
			
			mapped_used_reg_set.set($v0);
			
			for (int i = 0, tar = (funcEntry.getArgumentTable().size() > 4) ? 4 : funcEntry.getArgumentTable().size(); i < tar; ++i)
				mapped_used_reg_set.set($a0 + i);
		
			funcEntry.setUsedRegisterSet(mapped_used_reg_set);
		}
		
	}
	
	/**
	 * Clears the data structures.
	 * 
	 */
	public static void clear(){
		funcEntry = null;
		graph = null;
		entry = null;
		exit = null;
		varRenamingStack = null;
		blockOrder = null;
		coalescedRep = null;
		instructions = null;
		
		varSet = null;
		variables = null;
		
		unhandled = null;
		active = null;
		inactive = null;
		handled = null;
		busy = null;
		callList = null;
		weight = null;
		precolored = null;
		
		random = null;
	}
	
	/**
	 * Add range [begin, end[ to interval[opd].
	 * 
	 * @param opd
	 * @param block_num
	 * @param end
	 */
	private static void addRange(int opd, int begin, int end){
		if (begin > end) return;
		
		ListIterator<Integer> iter_begin = liveBegin[opd].listIterator();
		ListIterator<Integer> iter_end = liveEnd[opd].listIterator();
		
		while (iter_begin.hasNext()){
			int next_begin = iter_begin.next();
			int next_end = iter_end.next();
			
			if (next_end < begin) continue;
			if (next_begin > end) {
				iter_begin.previous();
				iter_end.previous();
				break;
			}
			
			iter_begin.remove();
			iter_end.remove();
			
			if (next_begin < begin) begin = next_begin;
			if (next_end > end) end = next_end;
		}
		
		iter_begin.add(begin);
		iter_end.add(end);
	}

	/**
	 * Sets the begin of the first interval of opd.
	 * 
	 * @param opd
	 * @param begin
	 */
	private static void setBegin(int opd, int begin){
		if (liveBegin[opd].isEmpty()){
			liveBegin[opd].add(begin);
			liveEnd[opd].add(begin);
		}
		else {
			liveBegin[opd].set(0, begin);
		}
	}
	
	/**
	 * Gets father in the union-find set of coalesced variables.
	 * 
	 * @param n
	 * @return
	 */
	private static int fatherOfVariable(int n){
		if (coalescedRep[n] != n) coalescedRep[n] = fatherOfVariable(coalescedRep[n]);
		return coalescedRep[n];
	}
	
	/**
	 * Coalesce two variables.
	 * 
	 * @param n
	 * @param m
	 * @return
	 */
	private static void joinVariables(int n, int m){
		n = fatherOfVariable(n);
		m = fatherOfVariable(m);
		
		//if (intersectionIsEmpty(n, m) && compatible(n, m)){
			if (coalescedRank[n] > coalescedRank[m]){
				n ^= m; m ^= n; n ^= m;
			}
			
			coalescedRep[n] = m;
			unionInterval(m, n);
			
			if (coalescedRank[n] == coalescedRank[m]){
				++coalescedRank[m];
			}
		//}
	}
	
	/**
	 * Returns whether the two variables are compatible.
	 * 
	 * @param n
	 * @param m
	 * @return
	 */
	private static boolean compatible(int n, int m){
		VarWrapper var_n = variables[n];
		VarWrapper var_m = variables[m];
		
		if (var_n.getReg() == var_m.getReg()) return true;
		
		/* currently pre-colored intervals does not coalesce with other intervals */
		
//		if (m == -1){
//			n ^= m; m ^= n; n ^= m;
//			var_n = variables[n]; var_m = variables[m];
//		}
//		
//		LinkedList<Integer> m_begin = liveBegin[m];
//		LinkedList<Integer> m_end = liveEnd[m];
		
		return false;
	}
	
	/**
	 * Returns whether the two intervals' intersection is empty or not.
	 * 
	 * @return
	 */
	private static boolean intersectionIsEmpty(int n, int m){
		
		ListIterator<Integer> n_begin = liveBegin[n].listIterator();
		ListIterator<Integer> n_end = liveEnd[n].listIterator();
		ListIterator<Integer> m_begin = liveBegin[m].listIterator();
		ListIterator<Integer> m_end = liveEnd[m].listIterator();
		
		if (!n_begin.hasNext() || !m_begin.hasNext()) return true;
		
		int nl = n_begin.next(),
			nr = n_end.next(),
			ml = m_begin.next(),
			mr = m_end.next();
		
		while (true) {
			
			/* [nl, nr[ and [ml, mr[ does not overlaps; fetch next interval of n */
			if (nr <= ml){
				if (!n_begin.hasNext()) return true;
				
				nl = n_begin.next();
				nr = n_end.next();
			}
			
			/* [nl, nr[ and [ml, mr[ does not overlaps; fetch next interval of m */
			else if (mr <= nl){
				if (!m_begin.hasNext()) return true;
				
				ml = m_begin.next();
				mr = m_end.next();
			}
			
			/* [nl, nr[ and [ml, mr[ overlaps */
			else {
				return false;
			}
		}
	}
	
	/**
	 * Unions n and m's intervals. The result is placed in n.
	 * 
	 * @param n		the destination interval
	 * @param m		the source interval
	 */
	private static void unionInterval(int n, int m){
		
		if (liveBegin[m].size() > liveBegin[n].size()){
			LinkedList<Integer> tmp = liveBegin[m];
			liveBegin[m] = liveBegin[n];
			liveBegin[n] = tmp;
			
			tmp = liveEnd[m];
			liveEnd[m] = liveEnd[n];
			liveEnd[n] = tmp;
		}
		
		if (liveBegin[m].size() == 0) return;
		
		ListIterator<Integer> n_begin = liveBegin[n].listIterator();
		ListIterator<Integer> n_end = liveEnd[n].listIterator();
		ListIterator<Integer> m_begin = liveBegin[m].listIterator();
		ListIterator<Integer> m_end = liveEnd[m].listIterator();
		
		int nl = n_begin.next(),
			nr = n_end.next(),
			ml = m_begin.next(),
			mr = m_end.next();
		
		while (ml != -1 && nl != -1){
			
			/* ---ml---mr---nl---nr--- */
			if (mr < nl){
				n_begin.previous();
				n_begin.add(ml);
				n_begin.next();
				
				n_end.previous();
				n_end.add(mr);
				n_end.next();
			
				if (m_begin.hasNext()){
					ml = m_begin.next();
					mr = m_end.next();
				}
				else {
					ml = -1;
				}
			}
			
			/* ---nl---nr---ml---mr--- */
			else if ( ml > nr ){
				
				while (nl != -1 && nr < ml){
					if (n_begin.hasNext()){
						nl = n_begin.next();
						nr = n_end.next();
					}
					else {
						nl = -1;
					}
				}

			}
			
			/* joint intervals */
			else {
				
				if (ml < nl){
					n_begin.set(ml);
					nl = ml;
				}
				
				if (mr >= nr){
					nr = mr;
					n_end.set(mr);
				
					if (n_begin.hasNext()){
						nl = n_begin.next();
						nr = n_end.next();
					}
					else {
						nl = -1;
					}
					
					while (nl != -1 && nr < mr){
						n_begin.remove();
						n_end.remove();
						
						if (n_begin.hasNext()){
							nl = n_begin.next();
							nr = n_end.next();
						}
						else {
							nl = -1;
						}
					}
				
					if (nl != -1 && nl <= mr){
						n_end.previous();
						n_end.set(nr);
						nl = n_begin.previous();
					}
				}
				
				if (m_begin.hasNext()){
					ml = m_begin.next();
					mr = m_end.next();
				}
				else {
					ml = -1;
				}
				
			}
		}
		
		/* append the rest of m's interval to the end of n's */
		if (nl == -1){
			while (ml != -1){
				n_begin.add(ml);
				n_end.add(mr);
				
				if (m_begin.hasNext()){
					ml = m_begin.next();
					mr = m_end.next();
				}
				else {
					ml = -1;
				}
			}
		}
	}
	
	/**
	 * Returns whether the live interval n overlaps the given instruction.
	 * 
	 * @param n
	 * @param instructionIndex
	 */
	private static boolean overlaps(int n, int instructionIndex){
		
		for (ListIterator<Integer> n_begin = liveBegin[n].listIterator(),
								   n_end = liveEnd[n].listIterator();
								   n_begin.hasNext(); )
		{
			int l = n_begin.next(),
				r = n_end.next();
			
			if (r > instructionIndex){
				return (l <= instructionIndex);
			}
		}
		
		return false;
		
	}
	
	/* data */
	
	/** the function entry */
	private static FunctionEntry funcEntry;
	
	/** the number of blocks in the control flow graph*/
	private static int N;
	
	/** the control flow graph */
	private static Graph<Block> graph;
	
	/** the entry block of the function */
	private static Block entry;
	
	/** the exit block of the function */
	@SuppressWarnings("unused")
	private static Block exit;
	
	/** the variable renaming stack previously created by SSAConstructor */
	private static HashMap<MemLoc, SSAConstructor.RenamingStack> varRenamingStack;
	
	/** the topological sorted order of basic blocks */
	private static int blockOrder[];
	
	/** the counter for numbering instructions */
	private static int instructionCount;
	
	/** the number of reachable block */
	private static int blockCount;
	
	/** the instruction list */
	private static ArrayList<MyLinkedList<Quadruple>.Node> instructions;
	
	/** the union-find set of coalesced variables */
	private static int coalescedRep[];
	
	/** the rank of each node in the union-find set of coalesced variables */
	private static int coalescedRank[];
	
	/** the index of the first phi function in the block*/
	private static int phi[];
	
	/** the index of the first non-phi quadruple in the block */
	private static int first[];
	
	/** the index of the last quadruple in the block */
	private static int last[];
	
	/** the beginning of live intervals */
	private static LinkedList<Integer>[] liveBegin;
	
	/** the end of live intervals */
	private static LinkedList<Integer>[] liveEnd;
	
	/** the variable set */
	private static HashSet<VarWrapper> varSet;
	
	/** the variables */
	private static VarWrapper[] variables;
	
	/** the number of variables */
	private static int varCount;
	
	/** the unhandled interval set */
	private static ArrayList<Integer> unhandled;
	
	/** the active interval set */
	private static LinkedList<Integer> active;
	
	/** the inactive interval set */
	private static LinkedList<Integer> inactive;
	
	/** the handled interval set */
	private static BitSet handled;
	
	/** the occupied register set */
	private static BitSet busy;
	
	/** function call list */
	private static LinkedList<Integer> callList;
	
	/** register spill cost */
	private static int weight[];
	
	/** the unhandled pre-colored intervals */
	private static LinkedList<Integer> precolored;
	
	/** the in-degree */
	private static int[] in_degree;
	
	/** the random generator */
	private static Random random;
	
	/* registers */
		
	/** registers that takes part in register allocation, the actual mappings differ among different functions */
	public static final int regMapping[] = new int[]{
		
		$v0, $v1, $a0, $a1, $a2, $a3, $t0, $t1, $t2,
		$t3, $t4, $t5, $t6, $t7,
	};
	
	/** the total number of registers that participate in regular register allocation */
	public static final int regCount = regMapping.length;
	
	/** the registers for spill */
	public static final int spill1 = $t8;
	public static final int spill2 = $t9;
	
	public static final int v0 = 0;
	public static final int a[] = {2, 3, 4, 5};
}
