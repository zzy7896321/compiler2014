package optimization;

import java.util.BitSet;
import java.util.HashMap;
import java.util.LinkedList;

import semantics.StandardLibrary;
import irpack.*;
import graph.*;

/**
 * The FunctionRegisterUse class calculates the register usage according to calling relations among functions.
 * Standard library function entries are added to IR after this pass.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 20, 2014
 */
public class FunctionRegisterUse implements mips.Registers{

	/**
	 * Calculates register use for every function.
	 * 
	 * @param ir
	 */
	public static void calculateFunctionRegisterUse(IntermediateRepresentation ir){
		/* add stdlib function entries */
		StandardLibrary.createFunctionEntry(ir);
		
		/* initialize data structures */
		HashMap<Label, FunctionEntry> functionMap = new HashMap<Label, FunctionEntry>();
		
		@SuppressWarnings("unchecked")
		LinkedList<FunctionEntry>[] callerList = (LinkedList<FunctionEntry>[]) new LinkedList<?>[ir.getFunctionEntryTable().size()];
		for (int i = 0; i < ir.getFunctionEntryTable().size(); ++i) callerList[i] = new LinkedList<FunctionEntry>();
		
		WorkList<FunctionEntry> worklist = new WorkList<FunctionEntry>(ir.getFunctionEntryTable().size());
		
		/* the set containing all the virtual registers */
		BitSet all_reg = new BitSet(reg.length);
		for (int reg : SSABasedLinearScan.regMapping){
			all_reg.set(reg);
		}
				
		/* creates the inverse graph of the calling relation graph */
		for (FunctionEntry entry : ir.getFunctionEntryTable()){
			entry.setIndex(functionMap.size());
			
			Label funcLabel = ir.getFunctionLabel(entry.getName());
			functionMap.put(funcLabel, entry);
			
			worklist.insert(entry);
		}
		
		for (FunctionEntry entry : ir.getFunctionEntryTable()){	
			
			/* conservative approximation: has call to function pointer */
			if (entry.getCalledSet() == null){
				
				entry.getUsedRegisterSet().or(all_reg);
			}
			
			else {
				for (Label calledFuncLabel : entry.getCalledSet()){
					FunctionEntry calledFunc = functionMap.get(calledFuncLabel);
					callerList[calledFunc.getIndex()].add(entry);
				}
			}
		}
		
		/* calculates the minimum fixed point of register use */
		for (FunctionEntry entry = worklist.remove(); entry != null; entry = worklist.remove()){
			
			BitSet usedRegs = entry.getUsedRegisterSet();
			for (FunctionEntry caller : callerList[entry.getIndex()]){
				
				BitSet regs = caller.getUsedRegisterSet();
				BitSet diff = (BitSet) regs.clone();
				
				regs.or(usedRegs);
				diff.xor(regs);
				if (diff.previousSetBit(reg.length - 1) != -1){
					worklist.insert(caller);
				}
			}
			
		}
		
	}

}
