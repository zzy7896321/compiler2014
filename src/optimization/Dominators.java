package optimization;

import java.util.Arrays;
import java.util.Scanner;

import util.IntVector;
import graph.*;

/**
 * The Dominators class computes immediate dominators of a graph.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 8, 2014
 */
public class Dominators {
	
	/**
	 * Returns an array of immediate dominators of each node in the graph.
	 * 
	 * @param graph		the graph to calculate immediate dominators on
	 * @param start		the root of dfs tree
	 * @return
	 */
	public static int[] dominators(Graph<?> graph, int start){
		Dominators.graph = graph;
		init();
		
		Arrays.fill(dfn, -1);
		dfs(start);
		
		computeDominators();
		
		int[] ret = idom;
		clear();
		return ret;
	}
	
	/**
	 * Initialize data structures.
	 * 
	 */
	private static void init(){
		int n = graph.size();
		N = 0;
		idom = new int[n];
		dfn = new int[n];
		semi = new int[n];
		parent = new int[n];
		vertex = new int[n];
		ancestor = new int[n];
		best = new int[n];
		samedom = new int[n];
		bucket = new IntVector[n];
		for (int i = 0; i < n; ++i)
			bucket[i] = new IntVector();
	}
	
	/**
	 * Dfs starting at x.
	 * 
	 * @param x
	 */
	private static void dfs(int x){
		dfn[x] = N;
		vertex[N] = x;
		++N;
		
		for (int y : graph.successor(x)){
			if (dfn[y] == -1){
				parent[y] = x;
				dfs(y);
			}
		}
		
	}
	
	/**
	 * Computes dominators.
	 * 
	 */
	private static void computeDominators(){
		Arrays.fill(samedom, -1);
		Arrays.fill(ancestor, -1);
		
		// traverse the tree in the decreasing dfs number sequence
		for (int i = N - 1; i > 0; --i){
			int n = vertex[i];		// current node
			int p = parent[n];		// parent of n
			int s = p;				// semi-dominator of p (candidate)
			
			
			for (int v : graph.predecessor(n)){
				int s1;
				if (dfn[v] <= dfn[n])
					s1 = v;
				else
					s1 = semi[findAncestorWithLowestSemi(v)];
				if (dfn[s1] < dfn[s])
					s = s1;
			}
			
			semi[n] = s;
			bucket[s].add(n);
			link(p, n);
			
			for (int j = 0, sz = bucket[p].size(); j < sz; ++j){
				int v = bucket[p].get(j);
				int y = findAncestorWithLowestSemi(v);
				if (semi[y] == semi[v])
					idom[v] = p;
				else
					samedom[v] = y;
			}
		}
		
		for (int i = 1; i < N-1; ++i){
			int n = vertex[i];
			if (samedom[n] != -1)
				idom[n] = idom[samedom[n]];
		}
	}
	
	/**
	 * Find the ancestor with the lowest simi-dominator.
	 * 
	 * @param v
	 * @return
	 */
	private static int findAncestorWithLowestSemi(int v){
		int a = ancestor[v];
		if (ancestor[a] != -1){
			int b = findAncestorWithLowestSemi(a);
			ancestor[v] = ancestor[a];
			if (dfn[semi[b]] < dfn[semi[best[v]]])
				best[v] = b;
		}
		
		return best[v];
	}
	
	/**
	 * Link n to p.
	 * 
	 * @param p
	 * @param n
	 * @return
	 */
	private static void link(int p, int n){
		ancestor[n] = p;
		best[n] = n;
	}
	
	/**
	 * Clear data structures.
	 * 
	 */
	private static void clear(){
		graph = null;
		idom = null;
		dfn = null;
		semi = null;
		parent = null;
		vertex = null;
		ancestor = null;
		best = null;
		samedom = null;
		bucket = null;
	}
	
	private static int N;				// number of nodes
	private static Graph<?> graph;		// the graph
	private static int[] idom;			// immediate dominators
	private static int[] dfn;			// dfs number
	private static int[] semi;			// semi-dominators
	private static int[] parent;		// parent in the dfs tree
	private static int[] vertex;		// vertex number in the graph
	private static int[] ancestor;		// ancestor
	private static int[] best;			// ancestor with lowest semi-dominator
	private static int[] samedom;		// has the same dominator with
	private static IntVector[] bucket;		// nodes that has semi-dominator index
	
	/**
	 * Test simple cases.
	 * 
	 * @param args
	 */
	public static void main(String args[]){
		Scanner scanner = new Scanner(System.in);
		
		int n = scanner.nextInt();
		
		Graph<Block> graph = new Graph<Block>();
		for (int i = 0; i < n; ++i)
			graph.addNode(new Block());
		
		for (int x= scanner.nextInt(); x != -1; x = scanner.nextInt()){
			x = x - 1;
			int y = scanner.nextInt() - 1;
			graph.addEdge(x, y);
		}
		
		int[] idom = dominators(graph, 0);
		for (int i = 0; i < n; ++i){
			System.out.printf("idom[%d] = %d\n", i+1, idom[i]+1);
		}
		
		scanner.close();
	}
}
