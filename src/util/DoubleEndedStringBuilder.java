package util;


/**
 * The DoubleEndedStringBuilder node class. This class can build string from both directions.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 3, 2014
 */
public class DoubleEndedStringBuilder {

	/**
	 * Constructs an empty DoubleEndedStringBuilder.
	 */
	public DoubleEndedStringBuilder(){
		_head = new StringBuilder();
		_rear = new StringBuilder();
	}
	
	/**
	 * Append a string to the rear.
	 * 
	 * @param str		the string to append
	 * @return			this
	 */
	public DoubleEndedStringBuilder append_rear(String str){
		_rear.append(str);
		return this;
	}
	
	/**
	 * Append a string to the head.
	 * 
	 * @param str		the string to append
	 * @return			this
	 */
	public DoubleEndedStringBuilder append_head(String str){
		_head.append(util.StringUtil.reverse(str));
		return this;
	}
	
	/**
	 * 
	 * @return 		the string contained in the builder
	 */
	public String toString(){
		return util.StringUtil.reverse(_head.toString()) + _rear.toString();
	}
	
	private	StringBuilder _head;
	private StringBuilder _rear;
}
