package util;

public class StringUtil {
	
	/**
	 * Unescapes a character.
	 * 
	 * @param ch	the char to unescape
	 * @return
	 */
	public static String unescape(char ch){
		switch (ch){
			case '\'':
				return "\\'";
			case '\"':
				return "\\\"";
			case '\\':
				return "\\\\";
			case (char)0x07:
				return "\\a";
			case '\b':
				return "\\b";
			case '\f':
				return "\\f";
			case '\n':
				return "\\n";
			case '\r':
				return "\\r";
			case '\t':
				return "\\t";
			case (char)0x0b:
				return "\\v";
			case (char)0:
				return "\\0";
			default:
				if (Character.isISOControl(ch)){
					return String.format("\\%o", Character.getNumericValue(ch));
				}
		}
		
		return Character.toString(ch);
	}
	
	/**
	 * Unescapes a string.
	 * 
	 * @param str	the string to unescape
	 * @return
	 */
	public static String unescape(String str){
		final int extra_space = 16;
		StringBuilder builder = new StringBuilder(str.length() + extra_space);
		
		for (int i = 0; i < str.length(); ++i)
			builder.append(unescape(str.charAt(i)));
		
		return builder.toString();
	}
	
	public static String reverse(String str){
		return new StringBuilder(str).reverse().toString();
	}
	
	public static String repeat(int n, String str){
		if (n == 0) return "";
		return String.format("%0" + n + "d", 0).replace("0", str);
	}
	
	public static String unescape_with_octal(char ch){
		if (ch == '\"'){
			return "\\\"";
		}
		
		if (Character.isISOControl(ch)){
			return String.format("\\%03o", (int) ch & 0xFF);
		}
		
		return Character.toString(ch);
	}
	
	public static String unescape_with_octal(String str){
		final int extra_space = 16;
		StringBuilder builder = new StringBuilder(str.length() + extra_space);
		
		for (int i = 0; i < str.length()-1; ++i)	// skip last \0 character
			builder.append(unescape_with_octal(str.charAt(i)));
		
		return builder.toString();
	}
}
