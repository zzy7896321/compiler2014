package util;

/**
 * The MyLinkedList class implements a linked list that supports quick insertion, deletion.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 6, 2014
 */
public class MyLinkedList<ValueType> {
	
	public class Node{
	
		/**
		 * Constructs a dummy Node.
		 */
		private Node(){
			this.prev = this.next = this;
			this.value = null;
		}
		
		/**
		 * Append a new node to previous node.
		 * 
		 * @param prev
		 * @param value
		 */
		private Node(Node prev, ValueType value){
			prev.next.prev = this;
			this.next = prev.next;
			prev.next = this;
			this.prev = prev;
			
			this.value = value;
		}
		
		/**
		 * Returns if the node has a previous node.
		 * 
		 * @return
		 */
		public boolean hasPrevious(){
			return prev.value != null;
		}
		
		/**
		 * Returns if the node has a next node.
		 * 
		 * @return
		 */
		public boolean hasNext(){
			return next.value != null;
		}
		
		/**
		 * Returns if the node is not the end of list.
		 * 
		 * @return
		 */
		public boolean isNotEnd(){
			return value != null;
		}
		
		/**
		 * Returns the next node.
		 * 
		 * @return
		 */
		public Node next(){
			return next;
		}
		
		/**
		 * Returns the previous node.
		 * 
		 * @return
		 */
		public Node previous(){
			return prev;
		}
		
		/**
		 * Accesses the associated value.
		 * 
		 * @return
		 */
		public ValueType value(){
			return value;
		}
		
		/**
		 * Removes current node from the list.
		 * 
		 * @return next node
		 */
		public Node remove(){
			prev.next = next;
			next.prev = prev;
			
			--MyLinkedList.this.size;
			
			return next;
		}
		
		/**
		 * Inserts a node after the current node.
		 * 
		 * @return the new next node
		 */
		public Node insert(ValueType value){
			++MyLinkedList.this.size;
			
			return new Node(this, value);
		}
		
		/**
		 * Inserts a node before the current node.
		 * 
		 * @param node
		 * @return		this
		 */
		public Node insertBefore(ValueType value){
			Node node = new Node();
			node.value = value;
			
			prev.next = node;
			node.prev = prev;
			prev = node;
			node.next = this;
			
			++MyLinkedList.this.size;
			
			return this;
		}
		
		/**
		 * Replaces the value.
		 * 
		 * @param value
		 */
		public void setValue(ValueType value){
			this.value = value;
		}
		
		private Node prev, next;
		private ValueType value;
	}
	
	/**
	 * Constructs an empty linked list.
	 * 
	 */
	public MyLinkedList(){
		first = new Node();
		this.size = 0;
	}
	
	/**
	 * Returns a iterator at the first node.
	 * 
	 * @return
	 */
	public Node iterator(){
		return first;
	}
	
	/**
	 * Returns the value of the first node.
	 * 
	 * @return
	 */
	public ValueType getFirst(){
		return first.next().value;
	}
	
	/**
	 * Returns the value of the last node.
	 * 
	 * @return
	 */
	public ValueType getLast(){
		return first.previous().value;
	}
	
	/**
	 * Adds a new node to the first.
	 * 
	 * @param value
	 * @returns		the new node
	 */
	public Node addFirst(ValueType value){
		return first.insert(value);
	}
	
	/**
	 * Adds a new node to the last.
	 * 
	 * @param value
	 * @param		the new Node
	 */
	public Node addLast(ValueType value){
		first.insertBefore(value);
		return first.previous();
	}
	
	public void clear(){
		this.size = 0;
		this.first = new Node();
	}
	
	/**
	 * Returns the size of the linked list.
	 * 
	 * @return
	 */
	public int size(){
		return this.size;
	}
	
	private Node first;
	private int size;
}
