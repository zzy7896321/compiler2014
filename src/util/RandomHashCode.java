package util;

import java.util.Random;

/**
 * The RandomHashCode class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 18, 2014
 */
public class RandomHashCode {

	public static void initialize(){
		random = new Random(System.currentTimeMillis());
	}
	
	public static int getRandomHashCode(){
		return random.nextInt();
	}
	
	private static Random random;
}
