package util;

import java.util.Arrays;

/**
 * The IntVector class is an expandable array for int type.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 24, 2014
 */
public class IntVector {
	
	/**
	 * Constructs a new IntVector with a default initial size of storage 16.
	 * 
	 */
	public IntVector(){
		this(16);
	}
	
	/**
	 * Constructs a new IntVector with designated initial size of storage.
	 * 
	 * @param init_size
	 */
	public IntVector(int init_size){
		_storage = new int[init_size];
		_size = 0;
	}
	
	/**
	 * Accesses the element at the index.
	 * 
	 * @param index
	 * @return
	 */
	public int get(int index){
		if (index < 0 || index >= _size) throw new IndexOutOfBoundsException();
		
		return _storage[index];
	}
	
	/**
	 * Sets the value of the element at the index.
	 * 
	 * @param index
	 * @param value
	 * @return
	 */
	public IntVector set(int index, int value){
		if (index < 0 || index >= _size) throw new IndexOutOfBoundsException();
		
		_storage[index] = value;
		
		return this;
	}
	
	/**
	 * Adds a new element.
	 * 
	 * @param value
	 * @return	this
	 */
	public IntVector add(int value){
		if (_size == _storage.length){
			_storage = Arrays.copyOf(_storage, _storage.length << 1);
		}
		
		_storage[_size++] = value;
			
		return this;
	}
	
	/**
	 * Removes the last element;
	 * 
	 * @return	the removed element
	 */
	public int removeLast(){
		if (_size == 0) throw new IndexOutOfBoundsException();
		
		return _storage[--_size];
	}
	
	/**
	 * Returns the last element;
	 * 
	 * @return
	 */
	public int getLast(){
		return _storage[_size - 1];
	}
	
	/**
	 * 
	 * @return		the underlying array
	 */
	public int[] getStorage(){
		return _storage;
	}
	
	/**
	 * Returns the size.
	 * 
	 * @return
	 */
	public int size(){
		return _size;
	}
	
	/**
	 * Clear the container.
	 * 
	 */
	public void clear(){
		_size = 0;
	}
	
	private int[] _storage;
	private int _size;
}
