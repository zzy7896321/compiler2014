package environment;

/**
 * The StructType class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class StructType extends StructOrUnionType {

	/**
	 * Constructs an empty incomplete struct type.
	 */
	public StructType() {
		super();
		_next_bit = 0;
		_prev_type = null;
	}
	
	public StructType(String tag) {
		super(tag);
		_next_bit = 0;
		_prev_type = null;
	}
	

	@Override
	public boolean addField(String name, Type type){
		if (_map.containsKey(name)) return false;
		
		_size = _alignOffset(_size, type.getAlign());
		_map.put(name, new StructFieldInfo(name, type, _size, _map.size()));
		_size += type.getSize();
		
		_next_bit = 0;
		_prev_type = type;
		
		_fields.add(name);
		return true;
	}
	

	@Override
	public boolean addBitField(String name, Type type, int bit_length){
		if (_map.containsKey(name)) return false;
		
		if (_next_bit == 0 || _prev_type != type || bit_length + _next_bit > (type.getSize() << 3)){
			_size = _alignOffset(_size, type.getAlign());
			int mask = 1 << (bit_length - 1);
			mask = (mask - 1) | mask;
			_map.put(name, new StructFieldInfo(name, type, _size, bit_length, mask, _map.size()));
			_size += type.getSize();
			
			if (bit_length >= (type.getSize() << 3)){
				_next_bit = 0;
			}
			else {
				_next_bit = bit_length;
			}
			_prev_type = type;
		}
		else {
			int mask = 1 << (bit_length - 1);
			mask = ((mask-1) | mask) << _next_bit;
			_map.put(name, new StructFieldInfo(name, type, _size - type.getSize(), bit_length, mask, _map.size()));
			
			if (_next_bit + bit_length >= (type.getSize() << 3)){
				_next_bit = 0;
			}
			else {
				_next_bit += bit_length;
			}
			_prev_type = type;
		}
		
		_fields.add(name);
		return true;
	}
	

	@Override
	public boolean addBitField(Type type, int bit_length){
		if (bit_length == 0){
			_next_bit = 0;
			_prev_type = type;
		}
		else
		if (_next_bit == 0 || _prev_type != type || bit_length + _next_bit > (type.getSize() << 3)){
			_size = _alignOffset(_size, type.getAlign());
			_size += type.getSize();
			
			if (bit_length >= (type.getSize() << 3)){
				_next_bit = 0;
			}
			else {
				_next_bit = bit_length;
			}
			_prev_type = type;
		}
		else {		
			if (_next_bit + bit_length >= (type.getSize() << 3)){
				_next_bit = 0;
			}
			else {
				_next_bit += bit_length;
			}
			_prev_type = type;
		}		
		
		return true;
	}
	
	
	private int _next_bit;
	private Type _prev_type;
	
}
