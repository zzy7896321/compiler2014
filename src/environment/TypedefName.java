package environment;

/**
 * The TypedefName class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class TypedefName extends SymbolInfo {

	/**
	 * Constructs a typedef name with a name and a type.
	 * 
	 * @param name
	 * @param type
	 */
	public TypedefName(String name, Type type) {
		super(name, type, NO_LINKAGE);

	}
	

}
