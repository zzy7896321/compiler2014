package environment;

/**
 * The VoidType class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class VoidType extends BuiltinType {

	/**
	 * Default constructor.
	 */
	public VoidType() {
	}
	
	@Override
	public int getSize(){
		
		return 0;
	}
	
	@Override
	public boolean equals(Object rhs){
		return rhs instanceof VoidType;
	}
	
	@Override
	public boolean isComplete(){
		
		return false;
	}

	@Override
	public int getRank() {
		
		return 0;
	}


}
