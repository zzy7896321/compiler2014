package environment;

/**
 * The StructFieldInfo class contains information on a field of a struct or union.
 * equals only compares by name. hashCode is the hashCode of the name.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class StructFieldInfo {

	/**
	 * Constructs an ordinary struct or union field with name, type and the offset
	 * the field (starting from 0).
	 * 
	 * @param name		the name of the field
	 * @param type		the type of the field
	 * @param offset	the offset of the field
	 * @param order 	the sequential order of the field
	 */
	public StructFieldInfo(String name, Type type, int offset, int order) {
		_name = name;
		_type = type;
		_offset = offset;
		_bit_length = -1;
		_mask = 0xffffffff;
		_order = order;
	}
	
	/**
	 * Constructs a struct or union bit-field with name, type, the offset,
	 * bit length, and a mask.
	 * 
	 * @param name				the name of the field
	 * @param type				the type of the field
	 * @param offset			the offset of the field
	 * @param bit_length		the bit length
	 * @param mask				the mask
	 * @param order				the sequential order of the field
	 */
	public StructFieldInfo(String name, Type type, int offset, int bit_length, int mask, int order){
		_name = name;
		_type = type;
		_offset = offset;
		_bit_length = bit_length;
		_mask = mask;
	}
	
	/**
	 * Returns the name of the field. Or null if the field is anonymous.
	 * 
	 * @return		the name of the field, null if anonymous
	 */
	public String getName(){
		return _name;
	}
	
	/**
	 * Returns the type of the field.
	 * 
	 * @return		the type of the field
	 */
	public Type getType(){
		return _type;
	}
	
	/**
	 * Returns the offset of the field.
	 * 
	 * @return		the offset of the field
	 */
	public int getOffset(){
		return _offset;
	}
	
	/**
	 * Returns the bit length of the bit-field. -1 if the field is not bit-field.
	 * 
	 * @return		the bit length of the bit-field, -1 if it is not bit-field
	 */
	public int getBitLength(){
		return _bit_length;
	}
	
	/**
	 * Returns the mask of the bit-field.
	 * 
	 * @return	the mask
	 */
	public int getMask(){
		return _mask;
	}
	
	public int getOrder(){
		return _order;
	}
	
	@Override
	public boolean equals(Object rhs){
		if (!(rhs instanceof StructFieldInfo)) return false;
		
		StructFieldInfo crhs = (StructFieldInfo)rhs;
		return crhs._name.equals(_name);
	}
	
	@Override
	public int hashCode(){
		return _name.hashCode();
	}
	
	private String _name;
	private Type _type;
	private int _offset;
	private int _order;
	
	private int _bit_length;	// -1 if not an bit-field
	private int _mask;
}
