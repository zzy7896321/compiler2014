package environment;


/**
 * The base class of all symbols, which contains the name of the symbol.
 * 
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 *
 */
public abstract class SymbolInfo{
	
	/**
	 * Constructs SymbolInfo with the name of the symbol.
	 * @param name	the name of the symbol
	 */
	public SymbolInfo(String name, Type type, int linkage){
		_name = name;
		_type = type;
		_linkage = linkage;
	}
	
	public String getName(){
		return _name;
	}
	
	public Type getType(){
		return _type;
	}
	
	public int getLinkage(){
		return _linkage;
	}
	
	protected String _name;
	protected Type _type;
	protected int _linkage;
	
	static public final int EXTERNAL_LINKAGE = 1;
	static public final int INTERNAL_LINKAGE = 2;
	static public final int NO_LINKAGE = 3;
	static public final int ARG = 4;
	
	//static public final int STRING_LITERAL = 4;		// denotes a pseudo variable, memory location is yet to be allocated in global data segment
}