package environment;

/**
 * The FunctionDesignator class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class FunctionDesignator extends SymbolInfo {

	/**
	 * @param name
	 */
	public FunctionDesignator(String name, FunctionType func_type, int linkage) {
		super(name, func_type, linkage);
	}
	
	public FunctionDesignator(String name, FunctionType func_type){
		super(name, func_type, EXTERNAL_LINKAGE);
	}
	

	/**
	 *	Returns whether the function is defined.
	 *
	 * @return the defined
	 */
	public boolean isDefined() {
		return defined;
	}

	/**
	 *	Sets the defined.
	 * @param defined the defined to set
	 */
	public void setDefined(boolean defined) {
		this.defined = defined;
	}



	private boolean defined;

}
