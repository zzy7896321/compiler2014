package environment;

import java.util.LinkedList;

import irpack.*;

/**
 * The RuntimeEnvironment class provides information about the runtime environment for semantic check and translate.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 12, 2014
 */
public class RuntimeEnvironment {

	
	public RuntimeEnvironment(Type return_type, FunctionEntry entry){
		this.return_type = return_type;
		this.entry = entry;
		this.continue_target = new LinkedList<Label>();
		this.break_target = new LinkedList<Label>();
		
		this.sc_target_set = false;
		this.true_target = null;
		this.false_target = null;
	}
	
	/** return type of the function */
	public Type return_type;
	
	/** intermediate representation function entry */
	public FunctionEntry entry;

	/** continue target stack */
	public LinkedList<Label> continue_target;
	
	/** break target stack */
	public LinkedList<Label> break_target;
	
	/** whether the short-circuit operator target is set */
	public boolean sc_target_set;
	
	/** short-circuit operator true target, null if it falls on true branch */
	public Label true_target;
	
	/** short-circuit operator false target, null if it falls on false branch */
	public Label false_target;
}
