package environment;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * The Environment class stores the symbol table of current scope,
 * and links to upper scope.
 * 
 * @author zzy
 * @version last update: Apr 2, 2014
 *
 */
public class Environment{
	
	/**
	 * Constructs an empty environment with null previous environment.
	 */
	public Environment(){
		_prev = null;
		_map = new HashMap<String, SymbolInfo>();
		_tags = new HashMap<String, SymbolInfo>();
	}
	
	/**
	 * Constructs an empty environment with previous environment designated.
	 * 
	 * @param prev		the previous environment
	 */
	public Environment(Environment prev){
		_prev = prev;
		_map = new HashMap<String, SymbolInfo>();
		_tags = new HashMap<String, SymbolInfo>();
	}
	
	/**
	 * Insert a symbol into current environment.
	 * Insertion may fail if the symbol already exists in the current environment.
	 * 
	 * @param symbol		the symbol to insert
	 * @return				whether the insertion succeeds
	 */
	public boolean insert(SymbolInfo symbol){
		if (_map.containsKey(symbol.getName())){
			return false;
		}
		
		if (symbol.getLinkage() == SymbolInfo.EXTERNAL_LINKAGE){
			_external_linkage_object_list.add(symbol);
		}
		
		_map.put(symbol.getName(), symbol);
		return true;
	}
	
	/**
	 * Lookup the name in current environment.
	 * 
	 * @param name		the name to lookup
	 * @return			the SymbolInfo in current environment, null if not found 
	 */
	public SymbolInfo lookupOnCurrentLevel(String name){
		return _map.get(name);
	}
	
	/**
	 * Lookup the name along the environment chain.
	 * 
	 * @param name	the name to lookup
	 * @return		the first match up the chain, null if not found
	 */
	public SymbolInfo lookup(String name){
		SymbolInfo ret = _map.get(name);
		Environment cur_env = this;
		while (ret == null && cur_env._prev != null){
			cur_env = cur_env._prev;
			ret = cur_env._map.get(name);
		}
		return ret;
	}
	
	/**
	 * Insert a tag symbol into current environment.
	 * Insertion may fail if the symbol already exists in the current environment.
	 * 
	 * @param symbol		the tag symbol to insert
	 * @return				whether the insertion succeeds
	 */
	public boolean insertTag(SymbolInfo symbol){
		if (_tags.containsKey(symbol.getName())){
			return false;
		}
		
		_tags.put(symbol.getName(), symbol);
		return true;
	}
	
	/**
	 * Lookup the tag name in current environment.
	 * 
	 * @param name		the tag name to lookup
	 * @return			the SymbolInfo in current environment, null if not found 
	 */
	public SymbolInfo lookupTagOnCurrentLevel(String name){
		return _tags.get(name);
	}
	
	/**
	 * Lookup the tag name along the environment chain.
	 * 
	 * @param name	the tag name to lookup
	 * @return		the first match up the chain, null if not found
	 */
	public SymbolInfo lookupTag(String name){
		SymbolInfo ret = _tags.get(name);
		Environment cur_env = this;
		while (ret == null && cur_env._prev != null){
			cur_env = cur_env._prev;
			ret = cur_env._tags.get(name);
		}
		return ret;
	}
	
	/**
	 * Accesses the previous environment.
	 * 
	 * @return		the previous environment
	 */
	public Environment getPrevious(){
		return _prev;
	}
	
	private Environment _prev;
	private HashMap<String, SymbolInfo> _map;
	private HashMap<String, SymbolInfo> _tags;
	
	static public LinkedList<SymbolInfo> getExternalObjectList(){
		return _external_linkage_object_list;
	}
	
	static public void clearExternalObjectList(){
		_external_linkage_object_list.clear();
	}
	
	static private LinkedList<SymbolInfo> _external_linkage_object_list = new LinkedList<SymbolInfo>();
}