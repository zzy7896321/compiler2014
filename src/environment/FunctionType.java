package environment;

import java.util.LinkedList;
import java.util.ListIterator;

/**
 * The FunctionType class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class FunctionType extends Type {

	/**
	 * Constructs an empty and incomplete function type.
	 */
	public FunctionType(Type return_type) {
		_complete = false;
		_var_length = false;
		_return_type = return_type;
		_parameter_type = new LinkedList<Type>();
	}

	/**
	 * Returns true if the function type is supplied with no further information on the number
	 * and type after the given parameter list.
	 * 
	 * @return		true if the function parameter list has ellipsis
	 */
	public boolean isVarLength(){
		return _var_length;
	}
	
	public void setVarLength(boolean var_length){
		_var_length = var_length;
	}
	
	/**
	 * Appends a type to the end of the parameter list.
	 * 
	 * @param type		the type to be appended
	 */
	public void add(Type type){
		_parameter_type.add(type);
	}
	
	public Type getReturnType(){
		return _return_type;
	}
	
	public void setReturnType(Type return_type){
		_return_type = return_type;
	}
	
	public LinkedList<Type> getParameterTypeList(){
		return _parameter_type;
	}
	
	/**
	 * Set the completeness. 
	 * 
	 * @param complete
	 */
	public void setComplete(boolean complete){
		_complete = complete;
	}
	
	@Override
	public boolean equals(Object rhs){
		if (!(rhs instanceof FunctionType)) return false;
		
		FunctionType frhs = (FunctionType)rhs;
		if (!(_return_type.equals(frhs._return_type))) return false;
		if (_parameter_type.size() != frhs._parameter_type.size()) return false;
		if (_var_length != frhs._var_length) return false;
		for (ListIterator<Type> iter1 = _parameter_type.listIterator(), iter2 = frhs._parameter_type.listIterator();
				iter1.hasNext();){
			if (!(iter1.next().equals(iter2.next()))) return false;
		}
		
		return true;
	}
	
	@Override
	public int getSize() {

		
		return 0;
	}

	@Override
	public boolean isComplete() {

		return _complete;
	}

	private boolean _complete;
	private LinkedList<Type> _parameter_type;
	private Type _return_type;
	private boolean _var_length;
}
