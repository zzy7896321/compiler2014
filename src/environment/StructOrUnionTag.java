package environment;

/**
 * The StructOrUnionTag class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class StructOrUnionTag extends SymbolInfo {

	/**
	 * Constructs struct or union tag with a name and associated struct or union type.
	 * 
	 * @param name
	 * @param type
	 */
	public StructOrUnionTag(String name, StructOrUnionType type, int linkage) {
		super(name, type, linkage);
	}
	
	public StructOrUnionTag(String name, StructOrUnionType type){
		super(name, type, NO_LINKAGE);
	}
	
}
