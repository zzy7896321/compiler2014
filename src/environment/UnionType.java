package environment;

/**
 * The UnionType class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class UnionType extends StructOrUnionType{

	/**
	 * Default Constructor constructs an empty incomplete union type.
	 */
	public UnionType() {
		super();
	}
	
	public UnionType(String tag){
		super(tag);
	}
	
	@Override
	public boolean addField(String name, Type type){
		if (_map.containsKey(name)) return false;
		
		_map.put(name, new StructFieldInfo(name, type, 0, _map.size()));
		if (type.getSize() > _size) _size = type.getSize();
		
		_fields.add(name);

		return true;
	}
	

	@Override
	public boolean addBitField(String name, Type type, int bit_length){
		if (_map.containsKey(name)) return false;
		
		int mask = 1 << (bit_length-1);
		mask = (mask - 1) | mask;
		_map.put(name, new StructFieldInfo(name, type, 0, bit_length, mask, _map.size()));
		if (type.getSize() > _size) _size = type.getSize();
		
		_fields.add(name);

		return true;
	}
	
	@Override
	public boolean addBitField(Type type, int bit_length){
		
		return true;
	}


}
