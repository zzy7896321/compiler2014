package environment;

/**
 * The PointerType class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class PointerType extends Type {

	/**
	 * Constructs a pointer type from base type.
	 * 
	 * @param base_type		the base type
	 */
	public PointerType(Type base_type) {
		_base_type = base_type;
	}
	
	public Type getBaseType(){
		return _base_type;
	}

	@Override
	public boolean equals(Object rhs){
		return (rhs instanceof PointerType) ? ( _base_type.equals(((PointerType)rhs)._base_type) ) : false;
	}
	
	@Override
	public int getSize() {

		return config.TargetConfig.pointer_size;
	}
	
	@Override
	public int getAlign(){
		return getSize();
	}

	@Override
	public boolean isComplete() {

		return true;
	}

	protected Type _base_type;
}
