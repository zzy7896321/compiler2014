package environment;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * The StructOrUnionType class is the base class of StructType class and UnionType class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public abstract class StructOrUnionType extends Type{

	/**
	 * Default constructor. Initialize an empty field list.
	 * And declare the struct or union as incomplete type.
	 * 
	 */
	public StructOrUnionType(String tag) {
		_tag = tag;
		_complete = false;
		_map = new HashMap<String, StructFieldInfo>();
		_size = 0;
		_fields = new ArrayList<String>();
	}
	
	public StructOrUnionType() {
		_tag = null;
		_complete = false;
		_map = new HashMap<String, StructFieldInfo>();
		_size = 0;
		_fields = new ArrayList<String>();
	}
	
	public void setTag(String tag){
		_tag = tag;
	}
	
	public String getTag(){
		return _tag;
	}
	
	@Override
	public boolean isComplete(){
		return _complete;
	}
	
	@Override
	public int getSize(){
		return _size;
	}
	
	public void setComplete(boolean complete){		
		_size = _alignOffset(_size, config.TargetConfig.align);
		_complete = complete;
	}
	
	/**
	 * Returns the field map.
	 * 
	 * @return		the field map
	 */
	public HashMap<String, StructFieldInfo> getFieldMap(){
		return _map;
	}
	
	@Override
	public boolean equals(Object rhs){
		return rhs == this;
	}
	
	/**
	 * Add a field. Returns true if no conflict is found.
	 * 
	 * @param name		the name of the field
	 * @param type		the type of the field
	 * @return			true if no conflict is found
	 */
	abstract public boolean addField(String name, Type type);
	
	/**
	 * Add a bit field. Returns true if no conflict is found.
	 * 
	 * @param name				the name of the field
	 * @param type				the type of the field
	 * @param bit_length		the bit length of the field, assumed to be fit in the type and non-zero
	 * @return 					true if no conflict is found
	 */
	abstract public boolean addBitField(String name, Type type, int bit_length);
	
	/**
	 * Add an unnamed bit field. Returns true if no conflict is found.
	 * 
	 * @param type				the type of the field
	 * @param bit_length		the name of the field
	 * @return					true if no conflict is found
	 */
	abstract public boolean addBitField(Type type, int bit_length);
	
	public StructFieldInfo getField(String name){
		return _map.get(name);
	}
	
	public int getFieldCount(){
		return _map.size();
	}
	
	public StructFieldInfo getIthField(int i){
		return _map.get(_fields.get(i));
	}
	
	protected String _tag;
	protected boolean _complete;
	protected int _size;
	protected HashMap<String, StructFieldInfo> _map;
	protected ArrayList<String> _fields;
}
