package environment;

import irpack.MemLoc;
import irpack.op;

/**
 * The VariableSymbol class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class VariableSymbol extends SymbolInfo {

	/**
	 * @param name
	 */
	public VariableSymbol(String name, Type type, int linkage) {
		super(name, type, linkage);
		_initialized = false;
		if (linkage == EXTERNAL_LINKAGE || linkage == INTERNAL_LINKAGE){
			_memloc = op.global(type.getSize());
		}
		else if (linkage == NO_LINKAGE){
			_memloc = op.local(type.getSize());
		}
		else if (linkage == ARG){
			if (type instanceof environment.StructOrUnionType){
				_memloc = op.argCallee(0, type.getSize(), true);
			}
			else {
				_memloc = op.argCallee(0, type.getSize(), false);
			}
			_linkage = NO_LINKAGE;
		}
	}

	
	public boolean isInitialized(){
		return _initialized;
	}
	
	public void setInitialized(boolean initialized){
		_initialized = initialized;
	}
	
	public MemLoc getMemLoc() {
		return _memloc;
	}

	public void setMemLoc(MemLoc _memloc) {
		this._memloc = _memloc;
	}
	
	public void updateMemLoc(){
		this._memloc.setSize(_type.getSize());
	}

	protected boolean _initialized;
	
	protected MemLoc _memloc;
}
