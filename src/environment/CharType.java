package environment;

/**
 * The CharType class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class CharType extends BuiltinType {

	/**
	 * Default constructor.
	 */
	public CharType() {
	}

	@Override
	public int getSize(){
		
		return config.TargetConfig.char_size;
	}
	
	@Override
	public boolean equals(Object rhs){
		return rhs instanceof CharType;
	}
	
	@Override
	public int getAlign(){
		return getSize();
	}

	@Override
	public int getRank() {
		
		return 1;
	}
}
