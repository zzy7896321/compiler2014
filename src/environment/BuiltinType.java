package environment;

/**
 * The BuiltinType class is the base class of all builtin type classes.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public abstract class BuiltinType extends Type {

	/**
	 * Default constructor.
	 */
	public BuiltinType() {
	}
	
	@Override
	public boolean isComplete(){
		// builtin types are usually complete except void
		return true;
	}
	
	/**
	 * 
	 * @return		the rank of arithmetic type, nonsense for non-arithmetic type
	 */
	public abstract int getRank();

}
