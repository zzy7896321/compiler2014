package environment;

/**
 * The TypeFactory class is the factory class of all built-in and derived classes.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class TypeFactory {
	
	/**
	 * Returns an IntType type object. This function guarantees it always returns the same IntType object.
	 * 
	 * @return		an IntType object
	 */
	public static IntType newIntType(){
		return _IntType_obj;
	}
	
	/**
	 * Returns an CharType type object. This function guarantees it always returns the same CharType object.
	 * 
	 * @return		an CharType object
	 */
	public static CharType newCharType(){
		return _CharType_obj;
	}
	
	/**
	 * Returns an VoidType type object. This function guarantees it always returns the same VoidType object.
	 * 
	 * @return		an VoidType object
	 */
	public static VoidType newVoidType(){
		return _VoidType_obj;
	}
	
	public static StructType newStructType(String tag){
		return new StructType(tag);
	}
	
	public static UnionType newUnionType(String tag){
		return new UnionType(tag);
	}
	
	public static boolean isIntegerType(Type type){
		return (type instanceof IntType) || (type instanceof CharType);
	}
	
	public static boolean isFunctionPointer(Type type){
		return (type instanceof PointerType) ? (((PointerType)type).getBaseType() instanceof FunctionType) : false;
	}
	
	public static boolean isScalarType(Type type){
		return (type instanceof PointerType) || (isIntegerType(type));
	}
	
	public static boolean isVoidPointer(Type type){
		return (type instanceof PointerType) && (((PointerType)type).getBaseType() instanceof VoidType);
	}
	
	public static boolean isNullPointerConstant(Type type, Object value){
		return ((type instanceof IntType || type instanceof CharType) && value instanceof Number && ((Number)value).intValue() == 0);
	}
	
	public static boolean isAggregateType(Type type){
		return (type instanceof ArrayType) || (type instanceof StructOrUnionType);
	}
	
	public static boolean isCharArray(Type type){
		return (type instanceof ArrayType) && (((ArrayType)type).getBaseType() instanceof CharType);
	}
	
	public static boolean isPointerToChar(Type type){
		return (type instanceof PointerType) && (((PointerType)type).getBaseType() instanceof CharType);
	}
	
	private static IntType _IntType_obj = new IntType();
	private static CharType _CharType_obj = new CharType();
	private static VoidType _VoidType_obj = new VoidType();
}
