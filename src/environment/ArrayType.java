package environment;

/**
 * The ArrayType class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class ArrayType extends Type {

	/**
	 * Constructs an array type from base type and size.
	 *
	 * @param base_type 	the base type
	 * @param size			the size (element count)
	 */
	public ArrayType(Type base_type, int size) {
		_complete = true;
		_base_type = base_type;
		_size = size;
	}
	
	public ArrayType(Type base_type){
		_complete = false;
		_base_type = base_type;
		_size = 0;
	}

	@Override
	public boolean equals(Object rhs){
		if (!(rhs instanceof ArrayType)) return false;
		
		ArrayType arhs = (ArrayType)rhs;
		if (_complete && arhs._complete){
			if (_size != arhs._size) return false;
		}
		if (!(_base_type.equals(arhs._base_type))) return false;
		
		return true;
	}
	
	@Override
	public ArrayType shallowCopy(){
		return (_complete) ? (new ArrayType(_base_type, _size)) : (new ArrayType(_base_type));
	}
	
	@Override
	public int getSize() {

		return _base_type.getSize() * _size;
	}

	@Override
	public boolean isComplete() {

		return _complete;
	}
	
	public void setComplete(boolean complete){
		_complete = complete;
	}
	
	public Type getBaseType(){
		return _base_type;
	}
	
	public int getElementCount(){
		return _size;
	}
	
	public void setElementCount(int size){
		_size = size;
	}

	protected boolean _complete;
	protected Type _base_type;
	protected int _size;
}
