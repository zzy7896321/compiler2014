package environment;

/**
 * The Type class is the base class of all built-in and derived type classes.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public abstract class Type {

	/**
	 * Default constructor.
	 */
	public Type() {
	}

	/**
	 * Returns the size of the type.
	 * 	
	 * @return		the size of the type if it is complete, undefined otherwise
	 */
	public abstract int getSize();
	
	/**
	 * Returns true if the type is complete.
	 * 
	 * @return		true if the type is complete
	 */
	public abstract boolean isComplete();
	
	/**
	 * Returns the align requirement of the type.
	 * 
	 * @return		the align requirement
	 */
	public int getAlign(){
		return config.TargetConfig.align;
	}
	
	protected static final int _alignOffset(int offset, int align){
		int r = offset % align;
		if (r != 0) offset += (align - r);
		return offset;
	}

	public Type shallowCopy() {
		
		return this;
	}
	
}
