package environment;

public class AddressConstant {
	/**
	 * Constructs an address constant as result of ExpressionBase.checkEvaluate_NoGenerating(environment.Environment).
	 * base is set to null if the address is an constant cast from integer type.
	 * 
	 * @param base
	 * @param offset
	 */
	public AddressConstant(SymbolInfo base, int offset) {
		setBase(base);
		setOffset(offset);
		_mask = 0;
	}
	
	
	public SymbolInfo getBase() {
		return _base;
	}

	public AddressConstant setBase(SymbolInfo base) {
		this._base = base;
		return this;
	}

	public int getOffset() {
		return _offset;
	}

	public AddressConstant setOffset(int offset) {
		this._offset = offset;
		return this;
	}

	public AddressConstant setMask(int mask){
		_mask = mask;
		return this;
	}
	
	public int getMask(){
		return _mask;
	}
	
	public AddressConstant newAddrWithOffsetAddedTo(int offset){
		return new AddressConstant(_base, _offset + offset);
	}


	private SymbolInfo _base;
	private int _offset;
	private int _mask;

}
