package environment;

/**
 * The IntType class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class IntType extends BuiltinType {

	/**
	 * Default constructor.
	 */
	public IntType() {
	}

	@Override
	public int getSize() {

		return config.TargetConfig.int_size;
	}
	
	@Override
	public int getAlign(){
		return getSize();
	}

	@Override
	public boolean equals(Object rhs){
		return rhs instanceof IntType;
	}

	@Override
	public int getRank() {
		
		return 5;
	}

}
