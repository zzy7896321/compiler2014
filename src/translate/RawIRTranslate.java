package translate;

import graph.Block;
import graph.Graph;
import irpack.*;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;

import util.MyLinkedList;
import util.StringUtil;
import main.ErrorHandling;

/**
 * The RawIRTranslate class implements a translator from unoptimized IR to MIPS32 assembly code.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 3, 2014
 */
public final class RawIRTranslate {

	/**
	 * Translates data segment.
	 * 
	 * @param writer
	 * @param entry_table
	 * @return
	 * @throws IOException
	 */
	public static boolean translateDataSegment(Writer writer, ArrayList<StaticVarEntry> entry_table) throws IOException{
		
		allocateMemoryForStaticVariables(entry_table);	
		
		writeDataSegment(writer, entry_table);

		return true;
	}
	
	/**
	 * Allocates memory for static variables.
	 * 
	 * @param entry_table
	 */
	public static void allocateMemoryForStaticVariables(ArrayList<StaticVarEntry> entry_table){
		/* sort the entry table by size, to increase the efficiency of global variable memory access */
		Collections.sort(entry_table, new Comparator<StaticVarEntry>(){

			@Override
			public int compare(StaticVarEntry arg0, StaticVarEntry arg1) {
				
				return arg0.getMemLoc().getSize() - arg1.getMemLoc().getSize();
			}
			
		});
		
		int offset = 0;		// current offset
		
		/* allocate space for variables */
		for (StaticVarEntry entry : entry_table){
			/* allocate memory location */
			MemLoc memloc = entry.getMemLoc();
			assert(memloc.getLocation() == MemLoc.GLOBAL);		/* debug assert */
			
			memloc.setAllocated(true);
			if (memloc.getSize() != 1){
				// align on 4-byte boundary
				if ((offset & 0x3) != 0){					
					offset = (offset & ~0x3) + 0x4;
				}
			}
			memloc.setOffset(offset);
			offset += memloc.getSize();			
		}
	}
	
	/**
	 * Writes the data segment to output.
	 * 
	 * @param writer
	 * @param entry_table
	 * @throws IOException
	 */
	public static void writeDataSegment(Writer writer, ArrayList<StaticVarEntry> entry_table) throws IOException{
		/* expand data segment to 1 MB */
		printInstruction(writer, ".data 0x10000000", null);
		printInstruction(writer, ".space 0xFFFFF", null);
		writer.append("\n");
		
		/* data segment header*/
		writer.append(pre_instr_space).append(".data 0x10000000").append("\n");
		writer.append(pre_instr_space).append(".align 0\n");
		printInstruction(writer, ".word static_data_end", "# for malloc");
		writer.append("\n");
		
		int offset = 0;
		for (StaticVarEntry entry:entry_table){
			MemLoc memloc = entry.getMemLoc();
			if (memloc.getOffset() > offset){
				writer.append(pre_instr_space).append(".space ").append(Integer.toString(memloc.getOffset() - offset));
				if (write_comment) writer.append("\t# padding space");
				writer.append("\n");
			}
			offset = memloc.getOffset() + memloc.getSize();
			
			Object init_obj = entry.getInitializer();
			
			if (init_obj instanceof AggregateTypeInitializer){
				writeAggregateTypeDatum(writer, (AggregateTypeInitializer)init_obj, memloc.getSize());
			}
			
			else {
				writeSingleDatum(writer, init_obj, memloc.getSize());
			}
			
			if (write_comment) writer.append("\t# ").append(memloc.toString());
			writer.append("\n");
		}
		
		/* for malloc */
		if ((offset & 0x3) != 0){					
			writer.append(pre_instr_space).append(".space ").append(Integer.toString(4 - (offset & 0x3))).append("\n");
		}
		
		writer.append("static_data_end:\n");
		
		writer.append("\n");
	}

	/**
	 * Writes single datum to the output.
	 * 
	 * @param writer
	 * @param init_obj
	 * @param size
	 * @throws IOException
	 */
	public static void writeSingleDatum(Writer writer, Object init_obj, int size) throws IOException{
		writer.append(pre_instr_space);
		if (init_obj == null){
			writer.append(".space ").append(Integer.toString(size));
		}
		
		else if (init_obj instanceof String){
			writer.append(".asciiz \"").append(StringUtil.unescape_with_octal((String)init_obj)).append("\"");
			if (((String) init_obj).length() < size){
				writer.append("\n").append(pre_instr_space).append(".space ").append(Integer.toString(size - ((String) init_obj).length()));
			}
		}
		
		else if (init_obj instanceof Address){
			/* debug assert */
			assert(((MemLoc)((Address) init_obj).getBase()).getLocation() == MemLoc.GLOBAL);
			
			int addr = data_segment_start + ((MemLoc)((Address) init_obj).getBase()).getOffset() + ((Address) init_obj).getOffset();
			writer.append(".word 0x").append(Integer.toHexString(addr));
		}
		
		else if (init_obj instanceof Label){
			writer.append(".word ").append(init_obj.toString());
		}
		
		else if (init_obj instanceof Number){
			if (size == 1){
				writer.append(".byte ").append(Integer.toString(((Number) init_obj).byteValue()));
			}
			else {
				writer.append(".word ").append(Integer.toString(((Number) init_obj).intValue()));
			}
		}
		
		else {
			ErrorHandling.ReportError("unhandled category of initializer", ErrorHandling.WARNING);
			writer.append(".space ").append(Integer.toString(size));
		}
	}
	
	/**
	 * Writes aggregate type datum to the output
	 * 
	 * @param writer
	 * @param init
	 * @param size
	 * @throws IOException
	 */
	public static void writeAggregateTypeDatum(Writer writer, AggregateTypeInitializer init, int size) throws IOException{
		HashMap<Integer, ConstantValueEntry> content = init.getContent();
		
		Integer[] keys = content.keySet().toArray(new Integer[0]);
		Arrays.sort(keys);
		
		ConstantValueEntry entry = null;
		int tot_offset = 0;
		for (Integer offset : keys){
			if (offset > tot_offset){
				writer.append(pre_instr_space).append(".space ").append(Integer.toString(offset - tot_offset));
				writer.append("\n");
			}
			
			entry = content.get(offset);
			writeSingleDatum(writer, entry.value, entry.size);
			writer.append("\n");
			
			tot_offset = offset + entry.size;
		}
		
		if (tot_offset < size){
			writer.append(pre_instr_space).append(".space ").append(Integer.toString(size - tot_offset));
			writer.append("\n");
		}
	}
	
	/**
	 * Translates the text segment.
	 * 
	 * @param writer
	 * @param function_entry_table
	 * @return
	 * @throws IOException
	 */
	public static boolean translateTextSegment(Writer writer, LinkedList<FunctionEntry> function_entry_table) throws IOException{
		
		writer.append(pre_instr_space).append(".text\n");
		writer.append(pre_instr_space).append(".set noat\n");
		
		//main
		writer.append("\n");
		writer.append("main:\n");
		writer.append(pre_instr_space).append("jal LBFmain\n");
		writer.append(pre_instr_space).append("or $a0, $v0, $0\n");
		writer.append(pre_instr_space).append("ori $v0, $0, 17\n");
		writer.append(pre_instr_space).append("syscall\n");
		
		for (FunctionEntry entry : function_entry_table){
			writeFunctionEntry(writer, entry);
		}
		
		/* stdlib */
		String stdlib = semantics.StandardLibrary.get_stdlib_assembly();
		writer.append(stdlib);
		
		return true;
	}
	
	/**
	 * Writes the function to the text segment.
	 * 
	 * @param entry
	 */
	public static void writeFunctionEntry(Writer writer, FunctionEntry entry) throws IOException{
		
		/* currently everything is on the stack */
		{
			Argument ra = entry.getReturnValueAddress();
			if (ra != null) ra.setAllocated(true);
			
			for (Argument arg : entry.getArgumentTable()){
				arg.setAllocated(true);
			}
		}
		
		writer.append("\n");
		
		Graph<Block> cfg = entry.getControlFlowGraph();
		
		for (Block block : cfg.getNodeList())
		for (MyLinkedList<Quadruple>.Node node = block.getList().iterator(); node.hasNext();){
			node = node.next();
			Quadruple now = node.value();
			translateQuadruple(writer, now);
		}
	}

	/**
	 * Writes the entry block of a function.
	 * 
	 * @param writer
	 * @param entry
	 * @throws IOException
	 */
	public static void writeFunctionBegin(Writer writer, FunctionEntry entry) throws IOException{
		int frame_size = entry.getFrameSize();
		
		/*
		 * addiu $sp, $sp, frame_size
		 * sw $ra, (frame_size - 4)($sp)
		 * sw $fp, (frame_size - 8)($sp)
		 * addiu $fp, $sp, frame_size
		 * 
		 * push args if neccessary
		 * 
		 */
		
		printInstruction(writer, "sw $ra, " + "-4($sp)", null);
		printInstruction(writer, "sw $fp, " + "-8($sp)", null);
		printInstruction(writer, "addiu $fp, $sp, 0", null);
		if (frame_size > 32768){
			printInstruction(writer, "lui $at, 0x" + Integer.toHexString(frame_size >> 16), null);
			printInstruction(writer, "ori $at, $at, 0x" + Integer.toHexString(frame_size & 0xFFFF), null);
			printInstruction(writer, "subu $sp, $sp, $at", "frame size = " + Integer.toString(frame_size));
		}
		else {
			printInstruction(writer, "addiu $sp, $sp, -" + Integer.toString(frame_size), "frame size = " + Integer.toString(frame_size));
		}
	
		// push args onto the stack
		Argument rva = entry.getReturnValueAddress();
		if (rva != null && rva.isAllocated()){
			printInstruction(writer, "sw $v0, 0($fp)", null);
		}
		
		int argc = entry.getArgumentTable().size();
		argc = (argc > 4) ? 4 : argc;
		int now_arg = 0;
		for (Argument arg : entry.getArgumentTable()){
			if (arg.isAllocated()){
				printInstruction(writer, 
						new StringBuilder()
							.append("sw $a")
							.append(Integer.toString(now_arg))
							.append(", ")
							.append(Integer.toString(arg.getOffset()))
							.append("($fp)").toString(), null);
			}
			
			if (++now_arg >= argc) break;;
		}
	}

	
	public static void writeFunctionEnd(Writer writer, FunctionEntry entry) throws IOException{		
		printInstruction(writer, "or $sp, $fp, $0", null);
		printInstruction(writer, "lw $fp, -8($sp)", null);
		printInstruction(writer, "lw $ra, -4($sp)", null);
		printInstruction(writer, "jr $ra", null);
	}
	
	public static void printInstruction(Writer writer, String instr, String comment) throws IOException{
		writer.append(pre_instr_space).append(instr);
		if (write_comment && comment != null) writer.append("\t# ").append(comment);
		writer.append("\n");
	}

	@SuppressWarnings("deprecation")
	public static void translateQuadruple(Writer writer, Quadruple now) throws IOException {
		switch (now.getOpcode()){
		case Quadruple.LABEL:
			RawInstructionTranslation.LABEL(writer, now);
			break;

		case Quadruple.LA:
			RawInstructionTranslation.LA(writer, now);
			break;

		case Quadruple.LOAD:
			RawInstructionTranslation.LOAD(writer, now);
			break;

		case Quadruple.STORE:
			RawInstructionTranslation.STORE(writer, now);
			break;

		case Quadruple.MOV:
			RawInstructionTranslation.MOV(writer, now);
			break;

		case Quadruple.ADD:
			RawInstructionTranslation.ADD(writer, now);
			break;

		case Quadruple.SUB:
			RawInstructionTranslation.SUB(writer, now);
			break;

		case Quadruple.MUL:
			RawInstructionTranslation.MUL(writer, now);
			break;

		case Quadruple.DIV:
			RawInstructionTranslation.DIV(writer, now);
			break;

		case Quadruple.MOD:
			RawInstructionTranslation.MOD(writer, now);
			break;

		case Quadruple.SLL:
			RawInstructionTranslation.SLL(writer, now);
			break;

		case Quadruple.SRA:
			RawInstructionTranslation.SRA(writer, now);
			break;

		case Quadruple.SRL:
			RawInstructionTranslation.SRL(writer, now);
			break;

		case Quadruple.AND:
			RawInstructionTranslation.AND(writer, now);
			break;

		case Quadruple.OR:
			RawInstructionTranslation.OR(writer, now);
			break;

		case Quadruple.XOR:
			RawInstructionTranslation.XOR(writer, now);
			break;

		case Quadruple.NOR:
			RawInstructionTranslation.NOR(writer, now);
			break;

		case Quadruple.SLT:
			RawInstructionTranslation.SLT(writer, now);
			break;

		case Quadruple.SLE:
			RawInstructionTranslation.SLE(writer, now);
			break;

		case Quadruple.SGT:
			RawInstructionTranslation.SGT(writer, now);
			break;

		case Quadruple.SGE:
			RawInstructionTranslation.SGE(writer, now);
			break;

		case Quadruple.SEQ:
			RawInstructionTranslation.SEQ(writer, now);
			break;

		case Quadruple.SNE:
			RawInstructionTranslation.SNE(writer, now);
			break;

		case Quadruple.SLTU:
			RawInstructionTranslation.SLTU(writer, now);
			break;

		case Quadruple.BEQ:
			RawInstructionTranslation.BEQ(writer, now);
			break;

		case Quadruple.BNE:
			RawInstructionTranslation.BNE(writer, now);
			break;

		case Quadruple.J:
			RawInstructionTranslation.J(writer, now);
			break;

		case Quadruple.JAL:
			RawInstructionTranslation.JAL(writer, now);
			break;

		case Quadruple.CALL:
			RawInstructionTranslation.CALL(writer, now);
			break;

		case Quadruple.PUSH:
			RawInstructionTranslation.PUSH(writer, now);
			break;

		case Quadruple.MEMCPY:
			RawInstructionTranslation.MEMCPY(writer, now);
			break;

		case Quadruple.MOVRV:
			RawInstructionTranslation.MOVRV(writer, now);
			break;

		case Quadruple.STORERV:
			RawInstructionTranslation.STORERV(writer, now);
			break;
		
		case Quadruple.FUNCTIONENTRYBLOCK:
			writeFunctionBegin(writer, ((FunctionInfo)now).getEntry());
			break;
			
		case Quadruple.FUNCTIONEXITBLOCK:
			writeFunctionEnd(writer, ((FunctionInfo)now).getEntry());
			break;
			
		default:
			ErrorHandling.ReportError("Unknown opcode " + Integer.toString(now.getOpcode()), ErrorHandling.ERROR);
		}
	}
	
	final static String pre_instr_space = "\t";
	
	public static boolean write_comment = false;
	
	public final static int data_segment_start = 0x10000004;
	
	public final static int gp_offset = 0x8000 + 0x10000000 - data_segment_start;
}
