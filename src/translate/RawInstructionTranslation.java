package translate;

import irpack.*;

import java.io.IOException;
import java.io.Writer;

import main.ErrorHandling;

/**
 * The RawInstructionTranslation class implements an instruction translator from unoptimized IR to MIPS32 assembly code.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 3, 2014
 */
public class RawInstructionTranslation {

	public static void LABEL(Writer writer, Quadruple quad) throws IOException{
		writer.append(quad.toString()).append(":\n");
	}

	/**
	 * Load address of a memory location.
	 * 
	 * @param writer
	 * @param quad
	 * @throws IOException
	 */
	public static void LA(Writer writer, Quadruple quad) throws IOException{
		LoadStore now = (LoadStore)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		
		String[] address_rs = addressOf(writer, rs);
		String rs_reg = "$t2";
		
		if (address_rs[1] == null){
			printInstruction(writer, getOpString("la", rs_reg, address_rs[0]), null);
		}
		else {
			printInstruction(writer, getOpString("addiu", rs_reg, address_rs[0], address_rs[1]), null);
		}
		
		storeResult(writer, rd, rs_reg, quad.toString());
	}

	public static void LOAD(Writer writer, Quadruple quad) throws IOException{
		ErrorHandling.ReportError("No load instruction now.", ErrorHandling.ERROR);
	}

	public static void STORE(Writer writer, Quadruple quad) throws IOException{
		ErrorHandling.ReportError("No store instruction now.", ErrorHandling.ERROR);
	}

	public static void MOV(Writer writer, Quadruple quad) throws IOException{
		LoadStore now = (LoadStore)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		
		String rs_reg = "$t0";
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		storeResult(writer, rd, rs_reg, quad.toString());
	}

	public static void ADD(Writer writer, Quadruple quad) throws IOException{
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		if (rs instanceof IntConstant){
			int val = ((IntConstant) rs).getValue();
			if (val != 0 && !needLI(val)){
				Operand tmp = rs;
				rs = rt;
				rt = tmp;
			}
		}
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		if (rt instanceof IntConstant){
			int val = ((IntConstant) rt).getValue();
			if (needLI(val)){
				loadImmediate(writer, val, rt_reg);
				printInstruction(writer, getOpString("addu", rd_reg, rs_reg, rt_reg), null);
			}
			else {
				printInstruction(writer, getOpString("addiu", rd_reg, rs_reg, Integer.toString(val)), null);
			}
		}
		else {
			loadOperand(writer, rt, rt_reg);
			printInstruction(writer, getOpString("addu", rd_reg, rs_reg, rt_reg), null);
		}
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void SUB(Writer writer, Quadruple quad) throws IOException{
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		if (rt instanceof IntConstant){
			int val = ((IntConstant) rt).getValue();
			if (needLI(-val)){
				loadImmediate(writer, val, rt_reg);
				printInstruction(writer, getOpString("subu", rd_reg, rs_reg, rt_reg), null);
			}
			else {
				printInstruction(writer, getOpString("addiu", rd_reg, rs_reg, Integer.toString(-val)), null);
			}
		}
		else {
			loadOperand(writer, rt, rt_reg);
			printInstruction(writer, getOpString("subu", rd_reg, rs_reg, rt_reg), null);
		}
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void MUL(Writer writer, Quadruple quad) throws IOException{
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		rt_reg = loadALUOperandToReg(writer, rt, rt_reg);
		
		printInstruction(writer, getOpString("mul", rd_reg, rs_reg, rt_reg), null);
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void DIV(Writer writer, Quadruple quad) throws IOException{
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		rt_reg = loadALUOperandToReg(writer, rt, rt_reg);
		
		printInstruction(writer, getOpString("div", rs_reg, rt_reg), null);
		printInstruction(writer, getOpString("mflo", rd_reg), null);
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void MOD(Writer writer, Quadruple quad) throws IOException{
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		rt_reg = loadALUOperandToReg(writer, rt, rt_reg);
		
		printInstruction(writer, getOpString("div", rs_reg, rt_reg), null);
		printInstruction(writer, getOpString("mfhi", rd_reg), null);
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void SLL(Writer writer, Quadruple quad) throws IOException{
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		if (rt instanceof IntConstant){
			int val = ((IntConstant) rt).getValue() & 0x1F;
			printInstruction(writer, getOpString("sll", rd_reg, rs_reg, Integer.toString(val)), null);
		}
		else {
			loadOperand(writer, rt, rt_reg);
			printInstruction(writer, getOpString("sllv", rd_reg, rs_reg, rt_reg), null);
		}
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void SRA(Writer writer, Quadruple quad) throws IOException{
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		if (rt instanceof IntConstant){
			int val = ((IntConstant) rt).getValue() & 0x1F;
			printInstruction(writer, getOpString("sra", rd_reg, rs_reg, Integer.toString(val)), null);
		}
		else {
			loadOperand(writer, rt, rt_reg);
			printInstruction(writer, getOpString("srav", rd_reg, rs_reg, rt_reg), null);
		}
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void SRL(Writer writer, Quadruple quad) throws IOException{
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		if (rt instanceof IntConstant){
			int val = ((IntConstant) rt).getValue() & 0x1F;
			printInstruction(writer, getOpString("srl", rd_reg, rs_reg, Integer.toString(val)), null);
		}
		else {
			loadOperand(writer, rt, rt_reg);
			printInstruction(writer, getOpString("srlv", rd_reg, rs_reg, rt_reg), null);
		}
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void AND(Writer writer, Quadruple quad) throws IOException{
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		if (rt instanceof IntConstant){
			int val = ((IntConstant) rt).getValue();
			if (needLIU(val)){
				loadImmediate(writer, val, rt_reg);
				printInstruction(writer, getOpString("and", rd_reg, rs_reg, rt_reg), null);
			}
			else {
				printInstruction(writer, getOpString("andi", rd_reg, rs_reg, "0x" + Integer.toHexString(val)), null);
			}
		}
		else {
			loadOperand(writer, rt, rt_reg);
			printInstruction(writer, getOpString("and", rd_reg, rs_reg, rt_reg), null);
		}
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void OR(Writer writer, Quadruple quad) throws IOException{
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		if (rt instanceof IntConstant){
			int val = ((IntConstant) rt).getValue();
			if (needLIU(val)){
				loadImmediate(writer, val, rt_reg);
				printInstruction(writer, getOpString("or", rd_reg, rs_reg, rt_reg), null);
			}
			else {
				printInstruction(writer, getOpString("or", rd_reg, rs_reg, "0x" + Integer.toHexString(val)), null);
			}
		}
		else {
			loadOperand(writer, rt, rt_reg);
			printInstruction(writer, getOpString("or", rd_reg, rs_reg, rt_reg), null);
		}
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void XOR(Writer writer, Quadruple quad) throws IOException{
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		if (rt instanceof IntConstant){
			int val = ((IntConstant) rt).getValue();
			if (needLIU(val)){
				loadImmediate(writer, val, rt_reg);
				printInstruction(writer, getOpString("xor", rd_reg, rs_reg, rt_reg), null);
			}
			else {
				printInstruction(writer, getOpString("xori", rd_reg, rs_reg, "0x" + Integer.toHexString(val)), null);
			}
		}
		else {
			loadOperand(writer, rt, rt_reg);
			printInstruction(writer, getOpString("xor", rd_reg, rs_reg, rt_reg), null);
		}
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void NOR(Writer writer, Quadruple quad) throws IOException{
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		rt_reg = loadALUOperandToReg(writer, rt, rt_reg);
		
		printInstruction(writer, getOpString("nor", rd_reg, rs_reg, rt_reg), null);
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void SLT(Writer writer, Quadruple quad) throws IOException{
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		if (rt instanceof IntConstant){
			int val = ((IntConstant) rt).getValue();
			if (needLI(val)){
				loadImmediate(writer, val, rt_reg);
				printInstruction(writer, getOpString("slt", rd_reg, rs_reg, rt_reg), null);
			}
			else {
				printInstruction(writer, getOpString("slti", rd_reg, rs_reg, Integer.toString(val)), null);
			}
		}
		else {
			loadOperand(writer, rt, rt_reg);
			printInstruction(writer, getOpString("slt", rd_reg, rs_reg, rt_reg), null);
		}
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void SLE(Writer writer, Quadruple quad) throws IOException{
		/*
		 * slt rd, rt, rs
		 * xori rd, rd, 1		# !(rt < rs)
		 * */
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rt_reg = loadALUOperandToReg(writer, rt, rt_reg);
		
		if (rs instanceof IntConstant){
			int val = ((IntConstant) rs).getValue();
			if (needLI(val)){
				loadImmediate(writer, val, rs_reg);
				printInstruction(writer, getOpString("slt", rd_reg, rt_reg, rs_reg), null);
			}
			else {
				printInstruction(writer, getOpString("slti", rd_reg, rt_reg, Integer.toString(val)), null);
			}
		}
		else {
			loadOperand(writer, rs, rs_reg);
			printInstruction(writer, getOpString("slt", rd_reg, rt_reg, rs_reg), null);
		}
		
		printInstruction(writer, getOpString("xori", rd_reg, rd_reg, "1"), null);
		
		storeResult(writer, rd, rd_reg, quad.toString());
	}

	public static void SGT(Writer writer, Quadruple quad) throws IOException{
		/*
		 * slt rd, rt, rs		# rt < rs
		 * */
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rt_reg = loadALUOperandToReg(writer, rt, rt_reg);
		
		if (rs instanceof IntConstant){
			int val = ((IntConstant) rs).getValue();
			if (needLI(val)){
				loadImmediate(writer, val, rs_reg);
				printInstruction(writer, getOpString("slt", rd_reg, rt_reg, rs_reg), null);
			}
			else {
				printInstruction(writer, getOpString("slti", rd_reg, rt_reg, Integer.toString(val)), null);
			}
		}
		else {
			loadOperand(writer, rs, rs_reg);
			printInstruction(writer, getOpString("slt", rd_reg, rt_reg, rs_reg), null);
		}
		
		storeResult(writer, rd, rd_reg, quad.toString());
	}

	public static void SGE(Writer writer, Quadruple quad) throws IOException{
		/*
		 * slt rd, rs, rt
		 * xori rd, rd, 1		# !(rs < rt)
		 * */
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		if (rt instanceof IntConstant){
			int val = ((IntConstant) rt).getValue();
			if (needLI(val)){
				loadImmediate(writer, val, rt_reg);
				printInstruction(writer, getOpString("slt", rd_reg, rs_reg, rt_reg), null);
			}
			else {
				printInstruction(writer, getOpString("slti", rd_reg, rs_reg, Integer.toString(val)), null);
			}
		}
		else {
			loadOperand(writer, rt, rt_reg);
			printInstruction(writer, getOpString("slt", rd_reg, rs_reg, rt_reg), null);
		}
		
		printInstruction(writer, getOpString("xori", rd_reg, rd_reg, "1"), null);
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void SEQ(Writer writer, Quadruple quad) throws IOException{
		/*
		 * xor rd, rs, rt
		 * sltiu rd, rd, 1 
		 */
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		if (rt instanceof IntConstant){
			int val = ((IntConstant) rt).getValue();
			if (needLIU(val)){
				loadImmediate(writer, val, rt_reg);
				printInstruction(writer, getOpString("xor", rd_reg, rs_reg, rt_reg), null);
			}
			else {
				printInstruction(writer, getOpString("xori", rd_reg, rs_reg, "0x" + Integer.toHexString(val)), null);
			}
		}
		else {
			loadOperand(writer, rt, rt_reg);
			printInstruction(writer, getOpString("xor", rd_reg, rs_reg, rt_reg), null);
		}
	
		printInstruction(writer, getOpString("sltiu", rd_reg, rd_reg, "1"), null);
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void SNE(Writer writer, Quadruple quad) throws IOException{
		/*
		 * xor rd, rs, rt
		 * sltu rd, $0, rd 
		 */
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		if (rt instanceof IntConstant){
			int val = ((IntConstant) rt).getValue();
			if (needLIU(val)){
				loadImmediate(writer, val, rt_reg);
				printInstruction(writer, getOpString("xor", rd_reg, rs_reg, rt_reg), null);
			}
			else {
				printInstruction(writer, getOpString("xori", rd_reg, rs_reg, "0x" + Integer.toHexString(val)), null);
			}
		}
		else {
			loadOperand(writer, rt, rt_reg);
			printInstruction(writer, getOpString("xor", rd_reg, rs_reg, rt_reg), null);
		}
	
		printInstruction(writer, getOpString("sltu", rd_reg, "$0", rd_reg), null);
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void SLTU(Writer writer, Quadruple quad) throws IOException{
		ALUOp now = (ALUOp)quad;
		Operand rd = now.getRd();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rd_reg = "$t2";
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		if (rt instanceof IntConstant){
			int val = ((IntConstant) rt).getValue();
			if (needLIU(val)){
				loadImmediate(writer, val, rt_reg);
				printInstruction(writer, getOpString("sltu", rd_reg, rs_reg, rt_reg), null);
			}
			else {
				printInstruction(writer, getOpString("sltiu", rd_reg, rs_reg, "0x" + Integer.toHexString(val)), null);
			}
		}
		else {
			loadOperand(writer, rt, rt_reg);
			printInstruction(writer, getOpString("sltu", rd_reg, rs_reg, rt_reg), null);
		}
		
		storeResult(writer, rd, rd_reg, quad.toString());	
	}

	public static void BEQ(Writer writer, Quadruple quad) throws IOException{
		Branch now = (Branch)quad;
		Operand target = now.getTarget();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		rt_reg = loadALUOperandToReg(writer, rt, rt_reg);
		
		printInstruction(writer, getOpString("beq", rs_reg, rt_reg, target.toString()), quad.toString());
	}

	public static void BNE(Writer writer, Quadruple quad) throws IOException{
		Branch now = (Branch)quad;
		Operand target = now.getTarget();
		Operand rs = now.getRs();
		Operand rt = now.getRt();
		
		String rs_reg = "$t0";
		String rt_reg = "$t1";
		
		rs_reg = loadALUOperandToReg(writer, rs, rs_reg);
		
		rt_reg = loadALUOperandToReg(writer, rt, rt_reg);
		
		printInstruction(writer, getOpString("bne", rs_reg, rt_reg, target.toString()), quad.toString());
	}

	public static void J(Writer writer, Quadruple quad) throws IOException{
		Jump now = (Jump)quad;
		Operand target = now.getTarget();
		
		if (target instanceof Label || target instanceof IntConstant){
			printInstruction(writer, getOpString("j", target.toString()), quad.toString());
		}
		else {
			String target_reg = "$t0";
			target_reg = loadALUOperandToReg(writer, target, target_reg);
			printInstruction(writer, getOpString("jr", target_reg), quad.toString());
		}
	}

	public static void JAL(Writer writer, Quadruple quad) throws IOException{
		Jump now = (Jump)quad;
		Operand target = now.getTarget();
		
		if (target instanceof Label || target instanceof IntConstant){
			printInstruction(writer, getOpString("jal", target.toString()), quad.toString());
		}
		else {
			String target_reg = "$t0";
			target_reg = loadALUOperandToReg(writer, target, target_reg);
			printInstruction(writer, getOpString("jalr", target_reg), quad.toString());
		}
	}

	public static void CALL(Writer writer, Quadruple quad) throws IOException{
		JAL(writer, quad);		// currently does not need to save anything
	}

	public static void PUSH(Writer writer, Quadruple quad) throws IOException{
		Push now = (Push)quad;
		
		int offset = now.getOffset();
		String mem_addr_str = getMemoryAddressString(new String[]{"$sp", Integer.toString(offset)});
		
		String operand_reg = "$t0";
		operand_reg = loadALUOperandToReg(writer, now.getSrc(), operand_reg);
		
		/* return value address */
		if (now.getNum() == -2){
			printInstruction(writer, getOpString("sw", operand_reg, mem_addr_str), quad.toString());
		}
		else if (now.getNum() == -1){
			printInstruction(writer, getOpString("or", "$v0", operand_reg, "$0"), quad.toString());
		}
		
		else if (now.getNum() < 4){
			printInstruction(writer, getOpString("or", "$a" + Integer.toString(now.getNum()), operand_reg, "$0"), quad.toString());
		}
		
		else {
			printInstruction(writer, getOpString("sw", operand_reg, mem_addr_str), quad.toString());
		}
	}

	@Deprecated
	public static void MEMCPY(Writer writer, Quadruple quad) throws IOException{
		Memcpy now = (Memcpy)quad;
		
		Operand dest = now.getDest();
		Operand src = now.getSrc();
		int size = now.getSize();
		
		String dest_reg = "$a0";
		String src_reg = "$a1";
		String size_reg = "$a2";
		
		if (size != 0){
			loadALUOperandToReg(writer, dest, dest_reg);
			loadALUOperandToReg(writer, src, src_reg);
			loadALUOperandToReg(writer, op.constant(size), size_reg);
			
			printInstruction(writer, getOpString("jal", "LBFaligned_memcpy"), quad.toString());
		}
	}

	public static void MOVRV(Writer writer, Quadruple quad) throws IOException{
		MoveRV now = (MoveRV)quad;
		Operand dest = now.getDest();
		
		storeResult(writer, dest, "$v0", quad.toString());
	}

	public static void STORERV(Writer writer, Quadruple quad) throws IOException{
		StoreRV now = (StoreRV)quad;
		Operand src = now.getSrc();
		
		if (loadALUOperandToReg(writer, src, "$v0") == "$0"){
			printInstruction(writer, "or $v0, $0, $0", quad.toString());
		}
		else {
			writer.append("\t\t# ").append(quad.toString()).append("\n");
		}
	}
	
	public static void printInstruction(Writer writer, String instr, String comment) throws IOException{
		writer.append(RawIRTranslate.pre_instr_space).append(instr);
		if (RawIRTranslate.write_comment && comment != null) writer.append("\t# ").append(comment);
		writer.append("\n");
	}

	/**
	 * Calculates the address of rs. Necessary load may be inserted.
	 * <p>
	 * ret[0] is the base, ret[1] is the offset.
	 * 
	 * @param rs
	 * @return
	 */
	public static String[] addressOf(Writer writer, Operand rs) throws IOException{
	
		if (rs instanceof Indirection){
			Indirection ind = (Indirection) rs;
			String[] addr = direct_addressOf(writer, ((Indirection) rs).getSource());
			
			if (((Indirection) rs).getSource() instanceof Addressable){
				printInstruction(writer, getOpString("lw", "$at", getMemoryAddressString(addr)), null);
				return new String[]{"$at", Integer.toString(ind.getOffset())};
			}
			
			else {
				return addr;
			}
		}
		
		return direct_addressOf(writer, rs);
	}
	
	/**
	 * Calculates the address of rs itself. Necessary load may be inserted.
	 * <p>
	 * ret[0] is the base, ret[1] is the offset.
	 * 
	 * @param rs
	 * @return
	 */
	public static String[] direct_addressOf(Writer writer, Operand rs) throws IOException{
		int offset = 0;	
		
		if (rs instanceof Address){
			offset = ((Address) rs).getOffset();
			rs = ((Address) rs).getBase();
		}
		
		if (rs instanceof Global){
			Global gmem = (Global)rs;
			
			if (gmem.isAllocated()){
				offset += gmem.getOffset() - RawIRTranslate.gp_offset;
				if (needLI(offset)){
					int imm[] = separateImmediate(gp + offset);
					printInstruction(writer, "lui $at, 0x" + Integer.toHexString(imm[0]), Integer.toString(offset));
					return new String[]{"$at", Integer.toString(imm[1])};
				}
				
				return new String[]{"$gp", Integer.toString(offset)};
			}
			
			// else no address can be loaded
		}
		
		else if (rs instanceof MemLoc && ((MemLoc)rs).atLocal()){
			MemLoc lmem = (MemLoc) rs;
			
			if (lmem.isAllocated()){
				if (lmem instanceof Argument && ((Argument)lmem).isPassedByAddress()){					
					printInstruction(writer, getOpString("lw", "$at", Integer.toString(lmem.getOffset()) + "($fp)"), null);
					
					if (needLI(offset)){
						int imm[] = separateImmediate(offset);
						printInstruction(writer, "lui $k0, 0x" + Integer.toHexString(imm[0]), Integer.toString(offset));
						printInstruction(writer, "addu $at, $k0, $at", null);
						return new String[]{"$at", Integer.toString(imm[1])};
					}
					
					return new String[]{"$at", Integer.toString(offset)};
					
				}
				else {
					offset += lmem.getOffset();
					if (needLI(offset)){
						int imm[] = separateImmediate(offset);
						printInstruction(writer, "lui $at, 0x" + Integer.toHexString(imm[0]), Integer.toString(offset));
						printInstruction(writer, "addu $at, $fp, $at", null);
						return new String[]{"$at", Integer.toString(imm[1])};
					}
	
					return new String[]{"$fp", Integer.toString(offset)};
				}
			}
			
			//else no address can be loaded
		}
		
		else if (rs instanceof Label){
			if (offset > 0){
				return new String[]{rs.toString() + " + " + Integer.toString(offset), null};
			}
			else if (offset < 0){
				return new String[]{rs.toString() + " " + Integer.toString(offset), null};
			}
			
			return new String[]{rs.toString(), null};
		}
		
		ErrorHandling.ReportError("cannot load address of " + rs.toString(), ErrorHandling.ERROR);
		
		return null;
	}
	
	/**
	 * Returns if the immediate needs a load.
	 * 
	 * @param immediate
	 * @return
	 */
	public static boolean needLI(int immediate){
		return immediate < -0X8000 || immediate > 0X7FFF;
	}
	
	/**
	 * Returns if the unsigned immediate needs a load.
	 * 
	 * @param immediate
	 * @return
	 */
	public static boolean needLIU(int immediate){
		return immediate < 0 || immediate > 0xFFFF;
	}
	
	/**
	 * Returns an array containing the upper-half and lower-half at location 0 and 1 respectively.
	 * 
	 * @param immediate
	 * @return
	 */
	public static int[] separateImmediate(int immediate){
		int ret[] = new int[2];
		
		ret[0] = immediate >>> 16;
		immediate -= (ret[0] << 16);
		
		if (immediate < -0x8000){
			ret[0] -= 1;
			ret[1] = (immediate + 0x10000);
		}
		
		else if (immediate > 0x7FFF){
			ret[0] += 1;
			ret[1] = (immediate - 0x10000);
		}
		
		else {
			ret[1] = immediate;
		}
		
		return ret;
	}
	
	
	public static String getMemoryAddressString(String[] addr){
		return (addr[1] == null) ? addr[0] :
			new StringBuilder().append(addr[1]).append("(").append(addr[0]).append(")").toString();
	}
	
	/**
	 * Stores result from the designated register.	// may use $t9
	 * 
	 * @param writer
	 */
	public static void storeResult(Writer writer, Operand rd, String reg, String comment) throws IOException{
		String[] addr = addressOf(writer, rd);
				
		
		if (rd instanceof BitField){
			BitField bitfield = (BitField)rd;
			int mask = bitfield.getMask();
			int rshift_bits = Integer.numberOfLeadingZeros(mask);
			int lshift_bits = rshift_bits + Integer.numberOfTrailingZeros(mask);
			
			if (lshift_bits > 0)
				printInstruction(writer, getOpString("sll", reg, reg, Integer.toString(lshift_bits)), "mask = 0x" + Integer.toHexString(mask));
			if (rshift_bits > 0)
				printInstruction(writer, getOpString("srl", reg, reg, Integer.toString(rshift_bits)), null);
			if (lshift_bits > 0){
				
				printInstruction(writer, getOpString("lw", "$k0", getMemoryAddressString(addr)), null);
				loadImmediate(writer, ~mask, "$k1");
				printInstruction(writer, getOpString("and", "$k0", "$k0", "$k1"), null);
				printInstruction(writer, getOpString("or", reg, reg, "$k0"), null);
			}
		}
		
		int size = 4;
		
		if (rd instanceof MemLoc){
			size = ((MemLoc)rd).getObjectSize();
		}
		
		else if (rd instanceof Indirection){
			size = ((Indirection) rd).getSize();
		}
		else {
			/* debug assert */
			assert(false) : "invalid rd in storeResult";
		}
		
		if (size == 1){
			printInstruction(writer, getOpString("sb", reg, getMemoryAddressString(addr)), comment);
		}
		else {
			printInstruction(writer, getOpString("sw", reg, getMemoryAddressString(addr)), comment);
		}
	}
	
//	public static void storeAddress(Writer writer, Operand rd, String reg, String comment) throws IOException{
//		String[] addr = direct_addressOf(writer, rd);
//		
//		if (addr[1] == null){
//			printInstruction(writer, getOpString("la", "$at", addr[0]), null);
//			addr[0] = "$at";
//			addr[1] = "0";
//		}
//		
//		printInstruction(writer, getOpString("sw", reg, getMemoryAddressString(addr)), comment);
//	}
	
	/**
	 * Load operand to the designated register.
	 * 
	 * @param writer
	 * @param rs
	 * @param reg
	 */
	public static void loadOperand(Writer writer, Operand rs, String reg) throws IOException{
		String[] addr = addressOf(writer, rs);
		
		if (addr[1] == null){
			printInstruction(writer, getOpString("la", reg, addr[0]), null);
			return;
		}
		
		int size = 4;
		
		if (rs instanceof MemLoc){
			size = ((MemLoc)rs).getObjectSize();
		}
		
		else if (rs instanceof Indirection){
			size = ((Indirection) rs).getSize();
		}
		else {
			/* debug assert */
			assert(false) : "invalid rd in loadOperand";
		}
		
		if (size == 1){
			printInstruction(writer, getOpString("lb", reg, getMemoryAddressString(addr)), null);
		}
		else {
			printInstruction(writer, getOpString("lw", reg, getMemoryAddressString(addr)), null);
		}
		
		if (rs instanceof BitField){
			BitField bitfield = (BitField)rs;
			int mask = bitfield.getMask();
			int lshift_bits = Integer.numberOfLeadingZeros(mask) ;
			int rshift_bits = lshift_bits + Integer.numberOfTrailingZeros(mask);
			
			if (lshift_bits > 0)
				printInstruction(writer, getOpString("sll", reg, reg, Integer.toString(lshift_bits)), "mask = 0x" + Integer.toHexString(mask));
			if (rshift_bits > 0)
				printInstruction(writer, getOpString("sra", reg, reg, Integer.toString(rshift_bits)), null);
		}
	}
	
	/**
	 * Load ALU operand to designated register. The operand could be constant or memory location.
	 * 
	 * @param writer
	 * @param rs
	 * @param reg		the register where the register is actually loaded to (could be $0)
	 * @return
	 * @throws IOException
	 */
	public static String loadALUOperandToReg(Writer writer, Operand rs, String reg) throws IOException{
		if (rs instanceof IntConstant){
			int val = ((IntConstant) rs).getValue();
			if (val == 0) 
				reg = "$0";
			else {
				loadImmediate(writer, val, reg);
			}
		}
		else {
			loadOperand(writer, rs, reg);
		}
		
		return reg;
	}
	
	public static void loadImmediate(Writer writer, int val, String reg) throws IOException{
		if (!needLI(val)){
			printInstruction(writer, getOpString("addiu", reg, "$0", Integer.toString(val)), null);
		}
		
		else if (!needLIU(val)){
			printInstruction(writer, getOpString("ori", reg, "$0", "0x" + Integer.toHexString(val)), null);
		}
		else{
			int[] imm = separateImmediate(val);
			printInstruction(writer, getOpString("lui", reg, "0x" + Integer.toHexString(imm[0])), null);
			printInstruction(writer, getOpString("addiu", reg, reg, Integer.toString(imm[1])), null);
		}
	}
	
	public static String getOpString(String op, String rd){
		return String.format("%s %s", op, rd);
	}
	
	public static String getOpString(String op, String rd, String rs){
		return String.format("%s %s, %s", op, rd, rs);
	}
	
	public static String getOpString(String op, String rd, String rs, String rt){
		return String.format("%s %s, %s, %s", op, rd, rs, rt);
	}
	
//	public static void main(String args[]) throws IOException{
//		System.out.println("testing separateImmediate");
//		
//		int ret[] = separateImmediate(0XBBBBBBBC);
//		System.out.println(Integer.toHexString(ret[0]));
//		System.out.println(Integer.toHexString(ret[1]));
//		System.out.println(Integer.toHexString((ret[0] << 16) + ret[1]));
//	}
	
	public final static int gp = 0x10008000;
	
}
