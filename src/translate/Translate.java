package translate;

import irpack.Address;
import irpack.AggregateTypeInitializer;
import irpack.ConstantValueEntry;
import irpack.FunctionEntry;
import irpack.Label;
import irpack.MemLoc;
import irpack.StaticVarEntry;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;

import main.ErrorHandling;
import util.StringUtil;

/**
 * The Translate class implements a translator from optimized IR to MIPS32 assembly code.
 * Data segment translation is imported from RawIRTranslate.
 * Text segment translation is re-implemented to accommodate to optimized IR.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class Translate {
	
	/**
	 * Translates the text segment.
	 * 
	 * @param writer
	 * @param function_entry_table
	 * @return
	 * @throws IOException
	 */
	public static boolean translateTextSegment(Writer writer, LinkedList<FunctionEntry> function_entry_table) throws IOException{
		
		writer.append(pre_instr_space).append(".text\n");
		writer.append(pre_instr_space).append(".set noat\n");
		
		//main
		writer.append("\n");
		writer.append("main:\n");
		writer.append(pre_instr_space).append("jal LBFmain\n");
		writer.append(pre_instr_space).append("or $a0, $v0, $0\n");
		writer.append(pre_instr_space).append("ori $v0, $0, 17\n");
		writer.append(pre_instr_space).append("syscall\n");
		writer.append("\n");
		
		HashMap<String, FunctionEntry> func_table = new HashMap<String, FunctionEntry>();
		for (FunctionEntry entry : function_entry_table){
			String label_name = Label.function_head + entry.getName();
			
			func_table.put(label_name, entry);
		}
		InstructionTranslation.setFunctionEntryTable(func_table);
		
		for (FunctionEntry entry : function_entry_table){
			InstructionTranslation.writeFunctionEntry(writer, entry);
		}
		
		/* stdlib */
		String stdlib = semantics.StandardLibrary.get_stdlib_assembly();
		writer.append(stdlib);
		
		writer.flush();
		
		return true;
	}
	
	/**
	 * Translates data segment.
	 * 
	 * @param writer
	 * @param entry_table
	 * @return
	 * @throws IOException
	 */
	public static boolean translateDataSegment(Writer writer, ArrayList<StaticVarEntry> entry_table) throws IOException{
		
		allocateMemoryForStaticVariables(entry_table);	
		
		writeDataSegment(writer, entry_table);

		return true;
	}
	
	/**
	 * Allocates memory for static variables.
	 * 
	 * @param entry_table
	 */
	public static void allocateMemoryForStaticVariables(ArrayList<StaticVarEntry> entry_table){
		/* sort the entry table by size, to increase the efficiency of global variable memory access */
		Collections.sort(entry_table, new Comparator<StaticVarEntry>(){

			@Override
			public int compare(StaticVarEntry arg0, StaticVarEntry arg1) {
				
				return arg0.getMemLoc().getSize() - arg1.getMemLoc().getSize();
			}
			
		});
		
		int offset = 0;		// current offset
		
		/* allocate space for variables */
		for (StaticVarEntry entry : entry_table){
			/* allocate memory location */
			MemLoc memloc = entry.getMemLoc();
			assert(memloc.getLocation() == MemLoc.GLOBAL);		/* debug assert */
			
			memloc.setAllocated(true);
			if (memloc.getSize() != 1){
				// align on 4-byte boundary
				if ((offset & 0x3) != 0){					
					offset = (offset & ~0x3) + 0x4;
				}
			}
			memloc.setOffset(offset);
			offset += memloc.getSize();			
		}
	}
	
	/**
	 * Writes the data segment to output.
	 * 
	 * @param writer
	 * @param entry_table
	 * @throws IOException
	 */
	public static void writeDataSegment(Writer writer, ArrayList<StaticVarEntry> entry_table) throws IOException{
		/* expand data segment to 1 MB */
		printInstruction(writer, ".data 0x10000000", null);
		printInstruction(writer, ".space 0xFFFFF", null);
		writer.append("\n");
		
		/* data segment header*/
		writer.append(pre_instr_space).append(".data 0x10000000").append("\n");
		writer.append(pre_instr_space).append(".align 0\n");
		printInstruction(writer, ".word static_data_end", "# for malloc");
		writer.append("\n");
		
		int offset = 0;
		for (StaticVarEntry entry:entry_table){
			MemLoc memloc = entry.getMemLoc();
			if (memloc.getOffset() > offset){
				writer.append(pre_instr_space).append(".space ").append(Integer.toString(memloc.getOffset() - offset));
				if (write_comment) writer.append("\t# padding space");
				writer.append("\n");
			}
			offset = memloc.getOffset() + memloc.getSize();
			
			Object init_obj = entry.getInitializer();
			
			if (init_obj instanceof AggregateTypeInitializer){
				writeAggregateTypeDatum(writer, (AggregateTypeInitializer)init_obj, memloc.getSize());
			}
			
			else {
				writeSingleDatum(writer, init_obj, memloc.getSize());
			}
			
			if (write_comment) writer.append("\t# ").append(memloc.toString());
			writer.append("\n");
		}
		
		/* for malloc */
		if ((offset & 0x3) != 0){					
			writer.append(pre_instr_space).append(".space ").append(Integer.toString(4 - (offset & 0x3))).append("\n");
		}
		
		writer.append("static_data_end:\n");
		
		writer.append("\n");
	}

	/**
	 * Writes single datum to the output.
	 * 
	 * @param writer
	 * @param init_obj
	 * @param size
	 * @throws IOException
	 */
	public static void writeSingleDatum(Writer writer, Object init_obj, int size) throws IOException{
		writer.append(pre_instr_space);
		if (init_obj == null){
			writer.append(".space ").append(Integer.toString(size));
		}
		
		else if (init_obj instanceof String){
			writer.append(".asciiz \"").append(StringUtil.unescape_with_octal((String)init_obj)).append("\"");
			if (((String) init_obj).length() < size){
				writer.append("\n").append(pre_instr_space).append(".space ").append(Integer.toString(size - ((String) init_obj).length()));
			}
		}
		
		else if (init_obj instanceof Address){
			/* debug assert */
			assert(((MemLoc)((Address) init_obj).getBase()).getLocation() == MemLoc.GLOBAL);
			
			int addr = data_segment_start + ((MemLoc)((Address) init_obj).getBase()).getOffset() + ((Address) init_obj).getOffset();
			writer.append(".word 0x").append(Integer.toHexString(addr));
		}
		
		else if (init_obj instanceof Label){
			writer.append(".word ").append(init_obj.toString());
		}
		
		else if (init_obj instanceof Number){
			if (size == 1){
				writer.append(".byte ").append(Integer.toString(((Number) init_obj).byteValue()));
			}
			else {
				writer.append(".word ").append(Integer.toString(((Number) init_obj).intValue()));
			}
		}
		
		else {
			ErrorHandling.ReportError("unhandled category of initializer", ErrorHandling.WARNING);
			writer.append(".space ").append(Integer.toString(size));
		}
	}
	
	/**
	 * Writes aggregate type datum to the output
	 * 
	 * @param writer
	 * @param init
	 * @param size
	 * @throws IOException
	 */
	public static void writeAggregateTypeDatum(Writer writer, AggregateTypeInitializer init, int size) throws IOException{
		HashMap<Integer, ConstantValueEntry> content = init.getContent();
		
		Integer[] keys = content.keySet().toArray(new Integer[0]);
		Arrays.sort(keys);
		
		ConstantValueEntry entry = null;
		int tot_offset = 0;
		for (Integer offset : keys){
			if (offset > tot_offset){
				writer.append(pre_instr_space).append(".space ").append(Integer.toString(offset - tot_offset));
				writer.append("\n");
			}
			
			entry = content.get(offset);
			writeSingleDatum(writer, entry.value, entry.size);
			writer.append("\n");
			
			tot_offset = offset + entry.size;
		}
		
		if (tot_offset < size){
			writer.append(pre_instr_space).append(".space ").append(Integer.toString(size - tot_offset));
			writer.append("\n");
		}
	}
	
	public static void printInstruction(Writer writer, String instr, String comment) throws IOException{
		writer.append(pre_instr_space).append(instr);
		if (write_comment && comment != null) writer.append("\t# ").append(comment);
		writer.append("\n");
	}
	
	public final static String pre_instr_space = "\t";
	
	public static boolean write_comment = false;
	
	public final static int data_segment_start = 0x10000004;
	
	public final static int gp_offset = 0x8000 + 0x10000000 - data_segment_start;
	
	public final static int gp = 0x10008000;
}
