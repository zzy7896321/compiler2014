package translate;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;

import main.ErrorHandling;
import mips.*;
import irpack.*;
import graph.*;
import optimization.SSABasedLinearScan;
import util.MyLinkedList;

/**
 * The InstructionTranslation class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class InstructionTranslation implements Registers, MipsOpcode{


	/**
	 * Translates function and writes to the text segment.
	 * 
	 * @param writer
	 * @param entry
	 * @throws IOException
	 */
	public static void writeFunctionEntry(Writer writer, FunctionEntry entry) throws IOException{
		
		/* dummy entry */
		if (entry.getControlFlowGraph() == null) return;
		
		/* initializes */
		funcEntry = entry;
		initialize();
		
		/* translates code */
		translates();
		
		/* prints code */
		printCode(writer);
		
		writer.append("\n");
	}
	
	/**
	 * Calculates frame size and initializes data structures.
	 * 
	 */
	private static void initialize(){
		graph = funcEntry.getControlFlowGraph();
		
		/* aligns the local storage section */
		funcEntry.alignLocal();
		
		/* the size of the local storage section */
		localSize = funcEntry.getLocalSize();
		
		/* the size of the struct/union argument/return value section */
		tempSpaceSize = funcEntry.getTempSpaceSize();
		
		/* the maximum size of the called function's arguments */
		callerArgSize = funcEntry.getCallerArgSize();
		
		/* decide whether $ra and $fp need saving */
		ra_fp_size = 0;
		
		/* has call to subroutines, need to save $ra */
		if (funcEntry.getCalledSet() == null || !funcEntry.getCalledSet().isEmpty()){
			ra_offset = - (ra_fp_size += 4);
			
			funcEntry.updateSavedRegisterSize((SSABasedLinearScan.regCount + 8) << 2);	// now fixed to 88
		}
		else {
			ra_offset = 0;
		}
		
		savedRegisterSize = funcEntry.getSavedRegSize();
		
		/* the stack frame is non-empty, need to save $fp */
		if (localSize + tempSpaceSize + callerArgSize + ra_fp_size + savedRegisterSize > 0){
			fp_offset = - (ra_fp_size += 4);
		}
		else {
			fp_offset = 0;
		}
		
		/* updates the ra/fp size */
		funcEntry.set_ra_fp_size(ra_fp_size);
		
		/* initialize the register mapping */
		regMapping = new int[reg.length]; Arrays.fill(regMapping, -1);
		for (int reg : SSABasedLinearScan.regMapping)
			regMapping[reg] = reg;

		/* decides whether to map some caller-saved registers to callee-saved registers to reduce load/store */
		reMapping();
		
		/* now, the stack frame size can be decided prior to translation */
		frameSize = funcEntry.getFrameSize();
		
		/* initialize containers */
		code = new LinkedList<MipsCode>();
		comment = new LinkedList<String>();
		
		addCode(MipsCode.getDummyCode());
	
		labelMap = new HashMap<String, MipsLabel>();
		
		argStack = new LinkedList<Push>();
		callerSavedRegisterList = new LinkedList<Integer>();
	}
	
	/**
	 * Decides whether to map some caller-saved registers to callee-saved registers to reduce load/store.
	 * 
	 */
	private static void reMapping(){
		
		/* collect call list */
		LinkedList<Call> callList = new LinkedList<Call>();
		
		int exitBlockIndex = funcEntry.getExitBlock().getIndex();
		for (int block_index = 0; block_index <= exitBlockIndex; ++block_index){
			if (block_index != 0 && graph.predecessorCount(block_index) == 0) continue;
			
			Block block = graph.getIthNode(block_index);
			for (MyLinkedList<Quadruple>.Node node = block.getList().iterator(); node.hasNext(); ){
				node = node.next();
				Quadruple quad = node.value();
			
				if (quad instanceof Call){
					callList.add((Call) quad);
				}
			}
		}
		
		BitSet all_regs = new BitSet(regCount);
		for (int i = 0; i < SSABasedLinearScan.regCount; ++i)
			all_regs.set(SSABasedLinearScan.regMapping[i]);
		
		int[][] saved_times = new int[regCount][];
		for (int i = 0; i < regCount; ++i) {
			saved_times[i] = new int[2];
			saved_times[i][0] = i;
		}
		
		/* calculate the times that each variable is saved across call */
		for (Call call : callList){
			
			BitSet busy = (BitSet) call.getBusy().clone();
			
			Operand target = call.getTarget();
			
			/* call to function */
			if (target instanceof Label){
				
				FunctionEntry called_func_entry = functionEntryTable.get(((Label) target).getLabelName());
				
				busy.and(called_func_entry.getUsedRegisterSet());
			}
			
			/* call to function pointer*/
			else {
				
				busy.or(all_regs);
			}
			
			for (int reg = busy.nextSetBit(0); reg != -1; reg = busy.nextSetBit(reg+1)){
				
				++saved_times[reg][1];
			}
		}
		
		/* re-maps registers with at least one save to callee-saved registers */
		Arrays.sort(saved_times, new Comparator<int[]>(){

			@Override
			public int compare(int[] arg0, int[] arg1) {
				
				return arg1[1] - arg0[1];
			}
			
		});
		
		calleeSavedUseList = new LinkedList<Integer>();
		int callee_saved_reg = $s0;
		for (int[] reg_time_pair : saved_times){
			if (reg_time_pair[1] == 0) break;
			
			int reg = reg_time_pair[0];
			regMapping[reg] = callee_saved_reg;
			calleeSavedUseList.add(callee_saved_reg++);
			
			if (calleeSavedUseList.size() >= 8) break;
		}
		
		ra_fp_size += (calleeSavedUseList.size() << 2);
	}
	
	/**
	 * Translates the intermediate representation to mips code.
	 * 
	 */
	private static void translates(){
		
		currentBlock = graph.getIthNode(0);
		currentNode = currentBlock.getList().iterator();
		exitBlockIndex = funcEntry.getExitBlock().getIndex();
		
		/* traverses the control flow graph and emits code */
		for (Quadruple quad = nextQuadruple(); quad != null; quad = nextQuadruple()){
			
			translateQuadruple(quad);
		}
	}
	
	/** 
	 * Accesses the next quadruple and sets the pointer to the next.
	 * 
	 * @return	the next quadruple, null if the end of the function has been reached 
	 */
	private static Quadruple nextQuadruple(){
		
		if (currentNode != null && !currentNode.hasNext()){
			int block_index = currentBlock.getIndex() + 1;
			
			for ( ; block_index <= exitBlockIndex; ++block_index){
				
				if (graph.predecessorCount(block_index) != 0 && graph.getIthNode(block_index).getList().size() != 0) {
				
					currentBlock = graph.getIthNode(block_index);
					break;
				}
			}
			
			/* the end of the function */
			if (block_index > exitBlockIndex){
				currentNode = null;
				currentBlock = null;
			}
			
			else {
				currentNode = currentBlock.getList().iterator();
			}
		}
		
		if (currentNode == null) return null;	// end of the function
		
		currentNode = currentNode.next();
		return currentNode.value();
	}
	
	/**
	 * Accesses the next quadruple in the same block without advancing the pointer.
	 * 
	 * @return	the next quadruple, null if the end of the block has been reached
	 */
	private static Quadruple peekNextQuadrupleWithinBlock(){
		
		return currentNode.next().value();
	}
	
	/**
	 * Accesses the previous quadruple and sets the pointer to the previous.
	 * 
	 * @return	the previous quadruple, null if the beginning of the function has been reached
	 */
	private static Quadruple previousQuadruple(){
		
		if (currentNode == null || currentNode.value() == null){
			
			int block_index;
			if (currentNode == null)
				block_index = exitBlockIndex;
			else
				block_index = currentBlock.getIndex() - 1;
			
			for ( ; block_index >= 0; --block_index){
				
				if (graph.predecessorCount(block_index) != 0 && graph.getIthNode(block_index).getList().size() != 0){
					
					currentBlock = graph.getIthNode(block_index);
					break;
				}
			}
			
			/* the beginning of the function */
			if (block_index < 0){				
				return null;
			}
			
			else {
				
				currentNode = currentBlock.getList().iterator().previous();
			}
		}
		
		Quadruple ret = currentNode.value();
		currentNode = currentNode.previous();
		
		return ret;
	}
	
	/**
	 * Accesses the previous quadruple and sets the pointer to the previous.
	 * 
	 * @return	the previous quadruple, null if the beginning of the block has been reached
	 */
	private static Quadruple previousQuadrupleWithinBlock(){
		
		if (currentNode == null || currentNode.value() == null) return null;
		
		Quadruple ret = currentNode.value();
		currentNode = currentNode.previous();
		
		return ret;
	}
	
	/**
	 * Accesses the previous quadruple without setting the pointer to the previous.
	 * 
	 * @return	the previous quadruple, null if the beginning of the block has been reached
	 */
	private static Quadruple peekPreviousQuadruple(){
		if (currentNode == null) return null;
		
		return currentNode.value();
	}
	
	/**
	 * Translates the quadruple.
	 * 
	 * @param quad
	 */
	private static void translateQuadruple(Quadruple quad){
		
		
		switch (quad.getOpcode()){
		
		case Quadruple.LABEL:
			LABEL(quad);
			break;
		case Quadruple.LA:
			LA(quad);
			break;
		case Quadruple.MOV:
			MOVE(quad);
			break;
		case Quadruple.ADD:
			ADD(quad);
			break;
		case Quadruple.SUB:
			SUB(quad);
			break;
		case Quadruple.MUL:
			MUL(quad);
			break;
		case Quadruple.DIV:
			DIV(quad);
			break;
		case Quadruple.MOD:
			MOD(quad);
			break;
		case Quadruple.SLL:
			SLL(quad);
			break;
		case Quadruple.SRA:
			SRA(quad);
			break;
		case Quadruple.SRL:
			SRL(quad);
			break;
		case Quadruple.AND:
			AND(quad);
			break;
		case Quadruple.OR:
			OR(quad);
			break;
		case Quadruple.XOR:
			XOR(quad);
			break;
		case Quadruple.NOR:
			NOR(quad);
			break;
		case Quadruple.SLT:
			SLT(quad);
			break;
		case Quadruple.SLE:
			SLE(quad);
			break;
		case Quadruple.SGT:
			SGT(quad);
			break;
		case Quadruple.SGE:
			SGE(quad);
			break;
		case Quadruple.SEQ:
			SEQ(quad);
			break;
		case Quadruple.SNE:
			SNE(quad);
			break;
		case Quadruple.SLTU:
			SLTU(quad);
			break;
		case Quadruple.BEQ:
			BEQ(quad);
			break;
		case Quadruple.BNE:
			BNE(quad);
			break;
		case Quadruple.STORERV:
			STORERV(quad);
			break;
		case Quadruple.MOVRV:
			MOVRV(quad);
			break;
		case Quadruple.PUSH:
			PUSH(quad);
			break;
		case Quadruple.J:
			J(quad);
			break;
		case Quadruple.FUNCTIONENTRYBLOCK:
			FUNCTIONENTRYBLOCK(quad);
			break;
		case Quadruple.FUNCTIONEXITBLOCK:
			FUNCTIONEXITBLOCK(quad);
			break;
		case Quadruple.PHI:
			PHI(quad);
			break;
		case Quadruple.CALLDEF:
			CALLDEF(quad);
			break;
		case Quadruple.CALLEND:
			RELOADLIVEREGISTERS();
			break;
			
		default:
			
			ErrorHandling.ReportError("unrecognized IR opcode " + Integer.toString(quad.getOpcode()), ErrorHandling.ERROR);
		}
	}
	

	private static void LABEL(Quadruple quad){
		Label label = (Label) quad;
		
		MipsCode lastcode = code.getLast();
		if (lastcode != null && lastcode.getOpcode() == J){
			
			/* jump to the next instruction can be removed */
			if (((MipsJL) lastcode).getTarget().getName().equals(label.getLabelName())){
				
				removeLast();
			}
		}
		
		MipsLabel mipslabel = getMipsLabel(label.getLabelName());
		addCode(mipslabel);
	}

	private static void LA(Quadruple quad) {
		LoadStore loadstore = (LoadStore) quad;
		
		Operand rd = loadstore.getRd();
		Operand rs = loadstore.getRs();
		
		/* load operand */
		Object[] addr = addressOf(rs, spill1);
		
		/* find register to write to */
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill2;		// 	alternate use of spill register to avoid stall ^_^
		}
		
		/* la instruction */
		if (addr[0] instanceof MipsLabel){
			
			addCode(new MipsLA(LA, dest_reg, (MipsLabel) addr[0], ((Number) addr[1]).intValue()), quad.toString());
		}
		
		else {
			
			addCode(new MipsIS(ADDIU, dest_reg, ((Number) addr[0]).intValue(), ((Number) addr[1]).intValue()), quad.toString());
		}
		
		/* write back */
		if (dest_reg == spill2)
			storeResult((Addressable) rd, spill2, spill1);
	}
	
	private static void MOVE(Quadruple quad) {
		
		/* normal move */
		LoadStore loadstore = (LoadStore) quad;
		
		Operand rd = loadstore.getRd();
		Operand rs = loadstore.getRs();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		int loaded_to = ((Number) loadOperand(dest_reg, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		
		if (loaded_to != dest_reg){
			addCode(new MipsR(OR, dest_reg, loaded_to, $0));
		}
		
		if (dest_reg == spill1)
			storeResult((Addressable) rd, spill1, spill2);
	}
	
	private static void ADD(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		
		Object[] t_loaded_to = loadOperand(spill2, rt, REQUIRE_FOR_SIGNED_IMM_OPERAND);
		int t_reg = ((Number) t_loaded_to[0]).intValue();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		if (t_reg < regCount){
			
			addCode(new MipsR(ADDU, dest_reg, s_reg, t_reg), quad.toString());
		}
		
		else {
			
			addCode(new MipsIS(ADDIU, dest_reg, s_reg, t_reg - signedImmediateResultBase));
		}
		
		if (dest_reg == spill1)
			storeResult((Addressable) rd, spill1, spill2);
	}

	private static void SUB(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		if (rt instanceof IntConstant && !needLI(-((IntConstant) rt).getValue())){
			int value = -((IntConstant) rt).getValue();
			
			addCode(new MipsIS(ADDIU, dest_reg, s_reg, value), quad.toString());
		}
		
		else {
			int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
			
			addCode(new MipsR(SUBU, dest_reg, s_reg, t_reg), quad.toString());
		}
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void MUL(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		if (rs instanceof IntConstant){
			Operand tmp = rs;
			rs = rt;
			rt = tmp;
		}
		
		if (rt instanceof IntConstant){
			int value_t = ((IntConstant) rt).getValue();
			
			/* try transform multiplying 2's power to SLL */
			if (!(rs instanceof IntConstant)){
				
				if (value_t == 1){
					int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
					
					transferResult(dest_reg, s_reg, rd, spill1, spill2);
					
					return ;
				}
				
				else if (value_t == -1){
					
					int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
					
					addCode(new MipsR(SUBU, dest_reg, $0, s_reg), quad.toString());
					
					if (dest_reg == spill1){
						storeResult((Addressable) rd, spill1, spill2);
					}
					
					return ;
				}
				
				if (value_t > 0 && Integer.highestOneBit(value_t) == value_t){
					int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
					
					int shift_bits = Integer.numberOfTrailingZeros(value_t);
					
					addCode(new MipsIS(SLL, dest_reg, s_reg, shift_bits), quad.toString());
					
					if (dest_reg == spill1){
						storeResult((Addressable) rd, spill1, spill2);
					}
					
					return ;
				}
				
				else if (value_t == 0){
					
					transferResult(dest_reg, $0, rd, spill1, spill2);
					
					return ;
				}
			}
			
			/* two constants, rare case */
			else {
				int value_s = ((IntConstant) rs).getValue();
				
				int res = value_t * value_s;
				int loaded_to = ((Number) loadOperand(spill1, new IntConstant(res), REQUIRE_FOR_REG_OPERAND)[0]).intValue();
				
				transferResult(dest_reg, loaded_to, rd, spill1, spill2);
				
				return ;
			}
		}
		
		/* normal MUL instruction */
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		
		addCode(new MipsR(MUL, dest_reg, s_reg, t_reg), quad.toString());
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void DIV(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		/* normal DIV instruction*/
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		
		addCode(new MipsM(DIV, s_reg, t_reg), quad.toString());
		addCode(new MipsAcc(MFLO, dest_reg), quad.toString());	
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void MOD(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		if (rt instanceof IntConstant){
			int value_t = ((IntConstant) rt).getValue();
			
			if (value_t == 1){
				
				transferResult(dest_reg, $0, rd, spill1, spill2);
				
				return ;
			}
			
			else if (value_t > 0 && Integer.highestOneBit(value_t) == value_t){
				int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
				
				value_t -= 1;
				if (!needLIU(value_t)){
					addCode(new MipsIU(ANDI, dest_reg, s_reg, value_t));
					
				}
				
				else if (!needLI(value_t)){
					
					addCode(new MipsIS(ADDIU, $at, $0, value_t));
					addCode(new MipsR(AND, dest_reg, s_reg, $at));
				}
				
				else {
					addCode(new MipsLUI(LUI, $at, value_t >> 16));
					addCode(new MipsIU(ORI, $at, $at, value_t & 0xFFFF));
					addCode(new MipsR(AND, dest_reg, s_reg, $at));
				}
				
				if (dest_reg == spill1){
					storeResult((Addressable) rd, spill1, spill2);
				}
				
				return ;
			}
		}
		
		/* normal MOD instruction */
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		
		addCode(new MipsM(DIV, s_reg, t_reg), quad.toString());
		addCode(new MipsAcc(MFHI, dest_reg), quad.toString());	
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void SLL(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		if (rt instanceof IntConstant){
			int value = ((IntConstant) rt).getValue();
			if ((value & ~0x1F) != 0)
				rt = new IntConstant(((IntConstant) rt).getValue() & 0x1F);
		}
		
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_SIGNED_IMM_OPERAND)[0]).intValue();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		if (t_reg < regCount){
			
			addCode(new MipsR(SLLV, dest_reg, s_reg, t_reg), quad.toString());
		}
		
		else {
			
			addCode(new MipsIS(SLL, dest_reg, s_reg, t_reg - signedImmediateResultBase), quad.toString());
		}
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void SRA(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		if (rt instanceof IntConstant){
			int value = ((IntConstant) rt).getValue();
			if ((value & ~0x1F) != 0)
				rt = new IntConstant(((IntConstant) rt).getValue() & 0x1F);
		}
		
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_SIGNED_IMM_OPERAND)[0]).intValue();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		if (t_reg < regCount){
			
			addCode(new MipsR(SRAV, dest_reg, s_reg, t_reg), quad.toString());
		}
		
		else {
			
			addCode(new MipsIS(SRA, dest_reg, s_reg, t_reg - signedImmediateResultBase), quad.toString());
		}
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void SRL(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		if (rt instanceof IntConstant){
			int value = ((IntConstant) rt).getValue();
			if ((value & ~0x1F) != 0)
				rt = new IntConstant(((IntConstant) rt).getValue() & 0x1F);
		}
		
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_SIGNED_IMM_OPERAND)[0]).intValue();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		if (t_reg < regCount){
			
			addCode(new MipsR(SRLV, dest_reg, s_reg, t_reg), quad.toString());
		}
		
		else {
			
			addCode(new MipsIS(SRL, dest_reg, s_reg, t_reg - signedImmediateResultBase), quad.toString());
		}
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void AND(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_UNSIGNED_IMM_OPERAND)[0]).intValue();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		if (t_reg < regCount){
			
			addCode(new MipsR(AND, dest_reg, s_reg, t_reg), quad.toString());
		}
		
		else {
			
			addCode(new MipsIU(ANDI, dest_reg, s_reg, t_reg - unsignedImmediateResultBase), quad.toString());
		}
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void OR(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_UNSIGNED_IMM_OPERAND)[0]).intValue();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		if (t_reg < regCount){
			
			addCode(new MipsR(OR, dest_reg, s_reg, t_reg), quad.toString());
		}
		
		else {
			
			addCode(new MipsIU(ORI, dest_reg, s_reg, t_reg - unsignedImmediateResultBase), quad.toString());
		}
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void XOR(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_UNSIGNED_IMM_OPERAND)[0]).intValue();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		if (t_reg < regCount){
			
			addCode(new MipsR(XOR, dest_reg, s_reg, t_reg), quad.toString());
		}
		
		else {
			
			addCode(new MipsIU(XORI, dest_reg, s_reg, t_reg - unsignedImmediateResultBase), quad.toString());
		}
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void NOR(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
				
		addCode(new MipsR(NOR, dest_reg, s_reg, t_reg), quad.toString());

		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void SLT(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		/* try merging the condition test with the next branch */
		if (rd instanceof VarWrapper){
			
			VarWrapper var_d = (VarWrapper) rd;
			Quadruple nextQuad = peekNextQuadrupleWithinBlock();
			
			/* if the variable is only used by one instruction */
			if (nextQuad != null && nextQuad instanceof Branch && var_d.getUseList().size() == 1){
				Branch branch = (Branch) nextQuadruple();	// advance the pointer
				Operand other = null;
				
				if (branch.getRs() == var_d){
					other = branch.getRt();
				}
				else if (branch.getRt() == var_d){
					other = branch.getRs();
				}
				
				/* the variable is used by the branch */
				if (other != null){
					
					/* common case, generated by if/while/for statements and short-circuit and conditional expression */
					if (other instanceof IntConstant){
						int value = ((IntConstant) other).getValue();
						
						if (value == 0 || value == 1){
							
							/* just do it as normally, except for we can store the destination in a spill register 
							 * to avoid redundant memory accesses */
							
							int op;
							if (value == 0){
								op = (branch.getOpcode() == Quadruple.BEQ) ? BEQ : BNE;
							}
							else {
								/* swap the true/false branch to avoid load of immediate 1 */
								op = (branch.getOpcode() == Quadruple.BEQ) ? BNE : BEQ;	
							}
							
							int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
							int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_SIGNED_IMM_OPERAND)[0]).intValue();

							int dest_reg = spill1;

							if (t_reg < regCount){

								addCode(new MipsR(SLT, dest_reg, s_reg, t_reg), quad.toString());
							}

							else {

								addCode(new MipsIS(SLTI, dest_reg, s_reg, t_reg - signedImmediateResultBase), quad.toString());
							}

							addCode(new MipsB2(op, dest_reg, $0, getMipsLabel(branch.getTarget().getLabelName())), 
									"merged with previous " + branch.toString());
							return ;
						}
						
						/* not 0/1 operand, should be rare */
						else {
							
							if (branch.getOpcode() == Quadruple.BEQ){
								/* never true, just fall */
								
								addCode(null, "merged " + aluop.toString() + " and " + branch.toString());	// comment line
								
								return ;
							}
							
							else /* BNE */ {
								/* always true, just take the branch*/
								
								addCode(new MipsJL(J, getMipsLabel(branch.getTarget().getLabelName())),
										"merged " + aluop.toString() + " and " + branch.toString());
								
								return ;
							}
						}
					}
					
					/* testing on the same variable, should be really rare */
					if (var_d == other){
						
						if (branch.getOpcode() == Quadruple.BEQ){
							/* always true, just take the branch */
							
							addCode(new MipsJL(J, getMipsLabel(branch.getTarget().getLabelName())), 
									"merged " + aluop.toString() + " and " + branch.toString());
							return ;
						}
						
						else /* BNE */{
							/* never true, just fall */
							
							addCode(null, "merged " + aluop.toString() + " and " + branch.toString());	// comment line
							
							return ;
						}
					}
					
					/* Otherwise, there's no guarantee on the other operand's value always being 0/1. They cannot be merged. */
					
				}
			}
		}	// end of trying
		
		/* normal slt instruction */
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_SIGNED_IMM_OPERAND)[0]).intValue();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		if (t_reg < regCount){
			
			addCode(new MipsR(SLT, dest_reg, s_reg, t_reg), quad.toString());
		}
		
		else {
			
			addCode(new MipsIS(SLTI, dest_reg, s_reg, t_reg - signedImmediateResultBase), quad.toString());
		}
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void SLE(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		/* try merging the condition test with the next branch */
		if (rd instanceof VarWrapper){
			
			VarWrapper var_d = (VarWrapper) rd;
			Quadruple nextQuad = peekNextQuadrupleWithinBlock();
			
			/* if the variable is only used by one instruction */
			if (nextQuad != null && nextQuad instanceof Branch && var_d.getUseList().size() == 1){
				Branch branch = (Branch) nextQuadruple();	// advance the pointer
				Operand other = null;
				
				if (branch.getRs() == var_d){
					other = branch.getRt();
				}
				else if (branch.getRt() == var_d){
					other = branch.getRs();
				}
				
				/* the variable is used by the branch */
				if (other != null){
					
					/* common case, generated by if/while/for statements and short-circuit and conditional expression */
					if (other instanceof IntConstant){
						int value = ((IntConstant) other).getValue();
						
						if (value == 0 || value == 1){
							
							/* just do it as normally, except for we can store the destination in a spill register 
							 * to avoid redundant memory accesses */
							
							/* rs <= rt is equivalent to !(rt < rs) */{
								Operand tmp = rs;
								rs = rt;
								rt = tmp;
							}
							
							int op;
							if (value == 0){
								op = (branch.getOpcode() == Quadruple.BEQ) ? BNE : BEQ;
							}
							else {
								/* swap the true/false branch to avoid load of immediate 1 */
								op = (branch.getOpcode() == Quadruple.BEQ) ? BEQ : BNE;	
							}
							
							int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
							int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_SIGNED_IMM_OPERAND)[0]).intValue();

							int dest_reg = spill1;

							if (t_reg < regCount){

								addCode(new MipsR(SLT, dest_reg, s_reg, t_reg), quad.toString());
							}

							else {

								addCode(new MipsIS(SLTI, dest_reg, s_reg, t_reg - signedImmediateResultBase), quad.toString());
							}

							addCode(new MipsB2(op, dest_reg, $0, getMipsLabel(branch.getTarget().getLabelName())), 
									"merged with previous " + branch.toString());
							return ;
						}
						
						/* not 0/1 operand, should be rare */
						else {
							
							if (branch.getOpcode() == Quadruple.BEQ){
								/* never true, just fall */
								
								addCode(null, "merged " + aluop.toString() + " and " + branch.toString());	// comment line
								
								return ;
							}
							
							else /* BNE */ {
								/* always true, just take the branch*/
								
								addCode(new MipsJL(J, getMipsLabel(branch.getTarget().getLabelName())),
										"merged " + aluop.toString() + " and " + branch.toString());
								
								return ;
							}
						}
					}
					
					/* testing on the same variable, should be really rare */
					if (var_d == other){
						
						if (branch.getOpcode() == Quadruple.BEQ){
							/* always true, just take the branch */
							
							addCode(new MipsJL(J, getMipsLabel(branch.getTarget().getLabelName())), 
									"merged " + aluop.toString() + " and " + branch.toString());
							return ;
						}
						
						else /* BNE */{
							/* never true, just fall */
							
							addCode(null, "merged " + aluop.toString() + " and " + branch.toString());	// comment line
							
							return ;
						}
					}
					
					/* Otherwise, there's no guarantee on the other operand's value always being 0/1. They cannot be merged. */
					
				}
			}
		}	// end of trying
		
		/* normal SLE instruction */
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_SIGNED_IMM_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		/*
		 * slt rd, rt, rs
		 * xori rd, rd, 1		# !(rt < rs)
		 * */
		
		if (s_reg < regCount){
			
			addCode(new MipsR(SLT, dest_reg, t_reg, s_reg));
		}
		
		else {
			
			addCode(new MipsIS(SLTI, dest_reg, t_reg, s_reg - signedImmediateResultBase));
		}
		
		addCode(new MipsIU(XORI, dest_reg, dest_reg, 1), quad.toString());
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void SGT(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		/* try merging the condition test with the next branch */
		if (rd instanceof VarWrapper){
			
			VarWrapper var_d = (VarWrapper) rd;
			Quadruple nextQuad = peekNextQuadrupleWithinBlock();
			
			/* if the variable is only used by one instruction */
			if (nextQuad != null && nextQuad instanceof Branch && var_d.getUseList().size() == 1){
				Branch branch = (Branch) nextQuadruple();	// advance the pointer
				Operand other = null;
				
				if (branch.getRs() == var_d){
					other = branch.getRt();
				}
				else if (branch.getRt() == var_d){
					other = branch.getRs();
				}
				
				/* the variable is used by the branch */
				if (other != null){
					
					/* common case, generated by if/while/for statements and short-circuit and conditional expression */
					if (other instanceof IntConstant){
						int value = ((IntConstant) other).getValue();
						
						if (value == 0 || value == 1){
							
							/* just do it as normally, except for we can store the destination in a spill register 
							 * to avoid redundant memory accesses */
							
							/* rs > rt is equivalent to rt < rs; swap them */{
								Operand tmp = rs;
								rs = rt;
								rt = tmp;
							}
							
							int op;
							if (value == 0){
								op = (branch.getOpcode() == Quadruple.BEQ) ? BEQ : BNE;
							}
							else {
								/* swap the true/false branch to avoid load of immediate 1 */
								op = (branch.getOpcode() == Quadruple.BEQ) ? BNE : BEQ;	
							}
							
							int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
							int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_SIGNED_IMM_OPERAND)[0]).intValue();

							int dest_reg = spill1;

							if (t_reg < regCount){

								addCode(new MipsR(SLT, dest_reg, s_reg, t_reg), quad.toString());
							}

							else {

								addCode(new MipsIS(SLTI, dest_reg, s_reg, t_reg - signedImmediateResultBase), quad.toString());
							}

							addCode(new MipsB2(op, dest_reg, $0, getMipsLabel(branch.getTarget().getLabelName())), 
									"merged with previous " + branch.toString());
							return ;
						}
						
						/* not 0/1 operand, should be rare */
						else {
							
							if (branch.getOpcode() == Quadruple.BEQ){
								/* never true, just fall */
								
								addCode(null, "merged " + aluop.toString() + " and " + branch.toString());	// comment line
								
								return ;
							}
							
							else /* BNE */ {
								/* always true, just take the branch*/
								
								addCode(new MipsJL(J, getMipsLabel(branch.getTarget().getLabelName())),
										"merged " + aluop.toString() + " and " + branch.toString());
								
								return ;
							}
						}
					}
					
					/* testing on the same variable, should be really rare */
					if (var_d == other){
						
						if (branch.getOpcode() == Quadruple.BEQ){
							/* always true, just take the branch */
							
							addCode(new MipsJL(J, getMipsLabel(branch.getTarget().getLabelName())), 
									"merged " + aluop.toString() + " and " + branch.toString());
							return ;
						}
						
						else /* BNE */{
							/* never true, just fall */
							
							addCode(null, "merged " + aluop.toString() + " and " + branch.toString());	// comment line
							
							return ;
						}
					}
					
					/* Otherwise, there's no guarantee on the other operand's value always being 0/1. They cannot be merged. */
					
				}
			}
		}	// end of trying
		
		/* normal SGT instruction */
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_SIGNED_IMM_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		/*
		 * slt rd, rt, rs		# rt < rs
		 * */
		
		if (s_reg < regCount){
			
			addCode(new MipsR(SLT, dest_reg, t_reg, s_reg), quad.toString());
		}
		
		else {
			
			addCode(new MipsIS(SLTI, dest_reg, t_reg, s_reg - signedImmediateResultBase), quad.toString());
		}
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void SGE(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		/* try merging the condition test with the next branch */
		if (rd instanceof VarWrapper){
			
			VarWrapper var_d = (VarWrapper) rd;
			Quadruple nextQuad = peekNextQuadrupleWithinBlock();
			
			/* if the variable is only used by one instruction */
			if (nextQuad != null && nextQuad instanceof Branch && var_d.getUseList().size() == 1){
				Branch branch = (Branch) nextQuadruple();	// advance the pointer
				Operand other = null;
				
				if (branch.getRs() == var_d){
					other = branch.getRt();
				}
				else if (branch.getRt() == var_d){
					other = branch.getRs();
				}
				
				/* the variable is used by the branch */
				if (other != null){
					
					/* common case, generated by if/while/for statements and short-circuit and conditional expression */
					if (other instanceof IntConstant){
						int value = ((IntConstant) other).getValue();
						
						if (value == 0 || value == 1){
							
							/* just do it as normally, except for we can store the destination in a spill register 
							 * to avoid redundant memory accesses */
							
							/* rs >= rt is equivalent to !(rs < rt) */
							
							int op;
							if (value == 0){
								op = (branch.getOpcode() == Quadruple.BEQ) ? BNE : BEQ;
							}
							else {
								/* swap the true/false branch to avoid load of immediate 1 */
								op = (branch.getOpcode() == Quadruple.BEQ) ? BEQ : BNE;	
							}
							
							int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
							int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_SIGNED_IMM_OPERAND)[0]).intValue();

							int dest_reg = spill1;

							if (t_reg < regCount){

								addCode(new MipsR(SLT, dest_reg, s_reg, t_reg), quad.toString());
							}

							else {

								addCode(new MipsIS(SLTI, dest_reg, s_reg, t_reg - signedImmediateResultBase), quad.toString());
							}

							addCode(new MipsB2(op, dest_reg, $0, getMipsLabel(branch.getTarget().getLabelName())), 
									"merged with previous " + branch.toString());
							return ;
						}
						
						/* not 0/1 operand, should be rare */
						else {
							
							if (branch.getOpcode() == Quadruple.BEQ){
								/* never true, just fall */
								
								addCode(null, "merged " + aluop.toString() + " and " + branch.toString());	// comment line
								
								return ;
							}
							
							else /* BNE */ {
								/* always true, just take the branch*/
								
								addCode(new MipsJL(J, getMipsLabel(branch.getTarget().getLabelName())),
										"merged " + aluop.toString() + " and " + branch.toString());
								
								return ;
							}
						}
					}
					
					/* testing on the same variable, should be really rare */
					if (var_d == other){
						
						if (branch.getOpcode() == Quadruple.BEQ){
							/* always true, just take the branch */
							
							addCode(new MipsJL(J, getMipsLabel(branch.getTarget().getLabelName())), 
									"merged " + aluop.toString() + " and " + branch.toString());
							return ;
						}
						
						else /* BNE */{
							/* never true, just fall */
							
							addCode(null, "merged " + aluop.toString() + " and " + branch.toString());	// comment line
							
							return ;
						}
					}
					
					/* Otherwise, there's no guarantee on the other operand's value always being 0/1. They cannot be merged. */
					
				}
			}
		}	// end of trying
		
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_SIGNED_IMM_OPERAND)[0]).intValue();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		/*
		 * slt rd, rs, rt
		 * xori rd, rd, 1		# !(rs < rt)
		 * */
		
		if (t_reg < regCount){
			
			addCode(new MipsR(SLT, dest_reg, s_reg, t_reg));
		}
		
		else {
			
			addCode(new MipsIS(SLTI, dest_reg, s_reg, t_reg - signedImmediateResultBase));
		}
		
		addCode(new MipsIU(XOR, dest_reg, dest_reg, 1), quad.toString());
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void SEQ(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		
		/* try merging the condition test with the next branch */
		if (rd instanceof VarWrapper){
			
			VarWrapper var_d = (VarWrapper) rd;
			Quadruple nextQuad = peekNextQuadrupleWithinBlock();
			
			/* if the variable is only used by one instruction */
			if (nextQuad != null && nextQuad instanceof Branch && var_d.getUseList().size() == 1){
				Branch branch = (Branch) nextQuadruple();	// advance the pointer
				Operand other = null;
				
				if (branch.getRs() == var_d){
					other = branch.getRt();
				}
				else if (branch.getRt() == var_d){
					other = branch.getRs();
				}
				
				/* the variable is used by the branch */
				if (other != null){
					
					/* common case, generated by if/while/for statements and short-circuit and conditional expression */
					if (other instanceof IntConstant){
						int value = ((IntConstant) other).getValue();
						
						if (value == 0 || value == 1){
							
							/* The two instructions are fully merged; the rd register is even omitted */
							
							int op;
							if (value == 0){
								op = (branch.getOpcode() == Quadruple.BEQ) ? BNE : BEQ;
							}
							else {
								op = (branch.getOpcode() == Quadruple.BEQ) ? BEQ : BNE;	
							}
							
							int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
							int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_REG_OPERAND)[0]).intValue();

							addCode(new MipsB2(op, s_reg, t_reg, getMipsLabel(branch.getTarget().getLabelName())), 
									"merged " + aluop.toString() + " and " + branch.toString());
							return ;
						}
						
						/* not 0/1 operand, should be rare */
						else {
							
							if (branch.getOpcode() == Quadruple.BEQ){
								/* never true, just fall */
								
								addCode(null, "merged " + aluop.toString() + " and " + branch.toString());	// comment line
								
								return ;
							}
							
							else /* BNE */ {
								/* always true, just take the branch*/
								
								addCode(new MipsJL(J, getMipsLabel(branch.getTarget().getLabelName())),
										"merged " + aluop.toString() + " and " + branch.toString());
								
								return ;
							}
						}
					}
					
					/* testing on the same variable, should be really rare */
					if (var_d == other){
						
						if (branch.getOpcode() == Quadruple.BEQ){
							/* always true, just take the branch */
							
							addCode(new MipsJL(J, getMipsLabel(branch.getTarget().getLabelName())), 
									"merged " + aluop.toString() + " and " + branch.toString());
							return ;
						}
						
						else /* BNE */{
							/* never true, just fall */
							
							addCode(null, "merged " + aluop.toString() + " and " + branch.toString());	// comment line
							
							return ;
						}
					}
					
					/* Otherwise, there's no guarantee on the other operand's value always being 0/1. They cannot be merged. */
					
				}
			}
		}	// end of trying
		
		
		/* normal SEQ instruction */
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_UNSIGNED_IMM_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_UNSIGNED_IMM_OPERAND)[0]).intValue();
		
		/*
		 * xor rd, rs, rt
		 * sltiu rd, rd, 1 
		 */
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}		
		
		boolean need_sltiu = true;
		if (s_reg < regCount){
			if (t_reg < regCount){
				addCode(new MipsR(XOR, dest_reg, s_reg, t_reg));
			}
			
			else {
				addCode(new MipsIU(XORI, dest_reg, s_reg, t_reg - unsignedImmediateResultBase));
			}
		}
		
		else {
			if (t_reg < regCount){
				addCode(new MipsIU(XORI, dest_reg, t_reg, s_reg - unsignedImmediateResultBase));
			}
			
			else {
				need_sltiu = false;
				addCode(new MipsIU(OR, dest_reg, $0, (t_reg == s_reg) ? 1 : 0), quad.toString());
			}
		}
		
		if (need_sltiu){
			addCode(new MipsIU(SLTIU, dest_reg, dest_reg, 1), quad.toString());
		}
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void SNE(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		/* try merging the condition test with the next branch */
		if (rd instanceof VarWrapper){
			
			VarWrapper var_d = (VarWrapper) rd;
			Quadruple nextQuad = peekNextQuadrupleWithinBlock();
			
			/* if the variable is only used by one instruction */
			if (nextQuad != null && nextQuad instanceof Branch && var_d.getUseList().size() == 1){
				Branch branch = (Branch) nextQuadruple();	// advance the pointer
				Operand other = null;
				
				if (branch.getRs() == var_d){
					other = branch.getRt();
				}
				else if (branch.getRt() == var_d){
					other = branch.getRs();
				}
				
				/* the variable is used by the branch */
				if (other != null){
					
					/* common case, generated by if/while/for statements and short-circuit and conditional expression */
					if (other instanceof IntConstant){
						int value = ((IntConstant) other).getValue();
						
						if (value == 0 || value == 1){
							
							/* The two instructions are fully merged; the rd register is even omitted */
							
							int op;
							if (value == 0){
								op = (branch.getOpcode() == Quadruple.BEQ) ? BEQ : BNE;
							}
							else {
								op = (branch.getOpcode() == Quadruple.BEQ) ? BNE : BEQ;	
							}
							
							int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
							int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_REG_OPERAND)[0]).intValue();

							addCode(new MipsB2(op, s_reg, t_reg, getMipsLabel(branch.getTarget().getLabelName())), 
									"merged " + aluop.toString() + " and " + branch.toString());
							return ;
						}
						
						/* not 0/1 operand, should be rare */
						else {
							
							if (branch.getOpcode() == Quadruple.BEQ){
								/* never true, just fall */
								
								addCode(null, "merged " + aluop.toString() + " and " + branch.toString());	// comment line
								
								return ;
							}
							
							else /* BNE */ {
								/* always true, just take the branch*/
								
								addCode(new MipsJL(J, getMipsLabel(branch.getTarget().getLabelName())),
										"merged " + aluop.toString() + " and " + branch.toString());
								
								return ;
							}
						}
					}
					
					/* testing on the same variable, should be really rare */
					if (var_d == other){
						
						if (branch.getOpcode() == Quadruple.BEQ){
							/* always true, just take the branch */
							
							addCode(new MipsJL(J, getMipsLabel(branch.getTarget().getLabelName())), 
									"merged " + aluop.toString() + " and " + branch.toString());
							return ;
						}
						
						else /* BNE */{
							/* never true, just fall */
							
							addCode(null, "merged " + aluop.toString() + " and " + branch.toString());	// comment line
							
							return ;
						}
					}
					
					/* Otherwise, there's no guarantee on the other operand's value always being 0/1. They cannot be merged. */
					
				}
			}
		}	// end of trying
		
		
		/* normal SNE instruction */
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_UNSIGNED_IMM_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_UNSIGNED_IMM_OPERAND)[0]).intValue();
		
		/*
		 * xor rd, rs, rt
		 * sltu rd, $0, rd 
		 */
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}		
		
		boolean need_sltiu = true;
		if (s_reg < regCount){
			if (t_reg < regCount){
				addCode(new MipsR(XOR, dest_reg, s_reg, t_reg));
			}
			
			else {
				addCode(new MipsIU(XORI, dest_reg, s_reg, t_reg - unsignedImmediateResultBase));
			}
		}
		
		else {
			if (t_reg < regCount){
				addCode(new MipsIU(XORI, dest_reg, t_reg, s_reg - unsignedImmediateResultBase));
			}
			
			else {
				need_sltiu = false;
				addCode(new MipsIU(OR, dest_reg, $0, (t_reg != s_reg) ? 1 : 0), quad.toString());
			}
		}
		
		if (need_sltiu){
			addCode(new MipsR(SLTU, dest_reg, $0, dest_reg), quad.toString());
		}
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void SLTU(Quadruple quad){
		ALUOp aluop = (ALUOp) quad;
		
		Operand rd = aluop.getRd();
		Operand rs = aluop.getRs();
		Operand rt = aluop.getRt();
		
		/* try merging the condition test with the next branch */
		if (rd instanceof VarWrapper){
			
			VarWrapper var_d = (VarWrapper) rd;
			Quadruple nextQuad = peekNextQuadrupleWithinBlock();
			
			/* if the variable is only used by one instruction */
			if (nextQuad != null && nextQuad instanceof Branch && var_d.getUseList().size() == 1){
				Branch branch = (Branch) nextQuadruple();	// advance the pointer
				Operand other = null;
				
				if (branch.getRs() == var_d){
					other = branch.getRt();
				}
				else if (branch.getRt() == var_d){
					other = branch.getRs();
				}
				
				/* the variable is used by the branch */
				if (other != null){
					
					/* common case, generated by if/while/for statements and short-circuit and conditional expression */
					if (other instanceof IntConstant){
						int value = ((IntConstant) other).getValue();
						
						if (value == 0 || value == 1){
							
							/* just do it as normally, except for we can store the destination in a spill register 
							 * to avoid redundant memory accesses */
							
							int op;
							if (value == 0){
								op = (branch.getOpcode() == Quadruple.BEQ) ? BEQ : BNE;
							}
							else {
								/* swap the true/false branch to avoid load of immediate 1 */
								op = (branch.getOpcode() == Quadruple.BEQ) ? BNE : BEQ;	
							}
							
							int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
							int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_SIGNED_IMM_OPERAND)[0]).intValue();

							int dest_reg = spill1;

							if (t_reg < regCount){

								addCode(new MipsR(SLTU, dest_reg, s_reg, t_reg), quad.toString());
							}

							else {

								addCode(new MipsIS(SLTIU, dest_reg, s_reg, t_reg - signedImmediateResultBase), quad.toString());
							}

							addCode(new MipsB2(op, dest_reg, $0, getMipsLabel(branch.getTarget().getLabelName())), 
									"merged with previous " + branch.toString());
							return ;
						}
						
						/* not 0/1 operand, should be rare */
						else {
							
							if (branch.getOpcode() == Quadruple.BEQ){
								/* never true, just fall */
								
								addCode(null, "merged " + aluop.toString() + " and " + branch.toString());	// comment line
								
								return ;
							}
							
							else /* BNE */ {
								/* always true, just take the branch*/
								
								addCode(new MipsJL(J, getMipsLabel(branch.getTarget().getLabelName())),
										"merged " + aluop.toString() + " and " + branch.toString());
								
								return ;
							}
						}
					}
					
					/* testing on the same variable, should be really rare */
					if (var_d == other){
						
						if (branch.getOpcode() == Quadruple.BEQ){
							/* always true, just take the branch */
							
							addCode(new MipsJL(J, getMipsLabel(branch.getTarget().getLabelName())), 
									"merged " + aluop.toString() + " and " + branch.toString());
							return ;
						}
						
						else /* BNE */{
							/* never true, just fall */
							
							addCode(null, "merged " + aluop.toString() + " and " + branch.toString());	// comment line
							
							return ;
						}
					}
					
					/* Otherwise, there's no guarantee on the other operand's value always being 0/1. They cannot be merged. */
					
				}
			}
		}	// end of trying
		
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_UNSIGNED_IMM_OPERAND)[0]).intValue();
		
		int dest_reg = registerOf(rd);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		if (t_reg < regCount){
			
			addCode(new MipsR(SLT, dest_reg, s_reg, t_reg), quad.toString());
		}
		
		else {
			
			addCode(new MipsIU(SLTIU, dest_reg, s_reg, t_reg - unsignedImmediateResultBase), quad.toString());
		}
		
		if (dest_reg == spill1){
			storeResult((Addressable) rd, spill1, spill2);
		}
	}

	private static void BEQ(Quadruple quad){
		Branch branch = (Branch) quad;
		
		Operand rs = branch.getRs();
		Operand rt = branch.getRt();
		Label target = branch.getTarget();
	
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		
		addCode(new MipsB2(BEQ, s_reg, t_reg, getMipsLabel(target.getLabelName())), quad.toString());
	}

	private static void BNE(Quadruple quad){
		Branch branch = (Branch) quad;
		
		Operand rs = branch.getRs();
		Operand rt = branch.getRt();
		Label target = branch.getTarget();
		
		int s_reg = ((Number) loadOperand(spill1, rs, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		int t_reg = ((Number) loadOperand(spill2, rt, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		
		addCode(new MipsB2(BNE, s_reg, t_reg, getMipsLabel(target.getLabelName())), quad.toString());
	}

	private static void J(Quadruple quad){
		Jump jump = (Jump) quad;
		
		Label target = (Label) jump.getTarget();
		
		addCode(new MipsJL(J, getMipsLabel(target.getLabelName())), quad.toString());
	}

	private static void FUNCTIONENTRYBLOCK(Quadruple quad){
		
		/* save $fp, $ra*/
		if (ra_offset != 0){
			addCode(new MipsLS(SW, $ra, $sp, ra_offset));
		}
		if (fp_offset != 0){
			addCode(new MipsLS(SW, $fp, $sp, fp_offset));
			addCode(new MipsR(OR, $fp, $sp, $0));
			framePointer = $fp;
		}
		else {
			framePointer = $sp;
		}

		/* adjust $sp */
		if (frameSize > 0){
			
			/* addiu $sp, $sp, -frameSize*/
			if (!needLI(-frameSize)){
				addCode(new MipsIS(ADDIU, $sp, $sp, -frameSize), "frame size = " + Integer.toString(frameSize));
			}
			
			/* subu $sp, $sp, $at # $at is loaded with frameSize*/
			else if (!needLIU(frameSize)){
				
				addCode(new MipsIU(ORI, $at, $0, frameSize));
				addCode(new MipsR(SUBU, $sp, $sp, $at), "frame size = " + Integer.toString(frameSize));
			}
			
			/* addu $sp, $sp, $at # $at is loaded with -frameSize */
			else {
				int[] imm = separateImmediate(-frameSize);
				
				addCode(new MipsLUI(LUI, $at, imm[0]));
				addCode(new MipsIS(ADDIU, $at, $at, imm[1]));
				addCode(new MipsR(ADDU, $sp, $sp, $at), "frame size = " + Integer.toString(frameSize));
			}
		}
		
		/* save callee-saved registers */
		for (int reg : calleeSavedUseList){
			
			addCode(new MipsLS(SW, reg, $sp, callerArgSize + savedRegOffset[reg]));
		}
		
		/* move arguments to the right registers */
		ArrayList<Argument> args = funcEntry.getArgumentTable();
		ArrayList<VarWrapper> wrappedArgs = funcEntry.getWrappedArgumentTable();
		//VarWrapper[] occupied = new VarWrapper[$a3 + 1]; 
		int arg_cnt = args.size();
		
		int max_occupied_arg_reg = $a0 + arg_cnt - 1;
		if (max_occupied_arg_reg > $a3)
			max_occupied_arg_reg = $a3;
		
		/* move $v0 */
		if (funcEntry.hasReturnValueAddress()){
			 Argument rva = funcEntry.getReturnValueAddress();
			 VarWrapper wrappedRva = funcEntry.getWrappedReturnValueAddress();
			 
			 /* taken address */
			 if (wrappedRva == null){
				 addCode(new MipsLS(SW, $v0, framePointer, rva.getOffset()));
			 }
			 
			 else {
				 /* spilled */
				 if (wrappedRva.isAllocated()){
					 addCode(new MipsLS(SW, $v0, framePointer, wrappedRva.getOffset()));
				 }
				 
				 /* allocated with register */
				 else {
					 int dest_reg = regMapping[wrappedRva.getReg()];
					 
					 if (dest_reg == $v0){
						 // already in place, nothing to do
					 }
					 
					 else{
						 assert(dest_reg < $a0 || dest_reg > $a3): "rva's register should have been re-assigned to $v0";
						 
						 // okay to move
						 addCode(new MipsR(OR, dest_reg, $v0, $0));
					 }
				 }
			 }
		}
		
		/* move $a0 ~ $a3 */
		for (int i = 0; i <= max_occupied_arg_reg - $a0; ++i){
			Argument arg = args.get(i);
			VarWrapper var = wrappedArgs.get(i);
			
			/* taken address */
			if (var == null){
				addCode(new MipsLS(SW, $a0 + i, framePointer, arg.getOffset()));
			}
			
			else {
				/* spilled */
				if (var.isAllocated()){
					addCode(new MipsLS(SW, $a0 + i, framePointer, var.getOffset()));
				}
				
				/* allocated with register */
				else {
					int dest_reg = regMapping[var.getReg()];
					
					if (dest_reg == $a0 + i){
						// already in place, nothing to do
					}
					
					else{
						assert(dest_reg != $v0 && (dest_reg < $a0 || dest_reg > $a3)):
							"arguments' register should have been re-assigned to $ai";
						
						// okay to move
						addCode(new MipsR(OR, dest_reg, $a0 + i, $0));
					}
				}
			}
		}
		
		/* move other arguments */
		for (int i = 4; i < args.size(); ++i){
			Argument arg = args.get(i);
			VarWrapper var = wrappedArgs.get(i);
			
			/* taken address */
			if (var == null){
				// already on the stack, nothing to do
			}
			
			else {
				/* spilled */
				if (var.isAllocated()){
					// already on the stack, nothing to do
				}
				
				/* allocated with register */
				else {
					int dest_reg = regMapping[var.getReg()];
					
					addCode(new MipsLS(LW, dest_reg, framePointer, arg.getOffset()));
				}
			}
		}
		
	}

	private static void FUNCTIONEXITBLOCK(Quadruple quad){
		
		/* recover callee-saved registers */
		for (int reg : calleeSavedUseList){
			addCode(new MipsLS(LW, reg, $sp, callerArgSize + savedRegOffset[reg]));
		}
		
		/* function exit point */
		if (ra_offset != 0){
			addCode(new MipsLS(LW, $ra, framePointer, ra_offset));
		}
		
		if (fp_offset != 0){
			addCode(new MipsR(OR, $sp, $fp, 0));
			addCode(new MipsLS(LW, $fp, $sp, fp_offset));
		}
		
		addCode(new MipsJR(JR, $ra));
	}

	private static void PHI(Quadruple quad){
		/* just ignore*/
	}

	private static void PUSH(Quadruple quad){
		argStack.addLast((Push) quad);
	}
	
	private static void CALLDEF(Quadruple quad){
		Call call = (Call) quad;
		
		Operand target = call.getTarget();
		BitSet liveReg;
		if (call.getBusy() != null)
			
			liveReg = (BitSet) call.getBusy().clone();
		else {
			
			liveReg = new BitSet(regCount);
			for (int reg : SSABasedLinearScan.regMapping){
				liveReg.set(reg);
			}
		}
		
		MipsLabel target_label = null;
		int target_register = -1;
		
		/* intersects with used register set of the called function */
		if (target instanceof Label){
			FunctionEntry calledFunction = functionEntryTable.get(((Label) target).getLabelName());
			
			liveReg.and(calledFunction.getUsedRegisterSet());
		
			target_label = getMipsLabel(((Label) target).getLabelName());
		}
		
		else {
			
			target_register = ((Number) loadOperand(spill2, target, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		}
		
		/* save live registers */
		for (int vreg = liveReg.previousSetBit(regCount); vreg != -1; vreg = liveReg.previousSetBit(vreg-1)){
			int reg = regMapping[vreg];
			
			if (reg < $s0 || reg > $s7){
				addCode(new MipsLS(SW, reg, $sp, callerArgSize + savedRegOffset[reg]));
				callerSavedRegisterList.add(reg);
			}
		}
		
		/* push arguments */
		Object[] sources = new Object[$a3+1];
		
		for (Push push : argStack){
			int num = push.getNum();
			
			/* the variable length part or the arguments other than the first four */
			if (num == -2 || num >= 4){
				
				int offset = push.getOffset();
				Operand src = push.getSrc();
				
				int loaded_to = ((Number) loadOperand(spill1, src, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
				
				addCode(new MipsLS(SW, loaded_to, $sp, offset));
			}
			
			/* return value address */
			else if (num == -1){
				
				Operand src = push.getSrc();
				sources[$v0] = src;
				
				if (target_register == $v0){
					addCode(new MipsR(OR, spill2, $v0, $0));
					target_register = spill2;
				}
				
				if (src instanceof VarWrapper && !((VarWrapper) src).isAllocated()){
					int reg = regMapping[((VarWrapper) src).getReg()];
					
					/* no need to move */
					if (reg == $v0){
						sources[$v0] = null;
					}
					
					else {
						sources[$v0] = reg;
					}
				}
			}
			
			/* the first four arguments */
			else if (num < 4)  {
				
				Operand src = push.getSrc();
				int to = $a0 + num;
				sources[to] = src;
				
				if (target_register == to){
					addCode(new MipsR(OR, spill2, to, $0));
					target_register = to;
				}
				
				if (src instanceof VarWrapper && !((VarWrapper) src).isAllocated()){
					int reg = regMapping[((VarWrapper) src).getReg()];
					
					/* no need to move */
					if (reg == to){
						sources[to] = null;
					}
					
					else {
						sources[to] = reg;
					}
				}
			}
		}
		
		
		int[] to_load_stack = new int[$a3+1];
		boolean[] in_stack = new boolean[$a3+1];
		int stack_top = 0;
		/* load $a0 ~ $a3 and $v0*/
		for (int cur = $v0; cur <= $a3; ++cur){
			if (sources[cur] == null) continue;			
			Arrays.fill(in_stack, false);
			
			int to_load = cur;
			while (to_load !=-1 && !in_stack[to_load]){
				to_load_stack[stack_top++] = to_load;
				in_stack[to_load] = true;
				
				int i;
				for (i = cur; i <= $a3; ++i){
					if (sources[i] instanceof Number && ((Number) sources[i]).intValue() == to_load){
						to_load = i;
						break;
					}
				}
				
				if (i > $a3) to_load = -1;
			}
			
			if (to_load != -1){
				
				to_load = to_load_stack[stack_top-1];
				addCode(new MipsR(OR, spill1, to_load, $0));
			}
			
			Object[] new_source = new Object[$a3+1];
			for (int i = stack_top - 1; i >= 0; --i){
				
				int now = to_load_stack[i];
				
				if (sources[now] instanceof Number){
					int from = ((Number) sources[now]).intValue();
					
					for (int j = cur; j <= $a3; ++j){
						if (sources[j] instanceof Number && ((Number) sources[j]).intValue() == from){
							new_source[j] = now;
						}
					}
					
					if (from == to_load){
						from = spill1;
					}
					
					addCode(new MipsR(OR, now, from, $0));

				}
				
				else {
					
					int loaded_to = ((Number) loadOperand(now, (Operand) sources[now], REQUIRE_FOR_REG_OPERAND)[0]).intValue();
					
					if (loaded_to != now){
						addCode(new MipsR(OR, now, loaded_to, $0));
					}
				}
				
				sources[now] = null;
			}
			
			for (int i = cur; i <= $a3; ++i){
				if (sources[i] != null && new_source[i] != null){
					sources[i] = new_source[i];
				}
			}
			
			stack_top = 0;
		}
		
		
		/* call the function */
		if (target_register == -1){
			addCode(new MipsJL(JAL, target_label));
		}
		else {
			addCode(new MipsJR(JALR, target_register));
		}
		
		argStack.clear();
	}
	
	private static void MOVRV(Quadruple quad){	
		MoveRV moverv = (MoveRV) quad;
	
		int src_reg = $v0;
		if (callerSavedRegisterList.contains($v0)){
			addCode(new MipsR(OR, spill2, $v0, $0));
			src_reg = spill2;
		}
		
		RELOADLIVEREGISTERS();
		
		Operand dest = moverv.getDest();
		
		int dest_reg = registerOf(dest);
		if (dest_reg == -1){
			dest_reg = spill1;
		}
		
		addCode(new MipsR(OR, dest_reg, src_reg, $0), quad.toString());
		
		if (dest_reg == spill1)
			storeResult((Addressable) dest, spill1, spill2);

	}
	
	private static void RELOADLIVEREGISTERS(){
		
		for (int reg : callerSavedRegisterList){
			
			addCode(new MipsLS(LW, reg, $sp, callerArgSize + savedRegOffset[reg]));
		}
		
		callerSavedRegisterList.clear();
	}
	
	private static void STORERV(Quadruple quad){
		StoreRV storerv = (StoreRV) quad;
		
		Operand src = storerv.getSrc();
		
		int loaded_to = ((Number) loadOperand($v0, src, REQUIRE_FOR_REG_OPERAND)[0]).intValue();
		
		if (loaded_to != $v0){
			addCode(new MipsR(OR, $v0, loaded_to, $0));
		}
	}
	
	/**
	 * Prints the assembly code.
	 * 
	 */
	private static void printCode(Writer writer) throws IOException{
		
		if (Translate.write_comment){
			
			ListIterator<MipsCode> code_iter = code.listIterator();
			ListIterator<String> comment_iter = comment.listIterator();
			
			for (; code_iter.hasNext(); ){
				MipsCode next = code_iter.next();
				
				if (next != null){
					if (! (next instanceof MipsLabel)) writer.append(Translate.pre_instr_space);
					writer.append(next.toString());
				}
				
				String next_comment = comment_iter.next();
				if (next_comment != null)
					writer.append("\t\t\t# ").append(next_comment);
				writer.append("\n");
			}
		}
		
		else {
			
			ListIterator<MipsCode> code_iter = code.listIterator();
		
			for (; code_iter.hasNext(); ){
				MipsCode next = code_iter.next();
				
				if (next != null){
					if (! (next instanceof MipsLabel)) writer.append(Translate.pre_instr_space);
					writer.append(next.toString());
					writer.append("\n");
				}
			}
			
		}
	}
	
	/**
	 * Clears the data structures.
	 * 
	 */
	public static void clear(){
		funcEntry = null;
		
		code = null;
		comment = null;
		labelMap = null;
		argStack = null;
		functionEntryTable = null;
		
		callerSavedRegisterList = null;
	}
	
	/**
	 * Appends the instruction along with the comment to the end.
	 * 
	 * @param instr
	 * @param commentStr
	 */
	private static void addCode(MipsCode instr, String commentStr){
		code.add(instr);
		comment.add(commentStr);
	}

	/**
	 * Appends the instruction with no comment to the end
	 * 
	 * @param instr
	 */
	private static void addCode(MipsCode instr){
		code.add(instr);
		comment.add(null);
	}
	
	/**
	 * Remove the last instruction.
	 * 
	 */
	private static void removeLast() {
	
		code.removeLast();
		comment.removeLast();
	}

	
	/**
	 * Returns the unique MIPS label object that has the name.
	 * 
	 * @param name
	 * @return the unique MIPS label object that has the name
	 */
	private static MipsLabel getMipsLabel(String name){
		MipsLabel ret = labelMap.get(name);	
		
		if (ret == null){
			ret = new MipsLabel(name);
			labelMap.put(name, ret);
		}
		
		return ret;
	}

	/**
	 * Returns if the immediate needs a load.
	 * 
	 * @param immediate
	 * @return
	 */
	private static boolean needLI(int immediate){
		return immediate < -0X8000 || immediate > 0X7FFF;
	}
	
	/**
	 * Returns if the unsigned immediate needs a load.
	 * 
	 * @param immediate
	 * @return
	 */
	private static boolean needLIU(int immediate){
		return immediate < 0 || immediate > 0xFFFF;
	}
	
	/**
	 * Returns an array containing the upper-half and lower-half at location 0 and 1 respectively.
	 * immediate == reg[0] << 16 + reg[1].
	 * 
	 * @param immediate
	 * @return
	 */
	private static int[] separateImmediate(int immediate){
		int ret[] = new int[2];
		
		ret[0] = immediate >>> 16;
		immediate -= (ret[0] << 16);
		
		if (immediate < -0x8000){
			ret[0] -= 1;
			ret[1] = (immediate + 0x10000);
		}
		
		else if (immediate > 0x7FFF){
			ret[0] += 1;
			ret[1] = (immediate - 0x10000);
		}
		
		else {
			ret[1] = immediate;
		}
		
		return ret;
	}
	
	/**
	 * Loads an addressable object.<br>
	 * ret[0] is the register containing the result<br>
	 * 
	 * @param src
	 * @param spill
	 * @param size		the object size
	 * @return
	 */
	private static int loadAddressable(Addressable src, int spill, int size){
		
		if (src instanceof VarWrapper && !((VarWrapper) src).isAllocated()){
			VarWrapper var = (VarWrapper) src;
			
			return regMapping[var.getReg()];
		}
		
		else {
			
			Object addr[] = addressOf(src, spill);
			
			if (!((Boolean) addr[2]).booleanValue()){
				spill = $at;
			}
			
			int opcode = LW;
			
			if (size == 1){
				opcode = LB;
			}
			
			if (addr[0] instanceof MipsLabel){
				
				addCode(new MipsLA(opcode, spill, (MipsLabel) addr[0], ((Number) addr[1]).intValue() ));
			}
			
			else {
				
				addCode(new MipsLS(opcode, spill, ((Number) addr[0]).intValue(), ((Number) addr[1]).intValue() ));
			}
			
			return spill;
		}
		
	}
	
	/**
	 * Calculates the address of the operand. Necessary load may be inserted.
	 * <p>
	 * ret[0] is the base, ret[1] is the offset. reg[ret[0]] + ret[1] comprises the complete address.<br>
	 * or reg[0] is the MIPS label and reg[1] is the offset, ret[0] + ret[1] comprises the complete address.<br>
	 * reg[3] is a boolean indicating whether the backup spill register is used
	 * 
	 * @param operand
	 * @param spill 		the backup spill register
	 * @return
	 */
	private static Object[] addressOf(Operand operand, int spill){
		Object[] ret = null;
		
		if (operand instanceof Indirection){
			Indirection ind = (Indirection) operand;
			Operand src = ind.getSource();
			
			if (src instanceof Addressable){
				
				int reg = loadAddressable((Addressable) src, spill, 4);
				
				int offset = ind.getOffset();
				
				if (needLI(offset)){
					int[] imm = separateImmediate(offset);
					
					addCode(new MipsLUI(LUI, $at, imm[0]));
					addCode(new MipsR(ADDU, $at, $at, reg));
					
					return new Object[]{$at, imm[1], spill == reg};
				}
				
				return new Object[]{reg, offset, spill == reg};
			}
			
			else {
				
				assert(ind.getOffset() == 0) : "indirection of constant shall not have offset in indirection part";
			
				return addressOf(src, spill);
			}
			
		}
		
		else {
			int offset = 0;
			
			if (operand instanceof Address){
				offset += ((Address) operand).getOffset();
				operand = ((Address) operand).getBase();
			}
			
			if (operand instanceof MemLoc){
				
				if (operand instanceof Global){
					Global global = (Global) operand;
					
					if (global.isAllocated()){
						
						offset += global.getOffset() - Translate.gp_offset;
						if (needLI(offset)){
							int imm[] = separateImmediate(Translate.gp + offset);
							addCode(new MipsLUI(LUI, $at, imm[0]), Integer.toString(Translate.gp + offset));
							return new Object[]{$at, imm[1], false};
						}
						
						return new Object[]{$gp, offset, false};
					}
					
					/* otherwise, the global has no address */
				}
				
				/* has to do an indirection to fetch the address of the argument */
				else if (operand instanceof Argument && ((Argument) operand).isPassedByAddress()){
					
					if (((MemLoc) operand).isAllocated()){
						boolean at_used = false;
						
						if (needLI(offset)){
							at_used = true;
							
							int imm[] = separateImmediate(offset);
							addCode(new MipsLUI(LUI, $at, imm[0]), Integer.toString(offset));
							offset = imm[1];
						}
						
						int arg_offset = ((Argument) operand).getOffset();
						int available = (at_used) ? spill : $at;
						if (needLI(arg_offset)){						
							int imm[] = separateImmediate(arg_offset);
							addCode(new MipsLUI(LUI, available, imm[0]));
							addCode(new MipsR(ADDU, available, framePointer, available));
							addCode(new MipsLS(LW, available, available, imm[1]));
						}
						
						else {							
							addCode(new MipsLS(LW, available, framePointer, arg_offset));
						}
						
						if (at_used){
							addCode(new MipsR(ADDU, $at, $at, spill));
						}
						else {
							at_used = false;
						}
						
						return new Object[]{$at, offset, at_used};
					}
					
					/* otherwise, the argument has no address */
				}
				
				else {
					MemLoc memloc = (MemLoc) operand;
					
					if (memloc.isAllocated()){
						
						if (memloc instanceof Argument){
							offset += memloc.getOffset();
						}
						
						else if (memloc instanceof TempSpace) {
							offset -= ra_fp_size + localSize + memloc.getOffset();
						}
						
						else {
							offset -= ra_fp_size + memloc.getOffset();
						}
						
						if (needLI(offset)){
							int imm[] = separateImmediate(offset);
							
							addCode(new MipsLUI(LUI, $at, imm[0]), Integer.toString(offset));
							addCode(new MipsR(ADDU, $at, $at, framePointer));
							
							return new Object[]{$at, imm[1], false};
						}
						
						return new Object[]{framePointer, offset, false};
					}
					
					/* otherwise, the memloc has no address */
				}
			}
			
			else if (operand instanceof Label){
				Label label = (Label) operand;
				MipsLabel mipslabel = getMipsLabel(label.getLabelName());
				
				return new Object[]{mipslabel, offset, false};
			}
			
			else if (operand instanceof IntConstant){
				IntConstant cons = (IntConstant) operand;				
				
				offset += cons.getValue();
				
				if (needLI(offset)){
					int imm[] = separateImmediate(offset);
					
					addCode(new MipsLUI(LUI, $at, imm[0]), Integer.toString(offset));
					
					return new Object[]{$at, imm[1], false};
				}
				
				return new Object[]{$0, offset, false};
			}
			
			else if (operand instanceof VarWrapper){
				VarWrapper var = (VarWrapper) operand;
				
				if (var.isAllocated()){
					
					if (var.getNumber() == 0 && var.getOrigin() instanceof Argument){
						offset += var.getOffset();
					}
					else {
						offset -= ra_fp_size + var.getOffset();
					}
					
					if (needLI(offset)){
						int imm[] = separateImmediate(offset);
						
						addCode(new MipsLUI(LUI, $at, imm[0]), Integer.toString(offset));
						addCode(new MipsR(ADDU, $at, $at, framePointer));
						
						return new Object[]{$at, imm[1], false};
					}
					
					return new Object[]{framePointer, offset, false};
				}
				
				/* otherwise, the variable has no address */
			}
			
		}
		
		ErrorHandling.ReportError("taking address of non-addressable operand", ErrorHandling.ERROR);
		
		return ret;
	}
	
	/**
	 * Returns the register allocated to the operand, -1 if there is no such register.
	 * 
	 * @param operand
	 * @return 	the register allocated to the operand, -1 if there is no such register.
	 */
	private static int registerOf(Operand operand){
		
		if (operand instanceof VarWrapper && !((VarWrapper) operand).isAllocated()){
			return regMapping[((VarWrapper) operand).getReg()];
		}
		
		return -1;
	}
	
	/**
	 * Transfer the data in loaded_to to the right place.
	 * 
	 * @param dest_reg				the destination register
	 * @param loaded_to				the result loaded to
	 * @param original_operand		the original operand to store the result to
	 * @param spill					the spill register used as temporary destination for memory operation
	 * @param spill2				the backup spill register
	 * @return
	 */
	private static void transferResult(int dest_reg, int loaded_to, Operand original_operand, int spill, int spill2){
		
		if (dest_reg == spill){
			
			storeResult((Addressable)original_operand, loaded_to, spill2);
		}
		
		else {
			
			addCode(new MipsR(OR, dest_reg, loaded_to, $0));
		}
	}
	
	/**
	 * Stores the result in the register to the operand (in memory).
	 * 
	 * @param operand	the operand to write to
	 * @param reg		the register containing the result
	 * @param spill		the backup spill register
	 */
	private static boolean storeResult(Addressable operand, int reg, int spill){
		Object[] addr = addressOf(operand, spill);
		
		int op = SW;
		if (operand.getObjectSize() == 1)
			op = SB;
		
		if (addr[0] instanceof MipsLabel){
			
			addCode(new MipsLA(op, reg, (MipsLabel) addr[0], ((Number) addr[1]).intValue()), "write back to " + operand.toString());
		}
		
		else {
			
			addCode(new MipsLS(op, reg, ((Number) addr[0]).intValue(), ((Number) addr[1]).intValue()), "write back to " + operand.toString());
		}
		
		return ((Boolean) addr[2]).booleanValue();
	}
	
	/**
	 * Loads the operand to the designated register. (Possibly not in the register itself)
	 * 
	 * @param reg			the register to load to
	 * @param operand		the operand to perform load
	 * @param requirement	<br>
	 * 						1. bit 0~1 represents whether the an immediate is required to be signed or unsigned. 
	 * 						00 (unused), 01 (signed), 10(unsigned), 11(unspecified) <br>
	 * 						2. bit 2 represents whether the operand can be an 16-bit immediate (1) or not (0)<br>
	 * 						3. bit 3 represents whether the upper and lower halves of the immediate need to be merged (1) or not (0)<br>
	 * 						4. bit 4 represents whether the result could be a label + offset (1) or not (0)
	 * @return				an Object array. the return value means one of the following:<br>
	 * 						1. length == 1, 0 <= ret[0] < 32, ret[0] is the register containing the loaded operand<br>
	 * 						2. length == 1, 98304 <= ret[0] < 163840, ret[0] - 131072 (signedImmediateResultBase) is the signed 16-bit immediate<br>
	 * 						3. length == 1, 262144 <= ret[0] < 327680, ret[0] - 262144 (unsignedImmediateResultBase) is the unsigned 16-bit immediate <br>
	 * 						4. length == 2, 0 <= ret[0] < 32, reg[ret[0]] + ret[1] is the operand<br>
	 * 						5. length == 2, ret[0] is a MipsLabel, ret[1] is an offset
	 */
	private static Object[] loadOperand(int reg, Operand operand, int requirement){
		
		if (operand instanceof Addressable){
			int operand_reg = registerOf(operand);
			if (operand_reg != -1) return new Object[]{operand_reg};
		
			Object[] addr = addressOf(operand, reg);
			
			int op = LW;
			if (((Addressable) operand).getObjectSize() == 1) op = LB;
			
			if (addr[0] instanceof MipsLabel){
				
				addCode(new MipsLA(op, reg, (MipsLabel) addr[0], ((Number) addr[1]).intValue()), "load operand from " + operand.toString());
			}
			
			else {
				
				addCode(new MipsLS(op, reg, ((Number) addr[0]).intValue(), ((Number) addr[1]).intValue()), "load operand from " + operand.toString());
			}
			
			return new Object[]{reg};
		}
		
		else if (operand instanceof IntConstant){
			int value = ((IntConstant) operand).getValue();
						
			if ((requirement & 1) != 0 && !needLI(value)){
				
				if ((requirement & 4) != 0){
					return new Object[]{value + signedImmediateResultBase};
				}
				
				else {
					if (value == 0) return new Object[]{$0};
					
					addCode(new MipsIS(ADDIU, reg, $0, value));
					return new Object[]{reg};
				}
			}
			
			if ((requirement & 2) != 0 && !needLIU(value)){
				if ((requirement & 4) != 0){
					return new Object[]{value + unsignedImmediateResultBase};
				}
				
				else {
					if (value == 0) return new Object[]{$0};
					
					addCode(new MipsIU(ORI, reg, $0, value));
					return new Object[]{reg};
				}
			}
			
			if (needLI(value)){
				int[] imm = separateImmediate(value);
				
				addCode(new MipsLUI(LUI, reg, imm[0]));
				
				if ((requirement & 8) != 0){
					addCode(new MipsIS(ADDIU, reg, reg, imm[1]));
					return new Object[]{reg};
				}
				
				else {
					return new Object[]{reg, imm[1]};
				}
			}
			
			else {
				
				addCode(new MipsIS(ADDIU, reg, $0, value));
				return new Object[]{reg};
			}
			
		}
		
		else if (operand instanceof Address){
			
			Object[] addr = addressOf(operand, reg);
			
			if (addr[0] instanceof MipsLabel){
			
				if ((requirement & 0x10) != 0){
					return new Object[]{addr[0], addr[1]};
				}
				
				else {
					
					addCode(new MipsLA(LA, reg, (MipsLabel) addr[0], ((Number) addr[1]).intValue()));
					return new Object[]{reg};
				}
			}
			
			else {
				
				if ((requirement & 8) != 0){
					
					addCode(new MipsIS(ADDIU, reg, ((Number) addr[0]).intValue(), ((Number) addr[1]).intValue()));
					return new Object[]{reg};
				}
				
				else {
					
					return new Object[]{addr[0], addr[1]};
				}
			}
		}
	
		else if (operand instanceof Label){
			

			if ((requirement & 0x10) != 0){
				return new Object[]{getMipsLabel(((Label) operand).getLabelName()), 0};
			}

			else {

				addCode(new MipsLA(LA, reg, (MipsLabel) getMipsLabel(((Label) operand).getLabelName()), 0));
				return new Object[]{reg};
			}
			
		}
		
		assert(false) : "unreachable branch in loadOperand";
		
		return null;
	}
	
	/**
	 * Sets up the function entry table.
	 * 
	 * @param functionEntryTable
	 */
	public static void setFunctionEntryTable(HashMap<String, FunctionEntry> functionEntryTable){
		InstructionTranslation.functionEntryTable = functionEntryTable;
	}
	
	private static final int signedImmediateResultBase = 131072;
	private static final int unsignedImmediateResultBase = 262144;
	private static final int REQUIRE_SIGNED = 1;
	private static final int REQUIRE_UNSIGNED = 2;
	private static final int REQUIRE_UNSPECIFIED_SIGN = REQUIRE_SIGNED | REQUIRE_UNSIGNED;
	private static final int REQUIRE_IMM_ENABLED = 4;
	private static final int REQUIRE_IMM_MERGE = 8;
	@SuppressWarnings("unused")
	private static final int REQUIRE_LABEL = 0x10;
	
	private static final int REQUIRE_FOR_REG_OPERAND = REQUIRE_UNSPECIFIED_SIGN | REQUIRE_IMM_MERGE;
	private static final int REQUIRE_FOR_SIGNED_IMM_OPERAND = REQUIRE_SIGNED | REQUIRE_IMM_ENABLED | REQUIRE_IMM_MERGE;
	private static final int REQUIRE_FOR_UNSIGNED_IMM_OPERAND = REQUIRE_UNSIGNED | REQUIRE_IMM_ENABLED | REQUIRE_IMM_MERGE;
	
	/** the being translated function entry */
	private static FunctionEntry funcEntry;
	
	/** the control flow graph */
	private static Graph<Block> graph;
	
	/** the frame size of the function */
	private static int frameSize;
	
	/** the $ra and $fp size (adjusted accordingly) */
	private static int ra_fp_size;
	
	/** $fp's offset, 0 if fp does not need to be saved on the stack */
	private static int fp_offset;
	
	/** $ra's offset, 0 if ra does not need to be saved on the stack */
	private static int ra_offset;
	
	/** the local variable size */
	private static int localSize;
	
	/** the temporary space size */
	private static int tempSpaceSize;
	
	/** the maximum argument size of callee */
	private static int callerArgSize;
	
	/** the register containing the frame pointer*/
	private static int framePointer;
	
	/** the generated code */
	private static LinkedList<MipsCode> code;
	
	/** the comments */
	private static LinkedList<String> comment;
	
	/** the mapping from label name to the unique MIPS label object */
	private static HashMap<String, MipsLabel> labelMap;
	
	/** the arguments to push */
	private static LinkedList<Push> argStack;
	
	/** the function entry table */
	private static HashMap<String, FunctionEntry> functionEntryTable;
	
	/** the list of the caller-saved register that saved across the current call */
	private static LinkedList<Integer> callerSavedRegisterList;
	
	/** the saved register size */
	private static int savedRegisterSize;
	
	/** the current being translated block */
	private static Block currentBlock;
	
	/** the current being translated node */
	private static MyLinkedList<Quadruple>.Node currentNode;
	
	/** the index of the exit block */
	private static int exitBlockIndex;
	
	/** the use list of callee-saved registers */
	private static LinkedList<Integer> calleeSavedUseList;
	
	/* registers */	
	
	/** the total number of registers of the architecture */
	private static final int regCount = reg.length;
	
	/** the reassigned register mapping */
	private static int[] regMapping;
	
	/** spill registers */
	private static final int spill1 = SSABasedLinearScan.spill1;
	private static final int spill2 = SSABasedLinearScan.spill2;
	
	/**
	 * The fixed offset to caller's argument offset where to save the registers across call.
	 * -1 denotes not allocated.
	 */
	private static final int[] savedRegOffset = {
		-1, -1, 0, 4, 8, 12, 16, 20,
		24, 28, 32, 36, 40, 44, 48, 52,
		56, 60, 64, 68, 72, 76, 80, 84,
		-1, -1, -1, -1, -1, -1, -1, -1
	};
	
}
