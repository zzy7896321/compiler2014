package main;

import irpack.FunctionEntry;
import irpack.IntermediateRepresentation;
import irpack.StaticVarEntry;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import optimization.ConstantOptimization;
import optimization.FunctionRegisterUse;
import optimization.SSABasedLinearScan;
import optimization.SSAConstructor;
import optimization.SimpleDeadCodeElimination;
import parser.Lexer;
import parser.Parser;
import semantics.SemanticCheck;
import translate.RawIRTranslate;
import translate.Translate;
import java_cup.runtime.*;
import node.NodeBase;

/**
 * The Main class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 11, 2014
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args){
		
		util.RandomHashCode.initialize();
		
		checkArguments(args);
		
		NodeBase ast = parse();
		
		if (ErrorHandling.err_level == ErrorHandling.ERROR){
			if (!suppress_diagnostic) System.err.println("exit 1");
			System.exit(1);
		}
		
		if (ast_only){
			print_ast(ast);
			System.exit(0);
		}
		
		IntermediateRepresentation ir = semanticCheckAndIRGenerate(ast);		// has return
		ast = null;			// clean the AST for space efficiency
		
		if (ErrorHandling.err_level == ErrorHandling.ERROR){
			if (!suppress_diagnostic) System.err.println("exit 1");
			System.exit(1);
		}
		
		optimize(ir);
		
		if (ErrorHandling.err_level == ErrorHandling.ERROR){
			if (!suppress_diagnostic) System.err.println("exit 1");
			System.exit(1);
		}
		
		if (print_ir){
			OutputStream tmp = stdout;
			try {
				stdout = new FileOutputStream("ir.txt");
				printIR(ir);
				stdout.flush();
				stdout.close();
			} catch (Throwable e) {
				
				e.printStackTrace();
			}
			
			
			stdout = tmp;
		}
		
		translate(ir);
		
		if (ErrorHandling.err_level == ErrorHandling.ERROR){
			if (!suppress_diagnostic) System.err.println("exit 1");
			System.exit(1);
		}
		
		if (!suppress_diagnostic) System.err.println("exit 0");
		System.exit(0);
		
		
	}
	
	public static void checkArguments(String[] args){
		for (int i = 0; i != args.length; ++i){
			if (args[i].charAt(0) != '-'){
				if (input == null){
					try{
						input = new FileReader(args[i]);
					}
					catch (FileNotFoundException e){
						System.err.println("input file not found");
						System.exit(1);
					}
					catch (Exception e){
						System.err.println("cannot open input file");
						System.exit(1);
					}
				}
				else {
					System.err.println("warning: more than one file provided but only the first will be processed");
				}
			}
			else if (args[i].equals("-h") || args[i].equals("--help")){
				System.out.print(help_string);
				System.exit(0);
			}
			else if (args[i].equals("-q") || args[i].equals("--quiet")){
				suppress_diagnostic = true;
				ErrorHandling.suppress_output = true;
			}
			else if (args[i].equals("-o")){
				if (++i == args.length){
					System.err.println("output file name not found for -o option");
				}
				else {
					try{
						stdout = new FileOutputStream(args[i]);
					}
					catch (FileNotFoundException e){
						System.err.println("output file not found");
						stdout = System.out;
					}
					catch (Exception e){
						System.err.println("cannot open output file");
						stdout = System.out;
					}
				}
			}
			else if (args[i].equals("-v") || args[i].equals("--verbose")){
				verbose = true;
			}
			else if (args[i].equals("-a") || args[i].equals("--ast")){
				ast_only = true;
			}
			else if (args[i].equals("--ir")){
				print_ir = true;
			}
			else if (args[i].equals("--rem")){
				RawIRTranslate.write_comment = true;
				Translate.write_comment = true;
			}
		}
		
		if (input == null){
			System.err.println("input file not designated");
			System.exit(1);
		}
		
		if (stdout == null){
			stdout = System.out;
		}
	}
	
	public static NodeBase parse(){
		Lexer lexer = new Lexer(input);
		Parser parser = new Parser(lexer);
		
		NodeBase.setLexer(lexer); 		// don't forget
		
		Symbol result = null;
		
		try{
			if (verbose){
				result = parser.debug_parse();	
			}
			else {
				result = parser.parse();
			}
		}
		catch (Exception e) {
			if (!suppress_diagnostic) {
				System.err.println("exit 1");
			}
			System.exit(1);
		}
		
		try {
			input.close();
		} catch (IOException e) {	
			if (!suppress_diagnostic) {
				System.err.println("exit 1");
			}
			System.exit(1);
		}
		
		NodeBase ret = (NodeBase)result.value;
		if (ret != null){
			ret._line = lexer.get_yyline();
			ret._column = lexer.get_yycolumn();
		}
		
		return ret;
	}
	
	public static void print_ast(NodeBase ast){
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stdout));
		
		try{
			ast.traverse(writer);
			writer.flush();
		}
		catch (IOException e){
			if (!suppress_diagnostic) e.printStackTrace();
			System.exit(1);
		}
	}
	
	public static IntermediateRepresentation semanticCheckAndIRGenerate(NodeBase ast){
		SemanticCheck checker = new SemanticCheck();
		
		try{
			return checker.checkAndGenerate(ast);
		}
		catch (Exception e){
			if (!suppress_diagnostic) e.printStackTrace();
			System.exit(1);
		}
		
		return null;
	}
	
	public static void printIR(IntermediateRepresentation ir){
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stdout));
		
		try {
			writer.append("Static Variable Table: \n");
		} catch (IOException e1) {
			
			e1.printStackTrace();
		}
		
		ArrayList<StaticVarEntry> staticVarTable = ir.getStaticVariableTable();
		for (StaticVarEntry entry : staticVarTable){
			try {
				writer.append(entry.toString()).append('\n');
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
		
		try {
			writer.append("Function entry table: \n");
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		List<FunctionEntry> functionEntryTable = ir.getFunctionEntryTable();
		for (FunctionEntry entry : functionEntryTable){
			try{
				writer.append("[Function] name = ").append(entry.getName()).append('\n');
				writer.append("argcount = ").append(Integer.toString(entry.getArgCount())).append('\n');
				writer.append("maximal caller arg size = ").append(Integer.toString(entry.getCallerArgSize())).append('\n');
				writer.append("has return value address = ").append(Boolean.toString(entry.hasReturnValueAddress())).append("\n");
				writer.append("code: \n");
				if (entry.getControlFlowGraph() != null)
					writer.append(entry.getControlFlowGraph().toString());
				else
					writer.append("null\n");
			} catch (IOException e){
				
				e.printStackTrace();
			}
			
			
//			Graph<Block> graph = entry.getControlFlowGraph();
//			for (Block block : graph.getNodeList())
//			for (MyLinkedList<Quadruple>.Node node = block.getList().iterator(); node.hasNext(); )
//			{
//				node = node.next();
//				Quadruple now = node.value();
//				try {
//					writer.append(now.toString());
//					if (now.getOpcode() == Quadruple.LABEL){
//						writer.append(":");
//					}
//					writer.append('\n');
//				} catch (IOException e) {
//					
//					e.printStackTrace();
//				}
//			}
		}
		
		try {
			writer.flush();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	public static void translate(IntermediateRepresentation ir){
		try{
			Writer writer = new BufferedWriter(new OutputStreamWriter(stdout));
			ir.translate(writer);
			writer.flush();
		}
		catch (IOException e){
			e.printStackTrace();
		}
	}
	
	public static void optimize(IntermediateRepresentation ir) {
		for (FunctionEntry entry : ir.getFunctionEntryTable()){
			SSAConstructor.convertToSSA(entry.getControlFlowGraph(), entry.getEntryBlock(), entry.getExitBlock());	
					
//			if (print_ir){
//				OutputStream tmp = stdout;
//				try {
//					stdout = new FileOutputStream("old_ir.txt");
//					printIR(ir);
//					stdout.flush();
//					stdout.close();
//				} catch (Throwable e) {
//					
//					e.printStackTrace();
//				}
//				
//				
//				
//				stdout = tmp;
//			}
			
			ConstantOptimization.optimize(entry.getControlFlowGraph(), entry.getEntryBlock(), SSAConstructor.getVarWrapperSet());
			
			SimpleDeadCodeElimination.eliminateDeadCode(SSAConstructor.getVarWrapperSet());

			
			SSABasedLinearScan.allocateRegister(entry, SSAConstructor.getVarRenamingStack());
		}
		
		FunctionRegisterUse.calculateFunctionRegisterUse(ir);
	}
	
	static final String help_string = 
"compiler2014 project\n" +
"Usage: [Options] ... <filename>\n" +
"Note: only the first file will be processed.\n" +
"\n" +
"Options:\n" +
"		-h, --help			print the help prompt and exit\n" +
"		-a, --ast			only print the abstract syntax tree\n" +
"		-o <output>			redirect standard output to file <output>\n" +
"		-v, --verbose			print debug output\n" + 
"		-q, --quiet			print no diagnostic message\n" +
"		--ir				print intermediate representation\n" +
"		--rem				print debugging comment in assembly file\n"
;
	
	static boolean ast_only = false;
	static boolean verbose = false;
	static boolean suppress_diagnostic = false;
	static String redirect_output_file = null;
	static boolean print_ir = false;
	
	static OutputStream stdout = System.out;
	static FileReader input = null;
}
