package main;

import java.io.*;

/**
 * Defines the Errorhandling class and providies utilities for reporting compliation error.
 *
 * @author Zhao Zhuoyue
 * @version last update: 3/23/2014
 */
public class ErrorHandling{

	/**
	 * Prompt the error message to the user.
	 * DO NOT include an extra line terminator at the end of msg.
	 *
	 * @param	msg				the message to prompt
	 * @param	type			the type of error, 0 indicates no error, 1 warning, 2 error.
	 * 							type 2 will result in compilation ends without code emission
	 */
	static public void ReportError(String msg, int type){
		if (!suppress_output){
			System.err.println(msg);
		}
		if (type > err_level) err_level = type;
	}


	/**
	 * Prompt the error message to the user and terminate the compiler.
	 * DO NOT include an extra line terminator at the end of msg.
	 *
	 * @param	msg				the message to prompt
	 * @param	type			the type of error, 0 indicates no error, 1 warning, 2 error.
	 * 							type 2 will result in compilation ends without code emission
	 * @param	err_code		the exit code, currently overlooked
	 */
	static public void ReportFatalError(String msg, int type, int err_code) throws IOException{
		ReportError(msg, type);
		System.exit(1);
	}
	
	static public final int NOERROR = 0;
	static public final int WARNING = 1;
	static public final int ERROR = 2;
	
	static public int err_level = NOERROR;
	
	static boolean suppress_output = false;
}

