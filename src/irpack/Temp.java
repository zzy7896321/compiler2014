package irpack;

/**
 * The TempValue class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 11, 2014
 */
public class Temp extends MemLoc {

	/**
	 * @param loc
	 * @param size
	 * @param num
	 */
	public Temp() {
		super(TEMPVALUE, 4, tempValueCnt++);

	}

	@Override
	public String toString(){
		
		return new StringBuilder()
			.append("t_")
			.append(getIndex())
			.toString();
	}
	
	private static int tempValueCnt = 0;

	@Override
	public Addressable used() {
		
		return this;
	}

	@Override
	public boolean defined() {
		
		return true;
	}
}
