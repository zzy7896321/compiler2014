package irpack;

/**
 * The TempAddress class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 9, 2014
 */
public class TempAddress extends MemLoc {

	/**
	 * Constructs a TempAddress object.
	 * 
	 * @param loc
	 */
	public TempAddress() {
		super(TEMPADDR, 4, tempCount++);
		this.setObjectSize(4);
	}
	
	public TempAddress(int obj_size){
		super(TEMPADDR, 4, tempCount++);
		this.setObjectSize(obj_size);
	}
	
	/**
	 * For BitField.
	 * 
	 * @param loc
	 * @param obj_size
	 * @param num
	 */
	protected TempAddress(int loc, int obj_size, int num){
		super(loc, 4, num);
		this.setObjectSize(obj_size);
	}

	static public int tempCount = 0;

	@Override
	public boolean used(Quadruple quad){
		
		if (quad instanceof ALUOp){
			ALUOp aluop = (ALUOp) quad;
			
			if (aluop.getOpcode() == Quadruple.ADDA){
				return this == aluop.getRs() || this == aluop.getRt();
			}
			else {
				return this == aluop.getRd() || this == aluop.getRs() || this == aluop.getRt();
			}
		}
		
		else if (quad instanceof Branch){
			Branch branch = (Branch) quad;
			
			return this == branch.getRs() || this == branch.getRt();
		}
		
		else if (quad instanceof Jump){
			Jump jump = (Jump) quad;
			
			return this == jump.getTarget();
		}
		
		else if (quad instanceof LoadStore){
			LoadStore loadStore = (LoadStore) quad;
			
			if (loadStore.getOpcode() != Quadruple.LA)
				return this == loadStore.getRd() || this == loadStore.getRs();
			else
				return this == loadStore.getRs();
		}
		
		else if (quad instanceof MoveRV){
			MoveRV moverv = (MoveRV) quad;
			
			return this == moverv.getDest();
		}
		
		else if (quad instanceof StoreRV){
			StoreRV storerv = (StoreRV) quad;
			
			return this == storerv.getSrc();
		}
		
		else if (quad instanceof Push){
			Push push = (Push) quad;
			
			return this == push.getSrc();
		}
		
		// FunctionInfo
		// Label
		// Phi
		
		return false;
	}
	
	@Override
	public boolean defined(Quadruple quad){
		
		if (quad instanceof ALUOp){
			ALUOp aluop = (ALUOp) quad;
			
			if (aluop.getOpcode() == Quadruple.ADDA) 
				return this == aluop.getRd();
			else
				return false;
		}
		
		else if (quad instanceof LoadStore){
			LoadStore loadStore = (LoadStore) quad;
			
			if (loadStore.getOpcode() != Quadruple.LA)
				return this == loadStore.getRd();
			else
				return false;
		}
		
		// FunctionInfo
		// Label
		// Branch
		// Jump
		// MoveRV
		// StoreRV
		// Push
		// Phi
		
		return false;
	}
	
	@Override
	public boolean isTakenAddress(){
		return false;
	}

	@Override
	public String toString(){
		
		return new StringBuilder()
			.append("t")
			.append("_a_")
			.append(getNo())
			.toString();
	}
	
	
	@Override
	public int getObjectSize() {
		return obj_size;
	}

	/**
	 *	Sets the Object size.
	 * @param obj_size the obj_size to set
	 */
	public void setObjectSize(int obj_size) {
		this.obj_size = obj_size;
	}

	private int obj_size;
}
