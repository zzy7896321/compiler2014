package irpack;

import java.util.LinkedList;
import java.util.ListIterator;

import util.MyLinkedList;

/**
 * The VarWrapper class provides the renaming wrapper of variables for SSA.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 10, 2014
 */
public class VarWrapper implements Addressable, graph.Indexed {

	/**
	 * Constructs a new variable wrapper.
	 * 
	 * @param origin		the original variable
	 * @param number		the renaming number
	 */
	public VarWrapper(MemLoc origin, int number) {
		this.origin = origin;
		this.number = number;
		
		/* initial capacity ? */
		this.defSite = null;
		this.useList = new LinkedList<MyLinkedList<Quadruple>.Node>();
		
		this.reg = -1;
		this.setAllocated(false);
	}
	
	/**
	 * @return	the original variable
	 */
	public MemLoc getOrigin(){
		return this.origin;
	}

	/**
	 * 
	 * @return	the renaming number
	 */
	public int getNumber(){
		return this.number;
	}
	
	/**
	 *	Accesses the definition site.
	 *
	 * @return the defSite
	 */
	public MyLinkedList<Quadruple>.Node getDefSite() {
		return defSite;
	}

	/**
	 *	Sets the definition site.
	 * @param defSite the defSite to set
	 */
	public void setDefSite(MyLinkedList<Quadruple>.Node defSite) {
		this.defSite = defSite;
	}

	/**
	 *	Accesses the use list.
	 *
	 * @return the useList
	 */
	public LinkedList<MyLinkedList<Quadruple>.Node> getUseList() {
		return useList;
	}

	/**
	 *	Add the quadruple to the use list.
	 *
	 * @param node		the quadruple to add
	 */
	public void addUse(MyLinkedList<Quadruple>.Node node) {
		if (!useList.contains(node))
			useList.add(node);
	}
	
	/**
	 * Remove the quadruple from the use list.
	 * 
	 * @param quad		the quadruple to remove
	 */
	public void removeUse(Quadruple quad){
		for (ListIterator<MyLinkedList<Quadruple>.Node> iter = useList.listIterator(); iter.hasNext(); ){
			if (iter.next().value().equals(quad)){
				iter.remove();
			}
		}
	}
	
	/**
	 * Query whether the variable (renamed one) is used by the quadruple.
	 * 
	 * @param quad		the quadruple to query
	 * @return			whether the variable is used by the quadruple
	 */
	public boolean queryUse(Quadruple quad){
		for (MyLinkedList<Quadruple>.Node node : useList){
			if (node.value().equals(quad)) return true;
		}
		
		return false;
	}
	
	/**
	 * Returns the use of the quadruple.
	 * 
	 * @param quad
	 * @return
	 */
	public MyLinkedList<Quadruple>.Node getUse(Quadruple quad){
		for (MyLinkedList<Quadruple>.Node node : useList){
			if (node.value().equals(quad)) return node;
		}
		
		return null;
	}

	
	/**
	 *	Accesses the register.<br> 
	 * -1 denotes not allocated or spilled.<br> 
	 * -2 - k (k >= 0) denotes register k is preferred.<br>
	 *  k (k >= 0) denotes register k is allocated to the variable.<br>
	 *
	 * @return the reg
	 */
	public int getReg() {
		return reg;
	}

	/**
	 *	Sets the register.
	 * @param reg the reg to set
	 */
	public void setReg(int reg) {
		this.reg = reg;
	}

	/**
	 *	Accesses the offset.
	 *
	 * @return the offset
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 *	Sets the offset.
	 * @param offset the offset to set
	 */
	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	/**
	 * Approximate spill cost by use count. One additional cost is added to argument for storing it upon entry.
	 * 
	 * @return
	 */
	public int getSpillCost(){
		
		if (origin instanceof Argument)
			return useList.size() + 1;
		
		return useList.size();
	}

	/**
	 *	Whether the variable is allocated memory space.
	 *
	 * @return the allocated
	 */
	public boolean isAllocated() {
		return allocated;
	}

	/**
	 *	Sets the variable allocated memory space.
	 * @param allocated the allocated to set
	 */
	public void setAllocated(boolean allocated) {
		this.allocated = allocated;
	}

	@Override
	public String toString(){
		return new StringBuilder().append(origin.toString()).append("_").append(Integer.toString(number))
				.toString();
	}

	@Override
	public int hashCode() {
		return origin.hashCode() * 31 + number;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() != this.getClass()) return false;
		
		VarWrapper other = (VarWrapper) obj;
		return origin.equals(other.origin) && number == other.number;
	}

	/** the original variable */
	private MemLoc origin;
	
	/** the renaming number */
	private int number;

	/** the definition site */
	private MyLinkedList<Quadruple>.Node defSite;
	
	/** the use list, implemented with HashSet considering that a huge quantity of uses is rare for SSA variables */
	private LinkedList<MyLinkedList<Quadruple>.Node> useList;
	
	/** the allocated register */
	private int reg;
	
	/** the allocated memory space offset */
	private int offset;
	
	/** whether the variable is allocated memory space */
	private boolean allocated;
	
	/* implementation of the Indexed interface*/
	
	/** the index number */
	private int index;
	
	/**
	 * Sets the index.
	 * 
	 * @param index
	 * @return
	 */
	public void setIndex(int index){
		this.index = index;
	}
	
	@Override
	public int getIndex() {
		
		return index;
	}

	@Override
	public void setTakenAddress(boolean takenAddress) {
		assert(false) : "VarWrapper cannot be taken address";
	}

	@Override
	public boolean isTakenAddress() {
		
		return false;
	}

	@Override
	public Addressable used() {
		
		return this;
	}

	@Override
	public boolean defined() {
		
		return true;
	}

	@Override
	public int getObjectSize() {
		
		return this.origin.getSize();
	}
}
