package irpack;

/**
 * The Push class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 15, 2014
 */
public class Push extends Quadruple {

	/**
	 * @param opcode
	 */
	public Push(int num, int offset, Operand src) {
		super(Quadruple.PUSH);
		
		this.src = src;
		this.num = num;
		this.offset = offset;
	}
	
	/**
	 *	Accesses the number of argument. -1 denotes return value address. -2 denotes variable length part.
	 *
	 * @return the num
	 */
	public int getNum() {
		return num;
	}
	/**
	 *	Sets the number of argument.
	 * @param num the num to set
	 */
	private void setNum(int num) {
		this.num = num;
	}


	/**
	 *	Accesses the operand.
	 *
	 * @return the op
	 */
	public Operand getSrc() {
		return this.src;
	}

	/**
	 *	Sets the operand.
	 * @param op the op to set
	 */
	public void setSrc(Operand op) {
		this.src = op;
	}
	
	/**
	 *	Accesses the offset.
	 *
	 * @return the offset
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 *	Sets the offset.
	 * @param offset the offset to set
	 */
	private void setOffset(int offset) {
		this.offset = offset;
	}
	
	/** the source */
	private Operand src;
	
	/** the number of the argument */
	private int num;
	
	/** the offset of the argument */
	private int offset;
	
	@Override
	public String toString(){
		return new StringBuilder()
			.append("push")
			.append("\t(arg")
			.append(num)
			.append("),\t")
			.append(src.toString())
			.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + opcode;
		result = prime * result + num;
		result = prime * result + offset;
		result = prime * result + ((src == null) ? 0 : src.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Push other = (Push) obj;
		if (opcode != other.opcode) return false;
		if (num != other.num)
			return false;
		if (offset != other.offset)
			return false;
		if (src == null) {
			if (other.src != null)
				return false;
		} else if (!src.equals(other.src))
			return false;
		return true;
	}

	@Override
	public Addressable[] getUse() {
		Addressable[] ret = new Addressable[1];
		int now = 0;
		
		if (src instanceof Addressable){
			Addressable tmp = ((Addressable) src).used();
			if (tmp != null) ret[now++] = tmp;
		}
		
		return ret;
	}

	@Override
	public Addressable[] getDefine() {
		
		return zero_length_list;
	}

	@Override
	public boolean replaceUse(Operand use, Operand toSubstitute) {
		boolean hasReplacement = false;
		
		if (src instanceof Indirection){
			if (((Indirection) src).getSource() == use){
				hasReplacement = true;
				src = ((Indirection) src).newIndirection(toSubstitute, 0);
			}
		}
		else if (src == use){
			hasReplacement = true;
			src = toSubstitute;
		}
		
		return hasReplacement;
	}

	@Override
	public void replaceDefine(VarWrapper toSubstitute) {
	}

	@Override
	public Operand[] getOperands() {
		
		return new Operand[]{src};
	}

	
}
