package irpack;

import java.util.LinkedList;

/**
 * The FunctionInfo class records function entry and end.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 15, 2014
 */
public class FunctionInfo extends Quadruple {

	/**
	 * @param opcode
	 */
	public FunctionInfo(int opcode, FunctionEntry entry) {
		super(opcode);
		
		this.setEntry(entry);
		
	}
	
	
	/**
	 *	Accesses the function entry.
	 *
	 * @return the entry
	 */
	public FunctionEntry getEntry() {
		return entry;
	}


	/**
	 *	Sets the function entry.
	 * @param entry the entry to set
	 */
	private void setEntry(FunctionEntry entry) {
		this.entry = entry;
	}
	
	/**
	 * Adds to the defined list.
	 */
	public void addDefined(Addressable def){
		defined.add(def);
	}

	/** the table entry of the function */
	private FunctionEntry entry;
	
	/** the defined arguments */
	private LinkedList<Addressable> defined;
	
	final public static String Opname[] = {
		"FUNCTION ENTRY BLOCK",
		"FUNCTION EXIT BLOCK",
	};
	
	@Override
	public String toString(){
		return Opname[opcode - function_info_miscellaneous_start];
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + opcode;
		result = prime * result + ((entry == null) ? 0 : entry.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FunctionInfo other = (FunctionInfo) obj;
		if (opcode != other.opcode) return false;
		if (entry == null) {
			if (other.entry != null)
				return false;
		} else if (!entry.equals(other.entry))
			return false;
		return true;
	}


	@Override
	public Addressable[] getUse() {
		
		return zero_length_list;
	}


	@Override
	public Addressable[] getDefine() {
		
		return zero_length_list;
	}


	@Override
	public boolean replaceUse(Operand use, Operand toSubstitute) {
		return false;
	}


	@Override
	public void replaceDefine(VarWrapper toSubstitute) {
	}


	@Override
	public Operand[] getOperands() {
		
		return zero_length_list;
	}
	
	

}
