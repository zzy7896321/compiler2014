package irpack;

/**
 * The Jump class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 15, 2014
 */
public class Jump extends Quadruple {

	/**
	 * Constructs a jump quadruple.
	 * 
	 * @param opcode
	 */
	public Jump(int opcode, Operand target) {
		super(opcode);

		this.target = target;
	}
	
	/**
	 *	Accesses the operands[0].
	 *
	 * @return the operands[0]
	 */
	public Operand getTarget() {
		return target;
	}

	/**
	 *	Sets the operands[0].
	 * @param operands[0] the operands[0] to set
	 */
	private void setTarget(Operand target) {
		this.target = target;
	}

	/** the target of the jump */
	private Operand target;
	
	private static final String[] Opname= new String[]
	{
		"J",
		"JAL",
		"CALL"
	};
	
	@Override
	public String toString(){
		return new StringBuilder()
			.append(Opname[opcode - jump_start])
			.append("\t")
			.append(getTarget().toString())
			.toString(); 
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + opcode;
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Jump other = (Jump) obj;
		if (getOpcode() != other.getOpcode()) return false;
		if (!target.equals(other.target)) return false;
		return true;
	}

	@Override
	public Addressable[] getUse() {
		
		if (target instanceof Addressable){
			Addressable tmp = ((Addressable) target).used();
			if (tmp != null) return new Addressable[]{tmp};
		}
		
		return zero_length_list;
	}

	@Override
	public Addressable[] getDefine() {
		
		return zero_length_list;
	}

	@Override
	public boolean replaceUse(Operand use, Operand toSubstitute) {
		
		if (target instanceof Indirection){
			if (((Indirection) target).getSource() == use){
				target = ((Indirection) target).newIndirection(toSubstitute, 0);
				return true;
			}
		}
		else if (target == use){
			target = toSubstitute;
			return true;
		}
		
		return false;
	}

	@Override
	public void replaceDefine(VarWrapper toSubstitute) {
	}

	@Override
	public Operand[] getOperands() {
		
		return new Operand[]{target};
	}

	public void replaceTarget(Operand operand){
		target = operand;
	}
}
