package irpack;

public class ConstantValueEntry{
	public ConstantValueEntry(int size, Object value){
		this.size = size;
		this.value = value;
	}
	
	public int size;
	public Object value;
}
