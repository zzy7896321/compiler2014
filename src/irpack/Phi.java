package irpack;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * The Phi class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 15, 2014
 */
public class Phi extends Quadruple {

	/**
	 * Constructs a new Phi-function with n parameters of the given variable.
	 * 
	 * @param variable
	 * @param n
	 */
	public Phi(MemLoc variable, int n) {
		super(Quadruple.PHI);

		this.origin = variable;
		this.dest = null;
		this.src = new HashMap<Integer, Operand>();
	}
	
	/**
	 * Returns the original variable.
	 * 
	 * @return
	 */
	public MemLoc getOriginalVariable(){
		return origin;
	}
	
	/**
	 * Returns the destination.
	 * 
	 * @return
	 */
	public VarWrapper getDest(){
		return dest;
	}
	
	/**
	 * Sets the destination.
	 * 
	 * @param dest
	 */
	public void setDest(VarWrapper dest){
		this.dest = dest;
	}
	
	/**
	 * Sets the next source.
	 * 
	 * @param n			the number of the source to set
	 * @param src		the source
	 * @param from		the number of the block the source is from
	 */
	public void setSource(VarWrapper src, int from){
		this.src.put(from, src);
	}
	
	/**
	 * Returns the source list.
	 * 
	 * @return
	 */
	public HashMap<Integer, Operand> getSourceList(){
		return src;
	}
	
	/**
	 * 
	 * 
	 * @return		the number of sources
	 */
	public int numberOfSource(){
		return this.src.size();
	}

	/** the original variable */
	private MemLoc origin;
	
	/** the destination */
	private VarWrapper dest;
	
	/** the sources */
	private HashMap<Integer, Operand> src;
	
	@Override
	public String toString(){
		if (getDest() == null){
			return new StringBuilder()
				.append(origin.toString())
				.append(" = phi(")
				.append(Integer.toString(numberOfSource()))
				.append(")")
				.toString();
		}
		
		StringBuilder builder = new StringBuilder();
		
		builder.append(getDest().toString()).append(" = phi(");
		
		Iterator<Operand> iterator = src.values().iterator();
		if (iterator.hasNext()){
			builder.append(iterator.next().toString());
			for (; iterator.hasNext(); ){
				builder.append(", ").append(iterator.next().toString());
			}
		}
		builder.append(")");
		
		return builder.toString();
	}

	@Override
	public Addressable[] getUse() {
		Addressable[] ret = new Addressable[src.size()];
		
		int i = 0;
		for (Operand opd : src.values()){
			if (opd instanceof Addressable){
				ret[i++] = (Addressable) opd;
			}
		}
		
		return ret;
	}

	@Override
	public Addressable[] getDefine() {
		
		return new Addressable[]{dest};
	}

	@Override
	public boolean replaceUse(Operand use, Operand toSubstitute) {
		boolean hasReplacement = false;
		
		for (Map.Entry<Integer, Operand> entry : src.entrySet()){
			if (entry.getValue() == use){
				hasReplacement = true;
				entry.setValue(toSubstitute);
			}
		}
		
		return hasReplacement;
	}

	@Override
	public void replaceDefine(VarWrapper toSubstitute) {
		setDest(toSubstitute);
	}

	@Override
	public Operand[] getOperands() {
		Operand[] ret = src.values().toArray(new Operand[src.size() + 1]);
		ret[src.size()] = dest;
		
		return ret;
	}	
	
}
