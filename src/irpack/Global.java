package irpack;

/**
 * The GlobalMemLoc class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 28, 2014
 */
public class Global extends MemLoc {


	/**
	 * Constructs a GlobalMemLoc object.
	 * 
	 * @param size
	 */
	public Global(int size) {
		super(GLOBAL, size, globalMemLocCount++);
		
		this.setTakenAddress(true);	// global is currently forced to perform load/store
	}


	static private int globalMemLocCount = 0;
	
		@Override
	public String toString() {
		return new StringBuilder()
			.append("g_")
			.append(getIndex())
			.toString();
	}

	@Override
	public Addressable used() {

		return isTakenAddress() ? null : this;
	}

	@Override
	public boolean defined() {

		return !isTakenAddress();
	}

}
