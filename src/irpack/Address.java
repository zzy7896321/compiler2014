package irpack;

/**
 * The Address class denotes an address constant of an object plus an offset.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 12, 2014
 */
public class Address implements ConstantOperand{

	/**
	 * Constructs an address constant from the base and an offset.
	 * 
	 * @param base
	 * @param offset
	 */
	public Address(Operand base, int offset){
		this.base = base;
		this.offset = offset;
	}
	
	/**
	 * Accesses the base.
	 * 
	 * @return
	 */
	public Operand getBase() {
		return base;
	}
	
	/**
	 * Accesses the offset.
	 * 
	 * @return
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 * Returns a new address with the same base and offset added to the original one.
	 * 
	 * @param isValue
	 * @return
	 */
	public Address newAddress(int offset){
		return new Address(base, this.offset + offset);
	}

	/** the base */
	private Operand base;
	
	/** the offset */
	private int offset;
	
	
	@Override
	public String toString(){
		
		StringBuilder builder = new StringBuilder();
		
		builder.append(getBase().toString());
		
		if (getOffset() >= 0) 
			builder.append(" +");
		else
			builder.append(" ");
		
		builder.append(Integer.toString(getOffset()));
	
		return builder.toString();
	}

	@Override
	public ConstantOperand add(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			return newAddress(((IntConstant) rhs).getValue());
		}
		
		return null;
	}

	@Override
	public ConstantOperand sub(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			return newAddress(-((IntConstant) rhs).getValue());
		}
		
		else if (rhs instanceof Address){
			if (base == ((Address) rhs).base){
				return new IntConstant(offset - ((Address) rhs).offset);
			}
		}
		
		else if (rhs instanceof Label){
			if (base == rhs){
				return new IntConstant(offset);
			}
		}
		
		return null;
	}

	@Override
	public ConstantOperand mul(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value = ((IntConstant) rhs).getValue();
			
			if (value == 0) return new IntConstant(0);
			if (value == 1) return newAddress(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand div(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value = ((IntConstant) rhs).getValue();
			
			if (value == 1) return newAddress(0);
		}
		
		else if (rhs instanceof Address){
			if (base == ((Address) rhs).base && offset == ((Address) rhs).offset){
				return new IntConstant(1);
			}
		}
		
		else if (rhs instanceof Label){
			if (base == rhs && offset == 0) {
				return new IntConstant(1);
			}
		}
		
		return null;
	}

	@Override
	public ConstantOperand mod(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value = ((IntConstant) rhs).getValue();
			
			if (value == 1) return new IntConstant(0);
		}
		
		else if (rhs instanceof Address){
			if (base == ((Address) rhs).base && offset == ((Address) rhs).offset){
				return new IntConstant(0);
			}
		}
		
		else if (rhs instanceof Label){
			if (base == rhs && offset == 0) {
				return new IntConstant(0);
			}
		}
		
		return null;
	}

	@Override
	public ConstantOperand sll(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value= ((IntConstant) rhs).getValue();
			
			if (value == 0) return newAddress(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand sra(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value= ((IntConstant) rhs).getValue();
			
			if (value == 0) return newAddress(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand srl(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value= ((IntConstant) rhs).getValue();
			
			if (value == 0) return newAddress(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand and(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value = ((IntConstant) rhs).getValue();
			
			if (value == 0) return new IntConstant(0);
			if (value == 0xFFFFFFFF) return newAddress(0);
		}
		
		else if (rhs instanceof Address){
			if (base == ((Address) rhs).base && offset == ((Address) rhs).offset){
				return newAddress(0);
			}
		}
		
		else if (rhs instanceof Label){
			if (base == rhs && offset == 0) {
				return newAddress(0);
			}
		}
		
		return null;
	}

	@Override
	public ConstantOperand or(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value = ((IntConstant) rhs).getValue();
			
			if (value == 0) return newAddress(0);
			if (value == 0xFFFFFFFF) return new IntConstant(0XFFFFFFFF);
		}
		
		else if (rhs instanceof Address){
			if (base == ((Address) rhs).base && offset == ((Address) rhs).offset){
				return newAddress(0);
			}
		}
		
		else if (rhs instanceof Label){
			if (base == rhs && offset == 0) {
				return newAddress(0);
			}
		}
		
		return null;
	}

	@Override
	public ConstantOperand xor(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value = ((IntConstant) rhs).getValue();
			
			if (value == 0) return newAddress(0);
		}
		
		else if (rhs instanceof Address){
			if (base == ((Address) rhs).base && offset == ((Address) rhs).offset){
				return new IntConstant(0);
			}
		}
		
		else if (rhs instanceof Label){
			if (base == rhs && offset == 0) {
				return new IntConstant(0);
			}
		}
		
		return null;
	}

	@Override
	public ConstantOperand nor(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value = ((IntConstant) rhs).getValue();
			
			if (value == 0xFFFFFFFF) return new IntConstant(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand slt(ConstantOperand rhs) {
		
		
		return null;
	}

	@Override
	public ConstantOperand sle(ConstantOperand rhs) {
		
		
		return null;
	}

	@Override
	public ConstantOperand sgt(ConstantOperand rhs) {
		
		return null;
	}

	@Override
	public ConstantOperand sge(ConstantOperand rhs) {
				
		return null;
	}

	@Override
	public ConstantOperand seq(ConstantOperand rhs) {
		
		if (rhs instanceof Address){
			if (base == ((Address) rhs).base){
				return (offset == ((Address) rhs).offset) ? new IntConstant(1) : new IntConstant(0);
			}
		}
		
		else if (rhs instanceof Label){
			if (base == rhs){
				return (offset == 0) ? new IntConstant(1) : new IntConstant(0);
			}
		}
		
		return null;
	}

	@Override
	public ConstantOperand sne(ConstantOperand rhs) {
		
		if (rhs instanceof Address){
			if (base == ((Address) rhs).base){
				return (offset != ((Address) rhs).offset) ? new IntConstant(1) : new IntConstant(0);
			}
		}
		
		else if (rhs instanceof Label){
			if (base == rhs){
				return (offset != 0) ? new IntConstant(1) : new IntConstant(0);
			}
		}
		
		return null;
	}

	@Override
	public ConstantOperand sltu(ConstantOperand rhs) {
		
		if (rhs instanceof Address){
			if (base == ((Address) rhs).base){
				return (offset < ((Address) rhs).offset) ? new IntConstant(1) : new IntConstant(0);
			}
			
		}
		
		else if (rhs instanceof Label){
			if (base == rhs){
				return (offset < 0)  ? new IntConstant(1) : new IntConstant(0);
			}
		}
		
		return null;
	}
	
	@Override
	public boolean theSameValueWith(ConstantOperand rhs){
		if (rhs instanceof Address){
			if (base == ((Address) rhs).base){
				return offset == ((Address) rhs).offset;
			}
		}
		
		else if (rhs instanceof Label){
			if (base == rhs){
				return offset == 0;
			}
		}
		
		return false;
	}
}
