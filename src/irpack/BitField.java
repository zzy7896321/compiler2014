package irpack;

/**
 * The BitField class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 11, 2014
 */
public class BitField extends Indirection {

	/**
	 * @param source
	 * @param offset
	 */
	public BitField(Operand source, int offset, int mask) {
		super(source, offset, 4);
		
		this.mask = mask;
	}


	/**
	 *	Accesses the mask.
	 *
	 * @return the mask
	 */
	public int getMask() {
		return mask;
	}
	
	@Override
	public BitField newIndirection(int offset){
		return new BitField(getSource(), getOffset() + offset, mask);
	}
	
	/** the mask of the bit-field */
	private int mask;

	@Override
	public String toString(){
		
		return new StringBuilder()
			.append(Integer.toString(getOffset()))
			.append("(")
			.append(getSource().toString())
			.append(") & ")
			.append(Integer.toString(getMask()))
			.toString();
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + mask;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BitField other = (BitField) obj;
		
		if (getOffset() != other.getOffset())
			return false;
		if (getSize() != other.getSize())
			return false;
		if (getSource() == null) {
			if (other.getSource() != null)
				return false;
		} else if (!getSource().equals(other.getSource()))
			return false;
		
		if (mask != other.mask)
			return false;
		return true;
	}
	
	
}
	

