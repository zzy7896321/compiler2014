package irpack;

import java.util.Arrays;
import java.util.HashMap;

/**
 * The AggregateTypeInitializer class records the initializer for struct, union and array.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 24, 2014
 */
public class AggregateTypeInitializer {
	public AggregateTypeInitializer(){
		_content = new HashMap<Integer, ConstantValueEntry>();
	}
	
	public AggregateTypeInitializer set(int offset, int size, Object value){
		ConstantValueEntry entry = _content.get(offset);
		
		if (entry == null){
			entry = new ConstantValueEntry(size, value);
			_content.put(offset, entry);
		}
		else {
			entry.size = size;
			entry.value = value;
		}
		
		return this;
	}
	
	public AggregateTypeInitializer setBitField(int offset, int value, int mask){
		ConstantValueEntry entry = _content.get(offset);
		
		if (entry == null){
			entry = new ConstantValueEntry(4, (value << Integer.numberOfTrailingZeros(mask)) & mask);
			_content.put(offset, entry);
		}
		else {
			entry.size = 4;
			if (!(entry.value instanceof Number)){
				entry.value = 0;
			}
			
			int rshift_bits = Integer.numberOfLeadingZeros(mask);
			int lshift_bits = rshift_bits + Integer.numberOfTrailingZeros(mask);
			
			value = (value << lshift_bits) >> rshift_bits;
			entry.value = (((Number)entry.value).intValue() & (~mask)) | value;
		}
				
		return this;
	}
	
	/* _content contains pairs of <offset, {size of value, value} > */
	private HashMap<Integer, ConstantValueEntry> _content;
	
	/**
	 *	Accesses the content.
	 *
	 * @return the _content
	 */
	public HashMap<Integer, ConstantValueEntry> getContent() {
		return _content;
	}

	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();
		
		Integer[] keys = _content.keySet().toArray(new Integer[0]);
		Arrays.sort(keys);
		ConstantValueEntry entry = null;
		for (Integer offset : keys){
			entry = _content.get(offset);
			builder.append("\n")
				.append(offset)
				.append(" : ");
			if (entry == null){
				builder.append("null");
			}
			else {
				builder.append("[size = ")
					.append(entry.size)
					.append(" , value = ")
					.append(entry.value.toString())
					.append("]");
			}
		}
		
		return builder.toString();
	}
	
}

