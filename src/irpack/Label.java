package irpack;

/**
 * The Label class is the class representing labels in intermediate representation.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 15, 2014
 */
public class Label extends Quadruple implements ConstantOperand{

	/**
	 * Constructs a label with its name.
	 * 
	 * @param labelname
	 */
	public Label(String labelname){
		super(Quadruple.LABEL);
		
		labelName = labelname;
		
	}
	
	/** the name of the label */
	private String labelName;

	/**
	 * Accesses the name of the label.
	 * 
	 * @return
	 */
	public String getLabelName() {
		return labelName;
	}
	
	/** the header of a function label */
	public static String function_head = "LBF";
	
	/** the header of a function beginning label */
	public static String function_begin_head = "LBFB";
	
	/** the header of a function exit block label */
	public static String function_end_head = "LBFE";
	
	/** the header of other labels */
	public static String text_head = "LB";

	@Override
	public String toString(){
		
		return labelName;
	}

	
	/* optimization information */
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + opcode;
		result = prime * result + labelName.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Label other = (Label) obj;
		return labelName.equals(other.labelName);
	}


	@Override
	public Addressable[] getUse() {
		
		return zero_length_list;
	}


	@Override
	public Addressable[] getDefine() {
		
		return zero_length_list;
	}


	@Override
	public boolean replaceUse(Operand use, Operand toSubstitute) {
		return false;
	}


	@Override
	public void replaceDefine(VarWrapper toSubstitute) {
	}


	@Override
	public ConstantOperand add(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			return new Address(this, ((IntConstant) rhs).getValue());
		}
		
		return null;
	}


	@Override
	public ConstantOperand sub(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			return new Address(this, -((IntConstant) rhs).getValue());
		}
		
		else if (rhs instanceof Address){
			if (this == ((Address) rhs).getBase()){
				return new IntConstant( -((Address) rhs).getOffset());
			}
			
		}
		
		else if (rhs instanceof Label){
			if (this == rhs){
				return new IntConstant(0);
			}
		}
		
		return null;
	}


	@Override
	public ConstantOperand mul(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value = ((IntConstant) rhs).getValue();
			
			if (value == 0) return new IntConstant(0);
			if (value == 1) return this;
		}
		
		return null;
	}

	@Override
	public ConstantOperand div(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value = ((IntConstant) rhs).getValue();
			
			if (value == 1) return this;
		}
		
		else if (rhs instanceof Address){
			if (this == ((Address) rhs).getBase() && 0 == ((Address) rhs).getOffset()){
				return new IntConstant(1);
			}
		}
		
		else if (rhs instanceof Label){
			if (this == rhs) {
				return new IntConstant(1);
			}
		}
		
		return null;
	}

	@Override
	public ConstantOperand mod(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value = ((IntConstant) rhs).getValue();
			
			if (value == 1) return new IntConstant(0);
		}
		
		else if (rhs instanceof Address){
			if (this == ((Address) rhs).getBase() && 0 == ((Address) rhs).getOffset()){
				return new IntConstant(0);
			}
		}
		
		else if (rhs instanceof Label){
			if (this == rhs) {
				return new IntConstant(0);
			}
		}
		
		return null;
	}

	@Override
	public ConstantOperand sll(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value= ((IntConstant) rhs).getValue();
			
			if (value == 0) return this;
		}
		
		return null;
	}

	@Override
	public ConstantOperand sra(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value= ((IntConstant) rhs).getValue();
			
			if (value == 0) return this;
		}
		
		return null;
	}

	@Override
	public ConstantOperand srl(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value= ((IntConstant) rhs).getValue();
			
			if (value == 0) return this;
		}
		
		return null;
	}

	@Override
	public ConstantOperand and(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value = ((IntConstant) rhs).getValue();
			
			if (value == 0) return new IntConstant(0);
			if (value == 0xFFFFFFFF) return this;
		}
		
		else if (rhs instanceof Address){
			if (this == ((Address) rhs).getBase() && 0 == ((Address) rhs).getOffset()){
				return this;
			}
		}
		
		else if (rhs instanceof Label){
			if (this == rhs) {
				return this;
			}
		}
		
		return null;
	}

	@Override
	public ConstantOperand or(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value = ((IntConstant) rhs).getValue();
			
			if (value == 0) return this;
			if (value == 0xFFFFFFFF) return new IntConstant(0XFFFFFFFF);
		}
		
		else if (rhs instanceof Address){
			if (this == ((Address) rhs).getBase() && 0 == ((Address) rhs).getOffset()){
				return this;
			}
		}
		
		else if (rhs instanceof Label){
			if (this == rhs) {
				return this;
			}
		}
		
		return null;
	}

	@Override
	public ConstantOperand xor(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value = ((IntConstant) rhs).getValue();
			
			if (value == 0) return this;
		}
		
		else if (rhs instanceof Address){
			if (this == ((Address) rhs).getBase() && 0 == ((Address) rhs).getOffset()){
				return new IntConstant(0);
			}
		}
		
		else if (rhs instanceof Label){
			if (this == rhs) {
				return new IntConstant(0);
			}
		}
		
		return null;
	}

	@Override
	public ConstantOperand nor(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			int value = ((IntConstant) rhs).getValue();
			
			if (value == 0xFFFFFFFF) return new IntConstant(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand slt(ConstantOperand rhs) {
		
		
		return null;
	}

	@Override
	public ConstantOperand sle(ConstantOperand rhs) {
		
		
		return null;
	}

	@Override
	public ConstantOperand sgt(ConstantOperand rhs) {
		
		return null;
	}

	@Override
	public ConstantOperand sge(ConstantOperand rhs) {
				
		return null;
	}

	@Override
	public ConstantOperand seq(ConstantOperand rhs) {
		
		if (rhs instanceof Address){
			if (this == ((Address) rhs).getBase()){
				return (0 == ((Address) rhs).getOffset()) ? new IntConstant(1) : new IntConstant(0);
			}
		}
		
		else if (rhs instanceof Label){
			return (this == rhs) ? new IntConstant(1) : new IntConstant(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand sne(ConstantOperand rhs) {
		
		if (rhs instanceof Address){
			if (this == ((Address) rhs).getBase()){
				return (0 != ((Address) rhs).getOffset()) ? new IntConstant(1) : new IntConstant(0);
			}
		}
		
		else if (rhs instanceof Label){
			return (this != rhs) ? new IntConstant(1) : new IntConstant(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand sltu(ConstantOperand rhs) {
		
		if (rhs instanceof Address){
			if (this == ((Address) rhs).getBase()){
				return (0 < ((Address) rhs).getOffset()) ? new IntConstant(1) : new IntConstant(0);
			}
			
		}
		
		else if (rhs instanceof Label){
			if (this == rhs){
				return new IntConstant(0);
			}
		}
		
		return null;
	}


	@Override
	public boolean theSameValueWith(ConstantOperand rhs) {
		
		if (rhs instanceof Address){
			if (this == ((Address) rhs).getBase()){
				return 0 == ((Address) rhs).getOffset();
			}
		}
		
		else if (rhs instanceof Label){
			return this == rhs;
		}
		
		return false;
	}


	@Override
	public Operand[] getOperands() {
		
		return zero_length_list;
	}
	
}
