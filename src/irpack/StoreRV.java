package irpack;

/**
 * The StoreRV class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 15, 2014
 */
public class StoreRV extends Quadruple {

	/**
	 * Constructs a store-to-v0 operation.
	 * 
	 * @param opcode
	 */
	public StoreRV(Operand src) {
		super(STORERV);

		this.src = src;
	}

	
	/**
	 *	Accesses the source.
	 *
	 * @return the src
	 */
	public Operand getSrc() {
		return this.src;
	}


	/**
	 *	Sets the source.
	 * @param src the src to set
	 */
	private void setSrc(Operand src) {
		this.src = src;
	}
	
	/** the source */
	private Operand src;
	
	@Override
	public String toString(){
		return new StringBuilder()
			.append("storeRV")
			.append("\t")
			.append(src.toString())
			.toString();
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + opcode;
		result = prime * result + ((src == null) ? 0 : src.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StoreRV other = (StoreRV) obj;
		if (src != other.src) return false;
		if (src == null) {
			if (other.src != null)
				return false;
		} else if (!src.equals(other.src))
			return false;
		return true;
	}
	
	@Override
	public Addressable[] getUse() {
		Addressable[] ret = new Addressable[1];
		int now = 0;
		
		if (src instanceof Addressable){
			Addressable tmp = ((Addressable) src).used();
			if (tmp != null) ret[now++] = tmp;
		}
		
		return ret;
	}

	@Override
	public Addressable[] getDefine() {
		
		return zero_length_list;
	}

	@Override
	public boolean replaceUse(Operand use, Operand toSubstitute) {
		boolean hasReplacement = false;
		
		if (src instanceof Indirection){
			if (((Indirection) src).getSource() == use){
				hasReplacement = true;
				src = ((Indirection) src).newIndirection(toSubstitute, 0);
			}
		}
		else if (src == use){
			hasReplacement = true;
			src = toSubstitute;
		}
		
		return hasReplacement;
	}


	@Override
	public void replaceDefine(VarWrapper toSubstitute) {
	}


	@Override
	public Operand[] getOperands() {
		
		return new Operand[]{src};
	}
}
