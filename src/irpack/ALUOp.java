package irpack;

/**
 * The ALUOp class is the class of ALU operations in intermediate representation.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 15, 2014
 */
public class ALUOp extends Quadruple {

	/**
	 * Constructs an ALU operation from destination, source1 and source2 linked to itself.
	 * 
	 * @param opcode	the opcode
	 * @param rd		the destination
	 * @param rs		source 1
	 * @param rt		source 2
	 */
	public ALUOp(int opcode, Operand rd, Operand rs, Operand rt) {
		super(opcode);
		
		this.rd = rd;
		this.rs = rs;
		this.rt = rt;
	}
	
	/**
	 *	Accesses the destination.
	 *
	 * @return the rd
	 */
	public Operand getRd() {
		return this.rd;
	}

	/**
	 *	Sets the destination.
	 * @param rd the rd to set
	 */
	private void setRd(Operand rd) {
		this.rd = rd;
	}

	/**
	 *	Accesses source 1.
	 *
	 * @return the rs
	 */
	public Operand getRs() {
		return this.rs;
	}

	/**
	 *	Sets source 1.
	 * @param rs the rs to set
	 */
	private void setRs(Operand rs) {
		this.rs = rs;
	}

	/**
	 *	Accesses source 2.
	 *
	 * @return the rt
	 */
	public Operand getRt() {
		return this.rt;
	}

	/**
	 *	Sets source 2.
	 * @param rt the rt to set
	 */
	private void setRt(Operand rt) {
		this.rt = rt;
	}

	@Override
	public Addressable[] getUse() {
		Addressable[] ret = new Addressable[3];
		int now = 0;
		
		if (rd instanceof Indirection){
			Addressable tmp = ((Addressable) rd).used();
			if (tmp != null) ret[now++] = tmp;
		}
		
		if (rs instanceof Addressable){
			Addressable tmp = ((Addressable) rs).used();
			if (tmp != null) ret[now++] = tmp;
		}
		
		if (rt instanceof Addressable){
			Addressable tmp = ((Addressable) rt).used();
			if (tmp != null) ret[now++] = tmp;
		}
		
		return ret;
	}

	@Override
	public Addressable[] getDefine() {
		if (rd instanceof Addressable){
			if (((Addressable) rd).defined()) return new Addressable[]{(Addressable) rd};
		}
		
		return zero_length_list;
	}

	@Override
	public boolean replaceUse(Operand use, Operand toSubstitute) {		
		boolean hasReplacement = false;
		
		if (rd instanceof Indirection){
			if (((Indirection) rd).getSource() == use){
				hasReplacement = true;
				rd = ((Indirection) rd).newIndirection(toSubstitute, 0);
			}
		}
		
		if (rs instanceof Indirection){
			if (((Indirection) rs).getSource() == use){
				hasReplacement = true;
				rs = ((Indirection) rs).newIndirection(toSubstitute, 0);
			}
		}
		else if (rs == use){
			rs = toSubstitute;
			hasReplacement = true;
		}
		
		if (rt instanceof Indirection){
			if (((Indirection) rt).getSource() == use){
				hasReplacement = true;
				rt = ((Indirection) rt).newIndirection(toSubstitute, 0);
			}
		}
		else if (rt == use){
			hasReplacement = true;
			rt = toSubstitute;
		}
		
		return hasReplacement;
	}
	
	/** the destination */
	private Operand rd;
	
	/** source 1 */
	private Operand rs;
	
	/** source 2 */
	private Operand rt;
	
	/** the name of operations */
	private static final String[] Opname= new String[]
	{
		"ADD",
		"SUB",
		"MUL",
		"DIV",
		"MOD",
		"SLL",
		"SRA",
		"SRL",
		"AND",
		"OR",
		"XOR",
		"NOR",
		"SLT",
		"SLE",
		"SGT",
		"SGE",
		"SEQ",
		"SNE",
		"SLTU"
	};
	
	@Override
	public String toString(){
		return new StringBuilder()
			.append(Opname[opcode - ALU_start])
			.append("\t")
			.append(getRd().toString())
			.append(",\t")
			.append(getRs().toString())
			.append(",\t")
			.append(getRt().toString())
			.toString(); 
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + opcode;
		result = prime * result + ((rd == null) ? 0 : rd.hashCode());
		result = prime * result + ((rs == null) ? 0 : rs.hashCode());
		result = prime * result + ((rt == null) ? 0 : rt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ALUOp other = (ALUOp) obj;
		if (opcode != other.opcode) return false;
		if (rd == null) {
			if (other.rd != null)
				return false;
		} else if (!rd.equals(other.rd))
			return false;
		if (rs == null) {
			if (other.rs != null)
				return false;
		} else if (!rs.equals(other.rs))
			return false;
		if (rt == null) {
			if (other.rt != null)
				return false;
		} else if (!rt.equals(other.rt))
			return false;
		return true;
	}

	@Override
	public void replaceDefine(VarWrapper toSubstitute) {
		setRd(toSubstitute);
	}

	@Override
	public Operand[] getOperands() {
		
		return new Operand[]{rd, rs, rt};
	}

}
