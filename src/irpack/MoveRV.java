package irpack;

/**
 * The MoveRV class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 15, 2014
 */
public class MoveRV extends Quadruple {

	/**
	 * Constructs a move-rv-to-destination operation.
	 * 
	 * @param opcode
	 */
	public MoveRV(Operand dest) {
		super(MOVRV);

		this.dest = dest;
	}

	
	/**
	 *	Accesses the destination.
	 *
	 * @return the dest
	 */
	public Operand getDest() {
		return this.dest;
	}


	/**
	 *	Sets the destination.
	 * @param dest the dest to set
	 */
	private void setDest(Operand dest) {
		this.dest = dest;
	}

	/** the destination */
	private Operand dest;

	@Override
	public String toString(){
		return new StringBuilder()
			.append("moveRV")
			.append("\t")
			.append(dest.toString())
			.toString();
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + opcode;
		result = prime * result + ((dest == null) ? 0 : dest.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MoveRV other = (MoveRV) obj;
		if (opcode != other.opcode) return false;
		if (dest == null) {
			if (other.dest != null)
				return false;
		} else if (!dest.equals(other.dest))
			return false;
		return true;
	}


	@Override
	public Addressable[] getUse() {
		Addressable[] ret = new Addressable[1];
		int now = 0;
		
		if (dest instanceof Indirection){
			Addressable tmp = ((Addressable) dest).used();
			if (tmp != null) ret[now++] = tmp;
		}
		
		return ret;
	}


	@Override
	public Addressable[] getDefine() {
		if (dest instanceof Addressable){
			if (((Addressable) dest).defined()) return new Addressable[]{(Addressable) dest};
		}
		
		return zero_length_list;
	}


	@Override
	public boolean replaceUse(Operand use, Operand toSubstitute) {
		if (dest instanceof Indirection){
			if (((Indirection) dest).getSource() == use){
				dest = ((Indirection) dest).newIndirection(toSubstitute, 0);
				return true;
			}
		}
		
		return false;
	}


	@Override
	public void replaceDefine(VarWrapper toSubstitute) {
		setDest(toSubstitute);
	}


	@Override
	public Operand[] getOperands() {
		
		return new Operand[]{dest};
	}
	
	
}
