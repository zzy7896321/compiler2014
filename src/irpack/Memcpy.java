package irpack;

/**
 * The Memcpy class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 15, 2014
 * @deprecated
 */
public class Memcpy extends Quadruple {

	/**
	 * @param opcode
	 */
	public Memcpy(Operand dest, Operand src, int size) {
		super(MEMCPY);
		
		this.setDest(dest);
		this.setSrc(src);
		this.setSize(size);
	}

	
	/**
	 *	Accesses the source.
	 *
	 * @return the src
	 */
	public Operand getSrc() {
		return src;
	}
	/**
	 *	Sets the source.
	 * @param src the src to set
	 */
	public void setSrc(Operand src) {
		this.src = src;
	}


	/**
	 *	Accesses the destination.
	 *
	 * @return the dest
	 */
	public Operand getDest() {
		return dest;
	}


	/**
	 *	Sets the destination.
	 * @param dest the dest to set
	 */
	public void setDest(Operand dest) {
		this.dest = dest;
	}


	/**
	 *	Accesses the size.
	 *
	 * @return the size
	 */
	public int getSize() {
		return size;
	}


	/**
	 *	Sets the size.
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}


	private Operand dest, src;
	private int size;
	
	@Override
	public String toString(){
		return new StringBuilder()
			.append("memcpy")
			.append("\t")
			.append(dest.toString())
			.append(",\t")
			.append(src.toString())
			.append(",\t")
			.append(size)
			.toString();
	}


	@Override
	public Addressable[] getUse() {
		
		return null;
	}


	@Override
	public Addressable[] getDefine() {
		
		return null;
	}


	@Override
	public boolean replaceUse(Operand use, Operand toSubstitute) {
		return false;
	}


	@Override
	public void replaceDefine(VarWrapper toSubstitute) {
	}


	@Override
	public Operand[] getOperands() {
		
		return null;
	}
}
