package irpack;

/**
 * The ArgCallee class denotes arguments belonging to callee.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 28, 2014
 */
public class Argument extends MemLoc {

	/**
	 * Constructs an ArgCallee object.
	 * 
	 * @param num
	 */
	public Argument(int num, int size, boolean passByAddress) {
		super(ARGCALLEE, size, num);	
		
		this.passedByAddress = passByAddress;
	}

	
	/**
	 *  Returns whether the argument is passed by address or not (by value) .
	 *
	 * @return the passByAddress
	 */
	public boolean isPassedByAddress() {
		return passedByAddress;
	}

	/** whether the argument is passed by address or not (by value) */
	private boolean passedByAddress;

	@Override
	public String toString(){
		
		return new StringBuilder()
			.append("arg_")
			.append(getIndex())
			.toString();
	}

	@Override
	public Addressable used() {
		
		return (passedByAddress || isTakenAddress()) ? null : this;
	}

	@Override
	public boolean defined() {
		
		return !passedByAddress && !isTakenAddress();
	}
	
}
