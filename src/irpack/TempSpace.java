package irpack;

/**
 * The TempSpace class represents temporary space for struct/union.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 1, 2014
 */
public class TempSpace extends MemLoc {

	/**
	 * 
	 * 
	 * @param loc
	 * @param size
	 * @param num
	 */
	public TempSpace(int size) {
		super(TEMPSPACE, size, tempSpaceCount++);
		
	}

	static private int tempSpaceCount = 0;
	
	@Override
	public String toString(){
		return new StringBuilder()
			.append("t_space_")
			.append(getIndex())
			.toString();
	}

	@Override
	public Addressable used() {
		
		return null;
	}

	@Override
	public boolean defined() {
		
		return false;
	}
}
