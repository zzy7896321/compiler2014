package irpack;

import graph.Block;
import graph.Graph;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;

import optimization.ControlFlowGraphBuilder;
import optimization.SSABasedLinearScan;

/**
 * The FunctionEntry class records essential information of each function.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 24, 2014
 */
public class FunctionEntry implements graph.Indexed{

	public FunctionEntry(String name, IntermediateRepresentation ir){
		this.argCount = 0;
		this.labelCount = 0;
		this.name = name;
		this.ir = ir;
		
		this.returnValueAddress = null;
		this.wrappedReturnValueAddress = null;
		this.argumentTable = new ArrayList<Argument>();
		this.wrappedArgumentTable = new ArrayList<VarWrapper>();
		
		this.set_ra_fp_size(8);		// with space for ra and fp
		this.setLocalSize(0);
		this.setTempSize(0);
		this.setTempSpaceSize(0);
		this.offset = 8;
		this.calledSet = new HashSet<Label>();
		this.usedRegisterSet = new BitSet(SSABasedLinearScan.regCount);
		
		/* 
		 * LBF${function_name}:
		 * # function entry block
		 * LBFB${function_name}:
		 *  
		 **/
		
		this.controlFlowGraphBuilder = new ControlFlowGraphBuilder();
		
		this.controlFlowGraphBuilder.appendCode(ir.getFunctionLabel(name));
		this.controlFlowGraphBuilder.appendCode(op.functionEntryPoint(this));
		this.controlFlowGraphBuilder.appendCode(ir.getFunctionBeginLabel(name));
		
		this.entryBlock = this.exitBlock = null;
	}
	
	/**
	 * Creates a dummy function entry for stdlib.
	 * 
	 * @param name
	 */
	public FunctionEntry(String name){
		this.name = name;
		
		this.controlFlowGraph = null;
		this.calledSet = new HashSet<Label>();
	}

	/**
	 * Append function end block to the code.
	 * 
	 */
	public void finish(){
		this.controlFlowGraphBuilder.appendCode(ir.getFunctionEndLabel(name));
		this.controlFlowGraphBuilder.appendCode(op.functionEndPoint(this));
		this.controlFlowGraphBuilder.finish();
		
		this.controlFlowGraph = this.controlFlowGraphBuilder.getCfg();
		this.controlFlowGraphBuilder = null;
		
		this.entryBlock = this.controlFlowGraph.getFirstNode();
		this.exitBlock = this.controlFlowGraph.getLastNode();
	}
	
	/**
	 *	Accesses the argument count, including return value address.
	 *
	 * @return the argCount
	 */
	public int getArgCount() {
		return argCount;
	}

	/**
	 *	Sets the argument count.
	 * @param argCount the argCount to set
	 */
	public FunctionEntry setArgCount(int argCount) {
		this.argCount = argCount;
		return this;
	}
	
	/**
	 * Appends code to the function.
	 * 
	 * @param code_to_append
	 * @return
	 */
	public FunctionEntry appendCode(Quadruple code_to_append){
		this.controlFlowGraphBuilder.appendCode(code_to_append);
		
		return this;
	}
	
	/**
	 * Creates a new label in the function.
	 * 
	 * @return	the new label created
	 */
	public Label newLabel(){
		return ir.getLabel(
				new StringBuilder().append(Label.text_head).append(name).append(labelCount++).toString());
	}
	
	/**
	 * Get an existing label in the function. (No boundary checking)
	 * 
	 * @param count
	 * @return
	 */
	public Label getLabel(int count){
		return ir.getLabel(
				new StringBuilder().append(Label.text_head).append(name).append(count).toString());
	}
	
	/**
	 *	Accesses the label count.
	 *
	 * @return the labelCount
	 */
	public int getLabelCount() {
		return labelCount;
	}
	
	/**
	 * Accesses the function header label after construction of control flow graph.
	 * 
	 * @return
	 */
	public Label getFunctionHeaderLabel(){
		return (Label) this.controlFlowGraph.getFirstNode().getList().getFirst();
	}
	
	/**
	 * Accesses the function begin label after construction of control flow graph.
	 * 
	 * @return
	 */
	public Label getFunctionBeginLabel(){
		return (Label) this.controlFlowGraph.getIthNode(1).getList().getFirst();
	}
	
	/**
	 * Accesses the function end label after construction of control flow graph.
	 * 
	 * @return
	 */
	public Label getFunctionEndLabel(){
		return (Label) this.controlFlowGraph.getLastNode().getList().getFirst();
	}
	
	/**
	 *	Returns if the function has a return value address for struct/union.
	 *
	 * @return the hasReturnValueAddress
	 */
	public boolean hasReturnValueAddress() {
		return returnValueAddress != null;
	}
	
	/**
	 * Returns return value address for struct/union.
	 * 
	 * @return
	 */
	public Argument getReturnValueAddress(){
		return this.returnValueAddress;
	}

	/**
	 *	Sets the function to have a return value address for struct/union. 
	 *	This function is to be invoked before registration for any argument.
	 *
	 */
	public void setReturnValueAddress(int size) {
		this.returnValueAddress = new Argument(-1, size, true);
		this.returnValueAddress.setAllocated(false);
		this.returnValueAddress.setOffset(0);
		
		argCount = 1;
	}

	/**
	 *	Accesses the wrapped return value address.
	 *
	 * @return the wrappedReturnValueAddress
	 */
	public VarWrapper getWrappedReturnValueAddress() {
		return wrappedReturnValueAddress;
	}

	/**
	 *	Sets the wrapped return value address.
	 * @param wrappedReturnValueAddress the wrappedReturnValueAddress to set
	 */
	public void setWrappedReturnValueAddress(VarWrapper wrappedReturnValueAddress) {
		this.wrappedReturnValueAddress = wrappedReturnValueAddress;
	}

	/**
	 *	Accesses the wrapped argument table.
	 *
	 * @return the wrappedArgumentTable
	 */
	public ArrayList<VarWrapper> getWrappedArgumentTable() {
		return wrappedArgumentTable;
	}
	
	/**
	 * Sets the wrapped argument.
	 * 
	 * @param arg
	 */
	public void setWrapperArgument(VarWrapper arg){
		
		wrappedArgumentTable.set(arg.getOrigin().getIndex(), arg);
	}

	/**
	 * Register the MemLoc object as the next argument of the function.		-offset ($fp)
	 * 
	 * @param memloc
	 */
	public void registerArgument(Argument memloc){
		memloc.setIndex(argCount - ((returnValueAddress != null) ? 1 : 0));
		memloc.setAllocated(false);
		memloc.setOffset((argCount++) << config.TargetConfig.register_shift_size);
		
		argumentTable.add(memloc);
		wrappedArgumentTable.add(null);
	}

	/**
	 *	Accesses the maximum size of arguments to be passed to invoked functions.
	 *
	 * @return the callerArgSize
	 */
	public int getCallerArgSize() {
		return callerArgSize;
	}

	/**
	 *	Updates the the maximum size of arguments to be passed to invoked functions.
	 * @param callerArgSize the callerArgSize to set
	 */
	public void updateCallerArgSize(int callerArgSize) {
		this.callerArgSize = this.callerArgSize > callerArgSize ? this.callerArgSize : callerArgSize;
	}
	
	/**
	 *	Accesses the total size of saved registers.
	 *
	 * @return the savedRegSize
	 */
	public int getSavedRegSize() {
		return savedRegSize;
	}

	/**
	 *	Updates the maximum size of saved registers.
	 * @param savedRegSize the savedRegSize to set
	 */
	public void updateSavedRegisterSize(int savedRegSize) {
		this.savedRegSize = this.savedRegSize > savedRegSize ? this.savedRegSize : savedRegSize;
	}

	/**
	 * Registers the temporary space in the function.  - ra_fp_size - localSize - offset ($fp)
	 * 
	 * @param space
	 */
	public void registerTemporarySpace(TempSpace space){
		space.setAllocated(true);
		this.tempSpaceSize = alignAllocate(this.tempSpaceSize, space.getSize());
		space.setOffset(this.tempSpaceSize);
	}
	
	/**
	 * Registers local variables in the function.
	 * 
	 * @param memloc
	 */
	public void registerLocalVariable(MemLoc memloc){
//		memloc.setAllocated(true);
//		int new_offset = alignAllocate(this.offset, memloc.getSize());
//		memloc.setOffset(-new_offset);
//		tempSpaceSize += new_offset - this.offset;
//		this.offset = new_offset;
//		
	}
	
	/**
	 * Registers the temporaries in the function.
	 * 
	 * @param memloc
	 */
	public void registerTemp(MemLoc memloc){
//		memloc.setAllocated(true);
//		int new_offset = alignAllocate(this.offset, memloc.getSize());
//		memloc.setOffset(-new_offset);
//		tempSpaceSize += new_offset - this.offset;
//		this.offset = new_offset;
	}
	
	/**
	 * Allocates local storage space on the stack.  - ra_fp_size - offset ($fp)
	 * 
	 * @param size
	 * @return
	 */
	public int allocateLocalSpace(int size){
		
		this.localSize = alignAllocate(this.localSize, size);
		return this.localSize;
	}
	
	/**
	 *	Accesses the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 *	Sets the name.
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *	Accesses the return address and frame pointer reservation size.<br>
	 *	ra: -4($new_fp)
	 *	fp: -8($new_fp)
	 *
	 * @return the ra_fp_size
	 */
	public int get_ra_fp_size() {
		return ra_fp_size;
	}

	/**
	 *	Sets the return address and fram pointer reservation size.
	 * @param ra_fp_size the ra_fp_size to set
	 */
	public void set_ra_fp_size(int ra_fp_size) {
		this.ra_fp_size = ra_fp_size;
	}

	/**
	 *	Accesses the total size of local variables.
	 *
	 * @return the localSize
	 */
	public int getLocalSize() {
		return localSize;
	}

	/**
	 *	Sets the total size of local variables.
	 * @param localSize the localSize to set
	 */
	public void setLocalSize(int localSize) {
		this.localSize = localSize;
	}
	
	/**
	 * Aligns the local area.
	 * 
	 */
	public void alignLocal(){
		this.localSize = alignAllocate(this.localSize, 0);
	}

	/**
	 *	Accesses the total size of temporaries.
	 *
	 * @return the tempSize
	 */
	@Deprecated
	public int getTempSize() {
		return tempSize;
	}

	/**
	 *	Sets the total size of temporaries.
	 * @param tempSize the tempSize to set
	 */
	@Deprecated
	public void setTempSize(int tempSize) {
		this.tempSize = tempSize;
	}
	
	/**
	 *	Accesses the total size of temporary space for struct/union.
	 *
	 * @return the tempSpaceSize
	 */
	public int getTempSpaceSize() {
		return tempSpaceSize;
	}

	/**
	 *	Sets the total size of temporary space for struct/union.
	 * @param tempSpaceSize the tempSpaceSize to set
	 */
	public void setTempSpaceSize(int tempSpaceSize) {
		this.tempSpaceSize = tempSpaceSize;
	}
	
	/**
	 *	Accesses the argument table.
	 *
	 * @return the argumentTable
	 */
	public ArrayList<Argument> getArgumentTable() {
		return argumentTable;
	}
	
	/**
	 * Returns the stack frame size of the function.
	 * 
	 * @return
	 */
	public int getFrameSize(){
		int fp_based_offset = this.ra_fp_size + this.localSize + this.tempSpaceSize;
		fp_based_offset = alignAllocate(fp_based_offset, 0);
		
		int sp_based_offset = this.callerArgSize + this.savedRegSize;
		sp_based_offset = alignAllocate(sp_based_offset, 0);
		
		return fp_based_offset + sp_based_offset;
	}
	
	/**
	 * Align the offset at the 4-byte boundary.
	 * 
	 * @param offset
	 * @param size
	 * @return
	 */
	private static int alignAllocate(int offset, int size){
		offset += size;
		if (size != 1){
			offset += 3;
			offset >>= 2;
			offset <<= 2;
		}
		
		return offset;
	}
	
	/**
	 *	Accesses the intermediate representation the function entry belongs to.
	 *
	 * @return the ir
	 */
	public IntermediateRepresentation getIr() {
		return ir;
	}
	
	/**
	 * Accesses the control flow graph.
	 * 
	 * @return
	 */
	public Graph<Block> getControlFlowGraph(){
		return controlFlowGraph;
	}
	
	/**
	 * Returns the entry block.
	 * 
	 * @return
	 */
	public Block getEntryBlock(){
		return entryBlock;
	}
	
	/**
	 * Returns the exit block.
	 * 
	 * @return
	 */
	public Block getExitBlock(){
		return exitBlock;
	}
	
	/**
	 *	Accesses the called function set.
	 *
	 * @return the calledSet
	 */
	public HashSet<Label> getCalledSet() {
		return calledSet;
	}

	/**
	 * Adds the label to the called function set.
	 * 
	 * @param label
	 */
	public void addToCalledSet(Label label){
		if (calledSet != null)	
			calledSet.add(label);
	}
	
	/**
	 * Disables the called set. (call to unknown function pointer encountered)
	 * 
	 */
	public void disableCalledSet(){
		calledSet = null;
	}
	
	/**
	 *	Accesses the used register set.
	 *
	 * @return the usedRegisterSet
	 */
	public BitSet getUsedRegisterSet() {
		return usedRegisterSet;
	}

	/**
	 *	Sets the used register set.
	 * @param usedRegisterSet the usedRegisterSet to set
	 */
	public void setUsedRegisterSet(BitSet usedRegisterSet) {
		this.usedRegisterSet = usedRegisterSet;
	}
	
	/**
	 * Adds the register to the used register set.
	 * 
	 * @param reg
	 */
	public void addToUsedRegisterSet(int reg){
		this.usedRegisterSet.set(reg);
	}

	/* variable members */

	/** the control flow graph */
	private Graph<Block> controlFlowGraph;
	
	/** control flow graph builder */
	private ControlFlowGraphBuilder controlFlowGraphBuilder;
	
	/** the entry block of the function */
	private Block entryBlock;
	
	/** the exit block of the function */
	private Block exitBlock;
	
	/** label numbering variable */
	private int labelCount;

	/** argument count */
	private int argCount;
	
	/** name of the function */
	private String name;

	/** the intermediate representation that the entry is registered to */
	private IntermediateRepresentation ir;

	/* runtime information */

	/** return value address for struct/union */
	private Argument returnValueAddress;
	
	/** MemLoc objects of arguments. */
	private ArrayList<Argument> argumentTable;
	
	/** wrapped return value address for struct/union */
	private VarWrapper wrappedReturnValueAddress;

	/** wrapped MemLoc objects of arguments */
	private ArrayList<VarWrapper> wrappedArgumentTable;
	
	/** the maximum size of arguments to be passed to invoked functions */
	private int callerArgSize;
	
	/** size of space for reservation of return address and frame pointer */
	private int ra_fp_size;
	
	/** size of local variables */
	private int localSize;
	
	/** the total size of saved registers */
	private int savedRegSize;
	
	/** size of temporaries */
	@Deprecated
	private int tempSize;
	
	/** size of temporary space size for struct/union */
	private int tempSpaceSize;

	/** only used temporarily, will be deprecated in the future */
	@Deprecated
	private int offset;
	
	/** the called function list */
	private HashSet<Label> calledSet;
	
	/** registers used by the function */
	private BitSet usedRegisterSet;
	
	/* implementation of graph.Indexed */
	
	/** the index */
	private int index;
	
	@Override
	public int getIndex() {
		
		return index;
	}

	@Override
	public void setIndex(int index) {
		this.index = index;
	}
}
