package irpack;

import java.util.BitSet;

/**
 * The Call class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 15, 2014
 */
public class Call extends Quadruple{

	/**
	 * Constructs a call quadruple which is substituted for the previous Jump(CALL) in order to represent a definition of v0.
	 * 
	 * @param dest
	 * @param target
	 */
	public Call(VarWrapper dest, Operand target) {
		super(CALLDEF);
		
		this.setDest(dest);
		this.setTarget(target);
	}
	

	/**
	 *	Accesses the destination.
	 *
	 * @return the dest
	 */
	public VarWrapper getDest() {
		return dest;
	}

	/**
	 *	Sets the destination.
	 * @param dest the dest to set
	 */
	private void setDest(VarWrapper dest) {
		this.dest = dest;
	}

	/**
	 *	Accesses the target.
	 *
	 * @return the target
	 */
	public Operand getTarget() {
		return target;
	}

	/**
	 *	Sets the target.
	 * @param target the target to set
	 */
	private void setTarget(Operand target) {
		this.target = target;
	}


	/**
	 *	Accesses the active register set.
	 *
	 * @return the busy
	 */
	public BitSet getBusy() {
		return busy;
	}


	/**
	 *	Sets the active register set.
	 * @param busy the busy to set
	 */
	public void setBusy(BitSet busy) {
		this.busy = busy;
	}


	/** the destination representing v0 */
	private VarWrapper dest;

	/** the call target */
	private Operand target;

	/** the active register set */
	private BitSet busy;
	
	@Override
	public Addressable[] getUse() {
		
		if (target instanceof Addressable){
			Addressable tmp = ((Addressable) target).used();
			if (tmp != null) return new Addressable[]{tmp};
		}
		
		return zero_length_list;
	}


	@Override
	public Addressable[] getDefine() {
		return zero_length_list;
		//return new Addressable[]{dest};
	}


	@Override
	public boolean replaceUse(Operand use, Operand toSubstitute) {
		
		if (target instanceof Indirection){
			if (((Indirection) target).getSource() == use){
				target = ((Indirection) target).newIndirection(toSubstitute, 0);
				return true;
			}
		}
		else if (target == use){
			target = toSubstitute;
			return true;
		}
		
		return false;
	}


	@Override
	public void replaceDefine(VarWrapper toSubstitute) {
		
		//this.dest = toSubstitute;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + opcode;
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		result = prime * result + ((dest == null) ? 0 : dest.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Call other = (Call) obj;
		if (getOpcode() != other.getOpcode()) return false;
		if (!target.equals(other.target)) return false;
		if (!dest.equals(dest)) return false;
		
		return true;
	}


	@Override
	public Operand[] getOperands() {
		return new Operand[]{target};
		//return new Operand[]{dest, target};
	}

	@Override
	public String toString(){
		
		return "call " + target.toString();
	}
}
