package irpack;

/**
 * The LocalMemLoc class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 28, 2014
 */
public class Local extends MemLoc {

	/**
	 * Constructs a LocalMemLoc object.
	 * 
	 * @param size
	 */
	public Local(int size) {
		super(LOCAL, size, localMemLocCount++);

	}
	
	static private int localMemLocCount = 0;
	
	@Override
	public String toString() {
		return new StringBuilder()
			.append("l_")
			.append(getIndex())
			.toString();
	}

	@Override
	public Addressable used() {
		
		return isTakenAddress() ? null : this;
	}

	@Override
	public boolean defined() {
		
		return !isTakenAddress();
	}
}
