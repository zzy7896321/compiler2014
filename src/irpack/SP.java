package irpack;

/**
 * The SP class is a singleton class that denotes the stack pointer $sp.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 14, 2014
 */
public class SP implements Operand {

	private SP() {
	}

	
	@Override
	public int hashCode(){
		return 0;
	}
	
	@Override
	public boolean equals(Object obj){
		return obj == this;
	}
	
	/**
	 * Returns the single instance.
	 * 
	 * @return
	 */
	public static SP getInstance(){
		return instance;
	}
	
	private static final SP instance = new SP();
}
