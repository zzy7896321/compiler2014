package irpack;

import graph.Block;

/**
 * The Quadruple class is base class of all intermediate representation quadruples 
 * with a bidirectional linked list implemented internally.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 12, 2014
 */
public class Quadruple implements graph.Indexed {

	/**
	 * Constructs a single quadruple linked to itself.
	 */
	public Quadruple(int opcode){
		
		this.opcode = opcode;
		this.associatedBlock = null;
		
	}

	/**
	 *	Accesses the opcode.
	 *
	 * @return the opcode
	 */
	public int getOpcode() {
		return opcode;
	}


	/**
	 *	Sets the opcode.
	 * @param opcode the opcode to set
	 */
	public void setOpcode(int opcode) {
		this.opcode = opcode;
	}

	/**
	 *	Accesses the associated block.
	 *
	 * @return the associatedBlock
	 */
	public Block getAssociatedBlock() {
		return associatedBlock;
	}

	/**
	 *	Sets the associated block.
	 * @param associatedBlock the associatedBlock to set
	 */
	public void setAssociatedBlock(Block associatedBlock) {
		this.associatedBlock = associatedBlock;
	}

	/**
	 * Accesses the use list of the quadruple.
	 * 
	 * @return		the use list of the quadruple
	 */
	public Addressable[] getUse(){
		return zero_length_list;
	}
	
	/**
	 * Accesses the definition of the quadruple.
	 * 
	 * @return		the definition of the quadruple
	 */
	public Addressable[] getDefine(){
		return zero_length_list;
	}
	
	/**
	 * Substitute the latter Operand for the former one.
	 * 
	 * @param use				the used one
	 * @param toSubstitute		the one to substitute
	 * @returns 				whether there is some operand being replaced
	 */
	public boolean replaceUse(Operand use, Operand toSubstitute){
		return false;
	}
	
	/**
	 * Accesses all the operands of the quadruple.
	 * 
	 * @return
	 */
	public Operand[] getOperands(){
		
		return zero_length_list;
	}
	
	/**
	 * Substitute the VarWrapper Object for the defined operand.
	 * 
	 * @param toSubstitute
	 */
	public void replaceDefine(VarWrapper toSubstitute){
		
	}
	
	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public void setIndex(int index) {
		this.index = index;
	}

	/** the operation code */
	protected int opcode;
	
	/** the block that the quadruple is in */
	private Block associatedBlock;
	
	/** the index of the quadruple */
	private int index;
	
	
	/* label */
	final public static int LABEL = 0;
	
	/* load/store */
	final public static int load_store_start = 1;
	
	final public static int LA = 1;		//pseudo
	@Deprecated
	final public static int LOAD = 2;
	@Deprecated
	final public static int STORE = 3;
	final public static int MOV = 4;		//pseudo
	
	/* ALU */
	final public static int ALU_start = 10;
	
	final public static int ADD = 10;
	final public static int SUB = 11;
	final public static int MUL = 12;
	final public static int DIV = 13;
	final public static int MOD = 14;
	
	final public static int SLL = 15;
	final public static int SRA = 16;
	final public static int SRL = 17;
	final public static int AND = 18;
	final public static int OR = 19;
	final public static int XOR = 20;
	final public static int NOR = 21;
	
	final public static int SLT = 22;
	final public static int SLE = 23;
	final public static int SGT = 24;
	final public static int SGE = 25;
	final public static int SEQ = 26;
	final public static int SNE = 27;
	final public static int SLTU = 28;
	
	/* branch */
	final public static int branch_start = 50;
	
	final public static int BEQ = 50;
	final public static int BNE = 51;
	
	/* jump */
	final public static int jump_start = 70;
	
	final public static int J = 70;
	
	@Deprecated
	final public static int JAL = 71;
	final public static int CALL = 72;	// pseudo
	
	/* Miscellaneous */
	final public static int miscellaneuous_start = 80;
	
	final public static int PUSH = 80;
	
	@Deprecated
	final public static int MEMCPY = 81;
	
	final public static int MOVRV = 82;
	final public static int STORERV = 83;
	
	
	/* FunctionInfo Miscellaneous */
	final public static int function_info_miscellaneous_start = 120;
	
	final public static int FUNCTIONENTRYBLOCK = 120;
	final public static int FUNCTIONEXITBLOCK = 121;
	
	/* SSA */
	final public static int PHI = 140;
	
	/* call with definition */
	final public static int CALLDEF = 150;
	
	/* base class Miscellaneous */
	final public static int base_class_miscellaneous_start = 160;
	
	final public static int CALLSTART = 160;
	final public static int CALLEND = 161;
	
	/** an zero length list */
	protected Addressable[] zero_length_list = new Addressable[0];
	
	@Override
	public String toString(){
		if (opcode == CALLEND) return "CALLEND";
		else return "CALLSTART";
	}
	
}
