package irpack;

/**
 * The ConstantOperand class is required to be implemented by all constant operand classes.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 12, 2014
 */
public interface ConstantOperand extends Operand {

	/**
	 * Perform constant addition operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand add(ConstantOperand rhs);
	
	/**
	 * Perform constant subtraction operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand sub(ConstantOperand rhs);
	
	/**
	 * Perform constant multiplication operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand mul(ConstantOperand rhs);

	/**
	 * Perform constant division operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand div(ConstantOperand rhs);
	
	/**
	 * Perform constant modulo operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand mod(ConstantOperand rhs);

	/**
	 * Perform constant logical left shifting operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand sll(ConstantOperand rhs);
	
	/**
	 * Perform constant arithmetic right shifting operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand sra(ConstantOperand rhs);
	
	/**
	 * Perform constant logical right shifting operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand srl(ConstantOperand rhs);
	
	/**
	 * Perform constant bitwise and operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand and(ConstantOperand rhs);
	
	/**
	 * Perform constant bitwise or operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand or(ConstantOperand rhs);

	/**
	 * Perform constant bitwise xor operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand xor(ConstantOperand rhs);
	
	/**
	 * Perform constant bitwise nor operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand nor(ConstantOperand rhs);
	
	/**
	 * Perform constant set-on-less-than operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand slt(ConstantOperand rhs);
	
	/**
	 * Perform constant set-on-less-than-or-equal-to operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand sle(ConstantOperand rhs);
	
	/**
	 * Perform constant set-on-greater-than operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand sgt(ConstantOperand rhs);
	
	/**
	 * Perform constant set-on-greater-than-or-equal-to operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand sge(ConstantOperand rhs);
	
	/**
	 * Perform constant set-on-equal-to operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand seq(ConstantOperand rhs);
	
	/**
	 * Perform constant set-on-not-equal-to operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand sne(ConstantOperand rhs);
	
	/**
	 * Perform constant unsigned set-on-less-than operation, returns null if the result is not compile time constant.
	 * 
	 * @param rhs
	 * @return
	 */
	public ConstantOperand sltu(ConstantOperand rhs);
	
	/**
	 * Returns whether rhs has the same value with this.
	 * 
	 * @param rhs
	 * @return
	 */
	public boolean theSameValueWith(ConstantOperand rhs);
}
