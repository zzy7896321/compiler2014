package irpack;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import translate.RawIRTranslate;
import translate.Translate;

/**
 * The IntermediateRepresentation class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class IntermediateRepresentation {

	/**
	 * Constructs an empty intermediate representation output.
	 */
	public IntermediateRepresentation(){
		_static_variable_table = new ArrayList<StaticVarEntry>();
		_function_entry_table = new LinkedList<FunctionEntry>();
		_label_table = new HashMap<String, Label>();
	}
	
	/**
	 * Add a function entry.
	 * 
	 * @param entry
	 */
	public void addFunctionEntry(FunctionEntry entry){
		_function_entry_table.add(entry);
	}
	
	/**
	 * Returns the function entry table.
	 * 
	 * @return		the function entry table
	 */
	public List<FunctionEntry> getFunctionEntryTable(){
		return _function_entry_table;
	}
	
	/**
	 * Add a static variable entry.
	 * 
	 * @param entry
	 */
	public void addStaticVariableEntry(StaticVarEntry entry){
		_static_variable_table.add(entry);
	}
	
	/**
	 * Returns the static variable entry table
	 * 
	 * @return		the static variable entry table
	 */
	public ArrayList<StaticVarEntry> getStaticVariableTable(){
		return _static_variable_table;
	}
	
	/**
	 * Returns the unique label that has the name.
	 * 
	 * @param name
	 * @return		the unique label that has the name
	 */
	public Label getLabel(String name){
		name = name.replace('$', '.');
		
		Label ret = _label_table.get(name);
		
		if (ret == null){
			ret = op.createLabel(name);
			_label_table.put(name, ret);			
		}
		
		return ret;
	}
	
	/**
	 * Returns the unique label that has the name appended to function label header.
	 * 
	 * @param name
	 * @return		the unique label that has the name appended to function label haeder
	 */
	public Label getFunctionLabel(String name){
		return getLabel(Label.function_head + name);
	}
	
	/**
	 * Returns the unique function beginning label.
	 * 
	 * @param name
	 * @return
	 */
	public Label getFunctionBeginLabel(String name){
		return getLabel(Label.function_begin_head + name);
	}
	
	/**
	 * Returns the unique function end label.
	 * 
	 * @param name
	 * @return
	 */
	public Label getFunctionEndLabel(String name){
		return getLabel(Label.function_end_head + name);
	}
	
	/**
	 * Translates the unoptimized intermediate representation code into MIPS32 assembly code.
	 * 
	 * @param writer
	 * @return
	 * @throws IOException
	 */
	public boolean rawTranslate(Writer writer){
		
		try {
			RawIRTranslate.translateDataSegment(writer, _static_variable_table);
			
			RawIRTranslate.translateTextSegment(writer, _function_entry_table);
		}
		catch (IOException e){
			
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/**
	 * Translates the optimized intermediate representation code into MIPS32 assembly code.
	 * 
	 * @param writer
	 * @return
	 */
	public boolean translate(Writer writer){
		
		try {
			Translate.translateDataSegment(writer, _static_variable_table);
			
			Translate.translateTextSegment(writer, _function_entry_table);
			
		} catch (IOException e) {
			
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/** the static variable entry table */
	private ArrayList<StaticVarEntry> _static_variable_table;
	
	/** the function entry table */
	private LinkedList<FunctionEntry> _function_entry_table;
	
	/** the label table */
	private HashMap<String, Label> _label_table;
	
}
