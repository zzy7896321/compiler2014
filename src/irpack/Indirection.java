package irpack;

/**
 * The Indirection class denotes an operand that needs one indirection.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 11, 2014
 */
public class Indirection implements Addressable {

	/**
	 * Constructs an indirection with source of indirection and offset to be added to the content of source.
	 * 
	 * @param source
	 * @param offset
	 */
	public Indirection(Operand source, int offset, int size){
		this.source = source;
		this.offset = offset;
		this.size = size;
	}
			
	/**
	 *	Accesses the source of indirection.
	 *
	 * @return the source
	 */
	public Operand getSource() {
		return source;
	}

	/**
	 *	Accesses the offset to be added to the content of indirection.
	 *
	 * @return the offset
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 * Accesses the size of the indirected object.
	 * 
	 * @return
	 */
	public int getSize(){
		return this.size;
	}

	/** the source of indirection */
	private Operand source;
	
	/** offset to be added the content of source */
	private int offset;
	
	/** the size of indirected object */
	private int size;


	/**
	 * Returns a new indirection with the same source and additional offset added to the original one.
	 * 
	 * @param offset
	 * @return
	 */
	public Indirection newIndirection(int offset){
		return new Indirection(this.source, this.offset + offset, size);
	}
	
	/**
	 * Returns a new indirection with the replaced source and additional offset added to the original one.
	 * 
	 * @param offset
	 * @return
	 */
	public Indirection newIndirection(Operand source, int offset){
		return new Indirection(source, this.offset + offset, size);
	}

	@Override
	public void setTakenAddress(boolean takenAddress) {
		if (this.source instanceof MemLoc){
			((MemLoc) this.source).setTakenAddress(true);
		}
	}

	@Override
	public boolean isTakenAddress() {
		if (this.source instanceof MemLoc){
			return ((MemLoc) this.source).isTakenAddress();
		}
		
		return false;
	}
	
	@Override
	public Addressable used() {
		if (source instanceof Addressable && !((Addressable) source).isTakenAddress()){
			return (Addressable) source;
		}
		
		return null;
	}

	@Override
	public boolean defined() {
		
		return false;
	}
	
	@Override
	public String toString(){
		
		return new StringBuilder()
			.append(Integer.toString(getOffset()))
			.append("(")
			.append(getSource().toString())
			.append(")")
			.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + offset;
		result = prime * result + size;
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Indirection other = (Indirection) obj;
		if (offset != other.offset)
			return false;
		if (size != other.size)
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		return true;
	}

	@Override
	public int getObjectSize() {
		
		return this.size;
	}

	
}
