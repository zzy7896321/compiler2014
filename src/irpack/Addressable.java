package irpack;

/**
 * The Addressable class is required to be implemented by all addressable object classes.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 11, 2014
 */
public interface Addressable extends Operand {

	
	/**
	 * Sets whether the operand has been takenAddress.
	 * 
	 * @param takenAddress
	 */
	public void setTakenAddress(boolean takenAddress);
	
	/**
	 *	If the MemLoc has been taken address.
	 *
	 * @return the takenAddress
	 */
	public boolean isTakenAddress();
	
	
	/**
	 * Returns the actually used object.
	 * 
	 * @return
	 */
	public Addressable used();
	
	/**
	 * Returns whether the object is actually defined.
	 * 
	 * @return
	 */
	public boolean defined();
	
	/**
	 * Returns the size of the object.
	 * 
	 * @return
	 */
	public int getObjectSize();
}
