package irpack;

/**
 * The Branch class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 15, 2014
 */
public class Branch extends Quadruple {

	/**
	 * Constructs a branch object.
	 * 
	 * @param opcode
	 * @param target
	 * @param rs
	 * @param rt
	 */
	public Branch(int opcode, Label target, Operand rs, Operand rt) {
		super(opcode);
		
		this.rs = rs;
		this.rt = rt;
		this.target =target;
	}

	
	/**
	 *	Accesses the Target.
	 *
	 * @return the target
	 */
	public Label getTarget() {
		return this.target;
	}
	
	/**
	 *	Sets the Target.
	 * @param target the target to set
	 */
	private void setTarget(Label target) {
		this.target = target;
	}


	/**
	 *	Accesses source 1 .
	 *
	 * @return the rs
	 */
	public Operand getRs() {
		return this.rs;
	}


	/**
	 *	Sets source 1.
	 * @param rs the rs to set
	 */
	private void setRs(Operand rs) {
		this.rs = rs;
	}


	/**
	 *	Accesses source 2.
	 *
	 * @return the rt
	 */
	public Operand getRt() {
		return this.rt;
	}


	/**
	 *	Sets source 2.
	 * @param rt the rt to set
	 */
	private void setRt(Operand rt) {
		this.rt = rt;
	}

	
	/** source 1 */
	private Operand rs;
	
	/** source 2 */
	private Operand rt;
	
	/** the target */
	private Label target;
	
	private static final String[] Opname= new String[]
	{
		"BEQ",
		"BNE",
	};
	
	@Override
	public String toString(){
		return new StringBuilder()
			.append(Opname[opcode - branch_start])
			.append("\t")
			.append(getRs().toString())
			.append(",\t")
			.append(getRt().toString())
			.append(",\t")
			.append(getTarget().toString())
			.toString(); 
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + opcode;
		result = prime * result + ((rs == null) ? 0 : rs.hashCode());
		result = prime * result + ((rt == null) ? 0 : rt.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Branch other = (Branch) obj;
		if (opcode != other.opcode) return false;
		if (rs == null) {
			if (other.rs != null)
				return false;
		} else if (!rs.equals(other.rs))
			return false;
		if (rt == null) {
			if (other.rt != null)
				return false;
		} else if (!rt.equals(other.rt))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}


	@Override
	public Addressable[] getUse() {
		Addressable[] ret = new Addressable[2];
		int now = 0;
		
		if (rs instanceof Addressable){
			Addressable tmp = ((Addressable) rs).used();
			if (tmp != null) ret[now++] = tmp;
		}
		
		if (rt instanceof Addressable){
			Addressable tmp = ((Addressable) rt).used();
			if (tmp != null) ret[now++] = tmp;
		}
		
		return ret;
	}


	@Override
	public Addressable[] getDefine() {
		
		return zero_length_list;
	}


	@Override
	public boolean replaceUse(Operand use, Operand toSubstitute) {
		boolean hasReplacement = false;
		
		if (rs instanceof Indirection){
			if (((Indirection) rs).getSource() == use){
				hasReplacement = true;
				rs = ((Indirection) rs).newIndirection(toSubstitute, 0);
			}
		}
		else if (rs == use){
			hasReplacement = true;
			rs = toSubstitute;
		}
		
		if (rt instanceof Indirection){
			if (((Indirection) rt).getSource() == use){
				hasReplacement = true;
				rt = ((Indirection) rt).newIndirection(toSubstitute, 0);
			}
		}
		else if (rt == use){
			hasReplacement = true;
			rt = toSubstitute;
		}
		
		return hasReplacement;
	}


	@Override
	public void replaceDefine(VarWrapper toSubstitute) {
	}


	@Override
	public Operand[] getOperands() {
		
		return new Operand[]{rs, rt, target};
	}
	
	public void replaceTarget(Label operand){
		target = operand;
	}
	
}
