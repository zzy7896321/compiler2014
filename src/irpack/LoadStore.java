package irpack;

/**
 * The LoadStore class is the class of load/store operations.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 15, 2014
 */
public class LoadStore extends Quadruple {

	/**
	 * Constructs a load/store operation with destination and source linked to itself.
	 * 
	 * @param opcode	the opcode
	 * @param rd		the destination
	 * @param rs		source 1
	 */
	public LoadStore(int opcode, Operand rd, Operand rs) {
		super(opcode);
		
		this.rd = rd;
		this.rs = rs;
	}
	
	/**
	 *	Accesses the destination.
	 *
	 * @return the rd
	 */
	public Operand getRd() {
		return this.rd;
	}

	/**
	 *	Sets the destination.
	 * @param rd the rd to set
	 */
	private void setRd(Operand rd) {
		this.rd = rd;
	}

	/**
	 *	Accesses source 1.
	 *
	 * @return the rs
	 */
	public Operand getRs() {
		return this.rs;
	}
	
	/**
	 *	Sets source 1.
	 * @param rs the rs to set
	 */
	private void setRs(Operand rs) {
		this.rs = rs;
	}
	
	/** the destination */
	private Operand rd;
	
	/** the source */
	private Operand rs;
	
	
	private static final String[] Opname= new String[]
	{
		"LA",
		"LOAD",
		"STORE",
		"MOV"
	};
	
	@Override
	public String toString(){
		return new StringBuilder()
			.append(Opname[opcode - load_store_start])
			.append("\t")
			.append(rd.toString())
			.append(",\t")
			.append(rs.toString())
			.toString(); 
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + opcode;
		result = prime * result + ((rd == null) ? 0 : rd.hashCode());
		result = prime * result + ((rs == null) ? 0 : rs.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoadStore other = (LoadStore) obj;
		if (opcode != other.opcode) return false;
		if (rd == null) {
			if (other.rd != null)
				return false;
		} else if (!rd.equals(other.rd))
			return false;
		if (rs == null) {
			if (other.rs != null)
				return false;
		} else if (!rs.equals(other.rs))
			return false;
		return true;
	}

	@Override
	public Addressable[] getUse() {
		Addressable[] ret = new Addressable[2];
		int now = 0;
		
		if (rd instanceof Indirection){
			Addressable tmp = ((Addressable) rd).used();
			if (tmp != null) ret[now++] = tmp;
		}
		
		if (rs instanceof Addressable){
			Addressable tmp = ((Addressable) rs).used();
			if (tmp != null) ret[now++] = tmp;
		}
		
		return ret;
	}

	@Override
	public Addressable[] getDefine() {
		if (rd instanceof Addressable){
			if (((Addressable) rd).defined()) return new Addressable[]{(Addressable) rd};
		}
		
		return zero_length_list;
	}

	@Override
	public boolean replaceUse(Operand use, Operand toSubstitute) {
		boolean hasReplacement = false;
		
		if (rd instanceof Indirection){
			if (((Indirection) rd).getSource() == use){
				hasReplacement = true;
				rd = ((Indirection) rd).newIndirection(toSubstitute, 0);
			}
		}
		
		if (rs instanceof Indirection){
			if (((Indirection) rs).getSource() == use){
				hasReplacement = true;
				rs = ((Indirection) rs).newIndirection(toSubstitute, 0);
			}
		}
		else if (rs == use){
			hasReplacement = true;
			rs = toSubstitute;
		}
		
		return hasReplacement;
	}

	@Override
	public void replaceDefine(VarWrapper toSubstitute) {
		setRd(toSubstitute);
	}

	@Override
	public Operand[] getOperands() {
		
		return new Operand[]{rd, rs};
	}
	
	
}
