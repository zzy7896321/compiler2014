package irpack;

/**
 * The Operand interface is to be implemented by each quadruple operand class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 9, 2014
 */
public interface Operand {
	
	
	@Override
	public String toString();
	
	@Override
	public boolean equals(Object obj);

	@Override
	public int hashCode();
}
