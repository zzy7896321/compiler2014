package irpack;

/**
 * The ConstantOperand class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 12, 2014
 */
public class IntConstant implements ConstantOperand {

	public IntConstant(int value){
		this.value = value;
	}
	
	/**
	 *	Accesses the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	private int value;

	@Override
	public String toString(){
		
		return Integer.toString(value);
	}

	@Override
	public ConstantOperand add(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			return new IntConstant(value + ((IntConstant) rhs).getValue());
		}
		
		else if (rhs instanceof Address){
			return ((Address) rhs).newAddress(value);
		}
		
		else if (rhs instanceof Label){
			return new Address(rhs, value);
		}
		
		return null;
	}

	@Override
	public ConstantOperand sub(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			return new IntConstant(value - ((IntConstant) rhs).getValue());
		}
		
		return null;
	}

	@Override
	public ConstantOperand mul(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			return new IntConstant(value * ((IntConstant) rhs).getValue());
		}
		
		else if (rhs instanceof Address){
			if (value == 0) return new IntConstant(0);
			if (value == 1) return ((Address) rhs).newAddress(0);
		}
		
		else if (rhs instanceof Label){
			if (value == 0) return new IntConstant(0);
			if (value == 1) return rhs;
		}
		
		return null;
	}

	@Override
	public ConstantOperand div(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			if (((IntConstant) rhs).getValue() == 0) return null;
			
			return new IntConstant( value / ((IntConstant) rhs).getValue());
		}
		
		else if (rhs instanceof Address){
			if (value == 0) return new IntConstant(0);
		}
		
		else if (rhs instanceof Label){
			if (value == 0) return new IntConstant(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand mod(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			if (((IntConstant) rhs).getValue() == 0) return null;
			
			return new IntConstant( value % ((IntConstant) rhs).getValue());
		}
		
		else if (rhs instanceof Address){
			if (value == 0) return new IntConstant(0);
		}
		
		else if (rhs instanceof Label){
			if (value == 0) return new IntConstant(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand sll(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){			
			return new IntConstant( value << ((IntConstant) rhs).getValue());
		}
		
		return null;
	}

	@Override
	public ConstantOperand sra(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){			
			return new IntConstant( value >> ((IntConstant) rhs).getValue());
		}
		
		return null;
	}

	@Override
	public ConstantOperand srl(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){			
			return new IntConstant( value >>> ((IntConstant) rhs).getValue());
		}
		
		return null;
	}

	@Override
	public ConstantOperand and(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
	
			return new IntConstant( value & ((IntConstant) rhs).getValue());
		}
		
		else if (rhs instanceof Address){
			
			if (value == 0) return new IntConstant(0);
			if (value == 0XFFFFFFFF) return ((Address) rhs).newAddress(0);
		}
		
		else if (rhs instanceof Label){
			
			if (value == 0) return new IntConstant(0);
			if (value == 0XFFFFFFFF) return rhs;
		}
		
		return null;
	}

	@Override
	public ConstantOperand or(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			
			return new IntConstant( value | ((IntConstant) rhs).getValue());
		}
		
		else if (rhs instanceof Address){
			
			if (value == 0) return ((Address) rhs).newAddress(0);
			if (value == 0XFFFFFFFF) return new IntConstant(0XFFFFFFFF);
		}
		
		else if (rhs instanceof Label){
			
			if (value == 0) return rhs;
			if (value == 0XFFFFFFFF) return new IntConstant(0XFFFFFFFF);
		}
		
		return null;
	}

	@Override
	public ConstantOperand xor(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			
			return new IntConstant( value ^ ((IntConstant) rhs).getValue());
		}
		
		else if (rhs instanceof Address){
			
			if (value == 0) return ((Address) rhs).newAddress(0);
		}
		
		else if (rhs instanceof Label){
			
			if (value == 0) return rhs;
		}
		
		return null;
	}

	@Override
	public ConstantOperand nor(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			
			return new IntConstant( ~(value | ((IntConstant) rhs).getValue()));
		}
		
		else if (rhs instanceof Address){
			
			if (value == 0xFFFFFFFF) return new IntConstant(0);
		}
		
		else if (rhs instanceof Label){
			
			if (value == 0xFFFFFFFF) return new IntConstant(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand slt(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			
			return (value < ((IntConstant) rhs).getValue()) ? new IntConstant(1) : new IntConstant(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand sle(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			
			return (value <= ((IntConstant) rhs).getValue()) ? new IntConstant(1) : new IntConstant(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand sgt(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			
			return (value > ((IntConstant) rhs).getValue()) ? new IntConstant(1) : new IntConstant(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand sge(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			
			return (value >= ((IntConstant) rhs).getValue()) ? new IntConstant(1) : new IntConstant(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand seq(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			
			return (value == ((IntConstant) rhs).getValue()) ? new IntConstant(1) : new IntConstant(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand sne(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			
			return (value != ((IntConstant) rhs).getValue()) ? new IntConstant(1) : new IntConstant(0);
		}
		
		return null;
	}

	@Override
	public ConstantOperand sltu(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			
			return ((((long) value) & 0xFFFFFFFFL) < (((long) ((IntConstant) rhs).getValue()) & 0xFFFFFFFFL)) 
					? new IntConstant(1) : new IntConstant(0);
		}
	
		else if (rhs instanceof Address){
			if (value == 0) return new IntConstant(1);
		}
		
		else if (rhs instanceof Label){
			if (value == 0) return new IntConstant(1);
		}
		
		return null;
	}


	@Override
	public boolean theSameValueWith(ConstantOperand rhs) {
		
		if (rhs instanceof IntConstant){
			
			return value == ((IntConstant) rhs).getValue();
		}
		
		return false;
	}
}
