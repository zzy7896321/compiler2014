package irpack;

/**
 * The op class is the factory class of quadruples.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 14, 2014
 */
public class op {

	
	/* creates label */
	
	/**
	 * Creates a new label.
	 * 
	 * @param name
	 * @return
	 */
	public static Label createLabel(String name){
		return new Label(name);
	}
	
	/* creates constant*/
	public static IntConstant constant(int value){
		return new IntConstant(value);
	}
	
	/* creates MemLoc */
	
	/**
	 * Creates a new temporary space for struct/union
	 * 
	 * @param num
	 * @return
	 */
	public static TempSpace tempSpace(int size, FunctionEntry entry){
		TempSpace ret = new TempSpace(size);
		entry.registerTemporarySpace(ret);
		return ret;
	}
	
	/**
	 * Creates a new argument of callee.
	 *
	 * @param num
	 * @return
	 */
	public static Argument argCallee(int num, int size, boolean passByAddress){
		return new Argument(num, size, passByAddress);
	}
	
	/**
	 * Creates a new GlobalMemLoc object.
	 * 
	 * @param size
	 * @return
	 */
	public static Global global(int size){
		return new Global(size);
	}
	
	/**
	 * Creates a new LocalMemLoc object.
	 * 
	 * @param size
	 * @return
	 */
	public static Local local(int size){
		return new Local(size);
	}
	
	/**
	 * Creates a new Temp value.
	 * 
	 * @return
	 */
	public static Temp temp(FunctionEntry entry){
		Temp ret = new Temp();
		entry.registerTemp(ret);
		return ret;
	}
	
	/**
	 * Creates an indirection operand.
	 * 
	 * @param source
	 * @param offset
	 * @param size
	 * @param entry
	 * @return
	 */
	public static Indirection indirection(Operand source, int offset, int size, FunctionEntry entry){
		return new Indirection(source, offset, size);
	}
	
	/**
	 * Creates a bit-field indirection operand.
	 * 
	 * @param source
	 * @param offset
	 * @param entry
	 * @return
	 */
	public static BitField bitField(Operand source, int offset, int mask, FunctionEntry entry){
		return new BitField(source, offset, mask);
	}
	
	/**
	 * Creates an address constant operand.
	 * 
	 * @param base
	 * @param offset
	 * @param entry
	 * @return
	 */
	public static Address address(MemLoc base, int offset, FunctionEntry entry){
		return new Address(base, offset);
	}
	
	/* Load/Store */
	
//	/**
//	 * Creates a load operation.
//	 * 
//	 * @param dest
//	 * @param rs
//	 * @param entry
//	 * @return
//	 */
//	public static Operand load(Operand dest, Operand rs, FunctionEntry entry){
//		entry.appendCode(new LoadStore(Quadruple.LOAD, dest, rs)) ;
//		return dest;
//	}
//	
//	/**
//	 * Creates a save operation.
//	 * 
//	 * @param dest
//	 * @param rs
//	 * @param entry
//	 * @return
//	 */
//	public static Operand store(Operand dest, Operand rs, FunctionEntry entry){
//		entry.appendCode(new LoadStore(Quadruple.STORE, dest, rs));
//		return dest;
//	}

	/**
	 * Create an LA pseudo operation (Pseudo).
	 * 
	 * @param dest
	 * @param rs
	 * @return
	 */
	public static Operand la(Operand dest, Operand rs, FunctionEntry entry){
		if (rs instanceof Addressable){
			((Addressable)rs).setTakenAddress(true);	// collect information for future use
		}
		
		if (dest instanceof BitField){
			Operand temp = temp(entry);
			entry.appendCode(new LoadStore(Quadruple.LA, temp, rs));
			storeBitField(temp, (BitField) dest, entry);
		}
		
		else {
			entry.appendCode(new LoadStore(Quadruple.LA, dest, rs));
		}
		
		return dest;
	}
	
	/**
	 * Creates a move operation (Pseudo).
	 * 
	 * @param dest
	 * @param rs
	 * @return
	 */
	public static Operand mov(Operand dest, Operand rs, FunctionEntry entry){
		
		if (rs instanceof BitField){
			rs = loadBitField((BitField) rs, entry);
		}
		
		if (dest instanceof BitField){
			Operand temp = temp(entry);
			entry.appendCode(new LoadStore(Quadruple.MOV, temp, rs));
			storeBitField(temp, (BitField) dest, entry);
		}
		
		else {
			entry.appendCode(new LoadStore(Quadruple.MOV, dest, rs));
		}
		
		return dest;
	}
	
	/* ALU ops */
	
	/**
	 * Creates an addition operation. Load is performed when any one of the entry is TempAddress, BitField or more-than-16-bit-width constant.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand add(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		
		return ALU(Quadruple.ADD, dest, rs, rt, entry);
	}
	
	/**
	 * Creates a substraction operation. Load is performed when any one of the entry is TempAddress, BitField or more-than-16-bit-width constant.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand sub(Operand dest, Operand rs, Operand rt, FunctionEntry entry){

		return ALU(Quadruple.SUB, dest, rs, rt, entry);
	}
	
	/**
	 * Creates a multiplication operation. Load is performed when any one of the entry is TempAddress, BitField or more-than-16-bit-width constant.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand mul(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		
		return ALU(Quadruple.MUL, dest, rs, rt, entry);
	}
	
	/**
	 * Creates a division operation. Load is performed when any one of the entry is TempAddress, BitField or more-than-16-bit-width constant.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand div(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		
		return ALU(Quadruple.DIV, dest, rs, rt, entry);
	}
	
	/**
	 * Creates a modular operation. Load is performed when any one of the entry is TempAddress, BitField or more-than-16-bit-width constant.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand mod(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		
		return ALU(Quadruple.MOD, dest, rs, rt, entry);
	}
	
	/**
	 * Creates a left shift operation. Load is performed when any one of the entry is TempAddress, BitField or more-than-16-bit-width constant.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand sll(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		
		return ALU(Quadruple.SLL, dest, rs, rt, entry);
	}
	
	/**
	 * Creates an arithmetic right shift operation. Load is performed when any one of the entry is TempAddress, BitField or more-than-16-bit-width constant.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand sra(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		
		return ALU(Quadruple.SRA, dest, rs, rt, entry);
	}
	
	/**
	 * Creates an unsigned right shift operation. Load is performed when any one of the entry is TempAddress, BitField or more-than-16-bit-width constant.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand srl(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		
		return ALU(Quadruple.SRL, dest, rs, rt, entry);
	}
	
	/**
	 * Creates a negation operation. (Pseudo).
	 * 
	 * @param dest
	 * @param rs
	 * @param entry
	 * @return
	 */
	public static Operand neg(Operand dest, Operand rs, FunctionEntry entry){
		return ALU(Quadruple.SUB, dest, op.constant(0), rs, entry);
	}
	
	/**
	 * Creates a set-on-less-than operation.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand slt(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		return ALU(Quadruple.SLT, dest, rs, rt, entry);
	}
	
	/**
	 * Creates a set-on-less-than-or-equal-to operation.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand sle(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		return ALU(Quadruple.SLE, dest, rs, rt, entry);
	}
	
	/**
	 * Creates a set-on-greater-than operation.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand sgt(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		return ALU(Quadruple.SGT, dest, rs, rt, entry);
	}
	
	/**
	 * Creates a set-on-greater-than-or-equal-to operation.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand sge(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		return ALU(Quadruple.SGE, dest, rs, rt, entry);
	}
	
	/**
	 * Creates a set-on-equal-to operation.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand seq(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		return ALU(Quadruple.SEQ, dest, rs, rt, entry);
	}
	
	/**
	 * Creates a set-on-not-equal-to operation.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand sne(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		return ALU(Quadruple.SNE, dest, rs, rt, entry);
	}
	
	/**
	 * Creates an unsigned set-on-less-than operation.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand sltu(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		return ALU(Quadruple.SLTU, dest, rs, rt, entry);
	}
	
	/**
	 * Creates an and operation.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand and(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		return ALU(Quadruple.AND, dest, rs, rt, entry);
	}
	
	/**
	 * Creates an or operation.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand or(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		return ALU(Quadruple.OR, dest, rs, rt, entry);
	}
	
	
	/**
	 * Creates a nor operation.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand nor(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		return ALU(Quadruple.NOR, dest, rs, rt, entry);
	}
	
	/**
	 * Creates an xor operation.
	 * 
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	public static Operand xor(Operand dest, Operand rs, Operand rt, FunctionEntry entry){
		return ALU(Quadruple.XOR, dest, rs, rt, entry);
	}
	
	/**
	 * Creates a bitwise not operation. (Pseudo)
	 * 
	 * @param dest
	 * @param rs
	 * @param entry
	 * @return
	 */
	public static Operand bitwiseNot(Operand dest, Operand rs, FunctionEntry entry){
		return ALU(Quadruple.NOR, dest, rs, constant(0), entry);
	}
	
	/**
	 * Creates a logical not operation. (Pseudo)
	 * 
	 * @param dest
	 * @param rs
	 * @param entry
	 * @return
	 */
	public static Operand logicalNot(Operand dest, Operand rs, FunctionEntry entry){
		return sltu(dest, rs, constant(1), entry);
	}
		
	/**
	 * Creates an ALU operation. Load is performed when any one of the entry is TempAddress, BitField or more-than-16-bit-width constant.
	 * 
	 * @param op
	 * @param dest
	 * @param rs
	 * @param rt
	 * @param entry
	 * @return
	 */
	private static Operand ALU(int op, Operand dest, Operand rs, Operand rt, FunctionEntry entry){
	
		if (rs instanceof BitField){
			rs = loadBitField((BitField) rs, entry);
		}
		
		if (rt instanceof BitField){
			rt = loadBitField((BitField) rt, entry);
		}
		
		if (dest instanceof BitField){
			Operand temp = temp(entry);
			entry.appendCode(new ALUOp(op, temp, rs, rt));
			storeBitField(temp, (BitField) dest, entry);
		}
		
		else {
			entry.appendCode(new ALUOp(op, dest, rs, rt));
		}

		return dest;
	}
	
	/* branch */
	
	/**
	 * Creates a branch-on-equal operation.
	 * 
	 * @param rs
	 * @param rt
	 * @param target
	 * @param entry
	 */
	public static void beq(Operand rs, Operand rt, Operand target, FunctionEntry entry){
		
		Branch(Quadruple.BEQ, rs, rt, target, entry);
	}
	
	/**
	 * Creates a branch-on-not-equal operation.
	 * 
	 * @param rs
	 * @param rt
	 * @param target
	 * @param entry
	 */
	public static void bne(Operand rs, Operand rt, Operand target, FunctionEntry entry){
		
		Branch(Quadruple.BNE, rs, rt, target, entry);
	}
	
	/**
	 * Creates a branch operation. Note: the order of parameter is different from Branch class constructor.
	 * 
	 * @param op
	 * @param rs
	 * @param rt
	 * @param target
	 * @param entry
	 */
	private static void Branch(int op, Operand rs, Operand rt, Operand target, FunctionEntry entry){
		
		if (rs instanceof BitField){
			rs = loadBitField((BitField) rs, entry);
		}
		
		if (rt instanceof BitField){
			rt = loadBitField((BitField) rt, entry);
		}
		
		entry.appendCode(new Branch(op, (Label)target, rs, rt));	
	}
	
	/* jump */
	
	public static void j(Operand target, FunctionEntry entry){
		
		entry.appendCode(new Jump(Quadruple.J, target));
	}
	
//	public static void jal(Operand target, FunctionEntry entry){
//		
//		entry.appendCode(new Jump(Quadruple.JAL, target));
//	}
	
	/* Miscellaneous */
	
	/**
	 * Built-in function memcpy. (Only for used for struct/union assignment, argument passing, return value copy)
	 * 
	 * @param dest
	 * @param src
	 * @param entry
	 * @return
	 */
	public static Operand memcpy(Operand dest, Operand src, int size, FunctionEntry entry){
		
		op.push(0, 0, dest, entry);
		op.push(1, 4, src, entry);
		op.push(2, 8, op.constant(size), entry);
		op.call(entry.getIr().getFunctionLabel("memcpy"), entry);
		
		entry.updateCallerArgSize(12);
	
		return dest;
	}
	
	/**
	 * Creates a push parameter operation. -1 denotes return value address. -2 denotes variable length part.
	 * Bit-fields need to be loaded in advance.
	 * 
	 * @param num
	 * @param src
	 * @param entry
	 */
	public static void push(int num, int offset, Operand src, FunctionEntry entry){
		
		entry.appendCode(new Push(num, offset, src));
	}
	
	/**
	 * Creates a function call operation.
	 * 
	 * @param target
	 * @param entry
	 */
	public static void call(Operand target, FunctionEntry entry){
		entry.appendCode(new Jump(Quadruple.CALL, target));
	}
	
	/**
	 * Creates a move-return-value-to-destination operation.
	 * 
	 * @param dest
	 * @param entry
	 */
	public static Operand movReturnValue(Operand dest, FunctionEntry entry){
		
		if (dest instanceof BitField){
			Operand temp = temp(entry);
			entry.appendCode(new MoveRV(temp));
			storeBitField(temp, (BitField) dest, entry);
		}
		else {
			entry.appendCode(new MoveRV(dest));
		}
		
		return dest;
	}
	
	/**
	 * Creates a store-return-value-to-v0 operation.
	 * 
	 * @param src
	 * @param entry
	 */
	public static void storeReturnValue(Operand src, FunctionEntry entry){
		
		if (src instanceof BitField){
			src = loadBitField((BitField) src, entry);
		}
		
		entry.appendCode(new StoreRV(src));
	}
	
	
	/**
	 * Creates a function entry point quadruple.
	 * 
	 * @param entry
	 * @return
	 */
	public static Quadruple functionEntryPoint(FunctionEntry entry){
		return new FunctionInfo(Quadruple.FUNCTIONENTRYBLOCK, entry);
	}
	
	/**
	 * Creates a function exit point quadruple.
	 * 
	 * @param entry
	 * @return
	 */
	public static Quadruple functionEndPoint(FunctionEntry entry){
		return new FunctionInfo(Quadruple.FUNCTIONEXITBLOCK, entry);
	}
	
	/**
	 * Stores the bit-field from an temporary register.
	 * 
	 * @param from
	 * @param bitField
	 * @param entry
	 */
	public static void storeBitField(Operand from, BitField bitField, FunctionEntry entry){
		int mask = bitField.getMask();
		int rshift_bits = Integer.numberOfLeadingZeros(mask);
		int lshift_bits = Integer.numberOfTrailingZeros(mask) + rshift_bits;
		
		Operand temp = temp(entry);
		entry.appendCode(new ALUOp(Quadruple.SLL, temp, from, constant(lshift_bits)));
		from = temp; temp = temp(entry);
		entry.appendCode(new ALUOp(Quadruple.SRL, temp, from, constant(rshift_bits)));
		from = temp; Operand temp2 = temp(entry);
		entry.appendCode(new LoadStore(Quadruple.MOV, temp2, bitField));
		temp = temp(entry);
		entry.appendCode(new ALUOp(Quadruple.AND, temp, temp2, constant(~mask)));
		temp2 = temp; temp = temp(entry);
		entry.appendCode(new ALUOp(Quadruple.OR, temp, temp2, from));
		entry.appendCode(new LoadStore(Quadruple.MOV, bitField, temp));
	}
	
	/**
	 * Loads the bit-field to an temporary register.
	 * 
	 * @param bitField
	 * @param entry
	 * @return
	 */
	public static Operand loadBitField(BitField bitField, FunctionEntry entry){
		int mask = bitField.getMask();
		int lshift_bits = Integer.numberOfLeadingZeros(mask);
		int rshift_bits = Integer.numberOfTrailingZeros(mask) + lshift_bits;
		
		Operand ret = temp(entry);
		entry.appendCode(new LoadStore(Quadruple.MOV, ret, bitField));
		Operand from = ret; ret = temp(entry);
		entry.appendCode(new ALUOp(Quadruple.SLL, ret, from, constant(lshift_bits)));
		from = ret; ret = temp(entry);
		entry.appendCode(new ALUOp(Quadruple.SRA, ret, from, constant(rshift_bits)));
		
		return ret;
	}
}
