package irpack;

/**
 * The StaticVarEntry class represents the static allocated variables.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 23, 2014
 */
public class StaticVarEntry {
	
	public StaticVarEntry(MemLoc memloc, Object initializer){
		_memloc = memloc;
		_initializer = initializer;
	}
	
	private MemLoc _memloc;
	private Object _initializer;
	
	public MemLoc getMemLoc() {
		return _memloc;
	}
	public void setMemLoc(MemLoc _memloc) {
		this._memloc = _memloc;
	}
	
	public Object getInitializer() {
		return _initializer;
	}
	public void setInitializer(Object _initializer) {
		this._initializer = _initializer;
	}
	
	@Override
	public String toString(){
		return new StringBuilder()
			.append(_memloc.toString())
			.append(" = ")
			.append((_initializer == null) ? "0" : _initializer.toString())
			.toString();
	}
}
