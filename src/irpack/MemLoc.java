package irpack;

/**
 * The MemLoc class records the memory location of an object.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 11, 2014
 */
public abstract class MemLoc implements Addressable, graph.Indexed{

	/**
	 * Constructs a new memory location object.
	 * 
	 * @param loc
	 * @param size
	 * @param num
	 */
	protected MemLoc(int loc, int size, int num){
		this.loc = loc;
		this.allocated = false;
		this.offset = 0;
		this.size = size;
		this.setIndex(num);
	}
	
	/**
	 * Whether the memory location is allocated.
	 * 
	 * @return
	 */
	public boolean isAllocated() {
		return allocated;
	}
	
	/**
	 * Sets the memory location to be allocated
	 * 
	 * @param _allocated
	 */
	public void setAllocated(boolean _allocated) {
		this.allocated = _allocated;
	}
	
	/**
	 * Accesses the location.
	 * 
	 * @return
	 */
	public int getLocation() {
		return loc;
	}
	
	/**
	 * Sets the location.
	 * 
	 * @param _loc
	 */
	public void setLocation(int _loc) {
		this.loc = _loc;
	}

	/**
	 * Accesses the offset.
	 * 
	 * @return
	 */
	public int getOffset() {
		return offset;
	}
	
	/**
	 * Sets the offset.
	 * 
	 * @param _offset
	 */
	public void setOffset(int _offset) {
		this.offset = _offset;
	}
	
	/**
	 * Accesses the size.
	 * 
	 * @return
	 */
	public int getSize() {
		return size;
	}
	
	/**
	 * Sets the size.
	 * 
	 * @param _size
	 */
	public void setSize(int _size) {
		this.size = _size;
	}

	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public void setIndex(int index) {
		this.index = index;
	}
	
	/**
	 *	Accesses the object size.
	 *
	 * @return the obj_size
	 */
	public int getObjectSize(){
		return size;
	}
	
	@Override
	public boolean isTakenAddress() {
		return takenAddress;
	}


	@Override
	public void setTakenAddress(boolean takenAddress) {
		this.takenAddress = takenAddress;
	}


	/** whether the object is allocated with an memory location */
	private boolean allocated;
	
	/** the offset */
	private int offset;
	
	/** the size of the object */
	private int size;	
	
	/** the location */
	private int loc;
	
	/** the index */
	private int index;
	
	/** if the MemLoc has been taken address */
	private boolean takenAddress;
	
	/** whether the object is an temporary object */
	public boolean isTemp(){
		return (loc & IsTemp) == IsTemp;
	}
	
	/** whether the object is an temporary value */
	public boolean isTempValue(){
		return (loc & TEMPVALUE) == TEMPVALUE;
	}
	
	/** whether the object is at local memory */
	public boolean atLocal(){
		return (loc & LocalGlobal) != 0;
	}
	
	// bit 0: local(1), Global(0)
	// bit 1: is argument(1), not (0)
	// bit 2: caller's (1), callee's (0)
	// bit 3: is temporary (1), not (0)
	// bit 4: is address temporary (1), is value temporary (0)
	// bit 5: is bit field(1), not (0)
	// bit 6: is temporary space(1), not(0)
	
	private static int LocalGlobal = 0x1;
	private static int IsArgument = 0x2;
	private static int CallerCallee = 0x4;
	private static int IsTemp = 0x8;
	private static int IsTempSpace = 0x40;
	
	public static int GLOBAL = 0;
	public static int LOCAL = LocalGlobal;
	
	public static int ARGCALLEE = LocalGlobal | IsArgument;
	public static int ARGCALLER = LocalGlobal | IsArgument | CallerCallee;
	
	public static int TEMPVALUE = LocalGlobal | IsTemp;
	
	public static int TEMPSPACE = TEMPVALUE | IsTempSpace;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = loc;
		result = prime * result + index;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		return this == obj;
	}
	
	
}
