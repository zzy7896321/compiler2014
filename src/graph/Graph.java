package graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * The Graph class represents directed graph structures in optimization.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 5, 2014
 */

public class Graph<NodeType extends GraphNode> {

	/**
	 * Constructs an empty graph with default collection factory.
	 * The default collection for edges and reverse edges are LinkedList and HashSet, respectively.
	 * 
	 */
	public Graph(){
		this(
		new CollectionFactory<Integer>(){

			@Override
			public Collection<Integer> newCollection() {
				
				return new LinkedList<Integer>();
			}
			
		}, 
		new CollectionFactory<Integer>(){

			@Override
			public Collection<Integer> newCollection() {
				
				return new HashSet<Integer>();
			}
			
		});
	}
	
	/**
	 * Constructs the graph with collection factory for edges designated.
	 * The default collection for reverse edges is HashSet.
	 * 
	 * @param collectionFactory
	 */
	public Graph(CollectionFactory<Integer> collectionFactory){
		this(collectionFactory,
		new CollectionFactory<Integer>(){

			@Override
			public Collection<Integer> newCollection() {
				
				return new HashSet<Integer>();
			}
			
		});
	}
	
	/**
	 * Constructs the graph with collection factory for both edges and reverse edges designated.
	 * 
	 * @param collectionFactory
	 */
	public Graph(CollectionFactory<Integer> collectionFactory, CollectionFactory<Integer> collectionFactoryForReverseEdge){
		this.collectionFactory = collectionFactory;
		this.collectionFactoryForReverseEdge = collectionFactoryForReverseEdge;
		
		this.size = 0;
		this.nodeList = new ArrayList<NodeType>();
		this.edge = new ArrayList<Collection<Integer>>();
		this.reverseEdge = new ArrayList<Collection<Integer>>();
	}
	
	/**
	 * Add a new node to the graph.
	 * 
	 * @param node
	 */
	public void addNode(NodeType node){		
		this.nodeList.add(node);
		this.edge.add(collectionFactory.newCollection());
		this.reverseEdge.add(this.collectionFactoryForReverseEdge.newCollection());
		node.setIndex(this.size++);
	}
	
	/**
	 * Removes the Ith node.
	 * 
	 * @param i
	 */
	public void removeNode(int i){
		this.nodeList.remove(i);
		this.edge.remove(i);
		this.reverseEdge.remove(i);
		this.size--;
	}
	
	/**
	 * Add a new edge to the graph.
	 * 
	 * @param x
	 * @param y
	 */
	public void addEdge(int x, int y){
		this.edge.get(x).add(y);
		this.reverseEdge.get(y).add(x);
	}
	
	/**
	 * Remove an edge from the graph.
	 * 
	 * @param x
	 * @param y
	 */
	public void removeEdge(int x, int y){
		this.edge.get(x).remove(y);
		this.reverseEdge.get(y).remove(x);
	}
	
	/**
	 * Returns the number of nodes in the graph.
	 * 
	 * @return
	 */
	public int size(){
		return this.size;
	}
	
	/**
	 * Query if the edge <x, y> exists.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean queryEdge(int x, int y){
		return edge.get(x).contains(y);
	}
	
	/**
	 *	Accesses the node list.
	 *
	 * @return the node
	 */
	public ArrayList<NodeType> getNodeList() {
		return nodeList;
	}

	/**
	 * Accesses the successor set of nth node.
	 * 
	 * @param n
	 * @return
	 */
	public Collection<Integer> successor(int n){
		return edge.get(n);
	}
	
	/**
	 * Accesses the predecessor set of nth node.
	 * 
	 * @param n
	 * @return
	 */
	public Collection<Integer> predecessor(int n){
		return reverseEdge.get(n);
	}
	
	/**
	 * Returns the number of successors of nth node.
	 * 
	 * @param n
	 * @return
	 */
	public int successorCount(int n){
		return edge.get(n).size();
	}
	
	/**
	 * Returns the number of predecessors of nth node.
	 * 
	 * @param n
	 * @return
	 */
	public int predecessorCount(int n){
		return reverseEdge.get(n).size();
	}
	
	/**
	 * Accesses the ith node.
	 * 
	 * @param i
	 * @return
	 */
	public NodeType getIthNode(int i){
		return nodeList.get(i);
	}
	
	/**
	 * Accesses the first node.
	 * 
	 * @return
	 */
	public NodeType getFirstNode(){
		return nodeList.get(0);
	}
	
	/**
	 * Accesses the last node.
	 * 
	 * @return
	 */
	public NodeType getLastNode(){
		return nodeList.get(size-1);
	}
	
	/**
	 * Disconnects the node from other nodes.
	 * 
	 * @param n
	 */
	public void disconnectNode(int n){
		
		for (int y : successor(n)){
			reverseEdge.get(y).remove(n);
		}
		
		for (int x : predecessor(n)){
			edge.get(x).remove(n);
		}
		
		edge.get(n).clear();
		reverseEdge.get(n).clear();
	}

	/** size of the graph */
	int size;
	
	/** node container */
	private ArrayList<NodeType> nodeList;
	
	/** edge container */
	private ArrayList<Collection<Integer>> edge;
	
	/** reverse edge container */
	private ArrayList<Collection<Integer>> reverseEdge;
	
	/** factory class of edge collections */
	private CollectionFactory<Integer> collectionFactory;
	
	/** factory class of reverse edge collection */
	private CollectionFactory<Integer> collectionFactoryForReverseEdge;
	
	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();
		
		builder.append("<Graph [size = ").append(size).append("]>\n");
		
		for (NodeType node : this.nodeList){
			builder.append(node.toString());
		}
		
		builder.append("\n<Graph edges>\n");
		for (int i = 0; i < size; ++i){
			Collection<Integer> elist = edge.get(i);
			
			int n = 0;
			builder.append("[node ").append(i).append("]:");
			for (Integer to : elist){
				if (n == 0){
					builder.append("\n");
				}
				
				builder.append("\t").append(to);
				
				n = (n + 1) % 10;
			}
			
			builder.append("\n");
		}
		
		builder.append("\n\n");
		
		return builder.toString();
	}
	
}
