package graph;

import java.util.BitSet;
import java.util.LinkedList;

/**
 * The WorkList class implements a work list with a linked list and a bit set to provide quick insertion and quick membership query.
 * The WorkList only supports quick removal of an unspecified element, rather than a specified one.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 9, 2014
 */
public class WorkList<ElementType extends Indexed> {

	/**
	 * Constructs a work list with index ranging within [0 .. N-1].
	 * 
	 * 
	 */
	public WorkList(int N) {
		list = new LinkedList<ElementType>();
		inlist = new BitSet(N);
	}

	
	/**
	 *	Accesses the work list.
	 *
	 * @return the list
	 */
	public LinkedList<ElementType> getList() {
		return list;
	}

	/**
	 * Returns if the element is in the work list.
	 * 
	 * @param e
	 * @return
	 */
	public boolean isInList(ElementType e){
		return inlist.get(e.getIndex());
	}
	
	/**
	 * Append the element to the work list if it is not present in the work list.
	 * 
	 * @param e
	 * @return		if there is an actual insertion
	 */
	public boolean insert(ElementType e){
		if (inlist.get(e.getIndex())) return false;
		
		list.add(e);
		inlist.set(e.getIndex());
		
		return true;
	}
	
	/**
	 * Remove an unspecified element from the list.
	 * 
	 * @return		the element (or null if no element is in the list)
	 */
	public ElementType remove(){
		if (list.size() == 0)  return null;
		
		ElementType ret = list.removeFirst();
		inlist.clear(ret.getIndex());
		return ret;
	}
	
	/**
	 * Returns the size of the work list.
	 * 
	 * @return
	 */
	public int size(){
		return list.size();
	}
	
	/**
	 * Returns if the work list is empty.
	 * 
	 * @return
	 */
	public boolean isEmpty(){
		return list.isEmpty();
	}

	/** the work list */
	private LinkedList<ElementType> list;
	
	/** bit set indicates whether a element is in the list */
	private BitSet inlist;
}
