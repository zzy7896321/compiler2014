package graph;

import java.util.Collection;

/**
 * The CollectionFactory class provides interface for graph to obtain specific type of collection.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 7, 2014
 */
public interface CollectionFactory<ElementType> {
	
	/**
	 * Creates a new collection of ElementType.
	 * 
	 * @return
	 */
	public Collection<ElementType> newCollection();
	
}
