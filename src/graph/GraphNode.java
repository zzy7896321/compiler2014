package graph;

/**
 * The GraphNode class is the interface that a NodeType class has to implement.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 5, 2014
 */
public interface GraphNode extends Indexed{
	
	/**
	 * Creates a new empty node.
	 * 
	 * @return
	 */
	public GraphNode newEmptyNode();
	
	@Override
	public void setIndex(int index);
	
	
	@Override
	public int getIndex();
	
	@Override
	public String toString();
}
