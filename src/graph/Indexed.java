package graph;

/**
 * The Indexed interface requires the class to implement an index method.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 9, 2014
 */
public interface Indexed {

	/**
	 * Returns the index of the node.
	 * 
	 * @return
	 */
	public int getIndex();
	
	/**
	 * Sets the number of the node.
	 * 
	 * @param index
	 */
	public void setIndex(int index);
}
