package graph;

import util.MyLinkedList;
import irpack.*;

/**
 * The Block class records basic blocks of quadruples.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 5, 2014
 */
public class Block implements GraphNode{

	/**
	 * Constructs a new empty block.
	 * 
	 * @param prev
	 */
	public Block(){
		setList(new MyLinkedList<Quadruple>());
	}
	
	/**
	 * Constructs a new block with the list provided.
	 * 
	 * @param list
	 */
	public Block(MyLinkedList<Quadruple> list){
		setList(list);
	}
	
	@Override
	public void setIndex(int index) {
		this.index = index;
	}


	@Override
	public int getIndex() {
		
		return this.index;
	}

	/**
	 *	Accesses the quadruple list.
	 *
	 * @return the list
	 */
	public MyLinkedList<Quadruple> getList() {
		return list;
	}

	/**
	 *	Sets the quadruple list.
	 * @param list the list to set
	 */
	public void setList(MyLinkedList<Quadruple> list) {
		this.list = list;
	}
	
	/**
	 * Clears the block.
	 * 
	 */
	public void clear(){
		list.clear();
	}
	
	/**
	 * Adds the quadruple to the head (skip the first label if there is).
	 * 
	 * @param quad
	 * @returns the new node
	 */
	public MyLinkedList<Quadruple>.Node addFirst(Quadruple quad){
		MyLinkedList<Quadruple>.Node node = list.iterator();
		if (node.hasNext() && node.next().value() instanceof Label){
			node = node.next();
		}
		node = node.insert(quad);
		quad.setAssociatedBlock(this);
		
		return node;
	}
	
	/**
	 * Adds the quadruple to the tail.
	 * 
	 * @param quad
	 * @returns the new node
	 */
	public MyLinkedList<Quadruple>.Node addLast(Quadruple quad){
		MyLinkedList<Quadruple>.Node node = this.list.addLast(quad);
		quad.setAssociatedBlock(this);
		
		return node;
	}
	
	/**
	 * Adds the phi function move before branch/jump.
	 * 
	 * @param quad
	 * @return
	 */
	public MyLinkedList<Quadruple>.Node addPhiMove(Quadruple quad){
		MyLinkedList<Quadruple>.Node iter = list.iterator();
		if (list.size() != 0){
			Quadruple prev = iter.previous().value();
			if (prev instanceof Branch || prev instanceof Jump){
				iter = iter.previous();
			}
		}
		
		iter.insertBefore(quad);
		
		return iter.previous();
	}

	/** the head of the quadruple list in the block */
	private MyLinkedList<Quadruple> list;

	/** the number of the block */
	private int index;
	
	/** the true branch successor */
	private int trueBranchSuccessor;
	
	/** the false branch successor */
	private int falseBranchSuccessor;
	
	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();
		
		builder.append("[Block ").append(getIndex()).append("]\n");
		for (MyLinkedList<Quadruple>.Node node = list.iterator(); node.hasNext(); ){
			node = node.next();
			
			Quadruple quad = node.value();
			if (quad instanceof Label){
				builder.append(quad.toString()).append(":\n");
			}
			else {
				builder.append("\t").append(quad.toString()).append("\n");
			}
		}
	
		return builder.toString();
	}

	@Override
	public Block newEmptyNode() {
		
		return new Block();
	}

	/**
	 *	Accesses the true branch successor.
	 *
	 * @return the trueBranchSuccessor
	 */
	public int getTrueBranchSuccessor() {
		return trueBranchSuccessor;
	}

	/**
	 *	Sets the true branch successor.
	 * @param trueBranchSuccessor the trueBranchSuccessor to set
	 */
	public void setTrueBranchSuccessor(int trueBranchSuccessor) {
		this.trueBranchSuccessor = trueBranchSuccessor;
	}

	/**
	 *	Accesses the false branch successor.
	 *
	 * @return the falseBranchSuccessor
	 */
	public int getFalseBranchSuccessor() {
		return falseBranchSuccessor;
	}

	/**
	 *	Sets the false branch successor.
	 * @param falseBranchSuccessor the falseBranchSuccessor to set
	 */
	public void setFalseBranchSuccessor(int falseBranchSuccessor) {
		this.falseBranchSuccessor = falseBranchSuccessor;
	}
}
