package node;

import irpack.BitField;
import irpack.MemLoc;
import irpack.Operand;
import irpack.StaticVarEntry;
import irpack.op;
import semantics.SemanticCheck;
import environment.Environment;
import environment.RuntimeEnvironment;

/**
 * The AddressExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class AddressExpression extends UnaryExpression {

	/**
	 * @param expr1
	 */
	public AddressExpression(ExpressionBase expr1) {
		super(expr1);

	}

	@Override
	public String getName() {

		return "address &";
	}

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
			
		
		if (_operand instanceof PrimaryExpression){
			if (((PrimaryExpression) _operand).getVal() instanceof StringConstant){
				String val = ((StringConstant)((PrimaryExpression) _operand).getVal()).getVal();
				environment.VariableSymbol pseudo_sym = new environment.VariableSymbol(val, 
						new environment.ArrayType(environment.TypeFactory.newCharType(), val.length()), 
						environment.SymbolInfo.EXTERNAL_LINKAGE);		// pseudo variable symbol which needs memory allocation in global storage 
				pseudo_sym.setInitialized(true);
				SemanticCheck.getRunningInstance().getIR().addStaticVariableEntry(new StaticVarEntry(pseudo_sym.getMemLoc(), pseudo_sym.getName()));
				
				setType(new environment.PointerType(pseudo_sym.getType()));	
				setLvalue(false);
				
				_value = op.la(op.temp(runtime.entry), pseudo_sym.getMemLoc(), runtime.entry);
				
				return;
			}
			
			else if (((PrimaryExpression) _operand).getVal() instanceof Identifier){
				String name = ((Identifier)((PrimaryExpression) _operand).getVal()).getVal();
				environment.SymbolInfo info = cur_env.lookup(name);
				if (info instanceof environment.FunctionDesignator){
					_type = new environment.PointerType(info.getType());
					_lvalue = false;
					
					_value = SemanticCheck.getRunningInstance().getIR().getFunctionLabel(info.getName());
					
					return;
				}
				else if (info instanceof environment.VariableSymbol){
					_type = new environment.PointerType(info.getType());
					_lvalue = false;
					MemLoc memloc = ((environment.VariableSymbol) info).getMemLoc();
					
					_value = op.la(op.temp(runtime.entry), memloc, runtime.entry);
					
					return;
				}
				else if (info instanceof environment.TypedefName){
					// syntactically illegal
					report_error("cannot take address of typedef name", this);
					_type = null;
					return;
				}
			}
			else {
				report_error("cannot take address of constant", this);
				_type = null;
				return;
			}
		}
		else if (_operand instanceof IndirectionExpression){
			ExpressionBase true_oprand= ((IndirectionExpression) _operand).getOprand();
			
			true_oprand.checkEvaluate(cur_env, runtime);
			
			if (!(true_oprand._type instanceof environment.PointerType)){
				report_error(String.format("indirection operation requires a pointer type operand, but %s found", 
						semantics.ResourcePrinter.getTypeString(true_oprand._type)), _operand);
				_type = null;
				return;
			}
			
			_type = true_oprand._type;
			_value = true_oprand._value;
			_lvalue = false;
			return;
		}
		else if (_operand instanceof ArraysubscriptingExpression){
			ExpressionBase _left = ((ArraysubscriptingExpression) _operand).getLeft();
			ExpressionBase _right = ((ArraysubscriptingExpression) _operand).getRight();
			
			_left.checkEvaluate(cur_env, runtime);
			if (_left._type == null){
				_type = null;
				return;
			}
			_right.checkEvaluate(cur_env, runtime);
			if (_right._type == null) {
				_type = null;
				return;
			}
			
			if (_right._type instanceof environment.PointerType && environment.TypeFactory.isIntegerType(_left._type)){
				ExpressionBase tmp = _left;
				_left = _right;
				_right = tmp;
			}
			
			if (_left._type instanceof environment.PointerType && environment.TypeFactory.isIntegerType(_right._type)){
				_type = ((environment.PointerType)_left._type).getBaseType();
				if (_type instanceof environment.FunctionType){
					report_error("subscripting to pointer to function type", this);
					_type = null;
					return ;
				}
				if (!_type.isComplete()){
					report_error("subscripting to pointer to incomplete type", this);
					_type = null;
					return;
				}
				
				Operand result = null;
				int base_size = _type.getSize();
				if (Integer.highestOneBit(base_size) == base_size){
					result = op.sll(op.temp(runtime.entry), (Operand)_right._value, op.constant(Integer.numberOfTrailingZeros(base_size)), runtime.entry);
				}
				else {
					result = op.mul(op.temp(runtime.entry), (Operand)_right._value, op.constant(base_size), runtime.entry);
				}
				
				result = op.add(op.temp(runtime.entry), (Operand)_left._value, result, runtime.entry);
				
				_value = result;

				_lvalue = false;
				_type = _left._type;
				
				return ;	
			}
			
			else {
				report_invalid_operand_error(this, "array subscription", _left._type, _right._type);
				_type = null;
				return ;
			}			
		}
				
		_operand.checkEvaluate(cur_env, runtime);
		if (_operand._type == null){
			_type = null;
			return;
		}
		
		if (!_operand._lvalue){
			report_error("cannot take address of non-lvalue", this);
			_type = null;
			return;
		}
		
		if (_operand._value instanceof BitField){
			report_error("cannot take address of bit field", this);
			_type = null;
			return;
		}
		
		if (_operand._type instanceof environment.StructOrUnionType){
			_value = _operand._value;
		}
		else {
			_value = op.la(op.temp(runtime.entry), (Operand)_operand._value, runtime.entry);
		}
		
		
		_type = new environment.PointerType(_operand._type);
		_lvalue = false;
			
		return;
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			
		
		if (_operand instanceof PrimaryExpression){
			if (((PrimaryExpression) _operand).getVal() instanceof StringConstant){
				String val = ((StringConstant)((PrimaryExpression) _operand).getVal()).getVal();
				environment.VariableSymbol pseudo_sym = new environment.VariableSymbol(val, 
						new environment.ArrayType(environment.TypeFactory.newCharType(), val.length()), 
						environment.SymbolInfo.EXTERNAL_LINKAGE);		// pseudo variable symbol which needs memory allocation in global storage 
				pseudo_sym.setInitialized(true);
				SemanticCheck.getRunningInstance().getIR().addStaticVariableEntry(new StaticVarEntry(pseudo_sym.getMemLoc(), pseudo_sym.getName()));
				
				setType(new environment.PointerType(pseudo_sym.getType()));	
				setValue(new environment.AddressConstant(pseudo_sym, 0));
				setLvalue(false);	// ???
				return;
			}
			
			else if (((PrimaryExpression) _operand).getVal() instanceof Identifier){
				String name = ((Identifier)((PrimaryExpression) _operand).getVal()).getVal();
				environment.SymbolInfo info = cur_env.lookup(name);
				if (info instanceof environment.FunctionDesignator){
					_type = new environment.PointerType(info.getType());
					_value = new environment.AddressConstant(info, 0);
					_lvalue = false;
					return;
				}
				else if (info instanceof environment.VariableSymbol){				
					_type = new environment.PointerType(info.getType());
					_value = new environment.AddressConstant(info, 0);
					_lvalue = false;
					return;
				}
				else if (info instanceof environment.TypedefName){
					// syntactically illegal
					report_error("cannot take address of typedef name", this);
					_type = null;
					return;
				}
			}
			else {
				report_error("cannot take address of constant", this);
				_type = null;
				return;
			}
		}
		else if (_operand instanceof IndirectionExpression){
			ExpressionBase true_oprand= ((IndirectionExpression) _operand).getOprand();
			
			true_oprand.checkEvaluate_NoGenerating(cur_env);
			
			if (!(true_oprand._type instanceof environment.PointerType)){
				report_error(String.format("indirection operation requires a pointer type operand, but %s found", 
						semantics.ResourcePrinter.getTypeString(true_oprand._type)), _operand);
				_type = null;
				return;
			}
			
			_type = true_oprand._type;
			_value = true_oprand._value;
			_lvalue = false;
			return;
		}
		else if (_operand instanceof ArraysubscriptingExpression){
			ExpressionBase _left = ((ArraysubscriptingExpression) _operand).getLeft();
			ExpressionBase _right = ((ArraysubscriptingExpression) _operand).getRight();
			
			_left.checkEvaluate_NoGenerating(cur_env);
			if (_left._type == null){
				_type = null;
				return;
			}
			_right.checkEvaluate_NoGenerating(cur_env);
			if (_right._type == null) {
				_type = null;
				return;
			}
			
			if (_right._type instanceof environment.PointerType && environment.TypeFactory.isIntegerType(_left._type)){
				ExpressionBase tmp = _left;
				_left = _right;
				_right = tmp;
			}
			
			if (_left._type instanceof environment.PointerType && environment.TypeFactory.isIntegerType(_right._type)){
				_type = ((environment.PointerType)_left._type).getBaseType();
				if (_type instanceof environment.FunctionType){
					report_error("subscripting to pointer to function type", this);
					_type = null;
					return ;
				}
				
				if (!_left._lvalue && _left._value instanceof environment.AddressConstant 
						&& _right._value instanceof Number){
					_value = ((environment.AddressConstant)_left._value).newAddrWithOffsetAddedTo((int)_right._value * _type.getSize());
				}
				else {
					_value = null;
				}

				_lvalue = false;
				
				_type = _left._type;
				//implicitConversion(this); no need
				return ;
				
			}
			
			else {
				report_invalid_operand_error(this, "array subscription", _left._type, _right._type);
				_type = null;
				return ;
			}			
		}
				
		_operand.checkEvaluate_NoGenerating(cur_env);
		if (_operand._type == null){
			_type = null;
			return;
		}
		
		if (!_operand._lvalue){
			report_error("cannot take address of non-lvalue", this);
			_type = null;
			return;
		}
		
		if (_operand._value instanceof environment.AddressConstant && ((environment.AddressConstant)_operand._value).getMask() != 0){
			report_error("cannot take address of bit-field", this);
			_type = null;
			return;
		}
		
		_type = new environment.PointerType(_operand._type);
		if (_operand._value instanceof environment.AddressConstant){
			_value = ((environment.AddressConstant)_operand._value).newAddrWithOffsetAddedTo(0);
		}
		else {
			_value = null;		// non-constant
		}
		_lvalue = false;
		
	}

}
