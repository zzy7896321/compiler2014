package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The char constant node class.
 * 
 * 
 * @author Zhao Zhuoyue
 * @version last update: 3/26/2012
 *
 */
public class CharConstant extends Constant {

	/**
	 * Constructs a char constant from a char.
	 * 
	 * @param val	the char constant
	 */
	public CharConstant(char val){
		super();
		_val = val;
	}
	
	char _val;
	
	/**
	 * Accesses the char constant.
	 * 
	 * @return the char constant
	 */
	public char getChar(){
		return _val;
	}
	
	/**
	 * Sets the char constant
	 * @param val	the char constant to set
	 */
	public void setChar(char val){
		_val = val;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		String[] properties;
		properties = new String[]{"value", util.StringUtil.unescape(_val)};
		traverseFormatterHead(writer, name, properties);
		traverseFormatterTail(writer, name, properties);
		return writer;
	}

	static final String name = "char constant";

	@Override
	public String getName() {
		
		return name;
	}

	@Override
	public String getValLiteral() {
		
		return "'" + util.StringUtil.unescape(_val) + "'";
	}

}
