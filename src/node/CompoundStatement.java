package node;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * The CompoundStatement node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class CompoundStatement extends Statement {

	/**
	 * Constructs a compound statement from a linked list of statements and declarations.
	 * NOTE: the list is possibly null.
	 * 
	 * @param list		a statement list
	 */
	public CompoundStatement(LinkedList<NodeBase> list) {
		_list = list;
	}

	public LinkedList<NodeBase> getList(){
		return _list;
	}
	
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		traverseFormatterHead(writer, this.getName(), null);
		
		if (_list != null)
		for (ListIterator<NodeBase> iter = _list.listIterator(); iter.hasNext(); iter.next().traverse(writer));
		
		traverseFormatterTail(writer, this.getName(), null);
		return writer;
	}

	@Override
	public String getName() {

		return "Compound statement (reprensents a new scope)";
	}

	protected LinkedList<NodeBase> _list;
}
