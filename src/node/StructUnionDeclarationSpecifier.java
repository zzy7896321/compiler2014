package node;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.ListIterator;
/**
 * The StructUnionDeclarationSpecifier node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public abstract class StructUnionDeclarationSpecifier extends DeclarationSpecifier {

	/**
	 * Constructs an struct or union declaration specifier base class.
	 */
	public StructUnionDeclarationSpecifier() {
		super();
		_id = null;
		_list = null;
	}
	
	public StructUnionDeclarationSpecifier(StructUnionDeclarationSpecifier rhs){
		super(rhs);
		_id = rhs._id;
		_list = rhs._list;
	}
	
	public void setId(Identifier id){
		_id = id;
	}
	
	public void setDeclarationList(LinkedList<StructDeclaration> list){
		_list = list;
	}
	
	public Identifier getIdentifier(){
		return _id;
	}
	
	public LinkedList<StructDeclaration> getList(){
		return _list;
	}
	
	public boolean hasSameContent(StructUnionDeclarationSpecifier rhs){
		return (_id == rhs._id) && (_list == rhs._list);
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException{
		if (_list != null){
			String[] properties = new String[]{"id", (_id != null) ? (_id.getVal()) : ("<anonymous>")};
			traverseFormatterHead(writer, this.getName(), properties);
			
			if (_list != null)
			for (ListIterator<StructDeclaration> iter = _list.listIterator(); iter.hasNext(); iter.next().traverse(writer));
			
			traverseFormatterTail(writer, this.getName(), properties);
		}
		return writer;
	}
	
	protected Identifier _id;
	protected LinkedList<StructDeclaration> _list;
}
