package node;

import irpack.Operand;
import irpack.op;
import environment.Environment;
import environment.RuntimeEnvironment;
import environment.TypeFactory;


/**
 * The NeqExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class NeqExpression extends BinaryExpression {

	public NeqExpression(ExpressionBase expr1, ExpressionBase expr2) {
		super(expr1, expr2);
	}

	@Override
	public String getName() {
		return "!=";
	}
	
	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
			

		_left.checkEvaluate(cur_env, runtime);
		if (_left._type == null){
			_type = null;
			return;
		}
		_right.checkEvaluate(cur_env, runtime);
		if (_right._type == null){
			_type = null;
			return;
		}
		
		if (environment.TypeFactory.isIntegerType(_left._type) && _right._type instanceof environment.PointerType){
			ExpressionBase tmp = _left;
			_left = _right;
			_right = tmp;
		}
		
		if (environment.TypeFactory.isIntegerType(_left._type) && environment.TypeFactory.isIntegerType(_right._type)){
			PromoteIntegerType(_left, _right);
			
		}
		else if (_left._type instanceof environment.PointerType && _right._type instanceof environment.PointerType){
			environment.Type left_base_type = ((environment.PointerType)_left._type).getBaseType();

			environment.Type right_base_type = ((environment.PointerType)_right._type).getBaseType();

			if (!(left_base_type.equals(right_base_type) || left_base_type instanceof environment.VoidType
				|| right_base_type instanceof environment.VoidType)){
				
				report_warning("comparison between imcompatible pointers", this);
			}
			
		}
		else if (_left._type instanceof environment.PointerType && environment.TypeFactory.isIntegerType(_right._type)){
			if (!(_right._value instanceof Number && ((Number)_right._value).intValue() == 0)){
				report_warning("comparison between pointer and integer", this);
			}
		}
		else {
			report_invalid_operand_error(this, "!=", _left._type, _right._type);
			_type = null;
			return;
		}
		
		_type = TypeFactory.newIntType();
		_lvalue = false;
		
		_value = op.sne(op.temp(runtime.entry), (Operand) _left._value, (Operand) _right._value, runtime.entry);
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		_left.checkEvaluate_NoGenerating(cur_env);
		if (_left._type == null){
			_type = null;
			return;
		}
		_right.checkEvaluate_NoGenerating(cur_env);
		if (_right._type == null){
			_type = null;
			return;
		}
		
		if (environment.TypeFactory.isIntegerType(_left._type) && _right._type instanceof environment.PointerType){
			ExpressionBase tmp = _left;
			_left = _right;
			_right = tmp;
		}
		
		if (environment.TypeFactory.isIntegerType(_left._type) && environment.TypeFactory.isIntegerType(_right._type)){
			PromoteIntegerType(_left, _right);
			
			_type = environment.TypeFactory.newIntType();
			if (_left._value instanceof Number && _right._value instanceof Number){
				
				_value = (((Integer)_left._value).intValue() != ((Integer)_right._value).intValue()) ? 1 : 0;
			}
			else {
				_value = null;
			}
			_lvalue = false;
			return;
		}
		else if (_left._type instanceof environment.PointerType && _right._type instanceof environment.PointerType){
			environment.Type left_base_type = ((environment.PointerType)_left._type).getBaseType();

			environment.Type right_base_type = ((environment.PointerType)_right._type).getBaseType();

			if (!(left_base_type.equals(right_base_type) || left_base_type instanceof environment.VoidType
				|| right_base_type instanceof environment.VoidType)){
				
				report_warning("comparison between imcompatible pointers", this);
			}
			
			_type = environment.TypeFactory.newIntType();
			if (!_left._lvalue && _left._value instanceof environment.AddressConstant &&
					!_right._lvalue && _right._value instanceof environment.AddressConstant){
				environment.AddressConstant laddr = (environment.AddressConstant)_left._value;
				environment.AddressConstant raddr = (environment.AddressConstant)_right._value;

				if (laddr.getBase() == raddr.getBase()){
					_value = (laddr.getOffset() != raddr.getOffset()) ? 1 : 0;
				}
				else{
					_value = null;
				}
			}
			else {
				_value = null;
			}
			_lvalue = false;
			return;
		}
		else if (_left._type instanceof environment.PointerType && environment.TypeFactory.isIntegerType(_right._type)){
			if (!(_right._value instanceof Number && ((Number)_right._value).intValue() == 0)){
				report_warning("comparison between pointer and integer", this);
			}
			
			_type = environment.TypeFactory.newIntType();
			if (!_left._lvalue && _left._value instanceof environment.AddressConstant  && _right._value instanceof Number){
				environment.AddressConstant addr = (environment.AddressConstant)_left._value;
				int val = ((Number)_right._value).intValue();
				if (addr.getBase() == null){
					_value =  (addr.getOffset() != val) ? 1 : 0;
				}
				else if (val == 0){
					if (addr.getOffset() == 0){
						_value = 1;
					}
					else {
						_value = null;
					}
				}
			}
			else {
				_value = null;
			}
			_lvalue = false;
			return;
		}
		
		report_invalid_operand_error(this, "!=", _left._type, _right._type);
		_type = null;
		
	}
}
