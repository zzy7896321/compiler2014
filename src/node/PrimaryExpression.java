package node;

import irpack.*;

import java.io.IOException;
import java.io.Writer;

import semantics.SemanticCheck;
import environment.RuntimeEnvironment;

/**
 * The PrimaryExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class PrimaryExpression extends ExpressionBase {

	/**
	 * Constructs a primary expression from identifier or constant
	 * 
	 * @param val		the node of the identifier or the constant
	 */
	public PrimaryExpression(NodeBase val) {
		super();
		_val = val;
	}

	@Override
	public String toString(){
		return _val.toString();
	}
	
	public NodeBase getVal(){
		return _val;
	}
	
	@Override
	public String getName() {
		
		return "";
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		_val.traverse(writer);
		return writer;
	}

	protected NodeBase _val;

	@Override
	public void checkEvaluate(environment.Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
		
		/* int literal */
		if (_val instanceof node.IntegerConstant){
			if (((node.IntegerConstant) _val).getVal().bitLength() >= config.TargetConfig.int_length){
				report_warning("integer constant overflows", _val);
			}
			setType(environment.TypeFactory.newIntType());
			setValue(op.constant(((node.IntegerConstant) _val).getVal().intValue()));
			setLvalue(false);
			return;
		}
		
		/* char literal */
		else if (_val instanceof node.CharConstant){
			char value = ((node.CharConstant) _val).getChar();
			if (value < config.TargetConfig.CHAR_MIN || value > config.TargetConfig.CHAR_MAX){
				report_warning("character constant overflows", _val);
			}
			setType(environment.TypeFactory.newCharType());
			setValue(op.constant(((node.CharConstant) _val).getChar()));
			setLvalue(false);
			return;
		}
		
		/* string literal */
		else if (_val instanceof node.StringConstant){
			environment.VariableSymbol pseudo_sym = new environment.VariableSymbol(((node.StringConstant) _val).getVal(), 
					new environment.ArrayType(environment.TypeFactory.newCharType(), ((node.StringConstant) _val).getVal().length()), 
					environment.SymbolInfo.EXTERNAL_LINKAGE);		// pseudo variable symbol which needs memory allocation in global storage 
			pseudo_sym.setInitialized(true);
			SemanticCheck.getRunningInstance().getIR().addStaticVariableEntry(new StaticVarEntry(pseudo_sym.getMemLoc(), pseudo_sym.getName()));
			
			
			setType(new environment.PointerType(environment.TypeFactory.newCharType()));	 //implicit conversion in expression evaluation
			setLvalue(false);
			
			_value = op.la(op.temp(runtime.entry), pseudo_sym.getMemLoc(), runtime.entry);
						
			return;
		}
		
		/* identifier */
		else if (_val instanceof node.Identifier){
			environment.SymbolInfo info = cur_env.lookup(((node.Identifier) _val).getVal());
			if (info == null){
				report_error("identifier is not declared", _val);
				_type = null;
				return;
			}
			
			if (info instanceof environment.TypedefName){
				report_error("typedef name is not a valid primary expression", this);
				_type = null;
				return;
			}
			
			_type = info.getType();
			
			if (_type instanceof environment.ArrayType){
				_lvalue = false;
				
				_value = op.la(op.temp(runtime.entry), ((environment.VariableSymbol)info).getMemLoc(), runtime.entry);
			}
			else if (_type instanceof environment.FunctionType){
				_lvalue = false;
				
				_value = SemanticCheck.getRunningInstance().getIR().getFunctionLabel(info.getName());
			}
			else if (_type instanceof environment.StructOrUnionType){
				_lvalue = true;
				
				MemLoc memloc = ((environment.VariableSymbol)info).getMemLoc();
				
				_value = op.la(op.temp(runtime.entry), memloc, runtime.entry);
			}
			else {
				_lvalue = true;
				
				_value = ((environment.VariableSymbol)info).getMemLoc();
			}
			
			implicitConversion_NoGenerating(this);
			return;
		}
		
		report_error("Invalid primary expression", this);		
		_type = null;
		return;
	}

	@Override
	public void checkEvaluate_NoGenerating(environment.Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
		
		/* int literal */
		if (_val instanceof node.IntegerConstant){
			if (((node.IntegerConstant) _val).getVal().bitLength() >= config.TargetConfig.int_length){
				report_warning("integer constant overflows", _val);
			}
			setType(environment.TypeFactory.newIntType());
			setValue(((node.IntegerConstant) _val).getVal());
			setLvalue(false);
			return;
		}
		
		/* char literal */
		else if (_val instanceof node.CharConstant){
			char value = ((node.CharConstant) _val).getChar();
			if (value < config.TargetConfig.CHAR_MIN || value > config.TargetConfig.CHAR_MAX){
				report_warning("character constant overflows", _val);
			}
			setType(environment.TypeFactory.newCharType());
			setValue((int)value);
			setLvalue(false);
			return;
		}
		
		/* string literal */
		else if (_val instanceof node.StringConstant){
			environment.VariableSymbol pseudo_sym = new environment.VariableSymbol(((node.StringConstant) _val).getVal(), 
					new environment.ArrayType(environment.TypeFactory.newCharType(), ((node.StringConstant) _val).getVal().length()), 
					environment.SymbolInfo.EXTERNAL_LINKAGE);		// pseudo variable symbol which needs memory allocation in global storage 
			pseudo_sym.setInitialized(true);
			SemanticCheck.getRunningInstance().getIR().addStaticVariableEntry(new StaticVarEntry(pseudo_sym.getMemLoc(), pseudo_sym.getName()));
			
			
			setType(new environment.PointerType(environment.TypeFactory.newCharType()));	 //implicit conversion in expression evaluation
			setValue(new environment.AddressConstant(pseudo_sym, 0));
			setLvalue(false);
			return;
		}
		
		/* identifier */
		else if (_val instanceof node.Identifier){
			environment.SymbolInfo info = cur_env.lookup(((node.Identifier) _val).getVal());
			if (info == null){
				report_error("identifier is not declared", _val);
				_type = null;
				return;
			}
			
			if (info instanceof environment.TypedefName){
				report_error("typedef name is not a valid primary expression", this);
				_type = null;
				return;
			}
			
			_type = info.getType();
			_value = new environment.AddressConstant(info, 0);
			_lvalue = true;
			implicitConversion_NoGenerating(this);
			return;
		}
		
		report_error("Invalid primary expression", this);		
		_type = null;
		return;
	}

	@Override
	public void clearEvaluatedState() {
		_evaluated = false;
		_type = null;
		_value = null;
		_lvalue = false;
	}
}
