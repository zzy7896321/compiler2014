package node;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * The Declaration node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class Declaration extends NodeBase {

	/**
	 * Constructs declaration node from a base type and a declarator list.
	 * 
	 * 
	 */
	public Declaration(DeclarationSpecifier spec, LinkedList<Declarator> list) {
		_spec = spec;
		_list = list;
	}
	
	/**
	 * Returns the declaration specifier.
	 * 
	 * @return 	the declaration specifier
	 */
	public DeclarationSpecifier getDeclarationSpecifier(){
		return _spec;
	}
	
	/**
	 * Returns the declarator list.
	 * 
	 * @return	the declarator list
	 */
	public LinkedList<Declarator> getList(){
		return _list;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		String[] properties = new String[]{ "declared type", _spec.getTypeName(), 
											"no. of declarator", Integer.toString(_list.size())};
		if (_spec.isTypedef()){
			traverseFormatterHead(writer, "Typedef", properties);
		}
		else {
			traverseFormatterHead(writer, "Declaration", properties);
		}
		_spec.traverse(writer);
		if (_list != null)
			for (ListIterator<Declarator> iter = _list.listIterator(); iter.hasNext(); iter.next().traverse(writer));
		
		if (_spec.isTypedef()){
			traverseFormatterTail(writer, "Typedef", properties);
		}
		else{
			traverseFormatterTail(writer, "Declaration", properties);
		}
		return writer;
	}

	@Override
	public String getName() {

		return "declaration";
	}
	
	protected DeclarationSpecifier _spec;
	protected LinkedList<Declarator> _list;

}
