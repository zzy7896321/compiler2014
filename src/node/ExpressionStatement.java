package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The ExpressionStatement node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class ExpressionStatement extends Statement {

	/**
	 * Constructs an expression statement from an expression.
	 * NOTE: expr is possibly null.
	 * 
	 * @param expr		an expression
	 */
	public ExpressionStatement(ExpressionBase expr) {
		_expr = expr;
	}

	public ExpressionBase getExpr(){
		return _expr;
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		if (_expr == null){
			traverseFormatterHead(writer, "null statement", null);
			traverseFormatterTail(writer, "null statement", null);
		}
		else {
			_expr.traverse(writer);
		}
		return writer;
	}

	@Override
	public String getName() {

		return "expression statement";
	}
	
	protected ExpressionBase _expr;

}
