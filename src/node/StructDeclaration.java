package node;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * The StructDeclaration node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class StructDeclaration extends NodeBase {

	/**
	 * Constructs a struct declaration from a type and a list of declarators.
	 */
	public StructDeclaration(DeclarationSpecifier spec, LinkedList<Declarator> list) {
		_spec = spec;
		_list = list;
	}

	public DeclarationSpecifier getDeclarationSpecifier(){
		return _spec;
	}
	
	public LinkedList<Declarator> getList(){
		return _list;
	}
	
	@Override
	public String getName() {

		return "struct declaration";
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		String[] properties = new String[]{ "declared type", _spec.getTypeName(), 
											"no. of declarator", Integer.toString(_list.size())};
		traverseFormatterHead(writer, "Struct Declaration", properties);
		_spec.traverse(writer);
		if (_list != null)
			for (ListIterator<Declarator> iter = _list.listIterator(); iter.hasNext(); iter.next().traverse(writer));
		traverseFormatterTail(writer, "Struct Declaration", properties);
		return writer;
	}
	
	protected DeclarationSpecifier _spec;
	protected LinkedList<Declarator> _list;

}
