package node;

import java.io.IOException;
import java.io.Writer;


/**
 * The ArrayType node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 4, 2014
 */
public class ArrayType extends TypeModifier {
	/**
	 * Constructs an array type from assignment expression.
	 * Expr can be null when expression in N/A.
	 *	
	 * @param expr			the dimension expression
	 */
	public ArrayType(ExpressionBase expr) {
		super();
		_expr = expr;
	}

	public ExpressionBase getExpr(){
		return _expr;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
			// do nothing
		return writer;
	}

	@Override
	public String getName() {

		return "array";
	}
	
	protected ExpressionBase _expr;


}
