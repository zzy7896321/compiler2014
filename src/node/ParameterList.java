package node;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.ListIterator;

import util.StringUtil;

/**
 * The ParameterList node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class ParameterList extends NodeBase {

	/**
	 * Constructs an parameter list from a list of declarators and a boolean value indicating
	 * whether the parameter list has ellipsis.
	 */
	public ParameterList(LinkedList<Declarator> list, boolean withEllipsis) {
		_list = list;
		_varl = withEllipsis;
	}

	public boolean getEllipsis(){
		return _varl;
	}
	
	public LinkedList<Declarator> getList(){
		return _list;
	}
	
	static public ParameterList newEmptyParameterList(){
		return new ParameterList(null, false);
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		String[] properties = new String[]{"ellipsis", Boolean.toString(_varl)};
		traverseFormatterHead(writer, this.getName(), properties);
		if (_list == null){
			writer.append(StringUtil.repeat(_indent, "\t")).append("<empty parameter list>\n");
		}
		else {
			writer.append(StringUtil.repeat(_indent, "\t")).append("<parameter list>\n");
			for (ListIterator<Declarator> iter = _list.listIterator(); iter.hasNext(); iter.next().traverse(writer));
		}
		traverseFormatterTail(writer, this.getName(), properties);
		return writer;
	}

	@Override
	public String getName() {

		return "parameter list";
	}
	
	LinkedList<Declarator> _list;
	boolean _varl;
}
