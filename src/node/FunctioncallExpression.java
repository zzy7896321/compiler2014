package node;

import environment.Environment;
import environment.RuntimeEnvironment;
import environment.TypeFactory;
import irpack.BitField;
import irpack.IntConstant;
import irpack.Operand;
import irpack.TempSpace;
import irpack.op;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * The FunctionCallExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 10, 2014
 */
public class FunctioncallExpression extends BinaryExpression {

	/**
	 * @param expr1
	 * @param expr2
	 */
	public FunctioncallExpression(ExpressionBase expr1, ExpressionBase expr2) {
		super(expr1, expr2);

	}

	@Override
	public String getName() {

		return "function call expr1(expr2)";
	}

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;

		_left.checkEvaluate(cur_env, runtime);
		if (_left._type == null) {
			_type = null;
			return;
		}

		/* check if left is a function */
		environment.FunctionType func_type = null;
		if (environment.TypeFactory.isFunctionPointer(_left._type)){
			func_type = (environment.FunctionType)((environment.PointerType)_left._type).getBaseType();
		}
		else{
			report_error("called object is not a function or a function pointer", this);
			_type = null;
			return;
		}
		
		/* argument expression list missing is not likely */
		if (!(_right instanceof ArgumentExpressionList)){
			report_error("function call without argument expression list?", this);
			_type = null;
			return;
		}
		
		List<environment.Type> type_list = func_type.getParameterTypeList();
		List<ExpressionBase> arg_list = ((ArgumentExpressionList)_right).getList();
		
		/* check arguments */
		ListIterator<ExpressionBase> arg_iter = arg_list.listIterator();
		ListIterator<environment.Type> type_iter = type_list.listIterator();
		environment.Type req_type = null;
		
		for (;arg_iter.hasNext() && type_iter.hasNext();){
			ExpressionBase cur = arg_iter.next();
			cur.checkEvaluate(cur_env, runtime);
			if (cur._type == null){
				_type = null;
				return;
			}
			
			req_type = type_iter.next();
			if (TypeFactory.isIntegerType(req_type)){
				if (TypeFactory.isIntegerType(cur._type)){
					continue;
				}
				else if (cur._type instanceof environment.PointerType){
					report_implicit_conversion_warning(cur, cur._type, req_type);
					continue;
				}
				else {
					report_incompatible_conversion_error(cur, cur._type, req_type);
					_type = null;
					return;
				}
			}
			
			else if (req_type instanceof environment.PointerType){
				if (TypeFactory.isIntegerType(cur._type)){
					if (cur._value instanceof IntConstant && ((IntConstant)cur._value).getValue() == 0){
						continue;
					}
					else {
						report_implicit_conversion_warning(cur, cur._type, req_type);
						continue;
					}
				}
				else if (cur._type instanceof environment.PointerType){
					
					if (req_type.equals(cur._type) || ((environment.PointerType)cur._type).getBaseType() instanceof environment.VoidType){
						continue;
					}
					else {
						report_implicit_conversion_warning(cur, cur._type, req_type);
						continue;
					}
				}
				else {
					report_incompatible_conversion_error(cur, cur._type, req_type);				
					_type = null;
					return;
				}
			}
			else {
				if (req_type.equals(cur._type)){
					continue;
				}
				else {
					report_incompatible_conversion_error(cur, cur._type, req_type);
					_type = null;
					return;
				}
			}
		}
		
		if (type_iter.hasNext()){
			report_error("fewer arguments provided than required", _right);
			_type = null;
			return;
		}
		else if (arg_iter.hasNext()){
			if (func_type.isVarLength()){
				for (; arg_iter.hasNext(); ){
					ExpressionBase cur = arg_iter.next();
					cur.checkEvaluate(cur_env, runtime);
					if (cur._type == null){
						_type = null;
						return;
					}
				}
			}
			else {
				report_error("more arguments provided than required", _right);
				_type = null;
				return;
			}
		}
		
		/* set up argument counting */
		int arg_cnt = 0;					// current argument count
		int offset = 0;
		
		LinkedList<PushInfo> push_info = new LinkedList<PushInfo>();
		
		/* need temporary space for return value and return value address for struct/union return value */
		if (func_type.getReturnType() instanceof environment.StructOrUnionType){
			environment.Type return_type = func_type.getReturnType();
			
			
			TempSpace retSpace = op.tempSpace(return_type.getSize(), runtime.entry);
			Operand addr = op.la(op.temp(runtime.entry), retSpace, runtime.entry);
			push_info.addLast(new PushInfo(-1, offset, addr));
			offset += 1 << config.TargetConfig.register_shift_size;
		}
		
		/* push argument */
		arg_iter = arg_list.listIterator();
		for (; arg_iter.hasNext(); ){
			ExpressionBase operand = arg_iter.next();
			operand.checkEvaluate(cur_env, runtime);
			if (operand._type == null){
				_type = null;
				return;
			}
			
			Operand arg = null;
			if (operand._type instanceof environment.StructOrUnionType){
				TempSpace tempSpace = op.tempSpace(operand._type.getSize(), runtime.entry);
				Operand result = op.la(op.temp(runtime.entry), tempSpace, runtime.entry);
				op.memcpy(result, (Operand) operand._value, operand._type.getSize(), runtime.entry);
				arg = result;
			}
			else {
				arg = (Operand) operand._value;
			}
			
			/* -2 denotes variable length part */
			push_info.addLast(new PushInfo((arg_cnt >= type_list.size()) ? -2 : (arg_cnt++), offset, arg));
			offset += 1 << config.TargetConfig.register_shift_size;
		}
		
		runtime.entry.updateCallerArgSize(offset);
		
		/* load bit-fields in advance */
		for (PushInfo pinfo : push_info){
			if (pinfo.addr instanceof BitField){
				pinfo.addr = op.loadBitField((BitField) pinfo.addr, runtime.entry);
			}
		}
		
		/* do actual push */
		for (PushInfo pinfo : push_info){
			op.push(pinfo.argcnt, pinfo.offset, pinfo.addr, runtime.entry);
		}
		
		op.call((Operand)_left._value, runtime.entry);
		_value = op.movReturnValue(op.temp(runtime.entry), runtime.entry);
		
		_type = func_type.getReturnType();
		_lvalue = false;
		return;
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		_left.checkEvaluate_NoGenerating(cur_env);
		
		if (_left._type == null) {
			_type = null;
			return;
		}

		/* check if left is a function */
		environment.FunctionType func_type = null;
		if (environment.TypeFactory.isFunctionPointer(_left._type)){
			func_type = (environment.FunctionType)((environment.PointerType)_left._type).getBaseType();
		}
		else{
			report_error("called object is not a function or a function pointer", this);
			_type = null;
			return;
		}
		
		/* argument expression list missing is not likely */
		if (!(_right instanceof ArgumentExpressionList)){
			report_error("function call without argument expression list?", this);
			_type = null;
			return;
		}
		
		
		List<environment.Type> type_list = func_type.getParameterTypeList();
		List<ExpressionBase> arg_list = ((ArgumentExpressionList)_right).getList();
		ListIterator<ExpressionBase> arg_iter = arg_list.listIterator();
		ListIterator<environment.Type> type_iter = type_list.listIterator();
		environment.Type req_type = null;
		
		/* check arguments */
		for (;arg_iter.hasNext() && type_iter.hasNext();){
			ExpressionBase cur = arg_iter.next();
			cur.checkEvaluate_NoGenerating(cur_env);
			if (cur._type == null){
				_type = null;
				return;
			}
			
			req_type = type_iter.next();
			if (TypeFactory.isIntegerType(req_type)){
				if (TypeFactory.isIntegerType(cur._type)){
					continue;
				}
				else if (cur._type instanceof environment.PointerType){
					report_implicit_conversion_warning(cur, cur._type, req_type);
					continue;
				}
				else {
					report_incompatible_conversion_error(cur, cur._type, req_type);
					_type = null;
					return;
				}
			}
			
			else if (req_type instanceof environment.PointerType){
				if (TypeFactory.isIntegerType(cur._type)){
					if (cur._value instanceof Number && ((Integer)cur._value).intValue() == 0){
						continue;
					}
					else {
						report_implicit_conversion_warning(cur, cur._type, req_type);
						continue;
					}
				}
				else if (cur._type instanceof environment.PointerType){
					
					if (req_type.equals(cur._type) || ((environment.PointerType)cur._type).getBaseType() instanceof environment.VoidType){
						continue;
					}
					else {
						report_implicit_conversion_warning(cur, cur._type, req_type);
						continue;
					}
				}
				else {
					report_incompatible_conversion_error(cur, cur._type, req_type);				
					_type = null;
					return;
				}
			}
			else {
				if (req_type.equals(cur._type)){
					continue;
				}
				else {
					report_incompatible_conversion_error(cur, cur._type, req_type);
					_type = null;
					return;
				}
			}
		}
		
		if (type_iter.hasNext()){
			report_error("fewer arguments provided than required", _right);
			_type = null;
			return;
		}
		else if (arg_iter.hasNext()){
			if (func_type.isVarLength()){
				for (; arg_iter.hasNext(); ){
					ExpressionBase cur = arg_iter.next();
					cur.checkEvaluate_NoGenerating(cur_env);
					if (cur._type == null){
						_type = null;
						return;
					}
				}
			}
			else {
				report_error("more arguments provided than required", _right);
				_type = null;
				return;
			}
		}
		
		_type = func_type.getReturnType();
		_value = null;		// non-constant
		_lvalue = false;
		return;
	}

}

class PushInfo{
	public PushInfo(int argcnt, int offset, Operand addr){
		this.argcnt = argcnt;
		this.offset = offset;
		this.addr = addr;
	}
	
	public int argcnt;
	public int offset;
	public Operand addr;
}
