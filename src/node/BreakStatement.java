package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The BreakStatement node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 28, 2014
 */
public class BreakStatement extends Statement {

	/**
	 * Constructs a break statement
	 */
	public BreakStatement() {
	}


	@Override
	public Writer traverse(Writer writer) throws IOException {
		traverseFormatterHead(writer, this.getName(), null);
		
		traverseFormatterTail(writer, this.getName(), null);
		return writer;
	}

	@Override
	public String getName() {

		return "break statement";
	}

}
