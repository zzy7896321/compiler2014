package node;

import irpack.*;
import environment.Environment;
import environment.RuntimeEnvironment;

/**
 * The ArraysubscriptingExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 10, 2014
 */
public class ArraysubscriptingExpression extends BinaryExpression {

	/**
	 * @param expr1
	 * @param expr2
	 */
	public ArraysubscriptingExpression(ExpressionBase expr1,
			ExpressionBase expr2) {
		super(expr1, expr2);

	}

	@Override
	public String getName() {
		
		return "array subscripting expr1[expr2]";
	}

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
			

		_left.checkEvaluate(cur_env, runtime);
		if (_left._type == null){
			_type = null;
			return;
		}
		_right.checkEvaluate(cur_env, runtime);
		if (_right._type == null) {
			_type = null;
			return;
		}
		
		/* integer[pointer] */
		if (_right._type instanceof environment.PointerType && environment.TypeFactory.isIntegerType(_left._type)){
			//swap left and right
			ExpressionBase tmp = _left;
			_left = _right;
			_right = tmp;
		}
		
		/* do subscription */
		if (_left._type instanceof environment.PointerType && environment.TypeFactory.isIntegerType(_right._type)){
			_type = ((environment.PointerType)_left._type).getBaseType();
			if (_type instanceof environment.FunctionType){
				report_error("subscripting to pointer to function type", this);
				_type = null;
				return ;
			}
			if (!_type.isComplete()){
				report_error("subscripting to pointer to incompletet type", this);
				_type = null;
				return;
			}
			
			Operand result = null;
			int base_size = _type.getSize();
			if (base_size == 1){
				result = (Operand)_right._value;
			}
			if (Integer.highestOneBit(base_size) == base_size){
				result = op.sll(op.temp(runtime.entry), (Operand)_right._value, op.constant(Integer.numberOfTrailingZeros(base_size)), runtime.entry);
			}
			else {
				result = op.mul(op.temp(runtime.entry), (Operand)_right._value, op.constant(base_size), runtime.entry);
			}
			result = op.add(op.temp(runtime.entry), (Operand)_left._value, result, runtime.entry);
			
			if (_type instanceof environment.ArrayType){
				_lvalue = false;		
			}
			
			else if (_type instanceof environment.StructOrUnionType){
				_lvalue = true;
			}
			
			else {
				_lvalue = true;
				
				result = op.indirection(result, 0, _type.getSize(), runtime.entry);
			
			}
			_value = result;
			implicitConversion_NoGenerating(this);
			
			return ;
		}
		
		/* incompatible types */
		report_invalid_operand_error(this, "array subscription", _left._type, _right._type);
		_type = null;
		return;
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		_left.checkEvaluate_NoGenerating(cur_env);
		if (_left._type == null){
			_type = null;
			return;
		}
		_right.checkEvaluate_NoGenerating(cur_env);
		if (_right._type == null) {
			_type = null;
			return;
		}
		
		/* integer[pointer] */
		if (_right._type instanceof environment.PointerType && environment.TypeFactory.isIntegerType(_left._type)){
			//swap left and right
			ExpressionBase tmp = _left;
			_left = _right;
			_right = tmp;
		}
		
		/* do subscription */
		if (_left._type instanceof environment.PointerType && environment.TypeFactory.isIntegerType(_right._type)){
			_type = ((environment.PointerType)_left._type).getBaseType();
			if (_type instanceof environment.FunctionType){
				report_error("subscripting to pointer to function type", this);
				_type = null;
				return ;
			}
			if (!_type.isComplete()){
				report_error("subscripting to pointer to incompletet type", this);
				_type = null;
				return;
			}
			
			if (!_left._lvalue && _left._value instanceof environment.AddressConstant 
					&& _right._value instanceof Number){
				_value = ((environment.AddressConstant)_left._value).newAddrWithOffsetAddedTo((int)_right._value * _type.getSize());
			}
			else {
				_value = null;
			}

			//_lvalue = !((_type instanceof environment.ArrayType) || (_type instanceof environment.FunctionType));
			_lvalue = true;
			implicitConversion_NoGenerating(this);
			return ;
		}
		
		/* incompatible types */
		report_invalid_operand_error(this, "array subscription", _left._type, _right._type);
		_type = null;
		return;
	}

}
