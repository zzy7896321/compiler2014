package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The UnaryExpression node base class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public abstract class UnaryExpression extends ExpressionBase {

	/**
	 * Contructs unary expression from a signle expression
	 * 
	 * @param expr1		the oprand
	 */
	public UnaryExpression(ExpressionBase expr1) {
		super();
		_operand = expr1;
	}
	
	/**
	 * Accesses the oprand.
	 * 
	 * @return	the oprand
	 */
	public ExpressionBase getOprand(){
		return _operand;
	}

	@Override
	public void clearEvaluatedState(){
		_evaluated = false;
		_type = null;
		_value = null;
		_lvalue = false;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException{
		traverseFormatterHead(writer, this.getName(), null);
		_operand.traverse(writer);
		traverseFormatterTail(writer, this.getName(), null);
		return writer;
	}
	
	protected ExpressionBase _operand;
}
