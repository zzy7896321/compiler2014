package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The identifier node class.
 * 
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class Identifier extends NodeBase {

	/**
	 * Constructs an identifier node.
	 * 
	 * @param val		the identifier name
	 */
	public Identifier(String val){
		_val = val;
	}
	
	/**
	 * Accesses the identifier name.
	 * 
	 * @return		the identifier name
	 */
	public String getVal(){
		return _val;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		String[] properties;
		properties = new String[]{"value", getVal()};
		traverseFormatterHead(writer, name, properties);
		traverseFormatterTail(writer, name, properties);
		return writer;
	}

	String _val;
	
	static final String name = "identifier";
	
	@Override
	public String getName() {
		
		return name;
	}
}
