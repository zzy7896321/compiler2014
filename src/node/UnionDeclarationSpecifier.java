package node;


/**
 * The UnionDeclarationSpecifier node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class UnionDeclarationSpecifier extends StructUnionDeclarationSpecifier {

	/**
	 * Constructs an union declaration specifier.
	 */
	public UnionDeclarationSpecifier() {
		super();
		_val = UNION;
	}
	
	public UnionDeclarationSpecifier(UnionDeclarationSpecifier rhs){
		super(rhs);
	}
	
	@Override
	public String getName(){
		return "union";
	}
	
	@Override
	public String getTypeName(){
		return (_id != null) ? ("union " + _id.getVal()) : ("union");
	}
	
}
