package node;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.LinkedList;
import java.util.ListIterator;

import environment.Environment;
import environment.RuntimeEnvironment;

/**
 * The comma expression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class CommaExpression extends ExpressionBase {

	/**
	 * Constructs an empty comma expression.
	 * Since empty comma expression is usually useless,
	 * the default constructor is kept for private use.
	 * 
	 */
	private CommaExpression(){
		super();
		_list = new LinkedList<ExpressionBase>();
	}
	
	/**
	 * Constructs a comma expression from a single expression.
	 * 
	 * @param expr1		an assignment expression
	 * @param expr2		a second expression
	 */
	public CommaExpression(ExpressionBase expr1, ExpressionBase expr2) {
		this();
		_list.add(expr1);
		_list.add(expr2);
	}
	
	/**
	 * Appends the expression to the end of the comma expression.
	 * 
	 * @param expr		the expression to append
	 */
	public void append(ExpressionBase expr){
		_list.add(expr);
	}
	
	/**
	 * Accesses the list.
	 * 
	 * @return	the list
	 */
	public List<ExpressionBase> getList(){
		return _list;
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		traverseFormatterHead(writer, this.getName(), null);
		
		for (ListIterator<ExpressionBase> iter = _list.listIterator(); iter.hasNext(); iter.next().traverse(writer));
		
		traverseFormatterTail(writer, this.getName() ,null);
		return writer;
	}

	List<ExpressionBase> _list;

	@Override
	public String getName() {
		return "comma expression";
	}

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
			

		ExpressionBase cur_expr = null;
		for (ListIterator<ExpressionBase> iter = _list.listIterator(); iter.hasNext();){
			cur_expr = iter.next();
			cur_expr.checkEvaluate(cur_env, runtime);
			if (cur_expr._type == null){
				_type = null;
				return;
			}
		}
		_type = cur_expr._type;
		_value = cur_expr._value;
		_lvalue = false;
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		ExpressionBase cur_expr = null;
		for (ListIterator<ExpressionBase> iter = _list.listIterator(); iter.hasNext();){
			cur_expr = iter.next();
			cur_expr.checkEvaluate_NoGenerating(cur_env);
			if (cur_expr._type == null){
				_type = null;
				return;
			}
		}
		_type = cur_expr._type;
		_value = null;	// non-constant
		_lvalue = false;
	}

	@Override
	public void clearEvaluatedState() {
		
		_evaluated = false;
		_type = null;
		_value = null;
		_lvalue = false;
		
		for (ExpressionBase expr : _list){
			expr.clearEvaluatedState();
		}
	}
}
