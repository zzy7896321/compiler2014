package node;

import java.io.*;
import util.StringUtil;

/**
 * The base class of abstract syntax tree nodes.
 * 
 * @author Zhao Zhuoyue
 * @version last update: Mar 31, 2014
 * 
 */
public abstract class NodeBase{
	
	public NodeBase(){
		_line = _lexer.get_yyline();
		_column = _lexer.get_yycolumn();
	}
	
	/**
	 * Converts the node into a string representation (AST).
	 * 
	 * @return	the AST of the node
	 */
	public String toString(){
		try{
			return this.traverse(new StringWriter()).toString();
		}
		catch (IOException e){
			e.printStackTrace();
			return "<Error: IOException>";
		}
	}
	
	/**
	 * Traverses the AST rooted at current node. 
	 * Output is written to the given writer.
	 * 
	 * @param writer	the destination where output is written to
	 * @return			the writer
	 */
	public abstract Writer traverse(Writer writer) throws IOException;
	
	/**
	 * Accesses the name of the node.
	 * 
	 * @return		the name of the node
	 */
	public abstract String getName();
	
	/**
	 * Formats the traversing output before the child nodes 
	 * and writes it to the writer.
	 * Caller should provide name and value of property in pairs.
	 * They should be stored in properties array consecutively.
	 * 
	 * 
	 * @param writer		the destination where output is written to
	 * @param nodeName  	the name of the caller
	 * @param properties	the name and value of properties
	 * @return				the writer
	 * @throws IOException
	 */
	protected static Writer traverseFormatterHead(Writer writer, String nodeName, String[] properties) throws IOException{
		
		
		writer.append(StringUtil.repeat(_indent++, "\t"));
		
		
		writer.append('(').append(nodeName);
		
		if (properties != null){
			for (int i = 0; i + 1 < properties.length; i += 2){
				writer.append(" [").append(properties[i]).append(" = ").append(properties[i+1]).append(']');
			}
		}
		
		writer.append(")\n");
		return writer;
	}
	
	/**
	 * Formats the traversing output after the child nodes 
	 * and writes it to the writer.
	 * Caller should provide name and value of property in pairs.
	 * They should be stored in properties array consecutively.
	 * 
	 * @param writer		the destination where output is written to
	 * @param nodeName  	the name of the caller
	 * @param properties	the name and value of properties
	 * @return				the writer
	 * @throws IOException
	 */
	protected static Writer traverseFormatterTail(Writer writer, String nodeName, String[] properties) throws IOException{
		--_indent;
		return writer;
	}
	
	/**
	 * Returns current indent level for traversing.
	 *  
	 */
	public static int getIndent(){
		return _indent;		
	}
	
	/**
	 * Sets current indent level for traversing.
	 * 
	 * @param indent		the indent to set
	 */
	public static void setIndent(int indent){
		_indent = indent;
	}
	
	/**
	 * Resets the indent of traversing output.
	 */
	public static void resetIndent(){
		_indent = 0;
	}
	
	static protected int _indent = 0;
	
	static public void setLexer(parser.Lexer lexer){
		_lexer = lexer;
	}
	
	static private parser.Lexer _lexer;
	
	public int _line, _column;
}