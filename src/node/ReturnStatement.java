package node;

import java.io.IOException;
import java.io.Writer;
import util.StringUtil;

/**
 * The ReturnStatement node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 28, 2014
 */
public class ReturnStatement extends Statement {

	/**
	 * Constructs a return statement with no expression specified.
	 */
	public ReturnStatement() {
		_expr = null;
	}

	/**
	 * Constructs a return statement with an expression to return.
	 * 
	 * @param expr		the expression to return
	 */
	public ReturnStatement(ExpressionBase expr) {
		_expr = expr;

	}
	
	public ExpressionBase getExpr(){
		return _expr;
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		traverseFormatterHead(writer, this.getName(), null);
			
		if (_expr == null){
			writer.append(StringUtil.repeat(_indent, "\t")).append("<no value to return>\n");
		}
		else{
			_expr.traverse(writer);
		}
		
		traverseFormatterTail(writer, this.getName(), null);
		return writer;
	}

	@Override
	public String getName() {

		return "return statement";
	}
	
	protected ExpressionBase _expr;

}
