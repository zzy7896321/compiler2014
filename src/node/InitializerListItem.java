package node;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * The InitializerListItem node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class InitializerListItem extends NodeBase {

	/**
	 * Constructs an initializer list item from a list of designators an an initializer
	 */
	public InitializerListItem(LinkedList<Designator> dest, Initializer init) {
		super();
		_dest = dest;
		_init = init;
	}

	/**
	 * Accesses the designator list.
	 * @return	the designator list
	 */
	public LinkedList<Designator> getDesignatorList(){
		return _dest;
	}
	
	/**
	 * Accesses the initializer.
	 * 
	 * @return	the initializer
	 */
	public Initializer getInitializer(){
		return _init;
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		String[] properties = new String[]{"destination", "", "init", _init.getInitName()};
		if (_dest != null){
			for (ListIterator<Designator> iter = _dest.listIterator(); iter.hasNext(); ){
				Designator designator = iter.next();
				properties[1] += designator.getDestLiteral();
			}
				traverseFormatterHead(writer, "initializer item", properties);
				traverseFormatterTail(writer, "initializer item", properties);
			
		}
		else{
			properties[1] = "<unnamed>";
			traverseFormatterHead(writer, "initializer item", properties);
			traverseFormatterTail(writer, "initializer item", properties);
		}
		_indent++;
		_init.traverse(writer);
		_indent--;
		return writer;
	}

	@Override
	public String getName() {

		return "initializerlist item";
	}

	protected LinkedList<Designator> _dest;
	protected Initializer _init;
}
