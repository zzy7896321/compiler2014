package node;

import java.io.IOException;
import java.io.Writer;

import util.StringUtil;

/**
 * The WhileStatement node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class WhileStatement extends Statement {

	/**
	 * Constructs a while statement from a controlling expression and a looping body.
	 * 
	 * @param expr		the controlling expression
	 * @param body		the looping body
	 */
	public WhileStatement(ExpressionBase expr, Statement body) {
		_expr = expr;
		_body = body;
	}

	public ExpressionBase getControllingExpression(){
		return _expr;
	}
	
	public Statement getStatement(){
		return _body;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		traverseFormatterHead(writer, this.getName(), null);
		
		writer.append(StringUtil.repeat(_indent, "\t")).append("<controlling expression>\n");
			_expr.traverse(writer);
		writer.append(StringUtil.repeat(_indent, "\t")).append("<looping body>\n");
			_body.traverse(writer);
		traverseFormatterTail(writer, this.getName(), null);
		return writer;
	}

	@Override
	public String getName() {

		return "while statement";
	}
	
	protected ExpressionBase _expr;
	protected Statement _body;

}
