package node;


/**
 * The DerivedType node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public abstract class TypeNode extends NodeBase {

	/**
	 * Default constructor of Type;
	 */
	public TypeNode() {
		super();
	}

	public abstract String getTypeName();
	
}
