package node;

import java.io.Writer;
import java.io.IOException;

/**
 * The TypedefNameDeclarationSpecifier node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 1, 2014
 */
public class TypedefNameDeclarationSpecifier extends DeclarationSpecifier {

	/**
	 * Constructs a typedef name declaration specifier from an identifier.
	 */
	public TypedefNameDeclarationSpecifier(Identifier id) {
		super();
		_val = TYPEDEF_NAME;
		_id = id;
	}
	
	public TypedefNameDeclarationSpecifier(TypedefNameDeclarationSpecifier rhs){
		super(rhs);
		_id = rhs._id;
	}
	
	public void setId(Identifier id){
		_id = id;
	}
	
	public Identifier getId(){
		return _id;
	}

	@Override
	public Writer traverse(Writer writer) throws IOException{
		// Do nothing
		return writer;
	}
	
	@Override
	public String getName(){
		return "typedef name declaration specifier";
	}
	
	@Override
	public String getTypeName(){
		return _id.getVal();
	}
	
	
	protected Identifier _id;

}
