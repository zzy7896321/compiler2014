package node;

import irpack.Operand;
import irpack.op;
import environment.Environment;
import environment.RuntimeEnvironment;

/**
 * The BitwisenotExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class BitwisenotExpression extends UnaryExpression {

	/**
	 * @param expr1
	 */
	public BitwisenotExpression(ExpressionBase expr1) {
		super(expr1);

	}

	@Override
	public String getName() {

		return "~";
	}

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
			

		_operand.checkEvaluate(cur_env, runtime);
		if (_operand._type == null){
			_type = null;
			return;
		}
		
		
		if (!environment.TypeFactory.isIntegerType(_operand._type)){
			report_error(String.format("operand of ~ shall have integer type, but %s found",
					semantics.ResourcePrinter.getTypeString(_operand._type)), _operand);
			_type = null;
			return;
		}
		
		_type = _operand._type;
		_lvalue = false;
		
		_value = op.bitwiseNot(op.temp(runtime.entry), (Operand) _operand._value, runtime.entry);
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		_operand.checkEvaluate_NoGenerating(cur_env);
		if (_operand._type == null){
			_type = null;
			return;
		}
		
		
		if (!environment.TypeFactory.isIntegerType(_operand._type)){
			report_error(String.format("operand of ~ shall have integer type, but %s found",
					semantics.ResourcePrinter.getTypeString(_operand._type)), _operand);
			_type = null;
			return;
		}
		
		_type = _operand._type;
		if (_operand._value instanceof Number){
			if (_operand._value instanceof Byte){
				_value = ~((Byte)_operand._value).byteValue();
			}
			else {
				_value = ~((Integer)_operand._value).intValue();
			}
		}
		else {
			_value = null;
		}
		_lvalue = false;
		
	}

}
