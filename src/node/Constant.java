package node;

/**
 * The Constant node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public abstract class Constant extends NodeBase{

	public Constant() {
		super();
	}
	
	/**
	 * Returns the string representation of the constant.
	 * @return 		the string representation of the constant
	 */
	public abstract String getValLiteral();

}
