package node;


/**
 * The StructDeclarationSpecifier node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class StructDeclarationSpecifier extends StructUnionDeclarationSpecifier {

	/**
	 * 
	 */
	public StructDeclarationSpecifier() {
		super();
		_val = STRUCT;
	}
	
	public StructDeclarationSpecifier(StructDeclarationSpecifier rhs){
		super(rhs);
	}

	@Override
	public String getName(){
		return "struct";
	}
	
	@Override
	public String getTypeName() {

		return (_id != null) ? ("struct " + _id.getVal()) : ("struct");
	}

}
