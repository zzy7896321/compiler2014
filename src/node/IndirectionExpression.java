                 package node;

import irpack.Indirection;
import irpack.Operand;
import irpack.op;
import environment.Environment;
import environment.RuntimeEnvironment;

/**
 * The IndirectionExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class IndirectionExpression extends UnaryExpression {

	/**
	 * @param expr1
	 */
	public IndirectionExpression(ExpressionBase expr1) {
		super(expr1);

	}

	@Override
	public String getName() {

		return "indirection *";
	}

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
			
		_operand.checkEvaluate(cur_env, runtime);
		
		if (_operand._type == null){
			_type = null;
			return;
		}
		
		if (!(_operand._type instanceof environment.PointerType)){
			report_error(String.format("indirection operation requires a pointer type operand, but %s found",
					semantics.ResourcePrinter.getTypeString(_operand._type)), _operand);
			_type = null;
			return;
		}
		
		_type = ((environment.PointerType)_operand._type).getBaseType();
		if (_type instanceof environment.FunctionType){
			_type = _operand._type;
			_lvalue = false;
			
			Operand result = (Operand)_operand._value;	// function indirection
			
			_value = result;
			
			return;
		}
		else if (_type instanceof environment.ArrayType){
			_type = new environment.PointerType(((environment.ArrayType) _type).getBaseType());
			_lvalue = false;
			
			Operand result = (Operand)_operand._value;
			
			_value = result;
			
			return;
		}
		else if (_type instanceof environment.StructOrUnionType){
			_lvalue = true;
			
			_value = _operand._value;
			
			return;
		}
		else {
		
			_lvalue = true;
			
			Operand result = (Operand)_operand._value;
			if (result instanceof Indirection){
				result = op.mov(op.temp(runtime.entry), result, runtime.entry);
			}
			result = op.indirection(result, 0, _type.getSize(), runtime.entry);
			
			_value = result;
			
			return;
		}
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
		
		_operand.checkEvaluate_NoGenerating(cur_env);
		
		if (_operand._type == null){
			_type = null;
			return;
		}
		
		if (!(_operand._type instanceof environment.PointerType)){
			report_error(String.format("indirection operation requires a pointer type operand, but %s found",
					semantics.ResourcePrinter.getTypeString(_operand._type)), _operand);
			_type = null;
			return;
		}
		
		_type = ((environment.PointerType)_operand._type).getBaseType();
		if (_type instanceof environment.FunctionType){
			_type = _operand._type;
			_value = (!_operand._lvalue && _operand._value instanceof environment.AddressConstant) ?
					((environment.AddressConstant)_operand._value).newAddrWithOffsetAddedTo(0) :
					null;
			_lvalue = false;
			return;
		}
		else {
			_value = (!_operand._lvalue && _operand._value instanceof environment.AddressConstant) ?
					((environment.AddressConstant)_operand._value).newAddrWithOffsetAddedTo(0) :
					null;
			_lvalue = !(_type instanceof environment.ArrayType);
			implicitConversion_NoGenerating(this);
			return;
		}
	}

}
