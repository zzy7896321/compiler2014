package node;

import irpack.Operand;
import irpack.op;
import environment.Environment;
import environment.RuntimeEnvironment;


/**
 * The BitwiseandExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class BitwiseandExpression extends BinaryExpression {

	public BitwiseandExpression(ExpressionBase expr1, ExpressionBase expr2) {
		super(expr1, expr2);
	}

	@Override
	public String getName() {
		return "&";
	}

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
			

		_left.checkEvaluate(cur_env, runtime);
		if (_left._type == null){
			_type = null;
			return;
		}
		_right.checkEvaluate(cur_env, runtime);
		if (_right._type == null){
			_type = null;
			return;
		}
		
		if (!(environment.TypeFactory.isIntegerType(_left._type) && environment.TypeFactory.isIntegerType(_right._type))){
			report_invalid_operand_error(this, "&", _left._type, _right._type);
			_type = null;
			return;
		}
		
		PromoteIntegerType(_left, _right);
		
		_type = _left._type;
		_lvalue = false;
		
		_value = op.and(op.temp(runtime.entry), (Operand) _left._value, (Operand) _right._value, runtime.entry);
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		_left.checkEvaluate_NoGenerating(cur_env);
		if (_left._type == null){
			_type = null;
			return;
		}
		_right.checkEvaluate_NoGenerating(cur_env);
		if (_right._type == null){
			_type = null;
			return;
		}
		
		if (!(environment.TypeFactory.isIntegerType(_left._type) && environment.TypeFactory.isIntegerType(_right._type))){
			report_invalid_operand_error(this, "&", _left._type, _right._type);
			_type = null;
			return;
		}
		
		PromoteIntegerType(_left, _right);
		
		_type = _left._type;
		if (_left._value instanceof Number && _right._value instanceof Number){
			
			
			_value = ((Number)_left._value).intValue() & ((Number)_right._value).intValue();
			
		}
		else {
			_value = null;
		}
		_lvalue = false;
	}
	
}
