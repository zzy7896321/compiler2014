package node;

import irpack.op;

import java.io.IOException;
import java.io.Writer;

import environment.Environment;
import environment.RuntimeEnvironment;
import environment.Type;

/**
 * The SizeofTypeExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class SizeofTypeExpression extends ExpressionBase {

	/**
	 * Constructs a sizeof expression from a Type.
	 * 
	 * @param type 		a Type node
	 */
	public SizeofTypeExpression(TypeNode type) {
		_type_node = type;
	}
	
	/**
	 * Accesses the type whose size is to be calculated.
	 * 
	 * @return	the type
	 */
	public TypeNode getTypeNode(){
		return _type_node;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		String[] properties = new String[]{"type", _type_node.getTypeName()};
		traverseFormatterHead(writer, this.getName(), properties);
		traverseFormatterTail(writer, this.getName(), properties);
		return writer;
	}

	@Override
	public String getName() {

		return "sizeof";
	}
	
	protected TypeNode _type_node;

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
			
		semantics.SemanticCheck semanticCheck = semantics.SemanticCheck.getRunningInstance();
		
		DeclarationSpecifier spec = (_type_node instanceof DerivedTypeList) ?
						(((DerivedTypeList)_type_node).getDeclarationSpecifier()) :
						(DeclarationSpecifier)_type_node;
		if (spec.isInvalid()){
			report_error("invalid declaration specifier", spec);
			_type = null; return;
		}
		
		if (!spec.typeSpecified()){
			report_error("type not specified in sizeof operator", spec);
			_type = null;
			return;
		
		}
		
		Type spec_type = semanticCheck.checkDeclarationSpecifier(spec);
		if (spec_type == null){
			_type = null;
			return;
		}
		
		environment.Type composedType = semanticCheck.composeType(spec_type, _type_node);
		if (composedType == null){
			_type = null;
			return;
		}
		
		if (!composedType.isComplete()){
			report_error("sizeof shall not have incomplete type operand", _type_node);
			_type = null;
			return;
		}
		
		_type = environment.TypeFactory.newIntType();
		_value = op.constant(composedType.getSize());
		_lvalue = false;
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		semantics.SemanticCheck semanticCheck = semantics.SemanticCheck.getRunningInstance();
		
		DeclarationSpecifier spec = (_type_node instanceof DerivedTypeList) ?
						(((DerivedTypeList)_type_node).getDeclarationSpecifier()) :
						(DeclarationSpecifier)_type_node;
		if (spec.isInvalid()){
			report_error("invalid declaration specifier", spec);
			_type = null; return;
		}
		
		if (!spec.typeSpecified()){
			report_error("type not specified in sizeof operator", spec);
			_type = null;
			return;
		
		}
		
		Type spec_type = semanticCheck.checkDeclarationSpecifier(spec);
		if (spec_type == null){
			_type = null;
			return;
		}
		
		environment.Type composedType = semanticCheck.composeType(spec_type, _type_node);
		if (composedType == null){
			_type = null;
			return;
		}
		
		if (!composedType.isComplete()){
			report_error("sizeof shall not have incomplete type operand", _type_node);
			_type = null;
			return;
		}
		
		_type = environment.TypeFactory.newIntType();
		_value = composedType.getSize();
		_lvalue = false;
	}

	@Override
	public void clearEvaluatedState() {
		_evaluated = false;
		_type = null;
		_value = null;
		_lvalue = false;
	}

}
