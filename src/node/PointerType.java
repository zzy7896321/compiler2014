package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The PointerType node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 4, 2014
 */
public class PointerType extends TypeModifier {

	/**
	 * Constructs a pointer type.
	 * 
	 * @param type				the type from which pointer derives
	 * @param qualifier			the type qualifier
	 */
	public PointerType(DeclarationSpecifier qualifier) {
		super();
		_qualifier = qualifier;
		
	}

	public DeclarationSpecifier getQualifier(){
		return _qualifier;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {

		return writer;
	}

	@Override
	public String getName() {

		return "pointer";
	}
	
	protected DeclarationSpecifier _qualifier;

}
