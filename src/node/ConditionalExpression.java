package node;

import irpack.Label;
import irpack.Operand;
import irpack.op;

import java.io.IOException;
import java.io.Writer;

import environment.Environment;
import environment.RuntimeEnvironment;
import environment.TypeFactory;

/**
 * The ConditionalExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class ConditionalExpression extends ExpressionBase {

	/**
	 * Constructs a conditional expression from three expressions.
	 * 
	 * @param expr1		the condition expression
	 * @param expr2		the true expression
	 * @param expr3		the false expression
	 */
	public ConditionalExpression(ExpressionBase expr1, ExpressionBase expr2, ExpressionBase expr3) {
		_cond = expr1;
		_true = expr2;
		_false = expr3;
	}
	
	/**
	 * Access the condition expression
	 * @return	the condition expression
	 */
	public ExpressionBase getCond(){
		return _cond;
	}
	
	/**
	 * Access the true branch expression
	 * @return	the true branch expression
	 */
	public ExpressionBase getTrue(){
		return _true;
	}
	
	/**
	 * Access the false branch expression
	 * @return	the false branch expression
	 */
	public ExpressionBase getFalse(){
		return _false;
	}

	@Override
	public String getName() {
		
		return "conditinal expression (expr1 ? expr2 : expr3)";
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		traverseFormatterHead(writer, this.getName(), null);
		_cond.traverse(writer);
		_true.traverse(writer);
		_false.traverse(writer);
		traverseFormatterTail(writer, this.getName(), null);
		return writer;
	}

	ExpressionBase _cond, _true, _false;

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
		
		boolean res = true;
		
		Label L1 = runtime.entry.newLabel();
		Label L2 = runtime.entry.newLabel();
		Operand result = op.local(4);	// note: two definitions to the same temporary value
		
		if (_cond instanceof ShortCircuitExpression){
			
			runtime.sc_target_set = true;
			runtime.true_target = null;		// fall
			runtime.false_target = L1;
		
			_cond.checkEvaluate(cur_env, runtime);
			res &= _cond._type != null;
			
			runtime.sc_target_set = false;
		}
		else {
			
			_cond.checkEvaluate(cur_env, runtime);
			res &= _cond._type != null;
			
			if (!environment.TypeFactory.isScalarType(_cond._type)){
				report_error("condition shall have scalar type", _cond);
				res = false;
			}
			
			op.beq((Operand) _cond._value, op.constant(0), L1, runtime.entry);
		}
		
		_true.checkEvaluate(cur_env, runtime);
		if (_true._type == null){
			res = false;
		}
		
		result = op.mov(result, (Operand) _true._value, runtime.entry);
		op.j(L2, runtime.entry);
		runtime.entry.appendCode(L1);
		
		_false.checkEvaluate(cur_env, runtime);
		if (_false._type == null){
			res = false;
		}
		
		result = op.mov(result, (Operand) _false._value, runtime.entry);
		runtime.entry.appendCode(L2);
		
		if (res){
			if (TypeFactory.isIntegerType(_true._type) && TypeFactory.isIntegerType(_false._type)){
				PromoteIntegerType(_true, _false);
				_type = _true._type;
			}
			else if (_true._type.equals(_false._type)){
				_type = _true._type;
			}
			else if (TypeFactory.isScalarType(_true._type) && TypeFactory.isScalarType(_false._type)){
				if (_true._type instanceof environment.PointerType && environment.TypeFactory.isVoidPointer(_false._type)){
					_type = _false._type;
				}
				else if (_true._type instanceof environment.PointerType && environment.TypeFactory.isNullPointerConstant(_false._type, _false._value)){
					_type = _true._type;
					_false._type = _true._type;
					_false._value = new environment.AddressConstant(null, 0);
				}
				else if (_false._type instanceof environment.PointerType && environment.TypeFactory.isVoidPointer(_true._type)){
					_type = _true._type;
				}
				else if (_false._type instanceof environment.PointerType && environment.TypeFactory.isNullPointerConstant(_true._type, _true._value)){
					_type = _false._type;
					_true._type = _false._type;
					_true._value = new environment.AddressConstant(null, 0);
				}	
				else {
					report_implicit_conversion_warning(this, _true._type, _false._type);
					res = false;
				}
			}
			else{
				report_invalid_operand_error(this, "conditional expression", _true._type, _false._type);
				res = false;
			}
		}
		
		if (res){
			_lvalue = false;
			_value = result;	
		}
		
		else {
			_type = null;
		}
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		_cond.checkEvaluate_NoGenerating(cur_env);
		if (_cond._type == null){
			_type = null;
			return;
		}
		_true.checkEvaluate_NoGenerating(cur_env);
		if (_true._type == null){
			_type = null;
			return;
		}
		_false.checkEvaluate_NoGenerating(cur_env);
		if (_false._type == null){
			_type = null;
			return;
		}
		
		if (!environment.TypeFactory.isScalarType(_cond._type)){
			report_error("condition shall have scalar type", _cond);
			_type = null;
			return;
		}
		
		if (TypeFactory.isIntegerType(_true._type) && TypeFactory.isIntegerType(_false._type)){
			PromoteIntegerType(_true, _false);
			_type = _true._type;
		}
		else if (_true._type.equals(_false._type)){
			_type = _true._type;
		}
		else if (TypeFactory.isScalarType(_true._type) && TypeFactory.isScalarType(_false._type)){
			if (_true._type instanceof environment.PointerType && environment.TypeFactory.isVoidPointer(_false._type)){
				_type = _false._type;
			}
			else if (_true._type instanceof environment.PointerType && environment.TypeFactory.isNullPointerConstant(_false._type, _false._value)){
				_type = _true._type;
				_false._type = _true._type;
				_false._value = new environment.AddressConstant(null, 0);
			}
			else if (_false._type instanceof environment.PointerType && environment.TypeFactory.isVoidPointer(_true._type)){
				_type = _true._type;
			}
			else if (_false._type instanceof environment.PointerType && environment.TypeFactory.isNullPointerConstant(_true._type, _true._value)){
				_type = _false._type;
				_true._type = _false._type;
				_true._value = new environment.AddressConstant(null, 0);
			}	
			else {
				report_implicit_conversion_warning(this, _true._type, _false._type);
				_type = null;
				return;
			}
		}
		else{
			report_invalid_operand_error(this, "conditional expression", _true._type, _false._type);
			_type = null;
			return;
		}
		
		_lvalue = false;
		if (TypeFactory.isIntegerType(_cond._type)){
			if (_cond._value instanceof Number){
				if (((Number)_cond._value).intValue() == 0){
					if (!_false._lvalue){
						_value = _false._value;
					}
					else {
						_value = null;
					}
				}
				else {
					if (!_true._lvalue){
						_value = _true._value;
					}
					else {
						_value = null;
					}
				}
				return;
			}
			else {
				_value = null;
				return;
			}
		}
		else if (_cond._type instanceof environment.PointerType){
			if (!_cond._lvalue && _cond._value instanceof environment.AddressConstant){
				environment.AddressConstant addr = (environment.AddressConstant)_cond._value;
				if (addr.getBase() == null){
					if (addr.getOffset() == 0){
						if (!_false._lvalue){
							_value = _false._value;
						}
						else {
							_value = null;
						}
					}
					else {
						if (!_true._lvalue){
							_value = _true._value;
						}
						else {
							_value = null;
						}
					}
					return;
				}
				else {
					if (addr.getOffset() == 0){
						if (!_true._lvalue){
							_value = _true._value;
						}
						else {
							_value = null;
						}
					}
					else {
						_value = null;
					}
					return ;
				}
			}
			else {
				_value = null;
				return;
			}
		}
	}

	@Override
	public void clearEvaluatedState() {
		_evaluated = false;
		_type = null;
		_value = null;
		_lvalue = false;
		
		_cond.clearEvaluatedState();
		_true.clearEvaluatedState();
		_false.clearEvaluatedState();
	}
}
