package node;

import environment.RuntimeEnvironment;
import main.ErrorHandling;


/**
 * 
 * The expression base node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 21, 2014
 */
public abstract class ExpressionBase extends NodeBase {
	
	/**
	 * Default constructor. Reserved for future use.
	 */
	protected ExpressionBase() {
		super();
	}
	
	
	// following are evaluation related members
	
	/**
	 * Check and generate code for the expression.
	 * 
	 * @param runtime
	 */
	abstract public void checkEvaluate(environment.Environment cur_env, RuntimeEnvironment runtime);
	
	/**
	 * Check the expression without generating code.
	 * 
	 */
	abstract public void checkEvaluate_NoGenerating(environment.Environment cur_env);
	
	public void setType(environment.Type type){
		_type = type;
	}
	
	public environment.Type getType(){
		return _type;
	}
	
	public void setValue(Object value){
		_value = value;
	}
	
	public Object getValue(){
		return _value;
	}
	
	public boolean isLvalue(){
		return _lvalue;
	}
	
	public void setLvalue(boolean lvalue){
		_lvalue = lvalue;
	}
	
	protected environment.Type _type;
	protected Object _value;
	protected boolean _lvalue;
	protected boolean _evaluated;
	
	/**
	 * Clears the evaluated state recursively.
	 * 
	 */
	public abstract void clearEvaluatedState();
	
	/**
	 * During expression evaluation, following conversion applies:<p>
	 * 1. Array to T is converted to pointer to T<p>
	 * 2. Function returning T is converted to pointer to function returning T
	 * 
	 * @param node
	 */
	protected static void implicitConversion_NoGenerating(ExpressionBase node){
		if (node._type instanceof environment.ArrayType){
			node._type = new environment.PointerType(((environment.ArrayType)node._type).getBaseType());
			node._lvalue = false;
		}
		else if (node._type instanceof environment.FunctionType){
			node._type = new environment.PointerType(node._type);
			node._lvalue = false;
		}
	}
	
//	/**
//	 * During expression evaluation, following conversion applies:<p>
//	 * 1. Array to T is converted to pointer to T<p>
//	 * 2. Function returning T is converted to pointer to function returning T
//	 * 
//	 * @param node		a node that contains an AddressConstant value, which will be converted to Address value
//	 */
//	protected static void implicitConversion(ExpressionBase node, RuntimeEnvironment runtime){
//		environment.AddressConstant addr_const = (environment.AddressConstant)node._value;
//		
//		if (node._type instanceof environment.ArrayType){
//			node._type = new environment.PointerType(((environment.ArrayType)node._type).getBaseType());
//				
//			node._value = new Address(QuadrupleFactory.createTemp(), 0);
//			Address src = new Address(((environment.VariableSymbol)addr_const.getBase()).getMemLoc(), 0);
//			runtime.entry.appendCode(QuadrupleFactory.createLA(node._value, src));	// la value, src	
//			
//			/* debug assert */
//			assert(addr_const.getOffset() == 0);
//			
//			node._lvalue = false;
//		}
//		
//		else if (node._type instanceof environment.FunctionType){
//			node._type = new environment.PointerType(node._type);
//			
//			node._value = new Address(QuadrupleFactory.createTemp(), 0);
//			Label src = SemanticCheck.getRunningInstance().getIR().getFunctionLabel(((environment.FunctionDesignator)addr_const.getBase()).getName());
//			runtime.entry.appendCode(QuadrupleFactory.createLA(node._value, src));	// la value, label
//			
//			/* debug assert */
//			assert(addr_const.getOffset() == 0);
//			
//			node._lvalue = false;
//		}
//		
//		/* other variable */
//		else {
//			
//			node._value = ((environment.VariableSymbol)addr_const.getBase()).getMemLoc();
//			
//		}
//	}
	
	protected static void PromoteIntegerType(ExpressionBase left, ExpressionBase right){
		//assume that left and right has integer type
		
		if (!(left._type.equals(right._type))){
			if (left._type instanceof environment.CharType){
				left._type = environment.TypeFactory.newIntType();
			}
			else {
				right._type = environment.TypeFactory.newIntType();
			}
		}
		
	}
	
	protected static void report_error(String msg, NodeBase root){
		try{
			ErrorHandling.ReportError(String.format("%d:%d:error: %s", root._line, root._column, msg), 
					ErrorHandling.ERROR);
		}
		catch (Exception e){
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	protected static void report_warning(String msg, NodeBase root){
		try{
			ErrorHandling.ReportError(String.format("%d:%d:warning: %s", root._line, root._column, msg), 
					ErrorHandling.WARNING);
		}
		catch (Exception e){
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	protected static void report_implicit_conversion_warning(NodeBase root, environment.Type from, environment.Type to){
		report_warning(String.format("implicit conversion from %s to %s", 
				semantics.ResourcePrinter.getTypeString(from),
				semantics.ResourcePrinter.getTypeString(to)), root);
	}
	
	protected static void report_incompatible_conversion_error(NodeBase root, environment.Type from, environment.Type to){
		report_error(String.format("cannot convert from %s to incompatible type %s",
				semantics.ResourcePrinter.getTypeString(from),
				semantics.ResourcePrinter.getTypeString(to)), root);
	}
	
	protected void report_invalid_operand_error(NodeBase root, String op, environment.Type left, environment.Type right){
		report_error(String.format("invalid operand to %s: %s and %s", op,
				semantics.ResourcePrinter.getTypeString(left),
				semantics.ResourcePrinter.getTypeString(right)), root);
	}
	
	protected void report_invalid_operand_error(NodeBase root, String op, environment.Type left){
		report_error(String.format("invalid operand to %s: %s", op,
				semantics.ResourcePrinter.getTypeString(left)), root);
	}
}
