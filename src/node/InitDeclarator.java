package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The InitDeclarator node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class InitDeclarator extends Declarator {

	/**
	 * Constructs an initDeclarator from type, id and initializer
	 * 
	 * @param type		the type
	 * @param id		the id
	 * @param init		the initializer
	 */
	public InitDeclarator(TypeNode type, Identifier id, Initializer init) {
		super(type, id);
		_init = init;
	}
	
	/**
	 * Constructs an initDeclarator from type, id and initializer
	 * 
	 * @param decl	the declarator
	 * @param id	the initializer
	 */
	public InitDeclarator(Declarator decl, Initializer init){
		super(decl._type, decl._id);
		_init = init;
	}
	
	/**
	 * Access the initializer.
	 * 
	 * @return 	the initializer
	 */
	public Initializer getInitializer(){
		return _init;
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		String[] properties = new String[]{"type", _type.getTypeName(), "identifier", _id.getVal(),
											"init", _init.getInitName()};
		traverseFormatterHead(writer, this.getName(), properties);
		_init.traverse(writer);
		traverseFormatterTail(writer, this.getName(), properties);
		return writer;
	}

	@Override
	public String getName() {

		return "init declarator";
	}
	
	Initializer _init;

	
}
