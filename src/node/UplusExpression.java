package node;

import environment.Environment;
import environment.RuntimeEnvironment;
import environment.TypeFactory;

/**
 * The UplusExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class UplusExpression extends UnaryExpression {

	/**
	 * @param expr1
	 */
	public UplusExpression(ExpressionBase expr1) {
		super(expr1);

	}

	@Override
	public String getName() {

		return "unary +";
	}

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
		
		_operand.checkEvaluate(cur_env, runtime);
		if (_operand._type == null){
			_type = null;
			return;
		}
	
		if (!TypeFactory.isIntegerType(_operand._type)){
			report_error(String.format("operand of unary + shall have arithmetic type, but %s found",
					semantics.ResourcePrinter.getTypeString(_operand._type)), _operand);
			_type = null;
			return;
		}
		
		_type = _operand._type;
		_value = _operand._value;
		_lvalue = false;
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		_operand.checkEvaluate_NoGenerating(cur_env);
		
		if (_operand._type == null){
			_type = null;
			return;
		}
	
		if (!TypeFactory.isIntegerType(_operand._type)){
			report_error(String.format("operand of unary + shall have arithmetic type, but %s found",
					semantics.ResourcePrinter.getTypeString(_operand._type)), _operand);
			_type = null;
			return;
		}
		
		_type = _operand._type;
		if (_operand._value instanceof Number){
			_value = _operand._value;
		}
		else {
			_value = null;
		}
		_lvalue = false;
	}

}
