package node;

import irpack.Operand;
import irpack.op;
import environment.Environment;
import environment.RuntimeEnvironment;

/**
 * The LogicalnotExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class LogicalnotExpression extends UnaryExpression {

	/**
	 * @param expr1
	 */
	public LogicalnotExpression(ExpressionBase expr1) {
		super(expr1);

	}

	@Override
	public String getName() {

		return "!";
	}

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
			

		_operand.checkEvaluate(cur_env, runtime);
		if (_operand._type == null){
			_type = null;
			return;
		}
		
		if (!environment.TypeFactory.isScalarType(_operand._type)){
			report_invalid_operand_error(this, "!", _operand._type);
			_type = null;
			return;
		}
		
		_type = environment.TypeFactory.newIntType();
		_lvalue = false;
		
		_value = op.logicalNot(op.temp(runtime.entry), (Operand)_operand._value, runtime.entry);
		
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		_operand.checkEvaluate_NoGenerating(cur_env);
		if (_operand._type == null){
			_type = null;
			return;
		}
		
		if (environment.TypeFactory.isIntegerType(_operand._type)){
			_type = environment.TypeFactory.newIntType();
			if (_operand._value != null && _operand._value instanceof Number){
				_value = (((Number)_operand._value).intValue() == 0) ? 1 : 0;
			}
			else {
				_value = null;
			}
			_lvalue = false;
			return;
		}
		else if (_operand._type instanceof environment.PointerType){
			_type = environment.TypeFactory.newIntType();
			environment.AddressConstant addr = (!_operand._lvalue && _operand._value instanceof environment.AddressConstant) ?
					(environment.AddressConstant)_operand._value : null;
			if (addr != null){
				if (addr.getBase() == null){
					if (addr.getOffset() == 0){
						_value = 1;
					}
					else {
						_value = 0;
					}
				}
				else {
					if (addr.getOffset() == 0){
						_value = 0;
					}
					else {
						_value = null;
					}
				}
			}
			else {
				_value = null;
			}
			_lvalue = false;
			return;
		}
		
		report_invalid_operand_error(this, "!", _operand._type);
		_type = null;
		return;
	}

}
