package node;

import java.io.IOException;
import java.io.Writer;

import environment.Environment;
import environment.RuntimeEnvironment;
import environment.Type;
import environment.TypeFactory;

/**
 * The CastExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class CastExpression extends ExpressionBase {

	/**
	 * Constructs a cast expression from a Type and an expression
	 *
	 * @param type		the type
	 * @param expr		the expr
	 */
	public CastExpression(TypeNode type, ExpressionBase expr) {
		_type_node = type;
		_expr = expr;
	}

	/**
	 * Accesses the type to cast the expression to.
	 * 
	 * @return	the type
	 */
	public TypeNode getTypeNode(){
		return _type_node;
	}
	
	/**
	 * Accesses the expression to be cast.
	 * 
	 * @return	the expression
	 */
	public ExpressionBase getExpr(){
		return _expr;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		String[] properties = new String[]{"type", _type_node.getTypeName()};
		traverseFormatterHead(writer, this.getName(), properties);
		_expr.traverse(writer);
		traverseFormatterTail(writer, this.getName(), properties);
		return writer;
	}

	@Override
	public String getName() {

		return "cast expression";
	}
	
	protected TypeNode _type_node;
	protected ExpressionBase _expr;
	
	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
			

		_expr.checkEvaluate(cur_env, runtime);
		if (_expr._type == null){
			_type = null;
			return;
		}
		
		semantics.SemanticCheck semanticCheck = semantics.SemanticCheck.getRunningInstance();
		
		DeclarationSpecifier spec = (_type_node instanceof DerivedTypeList) ?
				(((DerivedTypeList)_type_node).getDeclarationSpecifier()) :
				(DeclarationSpecifier)_type_node;
		
		Type spec_type = semanticCheck.checkDeclarationSpecifier(spec);
		if (spec_type == null){
			_type = null;
			return;
		}
		
		environment.Type composedType = semanticCheck.composeType(spec_type, _type_node);
		if (composedType == null){
			_type = null;
			return;
		}
		
		// special case: cast to void does not need to check the type of right operand
		if (composedType instanceof environment.VoidType){
			_type = composedType;
			_value = null;
			_lvalue = false;
			return;
		}
		
		if (!composedType.isComplete()){
			report_error("cast shall not have incomplete type operand", _type_node);
			_type = null;
			return;
		}
				
		if (TypeFactory.isIntegerType(_expr._type)){
			if (TypeFactory.isIntegerType(composedType)){
				_type = composedType;
				_value = _expr._value;
				_lvalue = false;
				return;
			}
			else if (composedType instanceof environment.PointerType){
				_type = composedType;
				_value = _expr._value;
				_lvalue = false;
				return;
			}
			else {
				report_error(String.format("undefined cast from %s to %s", 
						semantics.ResourcePrinter.getTypeString(_expr._type),
						semantics.ResourcePrinter.getTypeString(composedType)), this);
				_type = null;
				return;
			}
		}
		else if (_expr._type instanceof environment.PointerType){
			if (TypeFactory.isIntegerType(composedType)){
				_type = composedType;
				_value = _expr._value;
				_lvalue = false;
				return;
			}
			else if (composedType instanceof environment.PointerType){
				_type = composedType;
				_value = _expr._value;
				_lvalue = false;
				return;
			}
			else{
				report_error(String.format("undefined cast from pointer type to %s", semantics.ResourcePrinter.getTypeString(composedType)), this);
				_type = null;
				return;
			}
		}

		report_error(String.format("cast expression shall operate on scalar type, but %s found", semantics.ResourcePrinter.getTypeString(_expr._type)), _expr);
		_type = null;
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		_expr.checkEvaluate_NoGenerating(cur_env);
		if (_expr._type == null){
			_type = null;
			return;
		}
		
		semantics.SemanticCheck semanticCheck = semantics.SemanticCheck.getRunningInstance();
		
		DeclarationSpecifier spec = (_type_node instanceof DerivedTypeList) ?
				(((DerivedTypeList)_type_node).getDeclarationSpecifier()) :
				(DeclarationSpecifier)_type_node;
		
		Type spec_type = semanticCheck.checkDeclarationSpecifier(spec);
		if (spec_type == null){
			_type = null;
			return;
		}
		
		environment.Type composedType = semanticCheck.composeType(spec_type, _type_node);
		if (composedType == null){
			_type = null;
			return;
		}
		
		// special case: cast to void does not need to check the type of right operand
		if (composedType instanceof environment.VoidType){
			_type = composedType;
			_value = null;
			_lvalue = false;
			return;
		}
		
		if (!composedType.isComplete()){
			report_error("cast shall not have incomplete type operand", _type_node);
			_type = null;
			return;
		}
				
		if (TypeFactory.isIntegerType(_expr._type)){
			if (TypeFactory.isIntegerType(composedType)){
				_type = composedType;
				if (_expr._value instanceof Number){
					_value = _expr._value;
				}
				else {
					_value = null;
				}
				_lvalue = false;
				return;
			}
			else if (composedType instanceof environment.PointerType){
				_type = composedType;
				if (_expr._value instanceof Number){
					_value = new environment.AddressConstant(null, (Integer)_expr._value);
				}
				else {
					_value = null;
				}
				_lvalue = false;
				return;
			}
			else {
				report_error(String.format("undefined cast from %s to %s", 
						semantics.ResourcePrinter.getTypeString(_expr._type),
						semantics.ResourcePrinter.getTypeString(composedType)), this);
				_type = null;
				return;
			}
		}
		else if (_expr._type instanceof environment.PointerType){
			if (TypeFactory.isIntegerType(composedType)){
				_type = composedType;
				if (!_expr._lvalue && _expr._value instanceof environment.AddressConstant){
					environment.AddressConstant addr = (environment.AddressConstant)_expr._value;
					if (addr.getBase() == null){
						_value = addr.getOffset();
					}
					else {
						_value = null;
					}
				}
				else {
					_value = null;
				}
				_lvalue = false;
				return;
			}
			else if (composedType instanceof environment.PointerType){
				_type = composedType;
				if (!_expr._lvalue && _expr._value instanceof environment.AddressConstant){
					_value = ((environment.AddressConstant)_expr._value).newAddrWithOffsetAddedTo(0);
				}
				else {
					_value = null;
				}
				_lvalue = false;
				return;
			}
			else{
				report_error(String.format("undefined cast from pointer type to %s", semantics.ResourcePrinter.getTypeString(composedType)), this);
				_type = null;
				return;
			}
		}

		report_error(String.format("cast expression shall operate on scalar type, but %s found", semantics.ResourcePrinter.getTypeString(_expr._type)), _expr);
		_type = null;
	}

	@Override
	public void clearEvaluatedState() {
		_evaluated = false;
		_type = null;
		_value = null;
		_lvalue = false;
		
		_expr.clearEvaluatedState();
	}

}
