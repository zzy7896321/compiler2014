package node;

import java.io.IOException;
import java.io.Writer;
import java.math.BigInteger;

/**
 * The Integer Constant node Base class.
 * 
 * @author Zhao Zhuoyue
 * @version last update: 3/26/2014
 *
 */
public class IntegerConstant extends Constant{

	static public final int INT = 0;
	static public final int LONG = 1;
	static public final int LONGLONG = 2;
	
	static public final boolean SIGNED = true;
	static public final boolean UNSIGNED = false;
	
	public IntegerConstant(String val, int radix, int type, boolean signed){
		super();
		_val = new BigInteger(val, radix);
		_type = type;
		_signed = signed;
	}
	
	/**
	 * Creates a new integer constant node from string. 
	 * Returned constant shall conform to the constraints in C99.
	 * Null is returned if the constant is not in the range of any integer type.
	 * Current implementation only takes care with signed int.
	 * 
	 * @param val	the string literal
	 * @return		a new integer constant node or null
	 * 
	 */
	public static IntegerConstant newIntegerConstant(String val){		
		val = val.toUpperCase();
		int radix = 10, i = 0;
		if (val.charAt(0) == '0'){
			radix = 8;
			if (val.length() > 1 && val.charAt(1) == 'X'){
				radix = 16;
				i = 2;
			}
		}
		int j = val.length();
		for (; val.charAt(j-1) > 'F'; --j);
		return new IntegerConstant(val.substring(i, j), radix, INT, SIGNED);
	}
	
	/**
	 * Returns the name of the type that the constant fits in.
	 * @return the type name
	 */
	public String getTypeName(){
		return (!_signed) ? "unsigned " + type_name[_type] : type_name[_type];
	}
	
	/**
	 * Accesses the integer constant value.
	 * 
	 * @return	the value
	 */
	public BigInteger getVal(){
		return _val;
	}
	
	
	/**
	 * Sets the integer constant value.
	 * 
	 * @param val	the value
	 */
	public void setVal(int val){
		_val = new BigInteger(Integer.toString(val), 10);
	}
	
	/**
	 * Sets the integer constant value with a BigInteger Object.
	 * 
	 * @param val	the value
	 */
	public void setVal(BigInteger val){
		_val = val;
	}
	
	BigInteger _val;
	int _type;
	boolean _signed;

	@Override
	public Writer traverse(Writer writer) throws IOException {
		String[] properties;
		properties = new String[]{"value", _val.toString(), "type", getTypeName()};
		traverseFormatterHead(writer, name, properties);
		traverseFormatterTail(writer, name, properties);
		return writer;
	}	
	
	static final String name = "integer constant";
	static final String[] type_name = {"int", "long", "long long"};

	@Override
	public String getName() {
		
		return name;
	}

	@Override
	public String getValLiteral() {
		
		return _val.toString();
	}
	
	
}
