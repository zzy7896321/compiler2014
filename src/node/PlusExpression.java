package node;

import irpack.Operand;
import irpack.op;
import environment.Environment;
import environment.RuntimeEnvironment;


/**
 * The PlusExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class PlusExpression extends BinaryExpression {

	public PlusExpression(ExpressionBase expr1, ExpressionBase expr2) {
		super(expr1, expr2);
	}

	@Override
	public String getName() {
		return "+";
	}

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
			

		_left.checkEvaluate(cur_env, runtime);
		if (_left._type == null){
			_type = null;
			return;
		}
		_right.checkEvaluate(cur_env, runtime);
		if (_right._type == null){
			_type = null;
			return;
		}
		
		if (_right._type instanceof environment.PointerType && environment.TypeFactory.isIntegerType(_left._type)){
			ExpressionBase tmp = _left;
			_left = _right;
			_right = tmp;
		}
		
		if (environment.TypeFactory.isIntegerType(_left._type) && environment.TypeFactory.isIntegerType(_right._type)){
			PromoteIntegerType(_left, _right);
			
			_type = _left._type;
			_lvalue = false;
			
			_value = op.add(op.temp(runtime.entry), (Operand) _left._value, (Operand) _right._value, runtime.entry);
			return;
		}
		else if (_left._type instanceof environment.PointerType && environment.TypeFactory.isIntegerType(_right._type)){
			environment.Type base_type = ((environment.PointerType)_left._type).getBaseType();
			if (base_type instanceof environment.FunctionType){
				report_error("cannot apply + to function pointer", this);
				_type = null;
				return;
			}
			else if (!base_type.isComplete()){
				report_error("cannot apply + to incomplete type pointer: " + 
							semantics.ResourcePrinter.getTypeString(_left._type), this);
				_type = null;
				return;
			}
			
			_type = _left._type;
			_lvalue = false;
			
			Operand result = null;
			int base_size = base_type.getSize();
			if (base_size <= 1){
				result = op.add(op.temp(runtime.entry), (Operand) _left._value, (Operand) _right._value, runtime.entry);
			}
			else {
				if (Integer.highestOneBit(base_size) == base_size){
					result = op.sll(op.temp(runtime.entry), (Operand)_right._value, op.constant(Integer.numberOfTrailingZeros(base_size)), runtime.entry);
				}
				else {
					result = op.mul(op.temp(runtime.entry), (Operand)_right._value, op.constant(base_size), runtime.entry);
				}
				result = op.add(op.temp(runtime.entry), (Operand) _left._value, result, runtime.entry);
			}
			
			_value = result;
			
			return;
		}
		
		
		report_invalid_operand_error(this, "+", _left._type, _right._type);
		_type = null;
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		_left.checkEvaluate_NoGenerating(cur_env);
		if (_left._type == null){
			_type = null;
			return;
		}
		_right.checkEvaluate_NoGenerating(cur_env);
		if (_right._type == null){
			_type = null;
			return;
		}
		
		if (_right._type instanceof environment.PointerType && environment.TypeFactory.isIntegerType(_left._type)){
			ExpressionBase tmp = _left;
			_left = _right;
			_right = tmp;
		}
		
		if (environment.TypeFactory.isIntegerType(_left._type) && environment.TypeFactory.isIntegerType(_right._type)){
			PromoteIntegerType(_left, _right);
			
			_type = _left._type;
			if (_left._value instanceof Number && _right._value instanceof Number){
				
				_value = ((Number)_left._value).intValue() + ((Number)_right._value).intValue();
				
			}
			else {
				_value = null;
			}
			_lvalue = false;
			return;
		}
		else if (_left._type instanceof environment.PointerType && environment.TypeFactory.isIntegerType(_right._type)){
			environment.Type base_type = ((environment.PointerType)_left._type).getBaseType();
			if (base_type instanceof environment.FunctionType){
				report_error("cannot apply + to function pointer", this);
				_type = null;
				return;
			}
			else if (!base_type.isComplete()){
				report_error("cannot apply + to incomplete type pointer: " + 
							semantics.ResourcePrinter.getTypeString(_left._type), this);
				_type = null;
				return;
			}
			
			_type = _left._type;
			if (!_left._lvalue && _left._value instanceof environment.AddressConstant
					&& _right._value instanceof Number){
				if (base_type.getSize() != 0){
					_value = ((environment.AddressConstant)_left._value).
							newAddrWithOffsetAddedTo(((Number)_right._value).intValue() * base_type.getSize());
				}
				else {
					_value = ((environment.AddressConstant)_left._value).newAddrWithOffsetAddedTo(((Number)_right._value).intValue());
				}
			}
			else {
				_value = null;
			}
			_lvalue = false;
			return;
		}
		
		
		report_invalid_operand_error(this, "+", _left._type, _right._type);
		_type = null;
		
	}
	
}
