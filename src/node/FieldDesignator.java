package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The FieldDesignator node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class FieldDesignator extends Designator {

	/**
	 * Constructs a field designator from an identifier.
	 * 
	 * @param id		the identifier
	 */
	public FieldDesignator(Identifier id) {
		_id = id;
	}

	/**
	 * Access the destination identifier
	 * 
	 * @return	the identifier
	 */
	public Identifier getId(){
		return _id;
	}
	
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		//do nothing
		return writer;
	}

	@Override
	public String getName() {

		return "field designator";
	}

	protected Identifier _id;

	@Override
	public String getDestLiteral() {
		
		return "." + _id.getVal();
	}
}
