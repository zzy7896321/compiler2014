package node;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import environment.Environment;
import environment.RuntimeEnvironment;

/**
 * The ArgumentExpressionList node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class ArgumentExpressionList extends ExpressionBase {

	/**
	 * Construct an empty argument expression list.
	 */
	public ArgumentExpressionList() {
		_list = new LinkedList<ExpressionBase>();
	}

	/**
	 * Appends the expression to the argument expression list
	 * @param expr1
	 */
	public void append(ExpressionBase expr1){
		_list.add(expr1);
	}
	
	/**
	 * Access the expression list.
	 * 
	 */
	public List<ExpressionBase> getList(){
		return _list;
	}
	
	@Override
	public String getName() {
		
		return "argument expression list";
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		traverseFormatterHead(writer, this.getName(), null);
		
		for (ListIterator<ExpressionBase> iter = _list.listIterator(); iter.hasNext(); iter.next().traverse(writer));
		
		traverseFormatterTail(writer, this.getName(), null);
		
		return writer;
	}
	
	List<ExpressionBase> _list;

	@Override
	public void checkEvaluate(Environment cur_env,
			RuntimeEnvironment runtime) {
		//don't call
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		//don't call
	}

	@Override
	public void clearEvaluatedState() {
		
		_evaluated = false;
		_type = null;
		_value = null;
		_lvalue = false;
	}
	
	

}
