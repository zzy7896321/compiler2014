package node;

import java.io.IOException;
import java.io.Writer;


/**
 * The FunctionType node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 4, 2014
 */
public class FunctionType extends TypeModifier {

	/**
	 * 
	 * @param rhs
	 */
	public FunctionType(ParameterList parameterList) {
		super();
		_parameter_list = parameterList;
	}

	public ParameterList getParameterList(){
		return _parameter_list;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		// do nothing
		return writer;
	}

	@Override
	public String getName() {

		return "function";
	}
	
	protected ParameterList _parameter_list;

}
