package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The ConstantExprDesignator node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class IndexDesignator extends Designator {

	/**
	 * Constructs an index designator from an expression.
	 * 
	 * @param expr		the expression
	 */
	public IndexDesignator(ExpressionBase expr) {
		super();
		_expr = expr;
	}
	
	/**
	 * Access the destination expression.
	 * 
	 * @return the expression
	 */
	public ExpressionBase getExpr(){
		return _expr;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		if (_expr instanceof PrimaryExpression && ((PrimaryExpression)_expr).getVal() instanceof IntegerConstant)
			return writer;
		_expr.traverse(writer);
		return writer;
	}

	@Override
	public String getName() {

		return "index designator";
	}
	
	protected ExpressionBase _expr;

	@Override
	public String getDestLiteral() {
		if (_expr instanceof PrimaryExpression && ((PrimaryExpression)_expr).getVal() instanceof IntegerConstant){
			return "[" + ((Constant)((PrimaryExpression)_expr).getVal()).getValLiteral() + "]"; 
		}
		return "[<expr>]";
	}

}
