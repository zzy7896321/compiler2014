package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The string constant node class;
 * 
 * @author Zhao Zhuoyue
 * @version last update: 3/26/2014
 */
public class StringConstant extends Constant {

	/**
	 * Constructs a string constant node.
	 * 
	 * @param val		the string constant
	 */
	public StringConstant(String val){
		_val = val;
	}
	
	/**
	 * Accesses the string constant.
	 * 
	 * @return		the string constant
	 */
	public String getVal(){
		return _val;
	}
	
	/**
	 * Sets the string constant.
	 * 
	 * @param val	the string constant
	 */
	public void setVal(String val){
		_val = val;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		String[] properties;
		properties = new String[]{"value", "\"" + util.StringUtil.unescape(_val) + "\""};
		traverseFormatterHead(writer, name, properties);
		traverseFormatterTail(writer, name, properties);
		return writer;
	}
	
	String _val;

	static final String name = "string constant";

	@Override
	public String getValLiteral() {
		
		return "\"" + util.StringUtil.unescape(_val) + "\"";
	}
}
