package node;

import java.io.IOException;
import java.io.Writer;

import util.StringUtil;

/**
 * The IfStatement node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class IfStatement extends Statement {

	/**
	 *	Constructs an if statement from a controlling expression, then branch and else branch.
	 *	NOTE: else branch is possibly null
	 *
	 *	@param expr			the controlling expression
	 *	@param thenBranch	the statement to execute when expr is evaluated to true
	 *	@param elseBranch	the statement to execute when expr is evaluated to false (null when not present)
	 */
	public IfStatement(ExpressionBase expr, Statement thenBranch, Statement elseBranch) {
		_expr = expr;
		_then = thenBranch;
		_else = elseBranch;
	}

	public ExpressionBase getControllingExpr(){
		return _expr;
	}
	
	public Statement getThen(){
		return _then;
	}
	
	public Statement getElse(){
		return _else;
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		traverseFormatterHead(writer, this.getName(), null);
		
		writer.append(StringUtil.repeat(_indent, "\t")).append("<controlling expression>\n");
			_expr.traverse(writer);
		writer.append(StringUtil.repeat(_indent, "\t")).append("<then branch>\n");
			_then.traverse(writer);
		if (_else != null){
		writer.append(StringUtil.repeat(_indent, "\t")).append("<else branch>\n");
			_else.traverse(writer);		
		}
		
		traverseFormatterTail(writer, this.getName(), null);
		return writer;
	}

	@Override
	public String getName() {

		return "if statement";
	}

	protected ExpressionBase _expr;
	protected Statement _then, _else;
}
