package node;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;

import util.DoubleEndedStringBuilder;

import java.util.Iterator;

/**
 * The DerivedTypeList node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 4, 2014
 */
public class DerivedTypeList extends TypeNode {

	/**
	 * Constructs an empty derived type list.
	 */
	public DerivedTypeList(DeclarationSpecifier spec) {
		_list = new LinkedList<TypeModifier>();
		_spec = spec;
	}
	
	/**
	 * Constructs from a list.
	 * 
	 * 
	 */
	public DerivedTypeList(DeclarationSpecifier spec, LinkedList<TypeModifier> list){
		_spec = spec;
		_list = list;
	}
	
	public void addLast(TypeModifier modifier){
		_list.addLast(modifier);
	}
	
	public void addFirst(TypeModifier modifier){
		_list.addFirst(modifier);
	}
	
	public TypeModifier pollFirst(){
		return _list.pollFirst();
	}
	
	public TypeModifier pollLast(){
		return _list.pollLast();
	}
	
	public TypeModifier getFirst(){
		return _list.getFirst();
	}
	
	public TypeModifier getLast(){
		return _list.getLast();
	}
	
	public boolean listEmpty(){
		return _list.isEmpty();
	}
	
	public int listSize(){
		return _list.size();
	}
	
	public LinkedList<TypeModifier> getList(){
		return _list;
	}
	
	public DeclarationSpecifier getDeclarationSpecifier(){
		return _spec;
	}

	@Override
	public String getTypeName() {
		if (_list.isEmpty()){
			return _spec.getTypeName();
		}
		
		DoubleEndedStringBuilder builder = new DoubleEndedStringBuilder();
		
		TypeModifier cur = null;
		for (Iterator<TypeModifier> iter = _list.iterator(); iter.hasNext();){
			cur = iter.next();
			if (cur instanceof PointerType){
				builder.append_head("*");
			}
			else if (cur instanceof ArrayType){
				ArrayType acur = (ArrayType) cur;
				if (acur.getExpr() == null){
					builder.append_rear("[]");
				}
				else{
					if (acur.getExpr() instanceof PrimaryExpression && 
						((PrimaryExpression)acur.getExpr()).getVal() instanceof IntegerConstant){
						builder.append_rear("[");
						builder.append_rear(((IntegerConstant)(((PrimaryExpression)acur.getExpr()).getVal())).getValLiteral());
						builder.append_rear("]");
					}
					else{
						builder.append_rear("[<expr>]");
					}
				}
			}
			else{	//FunctionType
				builder.append_head("(").append_rear(")(");
				LinkedList<Declarator> para_list = ((FunctionType)cur).getParameterList().getList();
				if (para_list.isEmpty()){
					if (((FunctionType)cur).getParameterList().getEllipsis()){
						builder.append_rear("...)");
					}
					else {
						builder.append_rear(")");
					}
				}
				else {
					Iterator<Declarator> para_iter = para_list.iterator();
					builder.append_rear(para_iter.next().getType().getTypeName());
					for (; para_iter.hasNext(); builder.append_rear(", ").append_rear(para_iter.next().getType().getTypeName()));
					if (((FunctionType)cur).getParameterList().getEllipsis()){
						builder.append_rear(", ...)");
					}
					else {
						builder.append_rear(")");
					}
				}
			}
		}
		
		builder.append_head(" ").append_head(_spec.getTypeName());
		
		return builder.toString();
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		
		return writer;
	}

	@Override
	public String getName() {

		return "derived type";
	}
	
	protected LinkedList<TypeModifier> _list;
	protected DeclarationSpecifier _spec;
}
