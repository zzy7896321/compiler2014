package node;

import irpack.Operand;
import irpack.op;
import environment.Environment;
import environment.RuntimeEnvironment;
import environment.TypeFactory;

/**
 * The DivisionAssignmentExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class DivisionAssignmentExpression extends BinaryExpression {

	/**
	 * @param expr1
	 * @param expr2
	 */
	public DivisionAssignmentExpression(ExpressionBase expr1,
			ExpressionBase expr2) {
		super(expr1, expr2);

	}

	@Override
	public String getName() {
		
		return "/=";
	}

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
			
		_right.checkEvaluate(cur_env, runtime);
		if (_right._type == null){
			_type = null;
			return;
		}
		
		_left.checkEvaluate(cur_env, runtime);
		if (_left._type == null){
			_type = null;
			return;
		}
		if (!_left._lvalue){
			report_error("non-lvalue on the left hand side of /=", _left);
			_type = null;
			return;
		}
		
		_type = _left._type;
		_lvalue = false;	// the result of assignment expression is not lvalue
		if (TypeFactory.isIntegerType(_left._type)){
			if (environment.TypeFactory.isIntegerType(_right._type)){
				_value = op.div((Operand)_left._value, (Operand)_left._value, (Operand)_right._value, runtime.entry);
				
				return;
			}
			else {
				report_incompatible_conversion_error(_right, _right._type, _left._type);
				_type = null;
				return;
			}
		}

		report_error(String.format("left hand side of /= has invalid type: %s", semantics.ResourcePrinter.getTypeString(_left._type)), _left);
		_type = null;
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		_left.checkEvaluate_NoGenerating(cur_env);
		if (_left._type == null){
			_type = null;
			return;
		}
		if (!_left._lvalue){
			report_error("non-lvalue on the left hand side of /=", _left);
			_type = null;
			return;
		}
		
		_right.checkEvaluate_NoGenerating(cur_env);
		if (_right._type == null){
			_type = null;
			return;
		}
		
		_type = _left._type;
		_value = null;	//non-constant
		_lvalue = false;	// the result of assignment expression is not lvalue
		if (TypeFactory.isIntegerType(_left._type)){
			if (environment.TypeFactory.isIntegerType(_right._type)){
				return;
			}
			else {
				report_incompatible_conversion_error(_right, _right._type, _left._type);
				_type = null;
				return;
			}
		}

		report_error(String.format("left hand side of /= has invalid type: %s", semantics.ResourcePrinter.getTypeString(_left._type)), _left);
		_type = null;
	}
}
