package node;

import java.io.IOException;
import java.io.Writer;
import util.StringUtil;

/**
 * The ForStatement node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class ForStatement extends Statement {

	/**
	 * Constructs a for statement from an initialization expression, 
	 * a controlling expression , a step expression and a looping body.
	 * <p>
	 * NOTE: expr1 and expr3 are possibly null. A null expr2 will be
	 * replaced with a non-zero constant expression.
	 * 
	 *  
	 * @param expr1		the initialization expression
	 * @param expr2		the controlling expression
	 * @param expr3		the step expression
	 * @param body		the looping body
	 */
	public ForStatement(ExpressionBase expr1, ExpressionBase expr2, ExpressionBase expr3, Statement body) {
		_expr1 = expr1;
		_expr2 = expr2;
		_expr3 = expr3;
		_body = body;
		
		if (_expr2 == null){
			_expr2 = new PrimaryExpression(new IntegerConstant("1", 10, IntegerConstant.INT, true));
		}
	}

	public ExpressionBase getInitializationExpr(){
		return _expr1;
	}
	
	public ExpressionBase getControllingExpr(){
		return _expr2;
	}
	
	public ExpressionBase getStepExpr(){
		return _expr3;
	}
	
	public Statement getStatement(){
		return _body;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		traverseFormatterHead(writer, this.getName(), null);
		
		writer.append(StringUtil.repeat(_indent, "\t")).append("<initialization expression>\n");
			if (_expr1 != null){
				_expr1.traverse(writer);
			}
			else {
				writer.append(StringUtil.repeat(_indent + 1, "\t")).append("(null)\n");
			}
		writer.append(StringUtil.repeat(_indent, "\t")).append("<controlling expression>\n");
			_expr2.traverse(writer);
		writer.append(StringUtil.repeat(_indent, "\t")).append("<step expression>\n");
			if (_expr3 != null){
				_expr3.traverse(writer);
			}
			else {
				writer.append(StringUtil.repeat(_indent + 1, "\t")).append("(null)\n");
			}
		writer.append(StringUtil.repeat(_indent, "\t")).append("<looping body>\n");
			if (_body != null){
				_body.traverse(writer);
			}
			else {
				writer.append(StringUtil.repeat(_indent + 1, "\t")).append("(null)\n");
			}
		traverseFormatterTail(writer, this.getName(), null);
		return writer;
	}

	@Override
	public String getName() {

		return "for statement";
	}

	protected ExpressionBase _expr1, _expr2, _expr3;
	protected Statement _body;
}
