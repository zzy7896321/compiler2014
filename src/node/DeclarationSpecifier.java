package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The DeclarationSpecifier node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class DeclarationSpecifier extends TypeNode {

	/**
	 * Constructs an empty declaration specifier.
	 * 
	 */
	public DeclarationSpecifier() {
		super();
		_val = 0;
	}
	
	public DeclarationSpecifier(DeclarationSpecifier rhs){
		super();
		_val = rhs._val;
	}
	
	/* invalid */
	public void setInvalid(){
		_val = INVALID;
	}
	
	public boolean isInvalid(){
		return test(_val, INVALID);
	}
	
	/* storage class specifier */
	
	public void setTypedef(){
		if (test(_val, STORAGE_CLASS_MASK)){
			setInvalid();
		}
		else {
			_val |= TYPEDEF;
		}
	}
	
	public boolean isTypedef(){
		return test(_val, TYPEDEF);
	}
	
	public void setExtern(){
		if (test(_val, STORAGE_CLASS_MASK)){
			setInvalid();
		}
		else {
			_val |= EXTERN;
		}
	}
	
	public boolean isExtern(){
		return test(_val, EXTERN);
	}
	
	public void setStatic(){
		if (test(_val, STORAGE_CLASS_MASK)){
			setInvalid();
		}
		else {
			_val |= STATIC;
		}
	}
	
	public boolean isStatic(){
		return test(_val, STATIC);
	}
	
	public void setAuto(){
		if (test(_val, STORAGE_CLASS_MASK)){
			setInvalid();
		}
		else {
			_val |= AUTO;
		}
	}
	
	public boolean isAuto(){
		return test(_val, AUTO);
	}
	
	public void setRegister(){
		if (test(_val, STORAGE_CLASS_MASK)){
			setInvalid();
		}
		else {
			_val |= REGISTER;
		}
	}
	
	public boolean isRegister(){
		return test(_val, REGISTER);
	}
	
	/* type specifier */
	
	public void setInt(){
		if (test(_val, TYPE_SPECIFIER_MASK)) {
			setInvalid();
		}
		else {
			_val |= INT;
		}
	}
	
	public boolean isInt(){
		return test(_val, INT);
	}
	
	public void setVoid(){
		if (test(_val, TYPE_SPECIFIER_MASK)){
			setInvalid();
		}
		else {
			_val |= VOID;
		}
	}
	
	public boolean isVoid(){
		return test(_val, VOID);
	}
	
	public void setChar(){
		if (test(_val, TYPE_SPECIFIER_MASK)){
			setInvalid();
		}
		else {
			_val |= CHAR;
		}
	}
	
	public boolean isChar(){
		return test(_val, CHAR);
	}
	
	public boolean isBuiltin(){
		return test(_val, BUILTIN);
	}
	
	public void resetType(){
		_val &= (~TYPE_SPECIFIER_MASK);
	}
	
	//struct, union and typedef name use inherited class from DeclarationSpecifier
	
	final public boolean isStruct(){
		return test(_val, STRUCT);
	}
	
	final public boolean isUnion(){
		return test(_val, UNION);
	}
	
	final public boolean isTypedefName(){
		return test(_val, TYPEDEF_NAME);
	}
	
	final public boolean typeSpecified(){
		return test(_val, TYPE_SPECIFIER_MASK);
	}
	
	public static DeclarationSpecifier merge(DeclarationSpecifier spec1, DeclarationSpecifier spec2){
		DeclarationSpecifier ret = null;
		
		// 6.7.1.2 at most one storage class specifier may be given in the declaration specifier
		if (test(spec1._val, STORAGE_CLASS_MASK) && test(spec2._val, STORAGE_CLASS_MASK) &&
			applyMask(spec1._val, STORAGE_CLASS_MASK) != applyMask(spec2._val, STORAGE_CLASS_MASK)){
				ret = new DeclarationSpecifier();
				ret.setInvalid();
				return ret;
		}
		
		/*
		//Not implemented yet
		 
		// type qualifier
		// no constraints can be checked at syntactic stage
		  
		//function specifier
		// no constraints can be checked at syntactic stage
		*/
		
		// type specifier
		if (test(spec1._val, TYPE_SPECIFIER_MASK) && test(spec2._val, TYPE_SPECIFIER_MASK) &&
			applyMask(spec1._val, TYPE_SPECIFIER_MASK) != applyMask(spec2._val, TYPE_SPECIFIER_MASK)){
			ret = new DeclarationSpecifier();
			ret.setInvalid();
			return ret;
		}
		else {
			if (test(spec1._val, TYPE_SPECIFIER_MASK)){
				switch (applyMask(spec1._val, TYPE_SPECIFIER_MASK)){
				case INT:
				case VOID:
				case CHAR:
					ret = new DeclarationSpecifier(spec1);
					break;
				case STRUCT:
					if (spec2 instanceof StructDeclarationSpecifier && !((StructDeclarationSpecifier)spec1).hasSameContent((StructDeclarationSpecifier)spec2)){
						ret = new DeclarationSpecifier();
						ret.setInvalid();
						return ret;
					}
					ret = new StructDeclarationSpecifier((StructDeclarationSpecifier)spec1);
					break;
				case UNION:
					if (spec2 instanceof UnionDeclarationSpecifier && !((UnionDeclarationSpecifier)spec1).hasSameContent((UnionDeclarationSpecifier)spec2)){
						ret = new DeclarationSpecifier();
						ret.setInvalid();
						return ret;
					}
					ret = new UnionDeclarationSpecifier((UnionDeclarationSpecifier)spec1);
					break;
				case TYPEDEF_NAME:
					ret = new TypedefNameDeclarationSpecifier((TypedefNameDeclarationSpecifier)spec1);
					break;
				default:
					ret = new DeclarationSpecifier(spec1);
					
				}
			}
			else {
				switch (applyMask(spec2._val, TYPE_SPECIFIER_MASK)){
				case INT:
				case VOID:
				case CHAR:
					ret = new DeclarationSpecifier(spec2);
					break;
				case STRUCT:
					ret = new StructDeclarationSpecifier((StructDeclarationSpecifier)spec2);
					break;
				case UNION:
					ret = new UnionDeclarationSpecifier((UnionDeclarationSpecifier)spec2);
					break;
				case TYPEDEF_NAME:
					ret = new TypedefNameDeclarationSpecifier((TypedefNameDeclarationSpecifier)spec2);
					break;
				default:
					ret = new DeclarationSpecifier(spec2);
					
				}
			}
		}
		
		ret._val |= spec1._val | spec2._val;
		
		return ret;
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
			//Don't write
		return writer;
	}
	
	static public boolean test(int val, int mask){
		return (val & mask) != 0;
	}
	
	static public int applyMask(int val, int mask){
		return val & mask;
	}
	
	protected int _val;

	/* storage class specifier */
	static public final int EXTERN = 		0x00000001;
	static public final int STATIC = 		0x00000002;
	static public final int AUTO = 			0x00000004;
	static public final int REGISTER = 		0x00000008;
	static public final int TYPEDEF =		0x00000100;
	static public final int STORAGE_CLASS_MASK = EXTERN | STATIC | AUTO | REGISTER | TYPEDEF;
	
	/* type qualifier */
	static public final int CONST = 		0x00000010;
	static public final int RESTRICT =		0x00000020;
	static public final int VOLATILE = 		0x00000040;
	static public final int TYPE_QUALIFIER_MASK = CONST | RESTRICT | VOLATILE;
	
	/* function specifier */
	static public final int INLINE =		0x00000080;
	static public final int FUNCTION_SPECIFIER_MASK = INLINE;
	
	/* type specifier */
	static public final int VOID =			0x00000200;
	static public final int CHAR =			0x00000400;
	static public final int SHORT =			0x00000800;
	static public final int INT =			0x00001000;
	static public final int LONG =			0x00002000;
	static public final int FLOAT =			0x00004000;
	static public final int DOUBLE =		0x00008000;
	static public final int SIGNED =		0x00010000;
	static public final int UNSIGNED =		0x00020000;
	static public final int RESERVED1 = 	0x00040000;
	static public final int RESERVED2 = 	0x00080000;
	static public final int STRUCT =		0x00100000;
	static public final int UNION = 		0x00200000;
	static public final int ENUM =			0x00400000;
	static public final int TYPEDEF_NAME =	0x00800000;
	static public final int LONG_LONG =		0x01000000;
	static public final int BUILTIN = VOID | CHAR | INT;
	static public final int TYPE_SPECIFIER_MASK = VOID | CHAR | INT | STRUCT | UNION | TYPEDEF_NAME;
	
	/* invalid */
	static public final int INVALID =		0x02000000;
	
	@Override
	public String getName() {
		/*if (test(_val, INVALID)){
			return "Invalid type";
		}
		else if (test(_val, INT)){
			return "int";
		}
		else if (test(_val, VOID)){
			return "void";
		}
		else if (test(_val, CHAR)){
			return "char";
		}
		
		return "Oops, this statement should never be reached!";*/
		return "declaration specifier";
	}

	@Override
	public String getTypeName() {
		if (test(_val, INVALID)){
			return "Invalid type";
		}
		else if (test(_val, INT)){
			return "int";
		}
		else if (test(_val, VOID)){
			return "void";
		}
		else if (test(_val, CHAR)){
			return "char";
		}
		
		return "Oops, this statement should never be reached!";
	}
	
}
