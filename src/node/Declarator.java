package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The Declarator node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class Declarator extends NodeBase {

	/**
	 * Constructs a declarator from type and identifier.
	 */
	public Declarator(TypeNode type, Identifier id) {
		super();
		_type = type;
		_id = id;
	}
	
	public void setId(Identifier id){
		_id = id;
	}
	
	public Identifier getId(){
		return _id;
	}
	
	public void setType(TypeNode type){
		_type = type;
	}
	
	public TypeNode getType(){
		return _type;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		String[] properties = new String[]{"type", _type.getTypeName(), "identifier", _id.getVal()};
		traverseFormatterHead(writer, "declarator", properties);
		_type.traverse(writer);
		traverseFormatterTail(writer, "declarator", properties);
		return writer;
	}

	@Override
	public String getName() {

		return "declarator";
	}
	
	protected TypeNode _type;
	protected Identifier _id;

}
