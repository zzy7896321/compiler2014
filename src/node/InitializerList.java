package node;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * The InitializerList node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class InitializerList extends Initializer {

	/**
	 * Constructs an initializer from an initializer list.
	 */
	public InitializerList(LinkedList<InitializerListItem> list) {
		super();
		_list = list;
	}

	/**
	 * Access the initializer list.
	 * 
	 * @return	the list
	 */
	public LinkedList<InitializerListItem> getList(){
		return _list;
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		for (ListIterator<InitializerListItem> iter = _list.listIterator(); iter.hasNext(); iter.next().traverse(writer));
		
		return writer;
	}

	@Override
	public String getName() {

		return "initializer list";
	}

	protected LinkedList<InitializerListItem> _list;

	@Override
	public String getInitName() {
		
		return "initializer list";
	}
}
