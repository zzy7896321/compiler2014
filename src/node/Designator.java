package node;


/**
 * The Designator node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public abstract class Designator extends NodeBase {

	public Designator() {
		super();
	}

	/**
	 * Return the literal representation of the destination.
	 * 
	 * @return	the literal representation of the destination
	 */
	public abstract String getDestLiteral();
}
