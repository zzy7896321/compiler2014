package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The BinaryExpression node base class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public abstract class BinaryExpression extends ExpressionBase {

	/**
	 * Constructs Binary Expression from two expressions.
	 * 
	 * @param expr1		an expression
	 * @param expr2		a second expression
	 */
	public BinaryExpression(ExpressionBase expr1, ExpressionBase expr2){
		super();
		_left = expr1;
		_right = expr2;
	}
	
	/**
	 * Accesses the left oprand.
	 * 
	 * @return	the left oprand
	 */
	public ExpressionBase getLeft(){
		return _left;
	}
	
	/**
	 * Accesses the right oprand.
	 * 
	 * @return	the right operand
	 */
	public ExpressionBase getRight(){
		return _right;
	}
	
	@Override
	public void clearEvaluatedState(){
		_evaluated = false;
		_type = null;
		_value = null;
		_lvalue = false;
		
		_left.clearEvaluatedState();
		_right.clearEvaluatedState();;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		traverseFormatterHead(writer, this.getName(), null);
		_left.traverse(writer);
		_right.traverse(writer);
		traverseFormatterTail(writer, this.getName() ,null);
		return writer;
	}
	
	protected ExpressionBase _left, _right;

}
