package node;

import irpack.op;
import environment.Environment;
import environment.RuntimeEnvironment;

/**
 * The SizeofexprExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class SizeofexprExpression extends UnaryExpression {

	/**
	 * @param expr1
	 */
	public SizeofexprExpression(ExpressionBase expr1) {
		super(expr1);

	}
	
	@Override
	public String getName() {

		return "sizeof";
	}

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
			

		if (_operand instanceof PrimaryExpression){
			NodeBase val = ((PrimaryExpression) _operand).getVal();
			if (val instanceof IntegerConstant){
				_type = environment.TypeFactory.newIntType();
				_value = op.constant(config.TargetConfig.int_size);
				_lvalue = false;
				return;
			}
			else if (val instanceof CharConstant){
				_type = environment.TypeFactory.newIntType();
				_value = op.constant(config.TargetConfig.char_size);
				_lvalue = false;
				return;
			}
			else if (val instanceof StringConstant){
				_type = environment.TypeFactory.newIntType();
				_value = op.constant(config.TargetConfig.char_size * ((StringConstant) val).getVal().length());
				_lvalue = false;
			}
			else if (val instanceof Identifier){
				environment.SymbolInfo info = cur_env.lookup(((Identifier) val).getVal());
				if (info == null){
					report_error(((Identifier) val).getVal() + " is not declared", _operand);
					_type = null;
					return;
				}
				if (info instanceof environment.FunctionDesignator){
					report_error("sizeof shall not be applied to an expression with function type", _operand);
					_type = null;
					return;
				}
				else if (info instanceof environment.VariableSymbol){
					environment.Type var_type = info.getType();
					if (!var_type.isComplete()){
						report_error("sizeof shall not be applied to an expression with incomplete type", _operand);
						_type = null;
						return;
					}
					_type = environment.TypeFactory.newIntType();
					_value = op.constant(var_type.getSize());
					_lvalue = false;
				}
				else if (info instanceof environment.TypedefName){
					report_error("typedef name shall be bracketed in sizeof expression", _operand);
					_type = null;
					return;
				}
			}
			
			return;
		}
		
		_operand.checkEvaluate_NoGenerating(cur_env);
		if (_operand._type == null){
			_type = null;
			return;
		}
		
		if (!_operand._type.isComplete()){
			report_error("sizeof shall not be applied to an expression with incomplete type", _operand);
			_type = null;
			return;
		}
		
		// problems!!
		if (_operand._value instanceof environment.AddressConstant && ((environment.AddressConstant)_operand._value).getMask() != 0){
			report_error("sizeof shall not be applied to bit field", _operand);
			_type = null;
			return;
		}
		
		_type = environment.TypeFactory.newIntType();
		_value = op.constant(_operand._type.getSize());
		_lvalue = false;
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		if (_operand instanceof PrimaryExpression){
			NodeBase val = ((PrimaryExpression) _operand).getVal();
			if (val instanceof IntegerConstant){
				_type = environment.TypeFactory.newIntType();
				_value = config.TargetConfig.int_size;
				_lvalue = false;
				return;
			}
			else if (val instanceof CharConstant){
				_type = environment.TypeFactory.newIntType();
				_value = config.TargetConfig.char_size;
				_lvalue = false;
				return;
			}
			else if (val instanceof StringConstant){
				_type = environment.TypeFactory.newIntType();
				_value = config.TargetConfig.char_size * ((StringConstant) val).getVal().length();
				_lvalue = false;
			}
			else if (val instanceof Identifier){
				environment.SymbolInfo info = cur_env.lookup(((Identifier) val).getVal());
				if (info == null){
					report_error(((Identifier) val).getVal() + " is not declared", _operand);
					_type = null;
					return;
				}
				if (info instanceof environment.FunctionDesignator){
					report_error("sizeof shall not be applied to an expression with function type", _operand);
					_type = null;
					return;
				}
				else if (info instanceof environment.VariableSymbol){
					environment.Type var_type = info.getType();
					if (!var_type.isComplete()){
						report_error("sizeof shall not be applied to an expression with incomplete type", _operand);
						_type = null;
						return;
					}
					_type = environment.TypeFactory.newIntType();
					_value = var_type.getSize();
					_lvalue = false;
				}
				else if (info instanceof environment.TypedefName){
					report_error("typedef name shall be bracketed in sizeof expression", _operand);
					_type = null;
					return;
				}
			}
			
			return;
		}
		
		_operand.checkEvaluate_NoGenerating(cur_env);
		if (_operand._type == null){
			_type = null;
			return;
		}
		
		if (!_operand._type.isComplete()){
			report_error("sizeof shall not be applied to an expression with incomplete type", _operand);
			_type = null;
			return;
		}
		
		// problems!!
		if (_operand._value instanceof environment.AddressConstant && ((environment.AddressConstant)_operand._value).getMask() != 0){
			report_error("sizeof shall not be applied to bit field", _operand);
			_type = null;
			return;
		}
		
		_type = environment.TypeFactory.newIntType();
		_value = _operand._type.getSize();
		_lvalue = false;
		
	}

}
