package node;

import java.io.IOException;
import java.io.Writer;
import util.StringUtil;

/**
 * The FunctionDefinition node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 28, 2014
 */
public class FunctionDefinition extends NodeBase {

	/**
	 * Constructs a function definition from 
	 * a declaration specifier specifying the return type and function qualifier, 
	 * a declarator specifying the id and parameter list,
	 * a compound statement specifying the function body
	 * 
	 * @param
	 */
	public FunctionDefinition(DeclarationSpecifier spec, Declarator decl, CompoundStatement body) {
		_spec = spec;
		_decl = decl;
		_body = body;
	}
	
	/**
	 * Accesses the declaration specifier.
	 * @return	the declaration specifier
	 */
	public DeclarationSpecifier getDeclarationSpecifier(){
		return _spec;
	}
	
	/**
	 * Accesses the declarator.
	 * @return	the declarator
	 */
	public Declarator getDeclarator(){
		return _decl;
	}
	
	/**
	 * Accesses the statement.
	 * @return	the compound statement
	 */
	public CompoundStatement getStatement(){
		return _body;
	}
	
	/**
	 * Get a new invalid function definition.
	 *  
	 * @return	an invalid function definition
	 */
	static public FunctionDefinition getInvalidFunctionDefinition(){
		return new FunctionDefinition(null, null, null);
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		TypeNode type = _decl.getType();
		if (!(type instanceof DerivedTypeList && ((DerivedTypeList)type).listSize() == 1 &&
				((DerivedTypeList)type).getFirst() instanceof FunctionType) ){
			traverseFormatterHead(writer, this.getName(), null);
			writer.append(StringUtil.repeat(_indent, "\t")).append("<invalid>\n");
			traverseFormatterTail(writer, this.getName(), null);
			return writer;
		}
		
		FunctionType func = (FunctionType)(((DerivedTypeList)type).pollFirst());
		String[] properties = new String[]{ "return type", type.getTypeName(), "id", _decl.getId().getVal()};
		((DerivedTypeList)type).addFirst(func);		
		
		traverseFormatterHead(writer, this.getName(), properties);
			_spec.traverse(writer);			
			
			ParameterList list = func.getParameterList();
			list.traverse(writer);
			
			_body.traverse(writer);
			
		traverseFormatterTail(writer, this.getName(), properties);
		return writer;
	}

	@Override
	public String getName() {

		return "function definition";
	}
	
	protected DeclarationSpecifier _spec;
	protected Declarator _decl;
	protected CompoundStatement _body;

}
