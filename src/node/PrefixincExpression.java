package node;

import irpack.Operand;
import irpack.op;
import environment.Environment;
import environment.RuntimeEnvironment;
import environment.TypeFactory;

/**
 * The PrefixincExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class PrefixincExpression extends UnaryExpression {

	/**
	 * @param expr1
	 */
	public PrefixincExpression(ExpressionBase expr1) {
		super(expr1);

	}

	@Override
	public String getName() {

		return "prefix++";
	}
	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
			
		_operand.checkEvaluate(cur_env, runtime);
		if (_operand._type == null){
			_type = null;
			return;
		}
		
		/* lvalue requirement */
		if (!_operand._lvalue){
			report_error("lvalue required as operand of prefix ++", this);
			_type = null;
			return;
		}
		
		if (TypeFactory.isIntegerType(_operand._type)){
			_type = _operand._type;
			_lvalue = false;
			
			Operand result = op.add((Operand)_operand._value, (Operand)_operand._value, op.constant(1), runtime.entry);
			//result = op.mov(op.tempValue(runtime.entry), result, runtime.entry);
			
			_value = result;
			
			return;
		}
		
		else if (_operand._type instanceof environment.PointerType){
			environment.Type base_type = ((environment.PointerType)_operand._type).getBaseType();
			
			if (base_type instanceof environment.FunctionType){
				report_error("cannot apply prefix ++ to function pointer", _operand);
				_type = null;
				return;
			}
			
			if (!base_type.isComplete()){
				report_error("cannot apply prefix ++ to pointer to incomplete type", _operand);
				_type = null;
				return;
			}
			
			_type = _operand._type;
			_lvalue = false;
			
			Operand result = op.add((Operand)_operand._value, (Operand)_operand._value, op.constant(base_type.getSize()), runtime.entry);
			//result = op.mov(op.tempValue(runtime.entry), result, runtime.entry);
			
			_value = result;
			
			return;
		}
		
		report_invalid_operand_error(this, "prefix ++", _operand._type);
		_type = null;
		return;
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			
		_operand.checkEvaluate_NoGenerating(cur_env);
		if (_operand._type == null){
			_type = null;
			return;
		}
		
		/* lvalue requirement */
		if (!_operand._lvalue){
			report_error("lvalue required as operand of prefix ++", this);
			_type = null;
			return;
		}
		
		if (TypeFactory.isIntegerType(_operand._type)){
			_type = _operand._type;
			_value = null;
			_lvalue = false;
			return;
		}
		
		else if (_operand._type instanceof environment.PointerType){
			environment.Type base_type = ((environment.PointerType)_operand._type).getBaseType();
			
			if (base_type instanceof environment.FunctionType){
				report_error("cannot apply prefix ++ to function pointer", _operand);
				_type = null;
				return;
			}
			
			if (!base_type.isComplete()){
				report_error("cannot apply prefix ++ to pointer to incomplete type", _operand);
				_type = null;
				return;
			}
			
			_type = _operand._type;
			_value = null;
			_lvalue = false;
			
			return;
		}
		
		report_invalid_operand_error(this, "prefix ++", _operand._type);
		_type = null;
		return;
	}
}
