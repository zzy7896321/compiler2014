package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The StructDeclarator node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class StructDeclarator extends Declarator {

	/**
	 * Constructs a structDeclarator from type, id and an optional bitLengthExpr.
	 * BitLengthExpr should be null if it is not available.
	 * 
	 * @param type				the type
	 * @param id				the id
	 * @param bitLengthExpr		the bit length expression (optional)
	 */
	public StructDeclarator(TypeNode type, Identifier id, ExpressionBase bitLengthExpr) {
		super(type, id);
		_bitlength_expr = bitLengthExpr;
	}

	/**
	 * Constructs a structDeclarator from declarator and an optional bitLengthExpr.
	 * BitLengthExpr should be null if it is not available.
	 * 
	 * @param the declarator	the declarator
	 * @param bitLengthExpr		the bit length expression (optional)
	 */
	public StructDeclarator(Declarator decl, ExpressionBase bitLengthExpr){
		super(decl._type, decl._id);
		_bitlength_expr = bitLengthExpr;
	}
	
	public ExpressionBase getBitLengthExpr(){
		return _bitlength_expr;
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		String[] properties = new String[]{ "type", _type.getTypeName(), "identifier", (_id == null) ? "<anonymous>": _id.getVal(),
											"bit field = ", ""};
		if (_bitlength_expr instanceof PrimaryExpression && 
				((PrimaryExpression)_bitlength_expr).getVal() instanceof IntegerConstant)
			properties[5] = ((Constant)((PrimaryExpression)_bitlength_expr).getVal()).getValLiteral();
		traverseFormatterHead(writer, "declarator", properties);
		_type.traverse(writer);
		if (!(_bitlength_expr instanceof PrimaryExpression) || 
				!(((PrimaryExpression)_bitlength_expr).getVal() instanceof IntegerConstant))
			_bitlength_expr.traverse(writer);
		traverseFormatterTail(writer, "declarator", properties);
		return writer;
	}

	@Override
	public String getName() {

		return "struct declarator";
	}

	protected ExpressionBase _bitlength_expr;
}
