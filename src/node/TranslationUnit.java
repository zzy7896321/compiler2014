package node;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * The TranslationUnit node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 28, 2014
 */
public class TranslationUnit extends NodeBase {

	/**
	 * Constructs an empty translation unit.
	 */
	public TranslationUnit() {
		_list = new LinkedList<NodeBase>();
	}

	/**
	 * Add an external declaration to the translation unit.
	 * 
	 * @param item		the declaration to add
	 */
	public void add(NodeBase item){
		_list.add(item);
	}
	
	public LinkedList<NodeBase> getList(){
		return _list;
	}
	
	@Override
	public Writer traverse(Writer writer) throws IOException {
		traverseFormatterHead(writer, this.getName(), null);
		
		for (ListIterator<NodeBase> iter = _list.listIterator(); iter.hasNext(); iter.next().traverse(writer));
		
		traverseFormatterTail(writer, this.getName(), null);
		return writer;
	}
	
	@Override
	public String getName() {

		return "translation unit";
	}
	
	protected LinkedList<NodeBase> _list;

}
