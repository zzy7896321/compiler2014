package node;


/**
 * The InitializerBase node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public abstract class Initializer extends NodeBase {

	public Initializer() {
	}
	
	public abstract String getInitName();

}
