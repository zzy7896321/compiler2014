package node;

import java.io.IOException;
import java.io.Writer;

/**
 * The InitializerExpr node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 27, 2014
 */
public class InitializerExpr extends Initializer {

	/**
	 * Constructs an initializer from an expression.
	 * 
	 * @param expr		the expression
	 */
	public InitializerExpr(ExpressionBase expr) {
		super();
		_expr = expr;
	}

	/**
	 * Accesses the expression.
	 * 
	 * @return	the expression
	 */
	public ExpressionBase getExpr(){
		return _expr;
	}

	@Override
	public Writer traverse(Writer writer) throws IOException {
		if (_expr instanceof PrimaryExpression &&
				((PrimaryExpression)_expr).getVal() instanceof Constant) return writer;
		
		_expr.traverse(writer);
		
		return writer;
	}

	@Override
	public String getName() {

		return "initializer expr";
	}

	protected ExpressionBase _expr;

	@Override
	public String getInitName() {
		if (_expr instanceof PrimaryExpression &&
			((PrimaryExpression)_expr).getVal() instanceof Constant){
			return ((Constant)((PrimaryExpression)_expr).getVal()).getValLiteral();
		}
		return "expression";
	}
}
