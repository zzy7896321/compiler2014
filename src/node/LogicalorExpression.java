package node;

import irpack.Label;
import irpack.Operand;
import irpack.op;
import environment.AddressConstant;
import environment.Environment;
import environment.RuntimeEnvironment;
import environment.TypeFactory;


/**
 * The LogicalorExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Mar 26, 2014
 */
public class LogicalorExpression extends ShortCircuitExpression {

	public LogicalorExpression(ExpressionBase expr1, ExpressionBase expr2) {
		super(expr1, expr2);
	}

	@Override
	public String getName() {
		return "||";
	}
	
	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;
		
		/*
		 *	expressions
		 *	(false: fall)
		 * 	set result to 0
		 * 	J L2
		 * L1:
		 * 	(true: jump to L1)
		 * 	set result to 1
		 * L2:
		 * 
		 */
		
		Label L1 = null;
		Label L2 = null;
		if (! runtime.sc_target_set){
			L1 = runtime.entry.newLabel();
			L2 = runtime.entry.newLabel();
			
			runtime.sc_target_set = true;
			runtime.true_target = L1;
			runtime.false_target = null;
		}
		
		checkEvaluate_short_circuit(cur_env, runtime);
		
		
		if (L1 != null){
			Operand result = op.local(4);
			
			op.mov(result, op.constant(0), runtime.entry);
			op.j(L2, runtime.entry);
			
			runtime.entry.appendCode(L1);
			op.mov(result, op.constant(1), runtime.entry);
			
			runtime.entry.appendCode(L2);
			
			runtime.sc_target_set = false;
			
			_value = result;
			_lvalue = false;
		}
		
//		/*
//		 * Code for logical or:
//		 * 
//		 * expr1
//		 * mov result, 1
//		 * bne expr1, 0, L1
//		 * expr2
//		 * bne expr2, 0, L1
//		 * mov result, 0
//		 * L1:
//		 * 
//		 * */
//		
//		Label L1 = runtime.entry.newLabel();
//		
//		_left.checkEvaluate(cur_env, runtime);
//		if (_left._type == null){
//			_type = null;
//			return;
//		}
//
//		
//		if (!(environment.TypeFactory.isScalarType(_left._type))){
//			report_invalid_operand_error(_left, "||", _left._type);
//			_type = null;
//			return;
//		}
//		
//		Operand result = op.mov(op.local(4), op.constant(1), runtime.entry);
//		op.bne((Operand)_left._value, op.constant(0), L1, runtime.entry);
//		
//		_right.checkEvaluate(cur_env, runtime);
//		if (_right._type == null){
//			_type = null;
//			return;
//		}
//		
//		if (!(environment.TypeFactory.isScalarType(_right._type))){
//			report_invalid_operand_error(_right, "||", _right._type);
//			_type = null;
//			return;
//		}
//		
//		op.bne((Operand)_right._value, op.constant(0), L1, runtime.entry);
//		result = op.mov(result, op.constant(0), runtime.entry);
//		runtime.entry.appendCode(L1);
//		
//		_type = environment.TypeFactory.newIntType();
//		_lvalue = false;
		
//		
//		_value = result;		// note: two definitions of the same temporary value
	}
	
	@Override
	public void checkEvaluate_short_circuit(Environment cur_env, RuntimeEnvironment runtime){
		
		/*
		 * expr1
		 * expr2
		 * [L1:]
		 * 
		 */
		boolean succeed = true;
		
		
		Label true_target = runtime.true_target;
		Label false_target = runtime.false_target;
		
		Label L1 = (true_target == null) ? runtime.entry.newLabel() : true_target;
		
		if (_left instanceof ShortCircuitExpression){
			
			runtime.true_target = L1;		// to next
			runtime.false_target = null;	// fall
			
			_left.checkEvaluate(cur_env, runtime);
			
			succeed &= _left._type != null;
		}
		else {
			
			runtime.sc_target_set = false;
			
			_left.checkEvaluate(cur_env, runtime);
			if (!(environment.TypeFactory.isScalarType(_left._type))){
				report_invalid_operand_error(_left, "||", _left._type);
				succeed = false;
			}
		
			runtime.sc_target_set = true;
			
			op.bne((Operand) _left._value, op.constant(0), L1, runtime.entry);
		}
		
		if (_right instanceof ShortCircuitExpression){
			
			runtime.true_target = true_target;
			runtime.false_target = false_target;
			
			_right.checkEvaluate(cur_env, runtime);
			
			succeed &= _right._type != null;
		}
		else {
			
			runtime.sc_target_set = false;
			
			_right.checkEvaluate(cur_env, runtime);
			if (!(environment.TypeFactory.isScalarType(_right._type))){
				report_invalid_operand_error(_right, "||", _right._type);
				succeed = false;
			}
			
			runtime.sc_target_set = true;
			
			if (true_target == null){
				
				if (false_target != null){
					op.beq((Operand) _right._value, op.constant(0), false_target, runtime.entry);
				}
			}
			
			else {
				op.bne((Operand) _right._value, op.constant(0), true_target, runtime.entry);
				if (false_target != null){
					op.j(false_target, runtime.entry);
				}
			}
		}
		
		if (true_target == null){
			runtime.entry.appendCode(L1);
		}
		
		runtime.true_target = true_target;
		runtime.false_target = false_target;
		
		if (succeed) _type = environment.TypeFactory.newIntType();
	}
	
	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;
			

		_left.checkEvaluate_NoGenerating(cur_env);
		if (_left._type == null){
			_type = null;
			return;
		}
		_right.checkEvaluate_NoGenerating(cur_env);
		if (_right._type == null){
			_type = null;
			return;
		}
		
		if (!(environment.TypeFactory.isScalarType(_left._type) && environment.TypeFactory.isScalarType(_right._type))){
			report_invalid_operand_error(this, "||", _left._type, _right._type);
			_type = null;
			return;
		}
		
		_type = environment.TypeFactory.newIntType();
		if (TypeFactory.isIntegerType(_left._type)){
			if (_left._value instanceof Number){
				
				if (((Number)_left._value).intValue() != 0){
					_value = 1;
					_lvalue = false;
					return ;
				} else {
					_value = 0;
				}
			}
			else {
				_value = null;
			}
		}
		else if (_left._type instanceof environment.PointerType){
			if (!_left._lvalue && _left._value instanceof environment.AddressConstant){
				AddressConstant addr = (AddressConstant)_left._value;
				if (addr.getBase() == null){
					if (addr.getOffset() != 0){
						_value = 1;
						_lvalue = false;
						return;
					}
					else {
						_value = 0;
					}
				}
				else {
					if (addr.getOffset() == 0){
						_value = 1;
						_lvalue = false;
						return;
					}
					else {
						_value = null;
					}
				}
			}
			else {
				_value = null;
			}
		}
		
		if (TypeFactory.isIntegerType(_right._type)){
			if (_right._value instanceof Number){
				if (((Number)_right._value).intValue() != 0){
					_value = 1;
					_lvalue = false;
					return ;
				} else {
					if (_value instanceof Integer && ((Integer)_value).intValue() == 0){
						_lvalue = false;
						return;
					}
					else {
						_value = null;
						_lvalue = false;
						return ;
					}
				}
			}
			else {
				_value = null;
				_lvalue = false;
				return ;
			}
		}
		else if (_right._type instanceof environment.PointerType){
			if (!_right._lvalue && _right._value instanceof environment.AddressConstant){
				AddressConstant addr = (AddressConstant)_right._value;
				if (addr.getBase() == null){
					if (addr.getOffset() != 0){
						_value = 1;
						_lvalue = false;
						return;
					}
					else {
						if (_value instanceof Integer && ((Integer)_value).intValue() == 0){
							_lvalue = false;
							return;
						}
						else {
							_value = null;
							_lvalue = false;
							return ;
						}
					}
				}
				else {
					if (addr.getOffset() == 0){
						_value = 1;
						_lvalue = false;
						return;
					}
					else {
						_value = null;
						_lvalue = false;
						return;
					}
				}
			}
			else {
				_value = null;
				_lvalue = false;
				return;
			}
		}
	}
}
