package node;


import irpack.Operand;
import irpack.op;
import environment.Environment;
import environment.RuntimeEnvironment;

/**
 * The MemberExpression node class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 10, 2014
 */
public class MemberdotExpression extends BinaryExpression {

	/**
	 * @param expr1
	 * @param expr2
	 */
	public MemberdotExpression(ExpressionBase expr1, ExpressionBase expr2) {
		super(expr1, expr2);

	}

	@Override
	public String getName() {

		return "member expr1.id2";
	}

	@Override
	public void checkEvaluate(Environment cur_env, RuntimeEnvironment runtime) {
		if (_evaluated) return;
		_evaluated = true;

		_left.checkEvaluate(cur_env, runtime);
		if (_left._type == null){
			_type = null;
			return;
		}
		
		/* left operand is required to be a structure or an union */
		if (!(_left._type instanceof environment.StructOrUnionType)){
			report_error("request for member in something that is not struct or union", _left);
			_type = null;
			return;
		}
		
		/* left operand should be complete */
		if (!((environment.StructOrUnionType)_left._type).isComplete()){
			report_error("cannot take member of incomplete struct or union", _left);
			_type = null;
			return;
		}
		
		/* right operand should be an identifier */
		String name = null;
		if (_right instanceof PrimaryExpression){
			NodeBase val = ((PrimaryExpression) _right).getVal();
			if (val instanceof Identifier){
				name = ((Identifier) val).getVal();
			}
		}
		if (name == null){
			report_error("expecting an identifier for member op", _right);
			_type = null;
			return;
		}
		
		/* lookup the field */
		environment.StructFieldInfo field_info = ((environment.StructOrUnionType)_left._type).getField(name);
		if (field_info == null){
			report_error(name + " is not a member of " + ((environment.StructOrUnionType)_left._type).getTag(), this);
			_type = null;
			return;
		}
		
		/* set result */
		_type = field_info.getType();
				
		Operand result = (Operand)_left._value;
		result = op.add(op.temp(runtime.entry), result, op.constant(field_info.getOffset()), runtime.entry);
		/* non bit-field*/
		if (field_info.getBitLength() == -1){
			
			if (_type instanceof environment.ArrayType){
				_lvalue = false;
				
			}
			else if (_type instanceof environment.StructOrUnionType){
				_lvalue = _left._lvalue;
				
			}
			else {
				_lvalue = _left._lvalue;
				
				result = op.indirection(result, 0, _type.getSize(), runtime.entry);
			}
		}

		/* bit-field */
		else {
			_lvalue = _left._lvalue;
			
			result = op.bitField(result, 0, field_info.getMask(), runtime.entry);
		}
		
		_value = result;
		implicitConversion_NoGenerating(this);
		return;
	}

	@Override
	public void checkEvaluate_NoGenerating(Environment cur_env) {
		if (_evaluated) return;
		_evaluated = true;

		_left.checkEvaluate_NoGenerating(cur_env);
		if (_left._type == null){
			_type = null;
			return;
		}
		
		/* left operand is required to be a structure or an union */
		if (!(_left._type instanceof environment.StructOrUnionType)){
			report_error("request for member in something that is not struct or union", _left);
			_type = null;
			return;
		}
		
		/* left operand should be complete */
		if (!((environment.StructOrUnionType)_left._type).isComplete()){
			report_error("cannot take member of incomplete struct or union", _left);
			_type = null;
			return;
		}
		
		/* right operand should be an identifier */
		String name = null;
		if (_right instanceof PrimaryExpression){
			NodeBase val = ((PrimaryExpression) _right).getVal();
			if (val instanceof Identifier){
				name = ((Identifier) val).getVal();
			}
		}
		if (name == null){
			report_error("expecting an identifier for member op", _right);
			_type = null;
			return;
		}
		
		/* lookup the field */
		environment.StructFieldInfo field_info = ((environment.StructOrUnionType)_left._type).getField(name);
		if (field_info == null){
			report_error(name + " is not a member of " + ((environment.StructOrUnionType)_left._type).getTag(), this);
			_type = null;
			return;
		}
		
		/* set result */
		_type = field_info.getType();
		if (_left._value != null){
			/* non bit-field*/
			if (field_info.getBitLength() == -1){
				_value = (_left._value instanceof environment.AddressConstant) ?
						((environment.AddressConstant)_left._value).newAddrWithOffsetAddedTo(field_info.getOffset()) :
							null;
			}
			
			/* bit-field */
			else {
				_value = (_left._value instanceof environment.AddressConstant) ?
						((environment.AddressConstant)_left._value).newAddrWithOffsetAddedTo(field_info.getOffset())
						.setMask(field_info.getMask()): null;
			}
		
		}
		else {
			_left._value = null;
		}
		
		_lvalue = _left._lvalue;
		//		&& !((_type instanceof environment.ArrayType) || (_type instanceof environment.FunctionType));
		implicitConversion_NoGenerating(this);
	}

}
