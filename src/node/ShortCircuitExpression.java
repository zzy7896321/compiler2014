package node;

import environment.Environment;
import environment.RuntimeEnvironment;

public abstract class ShortCircuitExpression extends BinaryExpression {

	public ShortCircuitExpression(ExpressionBase expr1, ExpressionBase expr2) {
		super(expr1, expr2);
	}

	/**
	 * Checks and evaluates short circuit expressions.
	 * 
	 * @param cur_env
	 * @param runtime
	 */
	public abstract void checkEvaluate_short_circuit(Environment cur_env, RuntimeEnvironment runtime);

}