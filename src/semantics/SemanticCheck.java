package semantics;

import environment.*;
import node.*;
import irpack.*;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Iterator;
import java.util.Stack;

import main.ErrorHandling;

/**
 * The SemanticCheck class provides utilities to perform semantic check and
 * generate intermediate representation.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 1, 2014
 */
public class SemanticCheck {
	
	public SemanticCheck(){
		_top_env = null;
		_cur_env = null;
		_ir = null;
	}
	
	public IntermediateRepresentation checkAndGenerate(NodeBase root){
		if (!(root instanceof TranslationUnit)) return null;
		
		/* Set up running instance for semantic checking and ir generating for ExpressionBase.*/
		setRunningInstance(this);
		
		/* Set up environment. Create new ir output.*/
		init_env();
		
		/* Set up built-in environment. */
		include_library();
		
		/* Check semantics and generate ir code. */
		LinkedList<NodeBase> list = ((TranslationUnit)root).getList();
		NodeBase external_definition = null;
		for (ListIterator<NodeBase> iter = list.listIterator(); iter.hasNext();){
			external_definition = iter.next();
			if (external_definition instanceof node.Declaration){
				checkExternalDeclaration((node.Declaration)external_definition);
			}
			else if (external_definition instanceof node.FunctionDefinition){
				checkFunctionDefinition((node.FunctionDefinition)external_definition);
			}
			else{
				report_error(String.format("external declaration expected, but %s found", external_definition.getName()), external_definition);
			}
		}
		
		/* Check if all external symbols are defined. */
		checkExternalSymbols(root);
		
		/* Clean up. */
		IntermediateRepresentation ir = _ir;
		setRunningInstance(null);
		clean();
		
		return ir;
	}
	
	private void include_library() {
		TranslationUnit root = StandardLibrary.get_stdlib();
		
		NodeBase external_definition = null;
		for (ListIterator<NodeBase> iter = root.getList().listIterator(); iter.hasNext();){
			external_definition = iter.next();
			if (external_definition instanceof node.Declaration){
				checkExternalDeclaration((node.Declaration)external_definition);
			}
			else if (external_definition instanceof node.FunctionDefinition){
				checkFunctionDefinition((node.FunctionDefinition)external_definition);
			}
		}
		
		// TODO insert real code of built-ins here
		LinkedList<SymbolInfo> external_objects = Environment.getExternalObjectList();
		SymbolInfo cur_info = null;
		for (ListIterator<SymbolInfo> iter = external_objects.listIterator(); iter.hasNext();){
			cur_info = iter.next();
			if (cur_info instanceof FunctionDesignator){
				FunctionDesignator info = (FunctionDesignator)cur_info;
				info.setDefined(true);				// dummy code!!
			}
			else if (cur_info instanceof VariableSymbol){
				VariableSymbol info = (VariableSymbol)cur_info;
				info.setInitialized(true);  		// dummy value!!
			}
		}
	}

	public boolean checkExternalSymbols(NodeBase root){
		boolean final_result = true;
		
		LinkedList<SymbolInfo> extern_objects = environment.Environment.getExternalObjectList();
		
		SymbolInfo cur_info = null;
		for (ListIterator<SymbolInfo> iter = extern_objects.listIterator(); iter.hasNext();){
			cur_info = iter.next();
			if (cur_info instanceof VariableSymbol){				
				VariableSymbol info = (VariableSymbol)cur_info;
				
				cur_info = _top_env.lookup(cur_info.getName());
				if (info != cur_info){
					if (!(cur_info instanceof VariableSymbol)){
						report_error("conlicting symbol usage for external linkage object " + cur_info.getName(), root);
						final_result = false;
						continue;
					}
					if (!cur_info.getType().equals(info.getType())){
						report_error("conflicting type for external linkage symbols " + cur_info.getName(), root);
						final_result = false;
						continue;
					}
				}
				else {
					if (!info.getType().isComplete()){
						report_error(String.format("type of %s is incomplete at the end of translation unit", info.getName()), root);
						final_result = false;
						continue;
					}
					
					info.updateMemLoc();
					
					if (!info.isInitialized()){
						info.setInitialized(true);
						
						_ir.addStaticVariableEntry(new StaticVarEntry(info.getMemLoc(), null));
					}
				}
			}
			else if (cur_info instanceof FunctionDesignator){
				FunctionDesignator info = (FunctionDesignator)cur_info;
				
				cur_info = _top_env.lookup(cur_info.getName());
				if (info != cur_info){
					if (!(cur_info instanceof FunctionDesignator)){
						report_error("conflicting symbol usage for external linkage object " + cur_info.getName(), root);
						final_result = false;
						continue;
					}
					if (!cur_info.getType().equals(info.getType())){
						report_error("conflicting type for external linkage object " + cur_info.getName(), root);
						final_result = false;
						continue;
					}
				}
				else {
					if (!info.isDefined()){
						report_error("undefined function " + info.getName(), root);
						final_result = false;
						continue;
					}
				}
			}
			else {
				report_error("internal error: impossible branch", root);
				final_result = false;
				continue;
			}
		}
		
		cur_info = _top_env.lookup("main");
		if (cur_info instanceof FunctionDesignator){
			FunctionDesignator info = (FunctionDesignator)cur_info;
			if (info.isDefined()){
				environment.FunctionType func = (environment.FunctionType) info.getType();
				if (func.getReturnType() instanceof environment.IntType){
					LinkedList<Type> para_type_list = func.getParameterTypeList();
					
					if (para_type_list.isEmpty()){
						//ok
					}
					else if (para_type_list.size() == 2){
						ListIterator<Type> type_iter = para_type_list.listIterator();
						Type type = type_iter.next();
						if (!(type instanceof environment.IntType)){
							report_error("invalid entry function prototype", root);
							final_result = false;
						}
						
						type = type_iter.next();
						if (!type.equals(new environment.PointerType(new environment.PointerType(TypeFactory.newCharType())))){
							report_error("invalid entry function prototype", root);
							final_result = false;
						}
					}
					else {
						report_error("invalid entry function prototype", root);
						final_result = false;
					}
				}
				else {
					report_error("invalid entry function prototype", root);
					final_result = false;
				}
			}
		}
		else {
			report_error("program entry function 'main' is not found.", root);
			final_result = false;
		}
		
		return final_result;
	}
	
	
	public boolean checkExternalDeclaration(Declaration root){
		/* Check declaration specifier. */
		DeclarationSpecifier spec = root.getDeclarationSpecifier();
		Type spec_type = checkDeclarationSpecifier(spec);
		if (spec_type == null){
			return false;
		}
		
		/* Warning of useless declaration. */
		LinkedList<Declarator> list = root.getList();
		if (list.isEmpty() && (!(spec_type instanceof environment.StructOrUnionType) || 
				((environment.StructOrUnionType)spec_type).getTag() == null )){
			report_warning("useless declaration", root);
		}
		
		
		/* Check declarators. */
		Declarator decl = null;
		Type decl_type = null;
		SymbolInfo info = null;
		boolean final_result = true;
		
		/* Typedef, no code to generate */
		if (spec.isTypedef()){
			for (ListIterator<Declarator> iter = list.listIterator(); iter.hasNext();){
				decl = iter.next();
				decl_type = composeType(spec_type, decl.getType());
				if (decl_type == null){
					final_result = false;
				}
				
				if (decl instanceof InitDeclarator){
					report_error("initializer in typedef", decl);
					final_result = false;
				}
				
				info = _cur_env.lookup(decl.getId().getVal());
				if (info != null){
					if (info instanceof environment.TypedefName){
						if (!info.getType().equals(decl_type)){
							report_error("conflicting type for declared typedef name", decl);
							final_result = false;
						}
						else {
							if (info.getType().isComplete() ^ decl_type.isComplete()){
								report_error("conflicting type for declared typedef name", decl);
								final_result = false;
							}
						}
					}
					else {
						report_error("symbol redeclared as a different kind", decl);
						final_result = false;
					}
				}
				else {
					info = new TypedefName(decl.getId().getVal(), decl_type);
					_cur_env.insert(info);
				}
			}
		}
		
		/* Variable definition or function declaration. */
		else {
			for (ListIterator<Declarator> iter = list.listIterator(); iter.hasNext();){
				/* Check declared type. */
				decl = iter.next();
				decl_type = composeType(spec_type, decl.getType());
				if (decl_type == null){
					final_result = false;
				}
				
				/* void type, error*/
				else if (decl_type instanceof environment.VoidType){
					report_error("cannot create a void type object", decl);
					final_result = false;
				}
				
				/* function declaration*/
				else if (decl_type instanceof environment.FunctionType){
					if (decl instanceof node.InitDeclarator){
						report_error("function declaration is provided with initializer", decl);
						final_result = false;
					}
					else {
						info = _cur_env.lookup(decl.getId().getVal());
						if (info == null){
							_cur_env.insert(new FunctionDesignator(decl.getId().getVal(), (environment.FunctionType)decl_type));
						}
						else if (info instanceof environment.FunctionDesignator){
							if (!(info.getType().equals(decl_type))){
								report_error("conflicting type declared for the symbol", decl);
								final_result = false;
							}
						}
						else {
							report_error("symbol redeclared as a different kind", decl);
							final_result = false;
						}
					}
				}
				
				/* variable definition */
				else {	// other types
					info = _cur_env.lookup(decl.getId().getVal());
					
					/* definition with initializer. Static variable entry is set in checkInitializerForExternalDeclaration. */
					if (decl instanceof node.InitDeclarator){
						if (info == null){
							info = new VariableSymbol(decl.getId().getVal(), decl_type, SymbolInfo.EXTERNAL_LINKAGE);	// with MemLoc created
							_cur_env.insert(info);
							final_result &= checkInitializerForExternalDeclaration((VariableSymbol)info, ((node.InitDeclarator) decl).getInitializer());
						}
						else {							
							if (!decl_type.equals(info.getType())){
								report_error("conflicting type for " + decl.getId().getVal(), decl);
								final_result = false;
							}
							else if (((VariableSymbol)info).isInitialized()){
								report_error("redefinition of " + decl.getId().getVal(), decl);
								final_result = false;
							}
							else {
								if (info.getType() instanceof environment.ArrayType){
									if (!info.getType().isComplete() && decl_type.isComplete()){
										((environment.ArrayType)info.getType()).setElementCount(((environment.ArrayType)decl_type).getElementCount());
										((environment.ArrayType)info.getType()).setComplete(true);
										((VariableSymbol) info).updateMemLoc();
									}
								}
								final_result &= checkInitializerForExternalDeclaration((VariableSymbol)info, ((node.InitDeclarator) decl).getInitializer());
							}
						}
					}
					
					/* definition without initializer, tentative definition */
					else {
						if (info == null){
							_cur_env.insert(new VariableSymbol(decl.getId().getVal(), decl_type, SymbolInfo.EXTERNAL_LINKAGE));		// with MemLoc created
						}
						else {
							//tentative definition
							if (!decl_type.equals(info.getType())){
								report_error("conflicting type for " + decl.getId().getVal(), decl);
								final_result = false;
							}
							else {
								if (info.getType() instanceof environment.ArrayType){
									if (!info.getType().isComplete() && decl_type.isComplete()){
										((environment.ArrayType)info.getType()).setElementCount(((environment.ArrayType)decl_type).getElementCount());
										((environment.ArrayType)info.getType()).setComplete(true);
										((VariableSymbol) info).updateMemLoc();
									}
								}
							}
						}
					}
				}
			}
		}
		
		return final_result;
	}

	public boolean checkFunctionDefinition(node.FunctionDefinition root){
		/* check declaration specifier */
		DeclarationSpecifier spec = root.getDeclarationSpecifier();
		if (spec.isTypedef()){
			report_error("typedef in function definition", spec);
			return false;
		}
		
		Type spec_type = checkDeclarationSpecifier(spec);
		if (spec_type == null){
			return false;
		}
		
		
		/* check declarator */
		Declarator declarator = root.getDeclarator();
		Type decl_type = composeType(spec_type, declarator.getType());
		if (decl_type == null){
			return false;
		}
		if (!(decl_type instanceof environment.FunctionType)){
			report_error("non function type declared in function definition", declarator);
			return false;
		}
		environment.FunctionType func_type = (environment.FunctionType) decl_type;
		
		/* add definition to environment */
		SymbolInfo info = _top_env.lookup(declarator.getId().getVal());
		FunctionDesignator func_des = null;
		if (info == null){
			func_des = new FunctionDesignator(declarator.getId().getVal(), (environment.FunctionType)decl_type);
			_top_env.insert(func_des);
		}
		else {
			if (!(info instanceof FunctionDesignator)){
				report_error("symbol redeclared as another kind", declarator);
				return false;
			}
			func_des = (FunctionDesignator)info;
			if (func_des.isDefined()){
				report_error("redefinition of function " + declarator.getId().getVal(), declarator);
				return false;
			}
			if (!func_des.getType().equals(decl_type)){
				report_error("incompatible function type in function definition", root);
				return false;
			}
		}
		
		/*set up new environment, add parameters*/
		_cur_env = new environment.Environment(_top_env);
		_runtime = new RuntimeEnvironment(((environment.FunctionType)decl_type).getReturnType(),
					new FunctionEntry(func_des.getName(), _ir));
		
		/* optional return value address (for struct/union) */
		if (func_type.getReturnType() instanceof environment.StructOrUnionType){
			_runtime.entry.setReturnValueAddress(func_type.getReturnType().getSize());
		}
		
		/* in case of (void) function prototype */
		if (!((environment.FunctionType)decl_type).getParameterTypeList().isEmpty()){
			ListIterator<Declarator> para_iter = ((node.FunctionType)((node.DerivedTypeList)declarator.getType()).getFirst()).getParameterList().getList().listIterator();
			ListIterator<Type> type_iter = ((environment.FunctionType)decl_type).getParameterTypeList().listIterator();
			for (; para_iter.hasNext(); ){
				Declarator dtor = para_iter.next();
				if (dtor.getId() == null){
					report_error("parameter name omitted in function definition", dtor);
					_cur_env = _cur_env.getPrevious();
					_runtime = null;
					return false;
				}
				
				// register parameter in function entry
				VariableSymbol arg_info = new VariableSymbol(dtor.getId().getVal(), type_iter.next(), SymbolInfo.ARG);
				_cur_env.insert(arg_info);
				_runtime.entry.registerArgument((Argument)arg_info.getMemLoc());
			}
		}		
		
		//check body
		
		boolean result = checkCompoundStatement(root.getStatement());
		
		//restore environment and record function entry
		func_des.setDefined(true);
		_runtime.entry.finish();
		_ir.addFunctionEntry(_runtime.entry);
		
		_cur_env = _cur_env.getPrevious();
		_runtime = null;
		
		return result;
	}
	
	/**
	 * Checks compound statement. Environment shall be properly set before checkCompoundStatement is invoked and
	 * be properly cleared after checkCompoundStatement.
	 * 
	 * @param stmt
	 * @return
	 */
	public boolean checkCompoundStatement(CompoundStatement stmt){
		boolean final_result = true;
		
		NodeBase cur_item = null;;
		for (ListIterator<NodeBase> iter = stmt.getList().listIterator(); iter.hasNext(); ){
			cur_item = iter.next();
			if (cur_item instanceof Declaration){
				final_result &= checkBlockLevelDeclaration((Declaration)cur_item);
			}
			else if (cur_item instanceof Statement){
				final_result &= checkStatement((Statement)cur_item);
			}
			else{
				report_error("invalid block item in compound statement", cur_item);
				final_result = false;
			}
		
		}
		
		return final_result;
	}
	
	public boolean checkBlockLevelDeclaration(Declaration root) {
		/* Check declaration specifier. */
		DeclarationSpecifier spec = root.getDeclarationSpecifier();
		Type spec_type = checkDeclarationSpecifier(spec);
		if (spec_type == null){
			return false;
		}
		
		/* Warning of useless declaration. */
		LinkedList<Declarator> list = root.getList();
		if (list.isEmpty() && (!(spec_type instanceof environment.StructOrUnionType) || 
				((environment.StructOrUnionType)spec_type).getTag() == null )){
			report_warning("useless declaration", root);
		}
		
		/* Check declarators. */
		Declarator decl = null;
		Type decl_type = null;
		SymbolInfo info = null;
		boolean final_result = true;
		
		/* Typedef, no code to generate */
		if (spec.isTypedef()){
			for (ListIterator<Declarator> iter = list.listIterator(); iter.hasNext();){
				decl = iter.next();
				decl_type = composeType(spec_type, decl.getType());
				if (decl_type == null){
					final_result = false;
				}
				
				if (decl instanceof InitDeclarator){
					report_error("initializer in typedef", decl);
					final_result = false;
				}
				
				info = _cur_env.lookupOnCurrentLevel(decl.getId().getVal());
				if (info != null){
					if (info instanceof environment.TypedefName){
						if (!info.getType().equals(decl_type)){
							report_error("conflicting type for declared typedef name", decl);
							final_result = false;
						}
						else {
							if (info.getType().isComplete() ^ decl_type.isComplete()){
								report_error("conflicting type for declared typedef name", decl);
								final_result = false;
							}
						}
					}
					else {
						report_error("symbol redeclared as a different kind", decl);
						final_result = false;
					}
				}
				else {
					info = new TypedefName(decl.getId().getVal(), decl_type);
					_cur_env.insert(info);
				}
			}
		}
		
		/* Variable definition or function declaration */
		else {
			for (ListIterator<Declarator> iter = list.listIterator(); iter.hasNext();){
				/* Check declared type. */
				decl = iter.next();
				decl_type = composeType(spec_type, decl.getType());
				if (decl_type == null){
					final_result = false;
				}
				
				/* void type, error */
				else if (decl_type instanceof environment.VoidType){
					report_error("cannot create a void type object", decl);
					final_result = false;
				}
				
				/* function declaration */
				else if (decl_type instanceof environment.FunctionType){
					if (decl instanceof node.InitDeclarator){
						report_error("function declaration is provided with initializer", decl);
						final_result = false;
					}
					else {
						info = _cur_env.lookupOnCurrentLevel(decl.getId().getVal());
						if (info == null){
							_cur_env.insert(new FunctionDesignator(decl.getId().getVal(), (environment.FunctionType)decl_type));
						}
						else if (info instanceof environment.FunctionDesignator){
							if (!(info.getType().equals(decl_type))){
								report_error("conflicting type declared for the symbol", decl);
								final_result = false;
							}
						}
						else {
							report_error("symbol redeclared as a different kind", decl);
							final_result = false;
						}
					}
				}
				
				/* variable definition */
				else {	// other types
					info = _cur_env.lookupOnCurrentLevel(decl.getId().getVal());
					
					if (info != null){
						report_error("redefinition of " + decl.getId().getVal(), decl);
						final_result = false;
					}
					else {
						if (decl instanceof node.InitDeclarator){
							if (info == null){
								info = new VariableSymbol(decl.getId().getVal(), decl_type, SymbolInfo.NO_LINKAGE);
								_cur_env.insert(info);
								_runtime.entry.registerLocalVariable(((VariableSymbol)info).getMemLoc());
								final_result &= checkInitializerForBlockLevelDeclaration((VariableSymbol)info, ((node.InitDeclarator) decl).getInitializer());
							}

						}
						else {
							if (info == null){
								if (!decl_type.isComplete()){
									report_error("cannot create object of incomplete type", decl);
									final_result = false;
								}
								else {
									info = new VariableSymbol(decl.getId().getVal(), decl_type, SymbolInfo.NO_LINKAGE);
									_cur_env.insert(info);
									_runtime.entry.registerLocalVariable(((VariableSymbol)info).getMemLoc());
								}
							}

						}
					}
				}
			}
		}
		
		return final_result;
	}

	public boolean checkInitializerForBlockLevelDeclaration(VariableSymbol info, Initializer init){
		Type elem_type = info.getType();
		
		/* Scalar type*/
		if (environment.TypeFactory.isScalarType(elem_type)){
			if (checkInitializerForBlockLevelDeclarationWithScalarType(info, init)){
				info.setInitialized(true);
				return true;
			}
			else {
				return false;
			}
		}		
		
		/* Array type*/
		else if (elem_type instanceof environment.ArrayType){
			/* string literal */
			if (TypeFactory.isCharArray(elem_type)){
				Object val = null;
				if ((val = checkInitializerForExternalDeclarationWithCharArrayAndStringLiteral(elem_type, init)) != null){
					info.setInitialized(true);
					if (info.getMemLoc().getSize() == 0){
						info.getMemLoc().setSize(info.getType().getSize());
					}
					
					String str = (String)val;
					for (int i = 0; i < str.length(); ++i){
						Operand result = op.indirection(op.address(info.getMemLoc(), i, _runtime.entry), 
								0, 1, _runtime.entry);
						op.mov(result, op.constant(str.charAt(i)), _runtime.entry);
					}
					
					return true;
				}
			}
			
			if (!(init instanceof InitializerList)){
				report_error("array type requires the initializer to be a list", init);
				return false;
			}
			
			if (checkInitializerForBlockLevelDeclarationWithAggregateType(elem_type, info.getMemLoc(), (InitializerList)init, 0)){
				info.setInitialized(true);
				
				info.updateMemLoc();
				
				return true;
			}
			else {
				return false;
			}
		}
		
		
		else if (elem_type instanceof environment.StructOrUnionType){
			if (!elem_type.isComplete()){
				report_error("cannot initialize incomplete struct or union type", init);
				return false;
			}
			
			if (init instanceof InitializerExpr){
				ExpressionBase init_expr = ((InitializerExpr) init).getExpr();
				
				init_expr = new AssignmentExpression(new PrimaryExpression(new Identifier(info.getName())), init_expr);
				init_expr.checkEvaluate(_cur_env, _runtime);
				
				return init_expr.getType() != null;
			}
			
			if (checkInitializerForBlockLevelDeclarationWithAggregateType(elem_type, info.getMemLoc(), (InitializerList)init, 0)){
				info.setInitialized(true);
				return true;
			}
			else {
				return false;
			}
		}
		
		return false;
	}
	
	public boolean checkInitializerForBlockLevelDeclarationWithScalarType(VariableSymbol info, node.Initializer init){
		ExpressionBase init_expr = null;
		
		if (init instanceof node.InitializerExpr){
			init_expr = ((node.InitializerExpr) init).getExpr();
			
		}
		else if (init instanceof node.InitializerList){
			LinkedList<InitializerListItem> list = ((node.InitializerList) init).getList();
			if (list.size() >= 1){
				node.InitializerListItem init_item = list.getFirst();
				if (!init_item.getDesignatorList().isEmpty()){
					report_error("designator in scalar type initializer", init);
					return false;
				}
				if (init_item.getInitializer() instanceof InitializerExpr){
					init_expr = ((InitializerExpr)init_item.getInitializer()).getExpr();
					
					if (list.size() > 1){
						report_warning("excess elements in scalar type initializer", init);
					}
				}
				else {
					report_error("non-expression initializer for scalar type", init);
					return false;
				}
			}
			else {
				report_error("empty initializer list provided to scalar type variable", init);
				return false;
			}
		}
		
		init_expr = new AssignmentExpression(new PrimaryExpression(new Identifier(info.getName())), init_expr);
		
		init_expr.checkEvaluate(_cur_env, _runtime);
		return init_expr.getType() != null;
	}
	
	public boolean checkInitializerForBlockLevelDeclarationWithAggregateType(Type elem_type, MemLoc memloc, InitializerList init, int base_offset){
		/* initialize current object stack */
		Stack<Type> type_stack = new Stack<Type>();		// type of current object
		Stack<Integer> sub_object_stack = new Stack<Integer>();		// sub-object count of current object
		Stack<Integer> cur_offset_stack = new Stack<Integer>();		// base offset of current object
		
		type_stack.push(elem_type);
		sub_object_stack.push(0);
		cur_offset_stack.push(base_offset);
		
		/* current object variables */
		int sub_object = 0;
		Type cur_type = null;
		
		/* Iterate through the initializer list */
		ListIterator<InitializerListItem> init_item_iter = init.getList().listIterator();
		InitializerListItem init_item = null;
		for (; !type_stack.isEmpty() && init_item_iter.hasNext(); ){
			
			/* peek the top */
			sub_object = sub_object_stack.peek();
			base_offset = cur_offset_stack.peek();
			cur_type = type_stack.peek();
			
			
			
			init_item = init_item_iter.next();
			
			//sub-object designated
			if (init_item.getDesignatorList() != null){

				/* pop current object */
				while (!sub_object_stack.isEmpty()){
					sub_object = sub_object_stack.pop();
					base_offset = cur_offset_stack.pop();
					cur_type = type_stack.pop();
				}
				
				Designator designator = null;
				for (ListIterator<Designator> designator_iter = init_item.getDesignatorList().listIterator(); designator_iter.hasNext();){
					designator = designator_iter.next();
					
					/* field designator */
					if (designator instanceof FieldDesignator){
						if (cur_type instanceof environment.StructOrUnionType){		
							
							/* query the field */
							StructFieldInfo field_info = ((environment.StructOrUnionType) cur_type).getField(((FieldDesignator) designator).getId().getVal());
							if (field_info == null){
								String tag = ((environment.StructOrUnionType) cur_type).getTag();
								tag = (tag == null) ? "" : tag;
								report_error(new StringBuilder().append((cur_type instanceof environment.StructType) ? "struct " : "union ")
										.append(tag).append(" does not have field ")
										.append(((FieldDesignator) designator).getId().getVal()).toString(), designator);
								return false;
							}
												
							/* update current object */
							//if (TypeFactory.isAggregateType(field_info.getType())){
								sub_object_stack.push((cur_type instanceof StructType) ? (field_info.getOrder()+1) : 0x7FFFFFFF);
								cur_offset_stack.push(base_offset);
								type_stack.push(cur_type);
								
								cur_type = field_info.getType();
								sub_object = 0;
								base_offset += field_info.getOffset();
							
							
						}
						else {
							report_error("field designator appears in non-struct-or-union initializer", designator);
							return false;
						}
					}
					
					/* index designator */
					else if (designator instanceof IndexDesignator){
					
						if (cur_type instanceof environment.ArrayType){
							ExpressionBase designator_expr = ((IndexDesignator) designator).getExpr();
							designator_expr.checkEvaluate_NoGenerating(_cur_env);
							
							/* evaluation error */
							if (designator_expr.getType() == null){
								return false;
							}
							
							/* constant integer type requirement */
							if (!TypeFactory.isIntegerType(designator_expr.getType())){
								report_error("index designator shall evaluates to integer type", designator);
								return false;
							}
							if (designator_expr.getValue() == null || !(designator_expr.getValue() instanceof Number)){
								report_error("index designator shall be an integer constant expression", designator);
								return false;
							}
							
							
							int target = ((Number)designator_expr.getValue()).intValue();
							
							/* index out of bound */
							if (target < 0 || (cur_type.isComplete() && (target >= ((environment.ArrayType) cur_type).getElementCount()))){
								report_error("index designator exceeds array boundary", designator);
								return false;
							}
							
							
							//if (TypeFactory.isAggregateType(((environment.ArrayType) cur_type).getBaseType())){
								type_stack.push(cur_type);
								sub_object_stack.push(target+1);
								cur_offset_stack.push(base_offset);
								
								base_offset += ((environment.ArrayType) cur_type).getBaseType().getSize() * target;
								cur_type = ((environment.ArrayType) cur_type).getBaseType();
								sub_object = 0;
							
						}
						
						else {
							report_error("index designator appears not in array initializer", designator);
							return false;
						}
					}
				}		// end of iteration of designator list
				
				cur_type = type_stack.peek();
				sub_object = sub_object_stack.pop();
				if (sub_object == 0x7FFFFFFF)
					sub_object = 0;
				else
					sub_object -= 1;
				sub_object_stack.push(sub_object);
				base_offset = cur_offset_stack.peek();
			}
			
			
			/*check initializer*/
			
			/* end of current object, pop stack*/
			if (cur_type instanceof environment.ArrayType){
				if (cur_type.isComplete() && sub_object >= ((environment.ArrayType) cur_type).getElementCount()){
					type_stack.pop();
					sub_object_stack.pop();
					cur_offset_stack.pop();
					continue;
				}
			}
			else if (cur_type instanceof environment.StructOrUnionType){
				if (sub_object >= ((environment.StructOrUnionType) cur_type).getFieldCount()){
					type_stack.pop();
					sub_object_stack.pop();
					cur_offset_stack.pop();
					continue;
				}
			}
			
			/* elem_type is the element type of the current sub-object */
			if (cur_type instanceof environment.ArrayType){
				elem_type = ((environment.ArrayType) cur_type).getBaseType();
			}
			else if (cur_type instanceof environment.StructOrUnionType){
				elem_type = ((environment.StructOrUnionType) cur_type).getIthField(sub_object).getType();
			}
			
			Initializer cur_init = init_item.getInitializer();
			
			/* expression initializer */
			if (cur_init instanceof InitializerExpr){
				ExpressionBase expr = ((InitializerExpr) cur_init).getExpr();
				expr.checkEvaluate(_cur_env, _runtime);
				if (expr.getType() == null){
					return false;
				}
				Type expr_type = expr.getType();
				
				for (boolean finished = false; !finished; ){
					
					if (TypeFactory.isIntegerType(elem_type)){
						if (expr_type instanceof environment.PointerType){
							report_warning(String.format("implicit conversion from %s to %s",
									ResourcePrinter.getTypeString(expr_type), 
									ResourcePrinter.getTypeString(elem_type)), cur_init);
						}
						else if (! TypeFactory.isIntegerType(expr_type)){
							report_error(String.format("conversion from %s to incompatible type %s",
									ResourcePrinter.getTypeString(expr_type), 
									ResourcePrinter.getTypeString(elem_type)), cur_init);
							return false;
						}
						
						if (cur_type instanceof StructOrUnionType){
							StructFieldInfo field_info = ((StructOrUnionType) cur_type).getIthField(sub_object);
							
							/* bit-field */
							if (field_info.getBitLength() != -1){
								Operand result = op.bitField(op.address(memloc, base_offset + field_info.getOffset(), _runtime.entry), 
										0, field_info.getMask(), _runtime.entry);
								op.mov(result, (Operand) expr.getValue(), _runtime.entry);
							}
							
							/* non bit-field */
							else {
								Operand result = op.indirection(op.address(memloc, base_offset + field_info.getOffset(), _runtime.entry),
											0, field_info.getType().getSize(), _runtime.entry);
								op.mov(result, (Operand) expr.getValue(), _runtime.entry);
							}
						}
						
						/* cur_type is ArrayType */
						else {
							Operand result = op.indirection(op.address(memloc, base_offset + elem_type.getSize() * sub_object,  _runtime.entry)
									, 0, elem_type.getSize(), _runtime.entry);
							op.mov(result, (Operand) expr.getValue(), _runtime.entry);
						}
						
						finished = true;
					}
					
					else if (elem_type instanceof environment.PointerType){
						if (TypeFactory.isIntegerType(expr_type)){
							if (! (expr.getValue() instanceof IntConstant && ((IntConstant)expr.getValue()).getValue() == 0)){
								report_warning(String.format("implicit conversion from %s to %s",
										ResourcePrinter.getTypeString(expr_type), 
										ResourcePrinter.getTypeString(elem_type)), cur_init);
							}
						}
						else if (! (expr_type instanceof environment.PointerType)){
							report_error(String.format("conversion from %s to incompatible type %s",
									ResourcePrinter.getTypeString(expr_type), 
									ResourcePrinter.getTypeString(elem_type)), cur_init);
							return false;
						}
						
						if (cur_type instanceof StructOrUnionType){
							StructFieldInfo field_info = ((StructOrUnionType) cur_type).getIthField(sub_object);
												
							Operand result = op.indirection(op.address(memloc, base_offset + field_info.getOffset(), _runtime.entry),
									0, field_info.getType().getSize(), _runtime.entry);
							op.mov(result, (Operand) expr.getValue(), _runtime.entry);
						}
						
						/* cur_type is ArrayType */
						else { 
							Operand result = op.indirection(op.address(memloc, base_offset + elem_type.getSize() * sub_object,  _runtime.entry)
									, 0, elem_type.getSize(), _runtime.entry);
							op.mov(result, (Operand) expr.getValue(), _runtime.entry);
						}
						
						finished = true;
					}
					
					/* non-built-ins */
					if (!finished){
						//descend on aggregate types
						
						/* search for the next object that can be initialized */
						while (!finished && TypeFactory.isAggregateType(elem_type)){
							Object val = null;
							if (TypeFactory.isCharArray(elem_type) && (val = checkInitializerForExternalDeclarationWithCharArrayAndStringLiteral(elem_type, cur_init)) != null){
								int arr_base;
								if (cur_type instanceof environment.StructOrUnionType){
									arr_base = ((environment.StructOrUnionType) cur_type).getIthField(sub_object).getOffset() + base_offset;		
								}
								else /* ArrayType */ {
									arr_base = elem_type.getSize() * sub_object + base_offset;
								}
								
								String str = (String)val;
								for (int i = 0; i < str.length(); ++i){
									Operand result = op.indirection(op.address(memloc, arr_base + i, _runtime.entry), 
											0, 1, _runtime.entry);
									op.mov(result, op.constant(str.charAt(i)), _runtime.entry);
								}
								
								finished = true;
								break;
							}
							
							/* struct/union copy */
							else if (!(elem_type instanceof environment.ArrayType) && expr.getType().equals(elem_type)){
								Operand result = op.la(op.temp(_runtime.entry), 
										op.address(memloc, base_offset + ((StructOrUnionType)cur_type).getIthField(sub_object).getOffset(), _runtime.entry), 
										_runtime.entry);
								op.memcpy(result, (Operand) expr.getValue(), elem_type.getSize(), _runtime.entry);
								
								finished = true;
								break;
							}
							
							/* zero length array is skipped */
							if (! (elem_type instanceof environment.ArrayType && elem_type.isComplete() && ((environment.ArrayType)elem_type).getElementCount() == 0)){					
								
								if (cur_type instanceof environment.StructOrUnionType){
									base_offset += ((environment.StructOrUnionType) cur_type).getIthField(sub_object).getOffset();
								}
								else {
									base_offset += elem_type.getSize() * sub_object;
								}
								
								sub_object = (cur_type instanceof UnionType) ? (0x7FFFFFFF) : (sub_object + 1);
								sub_object_stack.pop();
								sub_object_stack.push(sub_object);
								
								type_stack.push(elem_type);
								sub_object_stack.push(0);
								cur_offset_stack.push(base_offset);
																
								cur_type = elem_type;
								sub_object = 0;								
							} else {
								sub_object = (cur_type instanceof UnionType) ? (0x7FFFFFFF) : (sub_object + 1);
								sub_object_stack.pop();
								sub_object_stack.push(sub_object);
							}							
							
							do {
								if (cur_type instanceof environment.ArrayType){
									if (cur_type.isComplete() && sub_object >= ((environment.ArrayType) cur_type).getElementCount()){
										type_stack.pop();
										sub_object_stack.pop();
										cur_offset_stack.pop();
									}
									break;
								}
								else if (cur_type instanceof environment.StructOrUnionType){
									if (sub_object >= ((environment.StructOrUnionType) cur_type).getFieldCount()){
										type_stack.pop();
										sub_object_stack.pop();
										cur_offset_stack.pop();
									}
									else {
										break;
									}
								}
								
								if (! type_stack.isEmpty()){
									cur_type = type_stack.peek();
									sub_object = sub_object_stack.peek();
									base_offset = cur_offset_stack.peek();
								}
								else {
									report_warning("excess elements in initializer list", init);
									return true;
								}
							} while(true);
							
							if (cur_type instanceof StructOrUnionType){
								elem_type = ((StructOrUnionType) cur_type).getIthField(sub_object).getType();
							}
							else if (cur_type instanceof environment.ArrayType){
								elem_type = ((environment.ArrayType) cur_type).getBaseType();
							}
							else{
								report_error("impossible branch in checkInitializerForBlockLevelDeclarationWithAggregateType elem_type update", init);
								return false;
							}
						}
					}
				}
				
				sub_object_stack.pop();
				sub_object_stack.push((cur_type instanceof UnionType) ? (0x7FFFFFFF) : (sub_object + 1));
			}
			
			else if (cur_init instanceof InitializerList){
				boolean finished = false;
				
				if (TypeFactory.isIntegerType(elem_type)){
					LinkedList<InitializerListItem> init_list = ((InitializerList) cur_init).getList();
					if (init_list.size() == 0){
						report_error("empty initializer list provided to scalar type variable", cur_init);
						return false;
					}
					
					InitializerListItem item = init_list.getFirst();
					if (!item.getDesignatorList().isEmpty()){
						report_error("designator in scalar type initializer", init);
						return false;
					}
					
					Initializer expr_init = item.getInitializer();
					if (!(expr_init instanceof InitializerExpr)){
						report_error("non expression initializer for scalar type", cur_init);
					}
					
					ExpressionBase expr = ((InitializerExpr)expr_init).getExpr();
					expr.checkEvaluate(_cur_env, _runtime);
					if (expr.getType() == null) return false;
					Type expr_type = expr.getType();
					
					if (expr_type instanceof environment.PointerType){
						report_warning(String.format("implicit conversion from %s to %s",
								ResourcePrinter.getTypeString(expr_type), 
								ResourcePrinter.getTypeString(elem_type)), cur_init);
					}
					else if (! TypeFactory.isIntegerType(expr_type)){
						report_error(String.format("conversion from %s to incompatible type %s",
								ResourcePrinter.getTypeString(expr_type), 
								ResourcePrinter.getTypeString(elem_type)), cur_init);
						return false;
					}
					
					if (cur_type instanceof StructOrUnionType){
						StructFieldInfo field_info = ((StructOrUnionType) cur_type).getIthField(sub_object);
						
						/* bit-field */
						if (field_info.getBitLength() != -1){
							Operand result = op.bitField(op.address(memloc, base_offset + field_info.getOffset(), _runtime.entry), 
									0, field_info.getMask(), _runtime.entry);
							op.mov(result, (Operand) expr.getValue(), _runtime.entry);
						}
						
						/* non bit-field */
						else {
							Operand result = op.indirection(op.address(memloc, base_offset + field_info.getOffset(), _runtime.entry), 
									0, field_info.getType().getSize(), _runtime.entry);
							op.mov(result, (Operand) expr.getValue(), _runtime.entry);
						}
					}
					
					/* cur_type is ArrayType */
					else { 
						Operand result = op.indirection(op.address(memloc, base_offset + elem_type.getSize() * sub_object, _runtime.entry), 
								0, elem_type.getSize(), _runtime.entry);
						op.mov(result, (Operand) expr.getValue(), _runtime.entry);
					}
					
					finished = true;
				}
				
				else if (elem_type instanceof environment.PointerType){
					LinkedList<InitializerListItem> init_list = ((InitializerList) cur_init).getList();
					if (init_list.size() == 0){
						report_error("empty initializer list provided to scalar type variable", cur_init);
						return false;
					}
					
					InitializerListItem item = init_list.getFirst();
					if (!item.getDesignatorList().isEmpty()){
						report_error("designator in scalar type initializer", init);
						return false;
					}
					
					Initializer expr_init = item.getInitializer();
					if (!(expr_init instanceof InitializerExpr)){
						report_error("non expression initializer for scalar type", cur_init);
					}
					
					ExpressionBase expr = ((InitializerExpr)expr_init).getExpr();
					expr.checkEvaluate(_cur_env, _runtime);
					if (expr.getType() == null) return false;
					Type expr_type = expr.getType();
					
					if (TypeFactory.isIntegerType(expr_type)){
						if (! (expr.getValue() instanceof IntConstant && ((IntConstant)expr.getValue()).getValue() == 0)){
							report_warning(String.format("implicit conversion from %s to %s",
									ResourcePrinter.getTypeString(expr_type), 
									ResourcePrinter.getTypeString(elem_type)), cur_init);
						}
					}
					else if (! (expr_type instanceof environment.PointerType)){
						report_error(String.format("conversion from %s to incompatible type %s",
								ResourcePrinter.getTypeString(expr_type), 
								ResourcePrinter.getTypeString(elem_type)), cur_init);
						return false;
					}
					
					if (cur_type instanceof StructOrUnionType){
						StructFieldInfo field_info = ((StructOrUnionType) cur_type).getIthField(sub_object);
						
						Operand result = op.indirection(op.address(memloc,  base_offset + field_info.getOffset(), _runtime.entry), 
								0, field_info.getType().getSize(), _runtime.entry);
						op.mov(result, (Operand) expr.getValue(), _runtime.entry);
					}
					
					/* cur_type is ArrayType */
					else { 
						Operand result = op.indirection(op.address(memloc, base_offset + elem_type.getSize() * sub_object, _runtime.entry), 
								0, elem_type.getSize(), _runtime.entry);
						op.mov(result, (Operand) expr.getValue(), _runtime.entry);
					}
					
					finished = true;
				}
				
				if (!finished){
					Object val = null;
					if (TypeFactory.isCharArray(elem_type) && 
							(val = checkInitializerForExternalDeclarationWithCharArrayAndStringLiteral(elem_type, cur_init)) != null){
						int arr_base;
						if (cur_type instanceof environment.StructOrUnionType){
							arr_base = ((environment.StructOrUnionType) cur_type).getIthField(sub_object).getOffset() + base_offset;		
						}
						else /* ArrayType */ {
							arr_base = elem_type.getSize() * sub_object + base_offset;
						}
						
						String str = (String)val;
						for (int i = 0; i < str.length(); ++i){
							Operand result = op.indirection(op.address(memloc, arr_base + i, _runtime.entry), 
									0, 1, _runtime.entry);
							op.mov(result, op.constant(str.charAt(i)), _runtime.entry);
						}
						
						finished = true;
					}
					else {

						if (!checkInitializerForBlockLevelDeclarationWithAggregateType(elem_type, memloc, (InitializerList)cur_init,
							(cur_type instanceof StructOrUnionType) ? (base_offset + ((StructOrUnionType) cur_type).getIthField(sub_object).getOffset())
							:(base_offset + elem_type.getSize() * sub_object))) return false;					
						
					}
				}
				
				sub_object_stack.pop();
				sub_object_stack.push((cur_type instanceof UnionType) ? (0x7FFFFFFF) : (sub_object + 1));
			}
			
			//end check initializer
		}
		
		//only the top one array could be incomplete
		if (!type_stack.isEmpty()){
			int sz = 0;
			while (!type_stack.isEmpty()) {
				elem_type = type_stack.pop();
				sz = sub_object_stack.pop();
			}
			
			if (elem_type instanceof environment.ArrayType && !elem_type.isComplete()){
				if (sz == 0){
					report_warning("zero length array", init);
				}
				
				((environment.ArrayType)elem_type).setElementCount(sz);
				((environment.ArrayType)elem_type).setComplete(true);
			}
		}
		if (init_item_iter.hasNext()){
			report_warning("excess elements in initializer", init);
		}
				
		return true;
	}
	
	public boolean checkStatement(Statement stmt){
		if (stmt instanceof BreakStatement){
			return checkBreakStatement((BreakStatement)stmt);
		}
		else if (stmt instanceof CompoundStatement){
			_cur_env = new Environment(_cur_env);
			boolean result = checkCompoundStatement((CompoundStatement)stmt);
			_cur_env = _cur_env.getPrevious();
			
			return result;
		}
		else if (stmt instanceof ContinueStatement){
			return checkContinueStatement((ContinueStatement)stmt);
		}
		else if (stmt instanceof ExpressionStatement){
			ExpressionBase expr = ((ExpressionStatement) stmt).getExpr();
			if (expr != null){
				expr.checkEvaluate(_cur_env, _runtime);
				if (expr.getType() == null) {
					return false;
				}
				
				if (!expr.getType().isComplete() && expr.getType() instanceof StructOrUnionType){
					report_error("expression statement has incomplete type", stmt);
					return false;
				}
			}
			
			return true;
		}
		else if (stmt instanceof ForStatement){
			return checkForStatement((ForStatement)stmt);
		}
		else if (stmt instanceof IfStatement){
			return checkIfStatement((IfStatement)stmt);
		}
		else if (stmt instanceof ReturnStatement){
			return checkReturnStatement((ReturnStatement)stmt);
		}
		else if (stmt instanceof WhileStatement){
			return checkWhileStatement((WhileStatement)stmt);
		}
		
		return false;
	}
	
	public boolean checkBreakStatement(BreakStatement stmt){
		if (_runtime.break_target.isEmpty()){
			report_error("break statement not in loop or switch", stmt);
			return false;
		}
		
		op.j(_runtime.break_target.peekLast(), _runtime.entry);
		
		return true;
	}
	
	public boolean checkContinueStatement(ContinueStatement stmt){
		if (_runtime.continue_target.isEmpty()){
			report_error("continue statement not in loop", stmt);
			return false;
		}
		
		op.j(_runtime.continue_target.peekLast(), _runtime.entry);
		
		return true;
	}
	
	public boolean checkForStatement(ForStatement stmt){
		
		/*
		 * code:
		 * 
		 * initialize
		 * j L3
		 * L1:
		 * statement
		 * L2: (continue target)
		 * step
		 * L3:
		 * condition
		 * bne condition, 0, L1
		 * L4:	(break target)
		 * 
		 */
		
		Label L1 = _runtime.entry.newLabel();
		Label L2 = _runtime.entry.newLabel();
		Label L3 = _runtime.entry.newLabel();
		Label L4 = _runtime.entry.newLabel();
		
		_runtime.continue_target.addLast(L2);
		_runtime.break_target.addLast(L4);
		
		boolean result = true;
		
		/* initialize */
		ExpressionBase expr = stmt.getInitializationExpr();
		if (expr != null){
			expr.checkEvaluate(_cur_env, _runtime);
			result &= (expr.getType() != null);
		}
		
		op.j(L3, _runtime.entry);
		
		/* statement */
		_runtime.entry.appendCode(L1);
		result &= checkStatement(stmt.getStatement());
	
		/* step */
		_runtime.entry.appendCode(L2);
		expr = stmt.getStepExpr();
		if (expr != null){
			expr.checkEvaluate(_cur_env, _runtime);
			result &= (expr.getType() != null);
		}
		
		/* control */
		_runtime.entry.appendCode(L3);
		expr = stmt.getControllingExpr();
		
		if (expr instanceof ShortCircuitExpression){
		
			_runtime.sc_target_set = true;
			_runtime.true_target = L1;
			_runtime.false_target = null;	// fall
			
			expr.checkEvaluate(_cur_env, _runtime);
			
			_runtime.sc_target_set = false;
			
			result &= expr.getType() != null;
		}
		else {
			
			expr.checkEvaluate(_cur_env, _runtime);
			result &= expr.getType() != null;
			if (!TypeFactory.isScalarType(expr.getType())){
				report_error("controlling expression of for statement shall have scalar type", expr);
				result = false;
			}
			
			op.bne((Operand) expr.getValue(), op.constant(0), L1, _runtime.entry);
		}
		
		
		/* next statement */
		_runtime.entry.appendCode(L4);
		
		_runtime.continue_target.removeLast();
		_runtime.break_target.removeLast();
		
		return result;
	}
	
	public boolean checkReturnStatement(ReturnStatement stmt){
		ExpressionBase expr = stmt.getExpr();
		if (expr != null){
			Type return_type = _runtime.return_type;
			if (return_type instanceof environment.VoidType){
				report_error("return a value in void function", stmt);
				return false;
			}
			
			expr.checkEvaluate(_cur_env, _runtime);
			Type expr_type = expr.getType();
			if (expr_type == null){
				return false;
			}
			
			if (TypeFactory.isIntegerType(return_type)){
				if (expr_type instanceof environment.PointerType){
					report_warning("implicit conversion from pointer to int", expr);
				}
				else if (! TypeFactory.isIntegerType(expr_type)){
					report_error("incompatible type in return statement", expr);
					return false;
				}
				
				op.storeReturnValue((Operand) expr.getValue(), _runtime.entry);
			}
			else if (return_type instanceof environment.PointerType){
				if (TypeFactory.isIntegerType(expr_type)){
					report_warning("implicit conversion from integer type to pointer", expr);
				}
				else if (! (expr_type instanceof environment.PointerType)){
					report_error("incompatible type in return statement", expr);
					return false;
				}
				
				op.storeReturnValue((Operand) expr.getValue(), _runtime.entry);
			}
			else if (return_type instanceof environment.StructOrUnionType){
				if (return_type.equals(expr_type)){
					Operand result = op.la(op.temp(_runtime.entry), _runtime.entry.getReturnValueAddress(), _runtime.entry);
					op.memcpy(result, (Operand) expr.getValue(), return_type.getSize(), _runtime.entry);
					op.storeReturnValue(result, _runtime.entry);
				}
				else {
					report_error("incompatible type in return statement", expr);
					return false;
				}
			}
		}
		
		op.j(_ir.getFunctionEndLabel(_runtime.entry.getName()), _runtime.entry);
		
		return true;
	}
	
	public boolean checkIfStatement(IfStatement stmt){
		/*
		 * code:
		 * 
		 * control
		 * beq control, 0, L1
		 * then_branch
		 * j L2
		 * L1:
		 * else_branch
		 * L2:
		 * 
		 */
		
		Label L1 = _runtime.entry.newLabel();
		Label L2 = null;
		
		if (stmt.getElse() != null){
			L2 = _runtime.entry.newLabel();
		}
		
		boolean result = true;
		
		/* control */
		ExpressionBase expr = stmt.getControllingExpr();
		
		if (expr instanceof ShortCircuitExpression){
			
			_runtime.sc_target_set = true;
			_runtime.true_target = null;
			_runtime.false_target = L1;
			
			expr.checkEvaluate(_cur_env, _runtime);
			
			_runtime.sc_target_set = false;
		}
		else {
			expr.checkEvaluate(_cur_env, _runtime);
			op.beq((Operand) expr.getValue(), op.constant(0), L1, _runtime.entry);
		}
		
		result &= expr.getType() != null;
		if (!TypeFactory.isScalarType(expr.getType())){
			report_error("controlling expression of if statement shall have scalar type", expr);
			return false;
		}
		
		result &= checkStatement(stmt.getThen());		// then branch
		
		if (stmt.getElse() != null){
			op.j(L2, _runtime.entry);
			_runtime.entry.appendCode(L1);
			result &= checkStatement(stmt.getElse());
			_runtime.entry.appendCode(L2);
		}
		else{
			_runtime.entry.appendCode(L1);
		}
		
		return result;
	}
	
	public boolean checkWhileStatement(WhileStatement stmt){
		/*
		 * code:
		 * 
		 * j L2
		 * L1:
		 * statement
		 * L2: (continue target)
		 * control
		 * bne control, 0, L1
		 * L3: (break target)
		 * 
		 */
		
		Label L1 = _runtime.entry.newLabel();
		Label L2 = _runtime.entry.newLabel();
		Label L3 = _runtime.entry.newLabel();
		
		_runtime.continue_target.addLast(L2);
		_runtime.break_target.addLast(L3);
		boolean result = true;
		
		op.j(L2, _runtime.entry);
		_runtime.entry.appendCode(L1);
		result &= checkStatement(stmt.getStatement());
		_runtime.entry.appendCode(L2);
		
		ExpressionBase expr = stmt.getControllingExpression();
		
		if (expr instanceof ShortCircuitExpression){
		
			_runtime.sc_target_set = true;
			_runtime.true_target = L1;
			_runtime.false_target = null;	// fall
			
			expr.checkEvaluate(_cur_env, _runtime);
			
			_runtime.sc_target_set = false;
			
			result &= expr.getType() != null;
		}
		else {
			
			expr.checkEvaluate(_cur_env, _runtime);
			result &= expr.getType() != null;
			if (!TypeFactory.isScalarType(expr.getType())){
				report_error("controlling expression of while statement shall have scalar type", expr);
				result = false;
			}
			
			op.bne((Operand) expr.getValue(), op.constant(0), L1, _runtime.entry);
		}
		
		_runtime.entry.appendCode(L3);
		
		_runtime.continue_target.removeLast();
		_runtime.break_target.removeLast();
		
		return result;
	}
	
	
	public Type checkDeclarationSpecifier(DeclarationSpecifier spec){
		/* no code generated here */
		if (spec.isInvalid()){
			report_error("invalid declaration specifier", spec);
			return null;
		}
		
		if (!spec.typeSpecified()){
			report_warning("type specifier missed and defaults to 'int'", spec);
			spec.setInt();
		}
		
		Type spec_type = null;
		if (spec.isTypedefName()){
			SymbolInfo info = _cur_env.lookup(((TypedefNameDeclarationSpecifier)spec).getId().getVal());
			if (info == null){
				report_error("typedef name is not declared", spec);
				return null;
			}
			else if (!(info instanceof environment.TypedefName)){
				if (info instanceof environment.VariableSymbol){
					report_error("typedef name expected, but variable name found", spec);
					return null;
				}
				else if (info instanceof environment.FunctionDesignator){
					report_error("typedef name expected, but function designator found", spec);
					return null;
				}
				else {
					report_error("typedef name expected, but nothing's found", spec);
					return null;
				}
			}
			spec_type = info.getType().shallowCopy();
		}
		else
		if (spec.isBuiltin()){
			if (spec.isInt()) spec_type = TypeFactory.newIntType();
			else if (spec.isChar()) spec_type = TypeFactory.newCharType();
			else if (spec.isVoid()) spec_type = TypeFactory.newVoidType();
		}
		else if (spec.isStruct()){
			spec_type = checkStructDeclarationSpecifier((StructDeclarationSpecifier) spec);
		}
		else if (spec.isUnion()){
			spec_type = checkUnionDeclarationSpecifier((UnionDeclarationSpecifier)spec);
		}
		
		return spec_type;
	}
	
	
	public Type checkStructDeclarationSpecifier(StructDeclarationSpecifier root){
		/* no code generated here */
		Type result = null;
		LinkedList<StructDeclaration> list = root.getList();
		if (list == null){
			// id has not to be null
			SymbolInfo info = _cur_env.lookupTag(root.getIdentifier().getVal());
			
			if (info == null){
				info = new StructOrUnionTag(root.getIdentifier().getVal(),
						new StructType(root.getIdentifier().getVal()),
						SymbolInfo.NO_LINKAGE);
				_cur_env.insertTag(info);
			}
			else{
				if (!(info.getType() instanceof StructType)){
					report_error(root.getIdentifier().getVal() + " defined as a wrong kind of tag", root);
					return null;
				}
			}
			
			result = info.getType();
		}
		else {
			SymbolInfo info = null;
			if (root.getIdentifier() != null){
				info = _cur_env.lookupTagOnCurrentLevel(root.getIdentifier().getVal());
				if (info != null){
					if (!(info.getType() instanceof StructType)){
						report_error(root.getIdentifier().getVal() + " defined as a wrong kind of tag", root);
						return null;
					}
					else if (info.getType().isComplete()){
						report_error("redefinition of struct " + root.getIdentifier().getVal() ,root);
						return null;
					}
				}
			}
			
			
			if (info != null){
				result = (StructType)info.getType();
			}
			else {
				result = new StructType((root.getIdentifier() == null) ? null : root.getIdentifier().getVal());
				if (root.getIdentifier() != null){
					_cur_env.insertTag(new StructOrUnionTag(root.getIdentifier().getVal(), (StructType)result, SymbolInfo.NO_LINKAGE));
				}
			}
			
			
			for (ListIterator<StructDeclaration> iter = list.listIterator(); iter.hasNext();){
				checkStructDeclaration(iter.next(), (StructType)result);
			}
			((StructType)result).setComplete(true);
		}
		
		return result;
	}
	
	
	public Type checkUnionDeclarationSpecifier(UnionDeclarationSpecifier root){
		// no code generated here
		Type result = null;
		LinkedList<StructDeclaration> list = root.getList();
		if (list == null){
			// id has not to be null
			SymbolInfo info = _cur_env.lookupTag(root.getIdentifier().getVal());
			
			if (info == null){
				info = new StructOrUnionTag(root.getIdentifier().getVal(),
											new UnionType(root.getIdentifier().getVal()),
											SymbolInfo.NO_LINKAGE);
				_cur_env.insertTag(info);
			}
			else if (!(info.getType() instanceof UnionType)){
				report_error(root.getIdentifier().getVal() + " defined as a wrong kind of tag", root);
				return null;
			}
			
			result = info.getType();
		}
		else {
			SymbolInfo info = null;
			if (root.getIdentifier() != null){
				info = _cur_env.lookupTagOnCurrentLevel(root.getIdentifier().getVal());
				if (info != null){
					if (!(info.getType() instanceof UnionType)){
						report_error(root.getIdentifier().getVal() + " defined as a wrong kind of tag", root);
						return null;
					}
					else if (info.getType().isComplete()){
						report_error("redefinition of union " + root.getIdentifier().getVal() ,root);
						return null;
					}
				}
			}
			
				
			if (info != null){
				result = (UnionType)info.getType();
			}
			else {
				result = new UnionType((root.getIdentifier() == null) ? null : root.getIdentifier().getVal());
				if (root.getIdentifier() != null){
					_cur_env.insertTag(new StructOrUnionTag(root.getIdentifier().getVal(), (UnionType)result, SymbolInfo.NO_LINKAGE));
				}
			}
			
			for (ListIterator<StructDeclaration> iter = list.listIterator(); iter.hasNext();){
				checkStructDeclaration(iter.next(), (UnionType)result);
			}
			((UnionType)result).setComplete(true);

		}
		
		return result;
	}
	
	
	public boolean checkStructDeclaration(StructDeclaration root, StructOrUnionType type){
		/* no code generated here */
		
		/* check declaration specifier */
		DeclarationSpecifier spec = root.getDeclarationSpecifier();
		LinkedList<Declarator> list = root.getList();
		Type spec_type = checkDeclarationSpecifier(spec);
		if (spec_type == null){
			return false;
		}
		
		/* warning of useless struct declaration */
		if (list.isEmpty() && (spec_type instanceof StructOrUnionType && ((StructOrUnionType)spec_type).getTag() == null)){
			report_warning("useless struct declaration", root);
		}
		
		//typedef is filtered during parsing stage
		boolean final_result = true;
		Declarator declarator = null;;
		Type decl_type = null;
		for (ListIterator<Declarator> iter = list.listIterator(); iter.hasNext();){
			declarator = iter.next();
			decl_type = composeType(spec_type, declarator.getType());
			if (decl_type instanceof environment.FunctionType){
				report_error("struct or union cannot have function type field", declarator);
				final_result = false;
			}
			else if (!decl_type.isComplete()){
				report_error("struct or union cannot have incomplete type field", declarator);
				final_result = false;
			}
			else {
				if (declarator instanceof node.StructDeclarator){
					if (! (decl_type instanceof environment.IntType)){
						report_error("bit field has invalid type", declarator);
						final_result = false;
					}
					else {
						int[] bit_length_res = checkIntegerConstantExpression(((node.StructDeclarator) declarator).getBitLengthExpr());
						if (bit_length_res[0] != CHECK_INTEGER_CONSTANT_EXPRESSION_SUCCEED){
							report_error("non-constant bit field length", declarator);
							final_result = false;
						}
						else if (bit_length_res[1] < 0){
							report_error("negative length bit field", declarator);
							final_result = false;
						}
						else if (bit_length_res[1] == 0){
							report_error("zero length bit field", declarator);
							final_result = false;
						}
						else if (bit_length_res[1] > (decl_type.getSize() << 3)){
							report_error("bit field length exceed type width", declarator);
							final_result = false;
						}
						else {
							if (declarator.getId() == null){
								type.addBitField(decl_type, bit_length_res[1]);
							}
							else {
								StructFieldInfo sfinfo = type.getField(declarator.getId().getVal());
								if (sfinfo != null){
									report_error("duplicate field " + declarator.getId().getVal(), declarator);
									final_result = false;
								}
								else {
									type.addBitField(declarator.getId().getVal(), decl_type, bit_length_res[1]);
								}
							}
						}
					}
				}
				else{
					StructFieldInfo sfinfo = type.getField(declarator.getId().getVal());
					if (sfinfo != null){
						report_error("duplicate field " + declarator.getId().getVal(), declarator);
						final_result = false;
					}
					else {
						type.addField(declarator.getId().getVal(), decl_type);
					}
				}
			}
		}
		
		return final_result;
	}
	
	
	public Type composeType(Type base, TypeNode derived){
		/* no code generated here */
		
		/* no type modifier provided */
		if (derived instanceof DeclarationSpecifier){
			return base;
		}
		
		/* check type modifiers */
		DerivedTypeList dlist = (DerivedTypeList)derived;
		LinkedList<TypeModifier> list = dlist.getList();
		TypeModifier tm = null;
		Type result = base;
		for (Iterator<TypeModifier> iter = list.descendingIterator(); iter.hasNext();){
			tm = iter.next();
			
			/* array modifier */
			if (tm instanceof node.ArrayType){
				if (!result.isComplete()){
					report_error("element type of array is incomplete", tm);
					return null;
				}
				if (result instanceof environment.FunctionType){
					report_error("element type of array is function type", tm);
					return null;
				}
				if (((node.ArrayType) tm).getExpr() == null){
					result = new environment.ArrayType(result);
				}
				else {
					int[] expr_eval_res = checkIntegerConstantExpression(((node.ArrayType) tm).getExpr());
				
					if (expr_eval_res[0] != CHECK_INTEGER_CONSTANT_EXPRESSION_SUCCEED){
						report_error("non-constant array size", tm);
						return null;
					}
					if (expr_eval_res[1] < 0){
						report_error("negative length array is forbidden", tm);
						return null;
					}
					if (expr_eval_res[1] == 0){
						report_warning("zero length array", tm);	// zero length array is allowed as extension
					}
					result = new environment.ArrayType(result, expr_eval_res[1]);
				}
			}
			
			/* function modifier */
			else if (tm instanceof node.FunctionType){
				if (result instanceof environment.FunctionType){
					report_error("return type of function cannot be function type", tm);
					return null;
				}
				if (result instanceof environment.ArrayType){
					report_error("return type of function cannot be array type", tm);
					return null;
				}
				
				if (!result.isComplete() && !(result instanceof environment.VoidType)){
					report_error("return type of function shall not be incomplete", tm);
					return null;
				}
				result = new environment.FunctionType(result);
				
				ParameterList plist = ((node.FunctionType) tm).getParameterList();
				((environment.FunctionType)result).setVarLength(plist.getEllipsis());
				LinkedList<Declarator> dlst = plist.getList();
				if (dlst != null && dlst.size() == 1){
					Declarator para_decl = dlst.getFirst();
					TypeNode node = para_decl.getType();
					
					// (void) denotes an empty parameter list
					if (para_decl.getId() == null && node instanceof DeclarationSpecifier && ((DeclarationSpecifier)node).isVoid()){
						((environment.FunctionType)result).setComplete(true);
						continue;
					}
				}
				if (dlst != null){
					//set up function prototype environment
					_cur_env = new Environment(_cur_env);
					
					Type paraType = null;
					Declarator para_decl = null;
					
					for (ListIterator<Declarator> iter2 = dlst.listIterator(); iter2.hasNext();){
						para_decl = iter2.next();
						
						paraType = checkFunctionParameterDeclarator(para_decl);
						if (paraType == null) {
							result = null;
							break;
						}
						
						if (!paraType.isComplete()){
							report_error("function parameter shall not have incomplete type", para_decl);
							result = null;
							break;
						}
					
						if (para_decl.getId() != null){
							if (_cur_env.lookupOnCurrentLevel(para_decl.getId().getVal()) != null){
								report_error("duplicate parameter name", para_decl);
							}
							_cur_env.insert(new VariableSymbol(para_decl.getId().getVal(), paraType, SymbolInfo.NO_LINKAGE));
						}
						((environment.FunctionType)result).add(paraType);
					}
					
					_cur_env = _cur_env.getPrevious();
				}
					
				if (result == null) return null;
				
				((environment.FunctionType)result).setComplete(true);
			}
			
			/* pointer modifier */
			else if (tm instanceof node.PointerType){
				result = new environment.PointerType(result);
			}
			else return null;
		}
		
		return result;
	}
	

	/**
	 * Returns a pair p. p[0] == 0 if root is constant expression, -1 if not.
	 * p[1] is the value.
	 * 
	 * @param root
	 * @return
	 */
	public int[] checkIntegerConstantExpression(ExpressionBase root){
		/* no code generated here */
		root.checkEvaluate_NoGenerating(_cur_env);
		if (root.getType() != null && environment.TypeFactory.isIntegerType(root.getType())
				&& root.getValue() instanceof Number){
			return new int[]{0, ((Number)root.getValue()).intValue()};
		}
		return new int[]{-1, 0};
	}
	public static final int CHECK_INTEGER_CONSTANT_EXPRESSION_FAIL = -1;
	public static final int CHECK_INTEGER_CONSTANT_EXPRESSION_SUCCEED = 0;
	
	public Type checkFunctionParameterDeclarator(Declarator root){
		/* no code generated here */
		TypeNode tn = root.getType();
		DeclarationSpecifier spec = null;
		if (tn instanceof DeclarationSpecifier){
			spec = (DeclarationSpecifier)tn;
		}
		else {
			spec = ((DerivedTypeList)tn).getDeclarationSpecifier();
		}
		if (spec.isTypedef()){
			report_error("typedef in function parameter", spec);
			return null;
		}
		
		Type spec_type = checkDeclarationSpecifier(spec);
		
		Type result = composeType(spec_type, tn);
		
		/* function parameter adjustment */
		if (result instanceof environment.ArrayType){
			result = new environment.PointerType(((environment.ArrayType) result).getBaseType());
		}
		else if (result instanceof environment.FunctionType){
			result = new environment.PointerType(result);
		}
		
		return result;
	}
	

	public boolean checkInitializerForExternalDeclaration(VariableSymbol info, node.Initializer init){
		/* need to create static variable entry for the initialized variable */
		Type elem_type = info.getType();
		
		/* int type */
		Object val = null;
		if (elem_type instanceof environment.IntType){
			if ((val = checkInitializerForExternalDeclarationWithIntType(init)) != null){
				info.setInitialized(true);
				
				/* set initializer */				
				_ir.addStaticVariableEntry(new StaticVarEntry(info.getMemLoc(), val));
				
				return true;
			}
			else {
				return false;
			}
		}
		
		/* char type */
		else if (elem_type instanceof environment.CharType){
			if ((val = checkInitializerForExternalDeclarationWithCharType(init)) != null){
				info.setInitialized(true);
				
				/* set initializer */
				_ir.addStaticVariableEntry(new StaticVarEntry(info.getMemLoc(), val));
				
				return true;
			}
			else {
				return false;
			}
		}
		
		/* pointer type */
		else if (elem_type instanceof environment.PointerType){
			if ((val = checkInitializerForExternalDeclarationWithPointerType(info.getType(), init)) != null){
				info.setInitialized(true);
				
				/* set initializer */
				_ir.addStaticVariableEntry(new StaticVarEntry(info.getMemLoc(), val));
				
				return true;
			}
			else {
				return false;
			}
		}
		
		/* array type */
		else if (elem_type instanceof environment.ArrayType){
			if (TypeFactory.isCharArray(elem_type)){
				if ((val = checkInitializerForExternalDeclarationWithCharArrayAndStringLiteral(elem_type, init)) != null){
					info.setInitialized(true);
					if (info.getMemLoc().getSize() == 0){
						info.getMemLoc().setSize(info.getType().getSize());
					}
					
					/* set initializer */
					_ir.addStaticVariableEntry(new StaticVarEntry(info.getMemLoc(), val));
					
					return true;
				}
			}
			
			if (!(init instanceof InitializerList)){
				report_error("array type requires the initializer to be a list", init);
				return false;
			}
			
			if ((val = checkInitializerForExternalDeclarationWithAggregateType(elem_type, (InitializerList)init)) != null){
				info.setInitialized(true);
				info.updateMemLoc();
				
				/* set initializer */
				_ir.addStaticVariableEntry(new StaticVarEntry(info.getMemLoc(), val));
				
				return true;
			}
			else {
				return false;
			}
		}
		
		/* struct type */
		else if (elem_type instanceof environment.StructType){
			if (!elem_type.isComplete()){
				report_error("cannot initialize incomplete struct type", init);
				return false;
			}
			if (init instanceof InitializerExpr){
				ExpressionBase init_expr = ((InitializerExpr) init).getExpr();
				init_expr.checkEvaluate_NoGenerating(_cur_env);
				if (init_expr.getType() == null){
					return false;
				}
				
				if (init_expr.getType().equals(elem_type)){
					if (!init_expr.isLvalue() && init_expr.getValue() instanceof AddressConstant){
						info.setInitialized(true);
						
						report_warning("I don't think constant struct object could be generated from our grammar.", init);
						
						return true;
					}
					else {
						report_error("non-constant initializer in file scope", init);
						return false;
					}
				}
				else{ 
					report_error("imcompatible type initializer for struct type", init);
					return false;
				}
			}
			
			if ((val = checkInitializerForExternalDeclarationWithAggregateType(elem_type, (InitializerList)init)) != null){
				info.setInitialized(true);

				/* set initializer */
				_ir.addStaticVariableEntry(new StaticVarEntry(info.getMemLoc(), val));
				
				return true;
			}
			else {
				return false;
			}
		}
		
		/* union type */
		else if (elem_type instanceof environment.UnionType){
			if (!elem_type.isComplete()){
				report_error("cannot initialize incomplete struct type", init);
				return false;
			}
			if (init instanceof InitializerExpr){
				ExpressionBase init_expr = ((InitializerExpr) init).getExpr();
				init_expr.checkEvaluate_NoGenerating(_cur_env);
				if (init_expr.getType() == null){
					return false;
				}
				
				if (init_expr.getType().equals(elem_type)){
					if (!init_expr.isLvalue() && init_expr.getValue() instanceof AddressConstant){
						info.setInitialized(true);

						report_warning("I don't think constant struct object could be generated from our grammar.", init);
						
						return true;
					}
					else {
						report_error("non-constant initializer in file scope", init);
						return false;
					}
				}
				else{ 
					report_error("imcompatible type initializer for union type", init);
					return false;
				}
			}
			
			if ((val = checkInitializerForExternalDeclarationWithAggregateType(elem_type, (InitializerList)init)) != null){
				info.setInitialized(true);

				/* set initializer */
				_ir.addStaticVariableEntry(new StaticVarEntry(info.getMemLoc(), val));
				
				return true;
			}
			else {
				return false;
			}
		}
		
		return false;
	}


	public Object checkInitializerForExternalDeclarationWithIntType(node.Initializer init){
		/* initializer defaults to 0 upon error */
		if (init instanceof node.InitializerExpr){
			
			/* check initializer expression */
			ExpressionBase init_expr = ((node.InitializerExpr) init).getExpr();
			init_expr.checkEvaluate_NoGenerating(_cur_env);
			
			if (init_expr.getType() == null){
				return 0;
			}
			
			if (TypeFactory.isIntegerType(init_expr.getType())){
				Object val = init_expr.getValue();
				if (val instanceof Number){
					return val;
				}
				else {
					report_error("non-constant initializer in file scope", init);
					return 0;
				}
			}
			else if (init_expr.getType() instanceof environment.PointerType){
				Object val = init_expr.getValue();
				if (!init_expr.isLvalue() && val instanceof environment.AddressConstant){
					report_warning("implicit conversion from pointer type to int", init);
					
					return AddressConstantToIRConstant((AddressConstant)val);
				}
				else {
					report_error("non-constant initializer in file scope", init);
					return 0;
				}
			}
			else {
				report_error("incompatible type initializer provided", init);
				return 0;
			}
		}
		else if (init instanceof node.InitializerList){
			LinkedList<InitializerListItem> list = ((node.InitializerList) init).getList();
			if (list.size() >= 1){
				node.InitializerListItem init_item = list.getFirst();
				if (!init_item.getDesignatorList().isEmpty()){
					report_error("designator in int initializer", init);
					return 0;
				}
				if (init_item.getInitializer() instanceof InitializerExpr){
					ExpressionBase init_expr = ((InitializerExpr)init_item.getInitializer()).getExpr();
					init_expr.checkEvaluate_NoGenerating(_cur_env);
					if (init_expr.getType() == null){
						return 0;
					}
					
					if (TypeFactory.isIntegerType(init_expr.getType())){
						Object val = init_expr.getValue();
						if (val instanceof Number){
							if (list.size() > 1){
								report_warning("excess elements in scalar initialization", init);
							}
							return val;
						}
						else {
							report_error("non-constant initializer in file scope", init);
							return 0;
						}
					}
					else if (init_expr.getType() instanceof environment.PointerType){
						Object val = init_expr.getValue();
						if (!init_expr.isLvalue() && val instanceof environment.AddressConstant){
							report_warning("implicit conversion from pointer type to int", init);
							if (list.size() > 1){
								report_warning("excess elements in scalar initialization", init);
							}
							
							return AddressConstantToIRConstant((AddressConstant)val);
						}
						else {
							report_error("non-constant initializer in file scope", init);
							return 0;
						}
					}
					else {
						report_error("incompatible type initializer provided", init);
						return 0;
					}
				}
				else {
					report_error("non-expression initializer for int", init);
					return 0;
				}
			}
			else {
				report_error("empty initializer list provided to int variable", init);
				return 0;
			}
		}
		return 0;
	}
	

	public Object checkInitializerForExternalDeclarationWithCharType(node.Initializer init){
		/* initializer defaults to 0 upon error */
		if (init instanceof node.InitializerExpr){
			ExpressionBase init_expr = ((node.InitializerExpr) init).getExpr();
			init_expr.checkEvaluate_NoGenerating(_cur_env);
			if (init_expr.getType() == null){
				return 0;
			}
			
			if (TypeFactory.isIntegerType(init_expr.getType())){
				Object val = init_expr.getValue();
				if (val instanceof Number){

					return val;
				}
				else {
					report_error("non-constant initializer in file scope", init);
					return 0;
				}
			}
			else if (init_expr.getType() instanceof environment.PointerType){
				Object val = init_expr.getValue();
				if (!init_expr.isLvalue() && val instanceof environment.AddressConstant){
					report_warning("implicit conversion from pointer type to char", init);

					return AddressConstantToIRConstant((AddressConstant) val);
				}
				else {
					report_error("non-constant initializer in file scope", init);
					return 0;
				}
			}
			else {
				report_error("incompatible type initializer provided", init);
				return 0;
			}
		}
		else if (init instanceof node.InitializerList){
			LinkedList<InitializerListItem> list = ((node.InitializerList) init).getList();
			if (list.size() >= 1){
				node.InitializerListItem init_item = list.getFirst();
				if (!init_item.getDesignatorList().isEmpty()){
					report_error("designator in char initializer", init);
					return 0;
				}
				if (init_item.getInitializer() instanceof InitializerExpr){
					ExpressionBase init_expr = ((InitializerExpr)init_item.getInitializer()).getExpr();
					init_expr.checkEvaluate_NoGenerating(_cur_env);
					if (init_expr.getType() == null){
						return 0;
					}
					
					if (TypeFactory.isIntegerType(init_expr.getType())){
						Object val = init_expr.getValue();
						if (val instanceof Number){
							if (list.size() > 1){
								report_warning("excess elements in scalar initialization", init);
							}

							return val;
						}
						else {
							report_error("non-constant initializer in file scope", init);
							return 0;
						}
					}
					else if (init_expr.getType() instanceof environment.PointerType){
						Object val = init_expr.getValue();
						if (!init_expr.isLvalue() && val instanceof environment.AddressConstant){
							report_warning("implicit conversion from pointer type to char", init);
							if (list.size() > 1){
								report_warning("excess elements in scalar initialization", init);
							}

//							{
//								SymbolInfo sym = ((AddressConstant) val).getBase();
//								if (sym.getLinkage() == SymbolInfo.STRING_LITERAL){
//									// allocate memory for the string literal
//									_ir.addStaticVariableEntry(new StaticVarEntry(((VariableSymbol)sym).getMemLoc(), sym.getName()));
//								}
//							}
							
							return AddressConstantToIRConstant((AddressConstant) val);
						}
						else {
							report_error("non-constant initializer in file scope", init);
							return 0;
						}
					}
					else {
						report_error("incompatible type initializer provided", init);
						return 0;
					}
				}
				else {
					report_error("non-expression initializer for int", init);
					return 0;
				}
			}
			else {
				report_error("empty initializer list provided to int variable", init);
				return 0;
			}
		}
		return 0;
	}
	

	public Object checkInitializerForExternalDeclarationWithPointerType(Type pointer_type, node.Initializer init){
		/* defaults to 0 upon error */
		if (init instanceof node.InitializerExpr){
			ExpressionBase init_expr = ((node.InitializerExpr) init).getExpr();				
			init_expr.checkEvaluate_NoGenerating(_cur_env);
			if (init_expr.getType() == null){
				return 0;
			}
			
			if (TypeFactory.isIntegerType(init_expr.getType())){
				Object val = init_expr.getValue();
				if (val instanceof Number){
					if (((Number)val).intValue() != 0){
						report_warning("implicit conversion from integer type to pointer type", init);
					}
					
					return val;
				}
				else {
					report_error("non-constant initializer in file scope", init);
					return 0;
				}
			}
			else if (init_expr.getType() instanceof environment.PointerType){
				Object val = init_expr.getValue();
				if (!init_expr.isLvalue() && val instanceof environment.AddressConstant){
					if (!pointer_type.equals(init_expr.getType())){
						report_warning("initialization from incompatible pointer type", init);
					}
					
					return AddressConstantToIRConstant((AddressConstant) val);
				}
				else {
					report_error("non-constant initializer in file scope", init);
					return 0;
				}
			}
			else {
				report_error("incompatible type initializer provided", init);
				return 0;
			}
		}
		else if (init instanceof node.InitializerList){
			LinkedList<InitializerListItem> list = ((node.InitializerList) init).getList();
			if (list.size() >= 1){
				node.InitializerListItem init_item = list.getFirst();
				if (!init_item.getDesignatorList().isEmpty()){
					report_error("designator in pointer initializer", init);
					return 0;
				}
				if (init_item.getInitializer() instanceof InitializerExpr){
					ExpressionBase init_expr = ((InitializerExpr)init_item.getInitializer()).getExpr();
					init_expr.checkEvaluate_NoGenerating(_cur_env);
					if (init_expr.getType() == null){
						return 0;
					}
					
					if (TypeFactory.isIntegerType(init_expr.getType())){
						Object val = init_expr.getValue();
						if (val instanceof Number){
							if (((Number)val).intValue() != 0){
								report_warning("implicit conversion from integer type to pointer type", init);
							}
							if (list.size() > 1){
								report_warning("excess elements in scalar initialization", init);
							}
							
							return val;
						}
						else {
							report_error("non-constant initializer in file scope", init);
							return 0;
						}
					}
					else if (init_expr.getType() instanceof environment.PointerType){
						Object val = init_expr.getValue();
						if (!init_expr.isLvalue() && val instanceof environment.AddressConstant){
							if (!pointer_type.equals(init_expr.getType())){
								report_warning("initialization from incompatible pointer type", init);
							}

							if (list.size() > 1){
								report_warning("excess elements in scalar initialization", init);
							}

//							{
//								SymbolInfo sym = ((AddressConstant) val).getBase();
//								if (sym.getLinkage() == SymbolInfo.STRING_LITERAL){
//									// allocate memory for the string literal
//									_ir.addStaticVariableEntry(new StaticVarEntry(((VariableSymbol)sym).getMemLoc(), sym.getName()));
//								}
//							}
							
							return AddressConstantToIRConstant((AddressConstant) val);
						}
						else {
							report_error("non-constant initializer in file scope", init);
							return 0;
						}
					}
					else {
						report_error("incompatible type initializer provided", init);
						return 0;
					}
				}
				else {
					report_error("non-expression initializer for int", init);
					return 0;
				}
			}
			else {
				report_error("empty initializer list provided to int variable", init);
				return 0;
			}
		}
		return 0;
	}
	

	public Object checkInitializerForExternalDeclarationWithCharArrayAndStringLiteral(Type elem_type, node.Initializer init)
	/* returns null if init is not string literal */
	{
		ExpressionBase init_expr = null;

		if (init instanceof InitializerExpr){
			init_expr = ((InitializerExpr) init).getExpr();		
			
		}
		else if (init instanceof InitializerList){
			if (((InitializerList) init).getList().size() == 1){
				InitializerListItem init_item = ((InitializerList) init).getList().getFirst();
				if (init_item.getDesignatorList().isEmpty()){
					if (init_item.getInitializer() instanceof InitializerExpr){
						init_expr = ((InitializerExpr)init_item.getInitializer()).getExpr();
					}
				}
			}
		}
		
		if (init_expr == null) return null;
		
		if (init_expr instanceof PrimaryExpression &&
				((PrimaryExpression)init_expr).getVal() instanceof node.StringConstant){
			String val = ((node.StringConstant)((PrimaryExpression)init_expr).getVal()).getVal();
			if (elem_type.isComplete()){
				if (elem_type.getSize() < val.length()){
					report_warning("initializer string for char array is too long", init);

					return val.substring(0, elem_type.getSize());
				}
				else {

					return val;
				}
			}
			else {
				((environment.ArrayType) elem_type).setElementCount(val.length());
				((environment.ArrayType) elem_type).setComplete(true);

				return val;
			}
		}
		
		return null;
	}
	
	
	public AggregateTypeInitializer checkInitializerForExternalDeclarationWithAggregateType(Type elem_type, InitializerList init){
		return checkInitializerForExternalDeclarationWithAggregateType(elem_type, init, new AggregateTypeInitializer(), 0);
	}
	
	public AggregateTypeInitializer checkInitializerForExternalDeclarationWithAggregateType(Type elem_type, InitializerList init, AggregateTypeInitializer output, int base_offset){
		/* initialize current object stack */
		Stack<Type> type_stack = new Stack<Type>();		// type of current object
		Stack<Integer> sub_object_stack = new Stack<Integer>();		// sub-object count of current object
		Stack<Integer> cur_offset_stack = new Stack<Integer>();		// base offset of current object
		
		type_stack.push(elem_type);
		sub_object_stack.push(0);
		cur_offset_stack.push(base_offset);
		
		/* current object variables */
		int sub_object = 0;
		Type cur_type = null;
		
		/* Iterate through the initializer list */
		ListIterator<InitializerListItem> init_item_iter = init.getList().listIterator();
		InitializerListItem init_item = null;
		for (; !type_stack.isEmpty() && init_item_iter.hasNext(); ){
			
			/* peek the top */
			sub_object = sub_object_stack.peek();
			base_offset = cur_offset_stack.peek();
			cur_type = type_stack.peek();
			
			init_item = init_item_iter.next();
			
			//sub-object designated
			if (init_item.getDesignatorList() != null){
				
				/* pop current object */
				while (!sub_object_stack.isEmpty()){
					sub_object = sub_object_stack.pop();
					base_offset = cur_offset_stack.pop();
					cur_type = type_stack.pop();
				}
				
				Designator designator = null;
				for (ListIterator<Designator> designator_iter = init_item.getDesignatorList().listIterator(); designator_iter.hasNext();){
					designator = designator_iter.next();
					
					/* field designator */
					if (designator instanceof FieldDesignator){
						if (cur_type instanceof environment.StructOrUnionType){		
							
							/* query the field */
							StructFieldInfo field_info = ((environment.StructOrUnionType) cur_type).getField(((FieldDesignator) designator).getId().getVal());
							if (field_info == null){
								String tag = ((environment.StructOrUnionType) cur_type).getTag();
								tag = (tag == null) ? "" : tag;
								report_error(new StringBuilder().append((cur_type instanceof environment.StructType) ? "struct " : "union ")
										.append(tag).append(" does not have field ")
										.append(((FieldDesignator) designator).getId().getVal()).toString(), designator);
								return null;
							}
												
							/* update current object */
							//if (TypeFactory.isAggregateType(field_info.getType())){
								sub_object_stack.push((cur_type instanceof StructType) ? (field_info.getOrder()+1) : 0x7FFFFFFF);
								cur_offset_stack.push(base_offset);
								type_stack.push(cur_type);
								
								cur_type = field_info.getType();
								sub_object = 0;
								base_offset += field_info.getOffset();
							
							
						}
						else {
							report_error("field designator appears in non-struct-or-union initializer", designator);
							return null;
						}
					}
					
					/* index designator */
					else if (designator instanceof IndexDesignator){
					
						if (cur_type instanceof environment.ArrayType){
							ExpressionBase designator_expr = ((IndexDesignator) designator).getExpr();
							designator_expr.checkEvaluate_NoGenerating(_cur_env);
							
							/* evaluation error */
							if (designator_expr.getType() == null){
								return null;
							}
							
							/* constant integer type requirement */
							if (!TypeFactory.isIntegerType(designator_expr.getType())){
								report_error("index designator shall evaluates to integer type", designator);
								return null;
							}
							if (designator_expr.getValue() == null || !(designator_expr.getValue() instanceof Number)){
								report_error("index designator shall be an integer constant expression", designator);
								return null;
							}
							
							
							int target = ((Number)designator_expr.getValue()).intValue();
							
							/* index out of bound */
							if (target < 0 || (cur_type.isComplete() && (target >= ((environment.ArrayType) cur_type).getElementCount()))){
								report_error("index designator exceeds array boundary", designator);
								return null;
							}
							
							
							//if (TypeFactory.isAggregateType(((environment.ArrayType) cur_type).getBaseType())){
								type_stack.push(cur_type);
								sub_object_stack.push(target+1);
								cur_offset_stack.push(base_offset);
								
								base_offset += ((environment.ArrayType) cur_type).getBaseType().getSize() * target;
								cur_type = ((environment.ArrayType) cur_type).getBaseType();
								sub_object = 0;
							
						}
						
						else {
							report_error("index designator appears not in array initializer", designator);
							return null;
						}
					}
				}		// end of iteration of designator list
				
				cur_type = type_stack.peek();
				sub_object = sub_object_stack.pop();
				if (sub_object == 0x7FFFFFFF)
					sub_object = 0;
				else
					sub_object -= 1;
				sub_object_stack.push(sub_object);
				base_offset = cur_offset_stack.peek();
			}
			
			/* end of current object, pop stack*/
			if (cur_type instanceof environment.ArrayType){
				if (cur_type.isComplete() && sub_object >= ((environment.ArrayType) cur_type).getElementCount()){
					type_stack.pop();
					sub_object_stack.pop();
					cur_offset_stack.pop();
					continue;
				}
			}
			else if (cur_type instanceof environment.StructOrUnionType){
				if (sub_object >= ((environment.StructOrUnionType) cur_type).getFieldCount()){
					type_stack.pop();
					sub_object_stack.pop();
					cur_offset_stack.pop();
					continue;
				}
			}
			
			/*check initializer*/
			
			/* elem_type is the element type of the current sub-object */
			if (cur_type instanceof environment.ArrayType){
				elem_type = ((environment.ArrayType) cur_type).getBaseType();
			}
			else if (cur_type instanceof environment.StructOrUnionType){
				elem_type = ((environment.StructOrUnionType) cur_type).getIthField(sub_object).getType();
			}
			
			Initializer cur_init = init_item.getInitializer();
			Object val = null;
			
			
			if (cur_init instanceof InitializerExpr){
				for (boolean finished = false; !finished; ){
					
					if (elem_type instanceof environment.IntType){
						if ((val = checkInitializerForExternalDeclarationWithIntType(cur_init)) != null){
							
							if (cur_type instanceof StructOrUnionType){
								StructFieldInfo field_info = ((StructOrUnionType) cur_type).getIthField(sub_object);
								if (field_info.getBitLength() != -1){
									if (!(val instanceof Number)){
										report_error("initialization value cannot be computed at compile time.", cur_init);
										return null;
									}
									output.setBitField(base_offset + field_info.getOffset(), ((Number)val).intValue(), field_info.getMask());
								}
								else {
									output.set(base_offset + field_info.getOffset(), elem_type.getSize(), val);
								}
								
							}
							
							/* cur_type is ArrayType*/
							else {
								output.set(base_offset + elem_type.getSize() * sub_object, elem_type.getSize(), val);
							}
							
							finished = true;
						}
						
						/* non-constant */
						else {
							return output;
						}
					}
					
					else if (elem_type instanceof environment.CharType){
						if ((val = checkInitializerForExternalDeclarationWithCharType(cur_init)) != null){
							if (cur_type instanceof StructOrUnionType){
								StructFieldInfo field_info = ((StructOrUnionType) cur_type).getIthField(sub_object);
								output.set(base_offset + field_info.getOffset(), elem_type.getSize(), val);
							}
							
							/* cur_type is ArrayType*/
							else {
								output.set(base_offset + elem_type.getSize() * sub_object, elem_type.getSize(), val);
							}
							
							finished = true;
						}
						
						/* non-constant */
						else {
							return output;
						}
					}
					
					else if (elem_type instanceof environment.PointerType){
						if ((val = checkInitializerForExternalDeclarationWithPointerType(elem_type, cur_init)) != null){
							if (cur_type instanceof StructOrUnionType){
								StructFieldInfo field_info = ((StructOrUnionType) cur_type).getIthField(sub_object);
								output.set(base_offset + field_info.getOffset(), elem_type.getSize(), val);
							}
							
							/* cur_type is ArrayType*/
							else {
								output.set(base_offset + elem_type.getSize() * sub_object, elem_type.getSize(), val);
							}
							
							finished = true;
						}
						
						/* non-constant */
						else {
							return output;
						}
					}
					
					/* non-built-ins */
					if (!finished){
						//descend on aggregate types
						//may result in at most twice evaluations of the same expression
						//re-evaluation is no longer a problem
						ExpressionBase expr = ((InitializerExpr) cur_init).getExpr();
						expr.checkEvaluate_NoGenerating(_cur_env);
						if (expr.getType() == null){
							return output;
						}
						
						/* search for the next object that can be initialized */
						while (!finished && TypeFactory.isAggregateType(elem_type)){
							
							if (TypeFactory.isCharArray(elem_type) && (val = checkInitializerForExternalDeclarationWithCharArrayAndStringLiteral(elem_type, cur_init)) != null){
								if (cur_type instanceof environment.StructOrUnionType){
									output.set(base_offset + ((environment.StructOrUnionType) cur_type).getIthField(sub_object).getOffset(), 
											elem_type.getSize(), val);
								}
								else /* ArrayType */ {
									output.set(base_offset + elem_type.getSize() * sub_object, elem_type.getSize(), val);
								}
								
								finished = true;
								break;
							}
							else if (!(elem_type instanceof environment.ArrayType) && expr.getType().equals(elem_type)){
								// not likely
								report_warning("I don't think a constant struct or union type could be generated from our grammar.", init_item);
								
								finished = true;
								break;
							}
							
							/* zero length array is skipped */
							if (! (elem_type instanceof environment.ArrayType && elem_type.isComplete() && ((environment.ArrayType)elem_type).getElementCount() == 0)){					
								
								if (cur_type instanceof environment.StructOrUnionType){
									base_offset += ((environment.StructOrUnionType) cur_type).getIthField(sub_object).getOffset();
								}
								else {
									base_offset += elem_type.getSize() * sub_object;
								}
								
								sub_object = (cur_type instanceof UnionType) ? (0x7FFFFFFF) : (sub_object + 1);
								sub_object_stack.pop();
								sub_object_stack.push(sub_object);
								
								type_stack.push(elem_type);
								sub_object_stack.push(0);
								cur_offset_stack.push(base_offset);
																
								cur_type = elem_type;
								sub_object = 0;								
							} else {
								sub_object = (cur_type instanceof UnionType) ? (0x7FFFFFFF) : (sub_object + 1);
								sub_object_stack.pop();
								sub_object_stack.push(sub_object);
							}							
							
							do {
								if (cur_type instanceof environment.ArrayType){
									if (cur_type.isComplete() && sub_object >= ((environment.ArrayType) cur_type).getElementCount()){
										type_stack.pop();
										sub_object_stack.pop();
										cur_offset_stack.pop();
									}
									break;
								}
								else if (cur_type instanceof environment.StructOrUnionType){
									if (sub_object >= ((environment.StructOrUnionType) cur_type).getFieldCount()){
										type_stack.pop();
										sub_object_stack.pop();
										cur_offset_stack.pop();
									}
									else {
										break;
									}
								}
								
								if (! type_stack.isEmpty()){
									cur_type = type_stack.peek();
									sub_object = sub_object_stack.peek();
									base_offset = cur_offset_stack.peek();
								}
								else {
									report_warning("excess elements in initializer list", init);
									return output;
								}
							} while(true);
							
							if (cur_type instanceof StructOrUnionType){
								elem_type = ((StructOrUnionType) cur_type).getIthField(sub_object).getType();
							}
							else if (cur_type instanceof environment.ArrayType){
								elem_type = ((environment.ArrayType) cur_type).getBaseType();
							}
							else{
								report_error("impossible branch in checkInitializerForExternalDeclarationWithAggregateType elem_type update", init);
								return null;
							}
						}
					}
				}
				
				sub_object_stack.pop();
				sub_object_stack.push((cur_type instanceof UnionType) ? (0x7FFFFFFF) : (sub_object + 1));
			}
			
			else if (cur_init instanceof InitializerList){
				boolean finished = false;
				
				if (elem_type instanceof environment.IntType){
					if ((val = checkInitializerForExternalDeclarationWithIntType(cur_init)) != null){
						
						if (cur_type instanceof StructOrUnionType){
							StructFieldInfo field_info = ((StructOrUnionType) cur_type).getIthField(sub_object);
							if (field_info.getBitLength() != -1){
								if (!(val instanceof Number)){
									report_error("initialization value cannot be computed at compile time.", cur_init);
									return null;
								}
								output.setBitField(base_offset + field_info.getOffset(), ((Number)val).intValue(), field_info.getMask());
							}
							else {
								output.set(base_offset + field_info.getOffset(), elem_type.getSize(), val);
							}
							
						}
						
						/* cur_type is ArrayType*/
						else {
							output.set(base_offset + elem_type.getSize() * sub_object, elem_type.getSize(), val);
						}
						
						finished = true;
					}
					
					/* non-constant */
					else {
						return output;
					}
				}
				
				else if (elem_type instanceof environment.CharType){
					if ((val = checkInitializerForExternalDeclarationWithCharType(cur_init)) != null){
						if (cur_type instanceof StructOrUnionType){
							output.set(base_offset + ((StructOrUnionType) cur_type).getIthField(sub_object).getOffset(), elem_type.getSize(), val);
						}
						
						/* cur_type is ArrayType*/
						else {
							output.set(base_offset + elem_type.getSize() * sub_object, elem_type.getSize(), val);
						}
						
						finished = true;
					}
					
					/* non-constant */
					else {
						return output;
					}
				}
				
				else if (elem_type instanceof environment.PointerType){
					if ((val = checkInitializerForExternalDeclarationWithPointerType(elem_type, cur_init)) != null){
						
						if (cur_type instanceof StructOrUnionType){
							output.set(base_offset + ((StructOrUnionType) cur_type).getIthField(sub_object).getOffset(), elem_type.getSize(), val);
						}
						
						/* cur_type is ArrayType*/
						else {
							output.set(base_offset + elem_type.getSize() * sub_object, elem_type.getSize(), val);
						}
						
						finished = true;
					}
					else {
						return output;
					}
				}
				
				if (!finished){
					if (TypeFactory.isCharArray(elem_type) && 
							(val = checkInitializerForExternalDeclarationWithCharArrayAndStringLiteral(elem_type, cur_init)) != null){
						if (cur_type instanceof StructOrUnionType){
							output.set(base_offset + ((StructOrUnionType) cur_type).getIthField(sub_object).getOffset(), elem_type.getSize(), val);
						}
						
						/* cur_type is ArrayType*/
						else {
							output.set(base_offset + elem_type.getSize() * sub_object, elem_type.getSize(), val);
						}
					}
					else {
						
						checkInitializerForExternalDeclarationWithAggregateType(elem_type, (InitializerList)cur_init, output, 
							(cur_type instanceof StructOrUnionType) ? (base_offset + ((StructOrUnionType) cur_type).getIthField(sub_object).getOffset())
							:(base_offset + elem_type.getSize() * sub_object));					
						
					}
				}
				
				sub_object_stack.pop();
				sub_object_stack.push((cur_type instanceof UnionType) ? (0x7FFFFFFF) : (sub_object + 1));
			}
			
			//end check initializer
		}
		
		//only the top one array could be incomplete
		if (!type_stack.isEmpty()){
			int sz = 0;
			while (!type_stack.isEmpty()) {
				elem_type = type_stack.pop();
				sz = sub_object_stack.pop();
			}
			
			if (elem_type instanceof environment.ArrayType && !elem_type.isComplete()){
				if (sz == 0){
					report_warning("zero length array", init);
				}
				
				((environment.ArrayType)elem_type).setElementCount(sz);
				((environment.ArrayType)elem_type).setComplete(true);
			}
		}
		if (init_item_iter.hasNext()){
			report_warning("excess elements in initializer", init);
		}
				
		return output;
	}
	
	private Object AddressConstantToIRConstant(AddressConstant addr_const){
		Object ret = null;
		SymbolInfo sym = addr_const.getBase();
		
		if (sym instanceof VariableSymbol){
			ret = new Address(((VariableSymbol) sym).getMemLoc(), addr_const.getOffset());
		}
		else if (sym instanceof FunctionDesignator){
			ret = _ir.getFunctionLabel(sym.getName());
		}
		
		return ret;
	}
	
	
	private void clean(){
		_top_env = null;
		_cur_env = null;
		_ir = null;
		_runtime = null;
	}
	

	private void init_env(){
		_ir = new IntermediateRepresentation();
		_cur_env = _top_env = new Environment();
		_runtime = null;
	}

	private static void report_error(String msg, NodeBase root){
		try{
			ErrorHandling.ReportError(String.format("%d:%d:error: %s", root._line, root._column, msg), 
					ErrorHandling.ERROR);
		}
		catch (Exception e){
			e.printStackTrace();
			System.exit(1);
		}
	}
	

	private static void report_warning(String msg, NodeBase root){
		try{
			ErrorHandling.ReportError(String.format("%d:%d:warning: %s", root._line, root._column, msg), 
					ErrorHandling.WARNING);
		}
		catch (Exception e){
			e.printStackTrace();
			System.exit(1);
		}
	}
	

	private Environment _top_env;
	private Environment _cur_env;
	private IntermediateRepresentation _ir;
	private RuntimeEnvironment _runtime;
	
	private static SemanticCheck _running_instance;
	
	public static void setRunningInstance(SemanticCheck instance){
		_running_instance = instance;
	}
	

	public static SemanticCheck getRunningInstance(){
		return _running_instance;
	}
	
	public IntermediateRepresentation getIR(){
		return _ir;
	}
}
