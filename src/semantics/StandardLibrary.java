package semantics;

import irpack.FunctionEntry;
import irpack.IntermediateRepresentation;

import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.BitSet;

import optimization.SSABasedLinearScan;
import node.NodeBase;
import node.TranslationUnit;
import parser.Lexer;
import parser.Parser;
import java_cup.runtime.*;

/**
 * The StandardLibrary class.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 13, 2014
 */
public class StandardLibrary implements mips.Registers{

	public static TranslationUnit get_stdlib(){
		StringReader reader = new StringReader(stdlib_prototype);
		Lexer lexer = new Lexer(reader);
		Parser parser = new Parser(lexer);
		
		NodeBase.setLexer(lexer);
		
		
		try {
			Symbol result = parser.parse();
			return (TranslationUnit)result.value;
		} catch (Exception e) {
			
			e.printStackTrace();
			System.exit(1);
		}
		
		return null;
	}
	
	public static void createFunctionEntry(IntermediateRepresentation ir){
		
		FunctionEntry entry = new FunctionEntry("malloc");
		BitSet regs = new BitSet(reg.length);
		regs.set($v0);
		regs.set($a0);
		entry.setUsedRegisterSet(regs);
		ir.addFunctionEntry(entry);
		
		entry = new FunctionEntry("memcpy");
		regs = new BitSet(reg.length);
		regs.set($a0, $a3);	// $a0, $a1, $a2, $t8
		entry.setUsedRegisterSet(regs);
		ir.addFunctionEntry(entry);
		
		entry = new FunctionEntry("printf");
		regs = new BitSet(reg.length);
		regs.set($a0);
		regs.set($v0);
		entry.setUsedRegisterSet(regs);
		ir.addFunctionEntry(entry);
		
	}
	
	public static String stdlib_prototype= 
"void *malloc(int size);\n"+
"//void free(void *ptr);\n"+
"//int getchar();\n"+
"//int putchar(int c);\n"+
"//char *strcpy(char *str1, char *str2);\n"+ 
"//int strcmp(char *str1, char *str2);\n"+
"//char *strcat(char *str1, char *str2);\n"+
"int printf(char *format, ...);\n"+
"//int scanf(char *format, ...);\n"+
"void *memcpy(void *s1, void *s2, int n);\n";

	public static String get_stdlib_assembly(){
		StringBuilder builder = new StringBuilder();
		char[] buffer = new char[1024];
		
		try{
			for (String filename : stdlib_file_name){
				FileReader reader = new FileReader(StandardLibrary.class.getResource(filename).getFile());
				int cnt = -1;
				while ((cnt = reader.read(buffer, 0, 1024)) != -1){
					builder.append(buffer, 0, cnt);
				}
				builder.append("\n");
				reader.close();
			}
		}
		catch (IOException e){
			e.printStackTrace();
			//System.exit(1);
		}
		
		return builder.toString();
	}
	
	public static String stdlib_file_name[] = 
		{
			"/stdlib/printf.s",
			"/stdlib/memcpy.s",
			"/stdlib/malloc.s"
		};
}
