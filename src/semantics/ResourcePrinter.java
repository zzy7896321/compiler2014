package semantics;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.LinkedList;

import util.DoubleEndedStringBuilder;
import node.*;
import environment.*;

/**
 * The ResourcePrinter class provides printing methods to print semantic checking result.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 16, 2014
 */
public class ResourcePrinter {

	static private PrintStream out = System.err;
	
	
	static public void printExpression(ExpressionBase node){
		out.print("expression: ");
		out.println(node.getName());
		
		out.print("\t");
		out.println(getTypeString(node.getType()));
		
		out.print("\t");
		Object val = node.getValue();
		if (val instanceof Number){
			out.println(val.toString());
		}
		else if (val instanceof AddressConstant){
			SymbolInfo base = ((AddressConstant) val).getBase();
			if (base != null){
				out.print(base.getName());
				out.print("+");
				out.println(((AddressConstant) val).getOffset());
			}
			else {
				out.println(((AddressConstant) val).getOffset());
			}
		}
		else {
			out.println("<non-constant>");
		}
		
		out.println(node.isLvalue() ? "\tlvalue = true" : "\tlvalue = false");
	}
	
	static public void printTypeString(Type type){
		out.println(getTypeString(type));
	}
	
	static public void printStructOrUnion(StructOrUnionType type){
		out.println(getStructOrUnionString(type));
	}
	
	static public String getStructOrUnionString(StructOrUnionType type){
		if (type == null) return "";
		StringBuilder builder = new StringBuilder();
		
		if (type instanceof StructType){
			builder.append("struct ");
		}
		else if (type instanceof UnionType){
			builder.append("union ");
		}
		
		if (type.getTag() != null) builder.append(type.getTag());
		
		if (type.isComplete()){
			builder.append("{\n");
			for (int i = 0; i < type.getFieldCount(); ++i){
				StructFieldInfo field = type.getIthField(i);
				builder.append("\t").append(getTypeString(field.getType())).append(" ");
				if (field.getName() != null){
					builder.append(field.getName()).append(" ");
				}
				if (field.getBitLength() != -1){
					builder.append(": ").append(Integer.toString(field.getBitLength())).append(" ");
				}
				
				builder.append(";\n");
			}
		}
		builder.append("}");
		
		return builder.toString();
	}
	
	static public String getTypeString(Type type){
		if (type == null) return "";
		DoubleEndedStringBuilder builder = new DoubleEndedStringBuilder();
		
		Type prev = null;
		for (; !(type instanceof BuiltinType || type instanceof StructOrUnionType);){
			if (type instanceof environment.PointerType){
				builder.append_head("*");
				prev = type;
				type = ((environment.PointerType) type).getBaseType();
			}
			else if (type instanceof environment.ArrayType){
				if (prev instanceof environment.PointerType){
					builder.append_head("(");
					builder.append_rear(")");
				}
				
				environment.ArrayType acur = (environment.ArrayType)type;
				if (!acur.isComplete()){
					builder.append_rear("[]");
				}
				else{
						builder.append_rear("[");
						builder.append_rear(Integer.toString(acur.getElementCount()));
						builder.append_rear("]");
				}
				prev = type;
				type = acur.getBaseType();
			}
			else{	//FunctionType
				environment.FunctionType fcur = (environment.FunctionType) type;
				builder.append_head("(").append_rear(")(");
				LinkedList<Type> para_list = fcur.getParameterTypeList();
				if (para_list.isEmpty()){
					if (fcur.isVarLength()){
						builder.append_rear("...)");
					}
					else {
						builder.append_rear(")");
					}
				}
				else {
					Iterator<Type> para_iter = para_list.iterator();
					builder.append_rear(getTypeString(para_iter.next()));
					for (; para_iter.hasNext(); builder.append_rear(", ").append_rear(getTypeString(para_iter.next())));
					if (fcur.isVarLength()){
						builder.append_rear(", ...)");
					}
					else {
						builder.append_rear(")");
					}
				}
				prev = type;
				type = fcur.getReturnType();
			}
		}
		
		builder.append_head(" ");
		if (type instanceof IntType){
			builder.append_head("int");
		}
		else if (type instanceof CharType){
			builder.append_head("char");
		}
		else if (type instanceof VoidType){
			builder.append_head("void");
		}
		else if (type instanceof StructType){
			if (((StructType) type).getTag() != null){
				builder.append_head(((StructType) type).getTag()).append_head("struct ");
			}
			else {
				builder.append_head("struct");
			}
		}
		else if (type instanceof UnionType){
			if (((UnionType) type).getTag() != null){
				builder.append_head(((UnionType) type).getTag()).append_head("union ");
			}
			else {
				builder.append_head("union");
			}
		}
		
		return builder.toString();
	}
	
}
