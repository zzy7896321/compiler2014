package config;

/**
 * The TargetConfig class contains information about the target platform.
 *
 * @author Zhao Zhuoyue
 * @version last update: Apr 5, 2014
 */
public class TargetConfig {
	
	public static final int char_length = 8;
	public static final int int_length = 32;
	public static final int pointer_length = 32;
	
	public static final int char_size = char_length >> 3;
	public static final int int_size = int_length >> 3;
	public static final int pointer_size = pointer_length >> 3;
	
	public static final int INT_MAX = (1 << (int_length-2)) | ((1 << (int_length-2)) - 1);
	public static final int INT_MIN = -INT_MAX - 1;
	
	public static final int CHAR_MAX = (1 << (char_length-2)) | ((1 << (char_length-2)) - 1);
	public static final int CHAR_MIN = -CHAR_MAX - 1;
	
	public static final int align = 4;
	
	public static final int argument_register_count = 4;
	public static final int register_shift_size = 2;
}
