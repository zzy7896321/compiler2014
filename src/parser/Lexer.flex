package parser;

import java_cup.runtime.*;
import node.*;
import main.ErrorHandling;

/**
 * Defines the class Lexer, which is used for lexical analysis and
 * break input into token stream.
 * Currently, simple symbol of cup runtime is used as return type of next_token.
 *
 * @author Zhao Zhuoyue
 * @version last updated: 3/23/2014
 *	
 */

%%

%class Lexer
%public

%unicode
%line
%column

%cupsym CUP_sym
%cup
%implements CUP_sym
%cupdebug

%{
/*----------------------- Customized code -------------------*/

	/**
	 * Creates a new Symbol object with no value.
	 *
	 * @param	type	the type of the recognized token
	 * @return			the newly created Symbol object	
	 */
	protected java_cup.runtime.Symbol newSymbol(int type){
		return new java_cup.runtime.Symbol(type, yyline + 1, yycolumn + 1);
	}
	
	
	/**
	 * Creates a new Symbol object with value.
	 *
	 * @param	type	the type of the recognized token
	 * @param	value	the value of the recognzied token
	 * @return			the newly created Symbol object	
	 */
	protected java_cup.runtime.Symbol newSymbol(int type, Object value){
		return new java_cup.runtime.Symbol(type, yyline + 1, yycolumn + 1, value);
	}

	/**
	 * Reports an error.
	 * 
	 * @param msg		the message to display
	 *
	 */
	protected void ReportError(String msg){
		ErrorHandling.ReportError(msg, ErrorHandling.ERROR);	
	}

	/**
	 * Reports a warning.
	 *
	 * @param msg		the message to display
	 *
	 */
	protected void ReportWarning(String msg){
		ErrorHandling.ReportError(msg, ErrorHandling.WARNING);
	}


	StringBuilder _string_buffer;


	public int get_yyline(){
		return yyline + 1;
	}

	public int get_yycolumn(){
		return yycolumn + 1;
	}

/*----------------------- End customized code ---------------*/
%}

%init{
	
	_string_buffer = new StringBuilder();

%init}

/*----------------------- Macro Definition ------------------*/

CLetter = [_$a-zA-Z]
CLetterDigit = {CLetter} | [0-9]
Identifier = {CLetter} {CLetterDigit}*

WhiteSpace = [ \f\n\r\t\v]
SingleLineComment = "//" .* \R
MultipleLineComment = "/*" ~"*/"
Comment = {SingleLineComment} | {MultipleLineComment}

IntegerSuffix = [uU]([lL] | ll | LL)? ([lL] | ll | LL)[uU]?
DecimalConstant = [1-9][0-9]*
OctalConstant = 0[0-7]*
HexadecimalConstant = 0[xX][0-9a-fA-F]+
IntegerConstant = ({DecimalConstant} | {OctalConstant} | {HexadecimalConstant}) {IntegerSuffix}?

/*----------------------- End Macro Definition --------------*/

%xstate YYSTRING
%xstate YYCHAR
%xstate YYIGNORENEXTLINE

%%

	/* preprocessing is not implemented	and ignored */
	^[ \f\t\v]*(#|%:).*\\\R			{ yybegin(YYIGNORENEXTLINE); }
	
	^[ \f\t\v]*(#|%:).*[^\\]\R		{ /* ignored */	}
	
	\\\R					{ /* ignored. But this is an exception that ignoring it is actually required. */ }

<YYINITIAL>{

	/*	keywords as defined in ISO/IEC 9899:TC3 */
	"auto"			{ /*return newSymbol(AUTO);*/ }
	"break"			{ return newSymbol(BREAK); }
	"case"			{ /*return newSymbol(CASE);*/ }
	"char"			{ return newSymbol(CHAR); }
	"const"			{ /*return newSymbol(CONST);*/ }
	"continue"		{ return newSymbol(CONTINUE); }
	"default"		{ /*return newSymbol(DEFAULT);*/ }
	"do"			{ /*return newSymbol(DO);*/ }
	"double"		{ /*return newSymbol(DOUBLE);*/ }
	"else"			{ return newSymbol(ELSE); }
	"enum"			{ /*return newSymbol(ENUM);*/ }
	"extern"		{ /*return newSymbol(EXTERN);*/ }
	"float"			{ /*return newSymbol(FLOAT);*/ }
	"for"			{ return newSymbol(FOR); }
	"goto"			{ /*return newSymbol(GOTO);*/ }
	"if"			{ return newSymbol(IF); }
	"inline"		{ /*return newSymbol(INLINE);*/ }
	"int"			{ return newSymbol(INT); }
	"long"			{ /*return newSymbol(LONG);*/ }
	"register"		{ /*return newSymbol(REGISTER);*/ }
	"restrict"		{ /*return newSymbol(RESTRICT);*/ }
	"return"		{ return newSymbol(RETURN); }
	"short"			{ /*return newSymbol(SHORT);*/ }
	"signed"		{ /*return newSymbol(SIGNED);*/ }
	"sizeof"		{ return newSymbol(SIZEOF); }
	"static"		{ /*return newSymbol(STATIC);*/ }
	"struct"		{ return newSymbol(STRUCT); }
	"switch"		{ /*return newSymbol(SWITCH);*/ }
	"typedef"		{ return newSymbol(TYPEDEF); }
	"union"			{ return newSymbol(UNION); }
	"unsigned"		{ /*return newSymbol(UNSIGNED);*/ }
	"void"			{ return newSymbol(VOID); }
	"volatile"		{ /*return newSymbol(VOLATILE);*/ }
	"while"			{ return newSymbol(WHILE); }
	"_Bool"			{ /*return newSymbol(_BOOL);*/ }
	"_Complex"		{ /*return newSymbol(_COMPLEX);*/ }
	"_Imaginary"	{ /*return newSymbol(_IMAGINARY);*/ }

	
	{Identifier}		{ return newSymbol(IDENTIFIER, new Identifier(yytext())); }

	{Comment}			{ /* ignored */ }
	{WhiteSpace}		{ /* ignored */ }
	
	{IntegerConstant}	{ return newSymbol(INTEGERCONSTANT, IntegerConstant.newIntegerConstant(yytext())); }

	"'"					{ yybegin(YYCHAR); }	

	"\""				{ 
							yybegin(YYSTRING);
							_string_buffer.setLength(0);
						}
	
	"[" | "<:"			{ return newSymbol(LSQRPAREN); }
	"]" | ":>"			{ return newSymbol(RSQRPAREN); }
	"{" | "<%"			{ return newSymbol(LCURLYBRACKET); }
	"}" | "%>"			{ return newSymbol(RCURLYBRACKET); }
	"("					{ return newSymbol(LPAREN); }
	")"					{ return newSymbol(RPAREN); }
	"."					{ return newSymbol(DOT);	}
	"->"				{ return newSymbol(PTR);	}
	"++"				{ return newSymbol(INC);	}
	"--"				{ return newSymbol(DEC);	}
	"&"					{ return newSymbol(BITWISEAND);	}
	"*"					{ return newSymbol(STAR);	}
	"+"					{ return newSymbol(PLUS);	}
	"-"					{ return newSymbol(MINUS);	}
	"~"					{ return newSymbol(BITWISENOT);	}
	"!"					{ return newSymbol(LOGICALNOT);	}
	"/"					{ return newSymbol(DIVIDE);	}
	"%"					{ return newSymbol(MODULAR);	}
	"<<"				{ return newSymbol(LSHIFT);	}
	">>"				{ return newSymbol(RSHIFT); }
	"<"					{ return newSymbol(LESSTHAN); }
	">"					{ return newSymbol(GREATERTHAN);	}
	"<="				{ return newSymbol(LEQ);	}
	">="				{ return newSymbol(GEQ);	}
	"=="				{ return newSymbol(EQUAL);	}
	"!="				{ return newSymbol(NEQ);	}
	"^"					{ return newSymbol(XOR);	}
	"|"					{ return newSymbol(BITWISEOR); }
	"&&"				{ return newSymbol(LOGICALAND);	}
	"||"				{ return newSymbol(LOGICALOR);	}
	"?"					{ return newSymbol(QMARK);	}
	":"					{ return newSymbol(COLON);	}
	";"					{ return newSymbol(SEMICOLON);	}
	"..."				{ return newSymbol(ELLIPSIS);	}
	"="					{ return newSymbol(ASSIGN);	}
	"*="				{ return newSymbol(MULASSIGN);	}
	"/="				{ return newSymbol(DIVASSIGN);	}
	"%="				{ return newSymbol(MODASSIGN);	}
	"+="				{ return newSymbol(ADDASSIGN);	}
	"-="				{ return newSymbol(SUBASSIGN);	}
	"<<="				{ return newSymbol(LSHASSIGN);	}
	">>="				{ return newSymbol(RSHASSIGN);	}
	"&="				{ return newSymbol(ANDASSIGN);	}
	"^="				{ return newSymbol(XORASSIGN);	}
	"|="				{ return newSymbol(ORASSIGN);	}
	","					{ return newSymbol(COMMA);	}
	

}

<YYIGNORENEXTLINE>{

	^.*\\\R		{ /*ignored, stay in YYIGNORENEXTLINE */ }

	^.*[^\\]\R	{  yybegin(YYINITIAL); }
}

<YYCHAR>{
	[^'\\\R] '		{ yybegin(YYINITIAL); return newSymbol(CHARCONSTANT, new CharConstant(yycharat(0)));  }

	/*simple escape sequence*/
	\\['\"?\\] '		{ yybegin(YYINITIAL); return newSymbol(CHARCONSTANT, new CharConstant(yycharat(1)));	}
	\\a	'				{ yybegin(YYINITIAL); return newSymbol(CHARCONSTANT, new CharConstant((char)0X07)); }
	\\b	'				{ yybegin(YYINITIAL); return newSymbol(CHARCONSTANT, new CharConstant('\b')); }
	\\f	'				{ yybegin(YYINITIAL); return newSymbol(CHARCONSTANT, new CharConstant('\f')); }
	\\n	'				{ yybegin(YYINITIAL); return newSymbol(CHARCONSTANT, new CharConstant('\n')); }
	\\r	'				{ yybegin(YYINITIAL); return newSymbol(CHARCONSTANT, new CharConstant('\r')); }
	\\t	'				{ yybegin(YYINITIAL); return newSymbol(CHARCONSTANT, new CharConstant('\t')); }
	\\v	'				{ yybegin(YYINITIAL); return newSymbol(CHARCONSTANT, new CharConstant((char)0X0B)); }
	\\0	'				{ yybegin(YYINITIAL); return newSymbol(CHARCONSTANT, new CharConstant((char)0)); }

	/*octal escape sequence*/
	\\ [0-7]{1,3} '		{ yybegin(YYINITIAL); 
						  return newSymbol(CHARCONSTANT,
										   new CharConstant((char)(Integer.parseInt(yytext().substring(1, yylength() - 1), 8)))); }
	
	/*hexadecimal-escape-sequence*/
	\\x [0-9a-fA-F]+ '	{ yybegin(YYINITIAL);
						  return newSymbol(CHARCONSTANT,
										   new CharConstant((char)(Integer.parseInt(yytext().substring(2, yylength() - 1), 16)))); }

	\\\R				{ /* ignored */}

	/* Error handling */
	
	\R
						{
							ReportError(String.format("%d:%d: error: unexpected line terminator in char literal", yyline + 1, yycolumn + 1));
							yybegin(YYINITIAL);
						}


	\\. '				{ 
							ReportWarning(String.format("%d:%d: warning: unrecognized escape \\%s", yyline + 1, yycolumn + 1, yytext())); 
							yybegin(YYINITIAL);
							return newSymbol(CHARCONSTANT,
											 new CharConstant(yycharat(1)));
						}
	
	[^]					{
							yybegin(YYINITIAL);
							ReportError(String.format("%d:%d: error: illegal char literal %s", yyline + 1, yycolumn + 1, yytext()));
						}
}

<YYSTRING>{
	\"				{
							_string_buffer.append((char)0);
							yybegin(YYINITIAL);
							return newSymbol(STRINGCONSTANT, new StringConstant(_string_buffer.toString()));
						}
	
	[^\"\\\R]			{ _string_buffer.append(yycharat(0)); }
	\\['\"?\\]			{ _string_buffer.append(yycharat(1)); }
	\\a					{ _string_buffer.append((char)0x07); }
	\\b					{ _string_buffer.append('\b'); }
	\\f					{ _string_buffer.append('\f'); }
	\\n					{ _string_buffer.append('\n'); }
	\\r					{ _string_buffer.append('\r'); }
	\\t					{ _string_buffer.append('\t'); }
	\\v					{ _string_buffer.append((char)0x0b); }
	\\0					{ _string_buffer.append((char)0); }

	/*octal escape sequence*/
	\\ [0-7]{1,3} 		{ _string_buffer.append((char)(Integer.parseInt(yytext().substring(1, yylength()), 8))); }
	
	/*hexadecimal-escape-sequence*/
	\\x [0-9a-fA-F]+ 	{ _string_buffer.append((char)(Integer.parseInt(yytext().substring(2, yylength()), 16))); }

	\\\R				{ /* ignored */ }

	/* error handling */
	\R					{ 
							ReportError(String.format("%d:%d: error: multiline string is not allowed", yyline + 1, yycolumn + 1));
							yybegin(YYINITIAL);
						}
	
	\\.					{
							ReportWarning(String.format("%d:%d: warning: unrecognized escape \\%s", yyline, yycolumn, yytext())); 
							_string_buffer.append(yycharat(1));
						}

	[^]					{
							ReportError(String.format("%d:%d: error: invalid string literal, %s encounterd", yyline + 1, yycolumn + 1, yytext()));
							yybegin(YYINITIAL);
						}
}

/* Unexpected character */
	[^]					{
							if (yystate() != YYINITIAL) yybegin(YYINITIAL);
							ReportError(String.format("%d:%d: error: illegal character %s", yyline + 1, yycolumn + 1, yytext()));
						}			

