package mips;

/**
 * The MipsAcc class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class MipsAcc extends MipsCode {

	/**
	 * MIPS accumulator access instruction.
	 * 
	 * @param opcode
	 * @param r
	 */
	public MipsAcc(int opcode, int r) {
		super(opcode);
		
		this.r = r;
	}

	
	/**
	 *	Accesses the .
	 *
	 * @return the rr
	 */
	public int getR() {
		return r;
	}


	/**
	 *	Sets the .
	 * @param r the r to set
	 */
	public void setReg(int r) {
		this.r = r;
	}


	protected int r;
	
	@Override
	public String toString(){
		return new StringBuilder()
					.append(opname[opcode])
					.append(" ")
					.append(reg[r])
					.toString();
	}
}
