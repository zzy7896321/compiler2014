package mips;

/**
 * The MipsLA class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class MipsLA extends MipsCode {

	/**
	 * MIPS la/load/store with label.
	 * 
	 * @param opcode
	 */
	public MipsLA(int opcode, int rd, MipsLabel label, int offset) {
		super(opcode);

		this.rd = rd;
		this.label = label;
		this.offset = offset;
	}
	
	/**
	 *	Accesses the .
	 *
	 * @return the rd
	 */
	public int getRd() {
		return rd;
	}
	/**
	 *	Sets the .
	 * @param rd the rd to set
	 */
	public void setRd(int rd) {
		this.rd = rd;
	}

	/**
	 *	Accesses the .
	 *
	 * @return the label
	 */
	public MipsLabel getLabel() {
		return label;
	}

	/**
	 *	Sets the .
	 * @param label the label to set
	 */
	public void setLabel(MipsLabel label) {
		this.label = label;
	}

	/**
	 *	Accesses the .
	 *
	 * @return the offset
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 *	Sets the .
	 * @param offset the offset to set
	 */
	public void setOffset(int offset) {
		this.offset = offset;
	}

	protected int rd;
	protected MipsLabel label;
	protected int offset;

	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();
		
		builder
			.append(opname[opcode])
			.append(" ")
			.append(reg[rd])
			.append(", ")
			.append(label.getName());
	
		if (offset != 0){
			if (offset > 0){
				builder.append("+");
			}
			
			builder.append(offset);
		}
		
		return builder.toString();
	}
}
