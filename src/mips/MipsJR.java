package mips;

/**
 * The MipsJR class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class MipsJR extends MipsJ {

	/**
	 * MIPS Jump register instruction.
	 * 
	 * @param opcode
	 */
	public MipsJR(int opcode, int target) {
		super(opcode);
		
		this.target = target;
	}

	/**
	 *	Accesses the .
	 *
	 * @return the target
	 */
	public int getTarget() {
		return target;
	}

	/**
	 *	Sets the .
	 * @param target the target to set
	 */
	public void setTarget(int target) {
		this.target = target;
	}

	/** the jump target */
	protected int target;
	
	@Override
	public String toString(){
		return new StringBuilder()
					.append(opname[opcode])
					.append(" ")
					.append(reg[target])
					.toString();
	}

}
