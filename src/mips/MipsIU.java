package mips;

/**
 * The MipsIU class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class MipsIU extends MipsI {

	/**
	 * Constructs an I-type MIPS instruction with unsigned 16-bit immediate
	 * 
	 * @param opcode
	 * @param rd
	 * @param rs
	 * @param imm
	 */
	public MipsIU(int opcode, int rd, int rs, int imm) {
		super(opcode, rd, rs, imm);

	}
	
	@Override
	public String toString(){
		return new StringBuilder()
					.append(opname[opcode])
					.append(" ")
					.append(reg[rd])
					.append(", ")
					.append(reg[rs])
					.append(", 0x")
					.append(Integer.toHexString(imm))
					.toString();
	}

}
