package mips;

/**
 * The MipsIS class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class MipsIS extends MipsI {

	/**
	 * Constructs an I-type MIPS instruction with signe 16-bit immediate.
	 * 
	 * @param opcode
	 */
	public MipsIS(int opcode, int rd, int rs, int imm) {
		super(opcode, rd, rs, imm);

	}

	@Override
	public String toString(){
		return new StringBuilder()
					.append(opname[opcode])
					.append(" ")
					.append(reg[rd])
					.append(", ")
					.append(reg[rs])
					.append(", ")
					.append(Integer.toString(imm))
					.toString();
	}
}
