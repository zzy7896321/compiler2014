package mips;

/**
 * The MipsLabel class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class MipsLabel extends MipsCode{

	/**
	 * Constructs an assembly label.
	 * 
	 * @param name
	 */
	public MipsLabel(String name) {
		super(LABEL);
		
		this.setName(name);
	}

	/**
	 *	Accesses the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 *	Sets the name.
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/** the name of the label */
	private String name;
	
	@Override
	public String toString(){
		
		return name + ":";
	}
}
