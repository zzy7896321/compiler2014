package mips;

/**
 * The MipsBZ class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class MipsBZ extends MipsB {

	/**
	 * MIPS B__Z instruction.
	 * 
	 * @param opcode
	 */
	public MipsBZ(int opcode, int rs, MipsLabel target) {
		super(opcode);
		
		this.setRs(rs);
		this.setTarget(target);
	}
	
	/**
	 *	Accesses the .
	 *
	 * @return the rs
	 */
	public int getRs() {
		return rs;
	}
	/**
	 *	Sets the .
	 * @param rs the rs to set
	 */
	public void setRs(int rs) {
		this.rs = rs;
	}

	/**
	 *	Accesses the .
	 *
	 * @return the target
	 */
	public MipsLabel getTarget() {
		return target;
	}

	/**
	 *	Sets the .
	 * @param target the target to set
	 */
	public void setTarget(MipsLabel target) {
		this.target = target;
	}

	protected int rs;
	protected MipsLabel target;
	
	
	@Override
	public String toString(){
		return new StringBuilder()
					.append(opname[opcode])
					.append(" ")
					.append(reg[rs])
					.append(", ")
					.append(target.getName())
					.toString();
		
	}
}
