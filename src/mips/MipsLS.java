package mips;

/**
 * The MipsLS class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class MipsLS extends MipsCode {

	/**
	 * MIPS load/store instruction.
	 * 
	 * @param opcode
	 */
	public MipsLS(int opcode, int rd, int rs, int offset) {
		super(opcode);
		
		this.rd = rd;
		this.rs = rs;
		this.offset = offset;
	}

	
	/**
	 *	Accesses the .
	 *
	 * @return the rd
	 */
	public int getRd() {
		return rd;
	}


	/**
	 *	Sets the .
	 * @param rd the rd to set
	 */
	public void setRd(int rd) {
		this.rd = rd;
	}


	/**
	 *	Accesses the .
	 *
	 * @return the rs
	 */
	public int getRs() {
		return rs;
	}


	/**
	 *	Sets the .
	 * @param rs the rs to set
	 */
	public void setRs(int rs) {
		this.rs = rs;
	}


	/**
	 *	Accesses the .
	 *
	 * @return the offset
	 */
	public int getOffset() {
		return offset;
	}


	/**
	 *	Sets the .
	 * @param offset the offset to set
	 */
	public void setOffset(int offset) {
		this.offset = offset;
	}


	protected int rd, rs, offset;
	
	@Override
	public String toString(){
		return new StringBuilder()
					.append(opname[opcode])
					.append(" ")
					.append(reg[rd])
					.append(", ")
					.append(Integer.toString(offset))
					.append("(")
					.append(reg[rs])
					.append(")")
					.toString();
		
	}
}
