package mips;

/**
 * 
 * The MipsR class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class MipsR extends MipsCode{

	/**
	 * Constructs an R-type MIPS instruction.
	 * 
	 * @param opcode
	 * @param rd
	 * @param rs
	 * @param rt
	 */
	public MipsR(int opcode, int rd, int rs, int rt) {
		super(opcode);

//		assert(rt >= 0 && rt < 32);
		
		this.rd = rd;
		this.rs = rs;
		this.rt = rt;
	}
	
	/**
	 *	Accesses the rd.
	 *
	 * @return the rd
	 */
	public int getRd() {
		return rd;
	}

	/**
	 *	Sets the rd.
	 * @param rd the rd to set
	 */
	public void setRd(int rd) {
		this.rd = rd;
	}

	/**
	 *	Accesses the rs.
	 *
	 * @return the rs
	 */
	public int getRs() {
		return rs;
	}

	/**
	 *	Sets the rs.
	 * @param rs the rs to set
	 */
	public void setRs(int rs) {
		this.rs = rs;
	}

	/**
	 *	Accesses the rt.
	 *
	 * @return the rt
	 */
	public int getRt() {
		return rt;
	}

	/**
	 *	Sets the rt.
	 * @param rt the rt to set
	 */
	public void setRt(int rt) {
		this.rt = rt;
	}

	/** registers */
	protected int rd, rs, rt;
	
	@Override
	public String toString(){
		return new StringBuilder()
				.append(opname[opcode])
				.append(" ")
				.append(reg[rd])
				.append(", ")
				.append(reg[rs])
				.append(", ")
				.append(reg[rt])
				.toString();
	}
}
