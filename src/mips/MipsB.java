package mips;

/**
 * The MipsB class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public abstract class MipsB extends MipsCode {

	/**
	 * MIPS branch instruction.
	 * 
	 * @param opcode
	 */
	public MipsB(int opcode) {
		super(opcode);

		
	}

}
