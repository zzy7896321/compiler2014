package mips;

/**
 * The MipsLUI class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class MipsLUI extends MipsCode {

	/**
	 * LUI instruction.
	 * 
	 * @param opcode
	 */
	public MipsLUI(int opcode, int rd, int imm) {
		super(opcode);

		this.rd = rd;
		this.imm = imm;
	}
	
	/**
	 *	Accesses the .
	 *
	 * @return the rd
	 */
	public int getRd() {
		return rd;
	}

	/**
	 *	Sets the .
	 * @param rd the rd to set
	 */
	public void setRd(int rd) {
		this.rd = rd;
	}

	/**
	 *	Accesses the .
	 *
	 * @return the imm
	 */
	public int getImm() {
		return imm;
	}

	/**
	 *	Sets the .
	 * @param imm the imm to set
	 */
	public void setImm(int imm) {
		this.imm = imm;
	}

	protected int rd, imm;

	@Override
	public String toString(){
		return new StringBuilder()
					.append(opname[opcode])
					.append(" ")
					.append(reg[rd])
					.append(", 0x")
					.append(Integer.toHexString(imm))
					.toString();
	}
}
