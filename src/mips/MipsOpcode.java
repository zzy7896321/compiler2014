package mips;

/**
 * 
 * The MipsOpcode class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public interface MipsOpcode {
	
	/* opcodes */
	public static final int EXIT = -4;
	public static final int ENTRY = -3;
	public static final int DUMMY = -2;
	public static final int LABEL = -1;
	
	public static final int ADDIU = 0;
	public static final int ADDU = 1;
	public static final int LA = 2;
	public static final int LUI = 3;
	public static final int SUBU = 4;
	public static final int SLL = 5;
	public static final int SLLV = 6;
	public static final int SRA = 7;
	public static final int SRAV = 8;
	public static final int SRL = 9;
	public static final int SRLV = 10;
	public static final int AND = 11;
	public static final int ANDI = 12;
	public static final int NOR = 13;
	public static final int OR = 14;
	public static final int ORI = 15;
	public static final int XOR = 16;
	public static final int XORI = 17;
	public static final int SLT = 18;
	public static final int SLTI = 19;
	public static final int SLTIU = 20;
	public static final int SLTU = 21;
	public static final int DIV = 22;
	public static final int MUL = 23;
	public static final int MADDU = 24;
	public static final int MSUBU = 25;
	public static final int MFHI = 26;
	public static final int MFLO = 27;
	public static final int MTHI = 28;
	public static final int MTLO = 29;
	public static final int BEQ = 30;
	public static final int BNE = 31;
	public static final int BGEZ = 32;
	public static final int BGTZ = 33;
	public static final int BLEZ = 34;
	public static final int BLTZ = 35;
	public static final int J = 36;
	public static final int JAL = 37;
	public static final int JALR = 38;
	public static final int JR = 39;	
	public static final int LB = 40;
	public static final int LW = 41;
	public static final int SB = 42;
	public static final int SW = 43;
	public static final int MULT = 44;

	/** assembly instruction name */
	public static final String[] opname = {
		"addiu",
		"addu",
		"la",
		"lui",
		"subu",
		"sll",
		"sllv",
		"sra",
		"srav",
		"srl",
		"srlv",
		"and",
		"andi",
		"nor",
		"or",
		"ori",
		"xor",
		"xori",
		"slt",
		"slti",
		"sltiu",
		"sltu",
		"div",
		"mul",
		"maddu",
		"msubu",
		"mfhi",
		"mflo",
		"mthi",
		"mtho",
		"beq",
		"bne",
		"bgez",
		"bgtz",
		"blez",
		"bltz",
		"j",
		"jal",
		"jalr",
		"jr",
		"lb",
		"lw",
		"sb",
		"sw",
		"mult",
	};
}
