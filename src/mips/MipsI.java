package mips;

/**
 * The MipsI class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public abstract class MipsI extends MipsCode {

	/**
	 * Constructs an I-type MIPS instruction.
	 * 
	 * @param opcode
	 */
	public MipsI(int opcode, int rd, int rs, int imm) {
		super(opcode);
		
		this.setRd(rd);
		this.setRs(rs);
		this.setImm(imm);
	}

	/**
	 *	Accesses the .
	 *
	 * @return the rd
	 */
	public int getRd() {
		return rd;
	}

	/**
	 *	Sets the .
	 * @param rd the rd to set
	 */
	public void setRd(int rd) {
		this.rd = rd;
	}

	/**
	 *	Accesses the .
	 *
	 * @return the rs
	 */
	public int getRs() {
		return rs;
	}

	/**
	 *	Sets the .
	 * @param rs the rs to set
	 */
	public void setRs(int rs) {
		this.rs = rs;
	}

	/**
	 *	Accesses the .
	 *
	 * @return the imm
	 */
	public int getImm() {
		return imm;
	}

	/**
	 *	Sets the .
	 * @param imm the imm to set
	 */
	public void setImm(int imm) {
		this.imm = imm;
	}

	/** operands */
	protected int rd, rs, imm;
}
