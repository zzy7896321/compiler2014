package mips;

/**
 * The MipsJ class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public abstract class MipsJ extends MipsCode {

	/**
	 * MIPS Jump instructions.
	 * 
	 * @param opcode
	 */
	public MipsJ(int opcode) {
		super(opcode);

	}

	
}
