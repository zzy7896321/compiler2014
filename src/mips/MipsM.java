package mips;

/**
 * The MipsM class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class MipsM extends MipsCode {

	/**
	 * MIPS multiplication or division instruction in the form of acc = Rs * / Rt.
	 * 
	 * @param opcode
	 */
	public MipsM(int opcode, int rs, int rt) {
		super(opcode);

		this.rs = rs;
		this.rt = rt;
	}
		
	/**
	 *	Accesses the .
	 *
	 * @return the rs
	 */
	public int getRs() {
		return rs;
	}



	/**
	 *	Sets the .
	 * @param rs the rs to set
	 */
	public void setRs(int rs) {
		this.rs = rs;
	}



	/**
	 *	Accesses the .
	 *
	 * @return the rt
	 */
	public int getRt() {
		return rt;
	}



	/**
	 *	Sets the .
	 * @param rt the rt to set
	 */
	public void setRt(int rt) {
		this.rt = rt;
	}

	protected int rs, rt;

	@Override
	public String toString(){
		return new StringBuilder()
					.append(opname[opcode])
					.append(" ")
					.append(reg[rs])
					.append(", ")
					.append(reg[rt])
					.toString();
	}
}
