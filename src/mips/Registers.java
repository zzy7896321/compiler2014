package mips;

/**
 * The Registers interface provides register constants.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public interface Registers {
	
	public final static int $0 = 0;
	public final static int $at = 1;
	public final static int $v0 = 2;
	public final static int $v1 = 3;
	public final static int $a0 = 4;
	public final static int $a1 = 5;
	public final static int $a2 = 6;
	public final static int $a3 = 7;
	public final static int $t0 = 8;
	public final static int $t1 = 9;
	public final static int $t2 = 10;
	public final static int $t3 = 11;
	public final static int $t4 = 12;
	public final static int $t5 = 13;
	public final static int $t6 = 14;
	public final static int $t7 = 15;
	public final static int $s0 = 16;
	public final static int $s1 = 17;
	public final static int $s2 = 18;
	public final static int $s3 = 19;
	public final static int $s4 = 20;
	public final static int $s5 = 21;
	public final static int $s6 = 22;
	public final static int $s7 = 23;
	public final static int $t8 = 24;
	public final static int $t9 = 25;
	public final static int $k0 = 26;
	public final static int $k1 = 27;
	public final static int $gp = 28;
	public final static int $sp = 29;
	public final static int $fp = 30;
	public final static int $ra = 31;
	
	public final static String[] reg =
	{	 "$0", "$at", "$v0", "$v1", "$a0", "$a1", "$a2", "$a3",
		"$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7",
		"$s0", "$s1", "$s2", "$s3", "$s4", "$s5", "$s6", "$s7",
		"$t8", "$t9", "$k0", "$k1", "$gp", "$sp", "$fp", "$ra"
	};
	
	public final static boolean[] isCallerSaved = 
	{	false, true, true, true, true, true, true, true,
		true, true, true, true, true, true, true, true,
		false, false, false, false, false, false, false, false,
		true, true, true, true, false, false, false, false
	};
}	