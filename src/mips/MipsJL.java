package mips;

/**
 * The MipsJL class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class MipsJL extends MipsJ {

	/**
	 * MIPS Jump label pseudo instruction.
	 * 
	 * @param opcode
	 */
	public MipsJL(int opcode, MipsLabel target) {
		super(opcode);

		this.target = target;
	}

	/**
	 *	Accesses the .
	 *
	 * @return the target
	 */
	public MipsLabel getTarget() {
		return target;
	}

	/**
	 *	Sets the .
	 * @param target the target to set
	 */
	public void setTarget(MipsLabel target) {
		this.target = target;
	}

	/** the jump target */
	protected MipsLabel target;
	
	@Override
	public String toString(){
		return new StringBuilder()
					.append(opname[opcode])
					.append(" ")
					.append(target.getName())
					.toString();
	}
}
