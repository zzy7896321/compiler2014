package mips;

/**
 * The MipsB2 class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class MipsB2 extends MipsB{

	/**
	 * MIPS branch instruction with two operands.
	 */
	public MipsB2(int opcode, int rs, int rt, MipsLabel target) {
		super(opcode);
		
		this.setRs(rs);
		this.setRt(rt);
		this.setTarget(target);
	}
	
	/**
	 *	Accesses the .
	 *
	 * @return the rs
	 */
	public int getRs() {
		return rs;
	}
	/**
	 *	Sets the .
	 * @param rs the rs to set
	 */
	public void setRs(int rs) {
		this.rs = rs;
	}

	/**
	 *	Accesses the .
	 *
	 * @return the rt
	 */
	public int getRt() {
		return rt;
	}

	/**
	 *	Sets the .
	 * @param rt the rt to set
	 */
	public void setRt(int rt) {
		this.rt = rt;
	}

	/**
	 *	Accesses the .
	 *
	 * @return the target
	 */
	public MipsLabel getTarget() {
		return target;
	}

	/**
	 *	Sets the .
	 * @param target the target to set
	 */
	public void setTarget(MipsLabel target) {
		this.target = target;
	}

	protected int rs, rt;
	protected MipsLabel target;
	
	
	@Override
	public String toString(){
		return new StringBuilder()
					.append(opname[opcode])
					.append(" ")
					.append(reg[rs])
					.append(", ")
					.append(reg[rt])
					.append(", ")
					.append(target.getName())
					.toString();
		
	}

}
