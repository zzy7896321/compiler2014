package mips;


/**
 * The MipsCode class.
 *
 * @author Zhao Zhuoyue
 * @version last update: May 16, 2014
 */
public class MipsCode implements Registers, MipsOpcode{

	/**
	 * Constructs an assembly instruction object.
	 * 
	 * @param opacode
	 */
	public MipsCode(int opcode) {
		this.setOpcode(opcode);		
	}

	/**
	 *	Accesses the opcode.
	 *
	 * @return the opcode
	 */
	public int getOpcode() {
		return opcode;
	}
	
	/**
	 *	Sets the opcode.
	 * @param opcode the opcode to set
	 */
	public void setOpcode(int opcode) {
		this.opcode = opcode;
	}

	/** the opcode */
	protected int opcode;
	
	
	private static final MipsCode dummy = new MipsCode(DUMMY); 
	
	/**
	 * Returns a dummy code with no content.
	 * 
	 * @return
	 */
	public static final MipsCode getDummyCode(){
		return dummy;
	}
	
	@Override
	public String toString(){
		return "";
	}
	
}
