LBFmax:
	# frame size = 0
	# no function call, no need to save $ra
LBFBmax:
	slt $t8, $a1, $a0
	beq $t8, $0, LBmax0
	or $v0, $a0, $0
	j LBmax1
LBmax0:
	or $v0, $a1, $0
LBmax1:
LBFEmax:
	jr $ra
