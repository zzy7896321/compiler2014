#include <stdio.h>
int arr[] = {1, 2, 5, 3, 0};
int max(int x, int y){
	return (x > y) ? x : y;
}
int max_elem_of(int* a, int n){
	int ret = -1;
	for (--n; n >= 0; --n)
		ret = max(ret, a[n]);
	return ret;
}
int main(){
	printf("%d\n", max_elem_of(arr, sizeof(arr)/sizeof(int)));
}
