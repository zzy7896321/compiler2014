LBFmax_elem_of:	# entry block
	sw $ra, -4($sp)
	sw $fp, -8($sp)
	or $fp, $sp, $0
	addiu $sp, $sp, -104 # frame size = 104
	sw $s0, 64($sp)
	or $s0, $a0, $0 # remap register $a0 to $s0
LBFBmax_elem_of:
...
LBFEmax_elem_of: # exit block
	lw $s0, 64($sp)
	lw $ra, -4($fp)
	or $sp, $fp, $0
	lw $fp, -8($sp)
	jr $ra
