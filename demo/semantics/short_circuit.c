#include <stdio.h>
int X = 1, *Y = 0;
char* str = "Hello, world!\n";
int main(){
	printf("%d %d\n", Y == 0 && X == 1, X == 1 || *Y == 0); // prints 1 1
	char* c;
	for (c = str; c != 0 && *c != '\0'; ++c)
		printf("%c", *c); // prints Hello, world!
}
