#include <stdio.h>
struct{
	int a:5;
	int : 3;
	int b:4;
} a = {0x10, 0x7};
int main(){
	printf("%d %d\n", a.a, a.b); // prints -16 7
	a.a = 1;
	a.b = -1;
	printf("%d %d\n", a.a, a.b); // prints 1 -1
}
