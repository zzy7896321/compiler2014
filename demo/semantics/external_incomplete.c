#include <stdio.h>
struct A a;
struct A{
	int x, y;
};
int b[];
int b[2] = {3, 4};
int main(){
	a.x = a.y = 2;
	printf("%d %d\n", a.x, a.y); // prints 2 2
	printf("%d %d\n", b[0], b[1]); // prints 3 4
}
