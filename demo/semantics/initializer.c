#include <stdio.h>
struct {
	char c[10];
	int a, b[2][2];
} arr[3] = {
	[1] = {"Hello!", .b[1][0] = 1, 2, .a = 3, 4, 5}
};

int b[10][10] = {
	{1, 2, 3, 4, 5, 6},
	[7][6] = 7, [2][3] = 8, [0][0] = 9
};

int main(){
	printf("%s %d %d %d %d %d\n", arr[1].c, arr[1].a, arr[1].b[0][0], arr[1].b[0][1], arr[1].b[1][0], arr[1].b[1][1]);
	// prints Hello! 3 4 5 1 2
	printf("%d %d %d\n", b[0][0], b[2][3], b[7][6]);
	// prints 9 8 7
}
