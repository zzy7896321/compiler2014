#include <stdio.h>

typedef struct A A; // declares A as an alias of type name struct A
struct A{ // A in tag space, a struct name
	A* A; // member in A
};

int main(){
	A A; // declares a struct A instance A by typedef name A
	A.A = 0; // sets the struct A instance A's member A to 0
	printf("%d\n", A.A);
}
