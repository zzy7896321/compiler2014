#include <stdio.h>

int a[10][10] = {1, 2};

void print(int x, int y){
	printf("%d %d\n", x, y);
}

int main(){
	int (*arr)[10] = a, (*p_arr)[10][10] = &a;
	int *arr_p[10] = {a[0]}, *arr_p2[10][10] = {{a[0]}};
	void (*f)(int, int) = print;
	f(arr[0][0], (*p_arr)[0][1]); // prints 1 2
	f(*arr_p[0], arr_p2[0][0][1]); // prints 1 2
}
