#include <stdio.h>


char* help_string =
"Usage: [Options] <filename>\n\
Options:\n\
	-h, --help		prints the help prompt and exit\n\
	--debug			enables debug print\n";

int main(){
	printf("%s", help_string);

	return 0;
}
